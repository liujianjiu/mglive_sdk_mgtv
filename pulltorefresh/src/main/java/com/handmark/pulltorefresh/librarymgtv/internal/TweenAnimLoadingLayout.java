package com.handmark.pulltorefresh.librarymgtv.internal;


import com.handmark.pulltorefresh.library.R;
import com.handmark.pulltorefresh.librarymgtv.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.librarymgtv.PullToRefreshBase.Orientation;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

/**
 * @date 2015/1/8
 * @author wuwenjie
 * @desc 帧动画加载布局
 */
public class TweenAnimLoadingLayout extends LoadingLayout {
    private ImageView fllowerImg;
    private AnimationDrawable animationDrawable;
    private Animation translateAnimation;
    public TweenAnimLoadingLayout(Context context, Mode mode,
                                  Orientation scrollDirection, TypedArray attrs) {
        super(context, mode, scrollDirection, attrs);
        // 初始化
        //加载动画XML文件,生成动画指令
        mHeaderImage.setImageResource(R.anim.pulltorefresh_anim);
        animationDrawable = (AnimationDrawable) mHeaderImage.getDrawable();

        fllowerImg = new ImageView(getContext());
        ViewGroup.LayoutParams ps = new ViewGroup.LayoutParams(48,36);
        fllowerImg.setLayoutParams(ps);
        fllowerImg.setImageResource(R.drawable.loading_fllower);
        fllowerImg.setVisibility(GONE);
        addView(fllowerImg);

    }
    // 默认图片
    @Override
    protected int getDefaultDrawableResId() {
        return R.drawable.loading_default;
    }

    @Override
    protected void onLoadingDrawableSet(Drawable imageDrawable) {
        // NO-OP
    }

    @Override
    protected void onPullImpl(float scaleOfLayout) {
        // NO-OP

    }
    // 下拉以刷新
    @Override
    protected void pullToRefreshImpl() {
        //花落到手上
        mHeaderImage.setImageResource(getDefaultDrawableResId());
        translateAnimation = getCenter2CenterAnimation(fllowerImg,mHeaderImage);
        translateAnimation.setDuration(400);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(!isCancle)
                {
                    mHeaderImage.setImageResource(R.drawable.loading_1);
                }
                fllowerImg.setVisibility(GONE);
                isCancle = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fllowerImg.setVisibility(VISIBLE);
        fllowerImg.setAnimation(translateAnimation);
        // NO-OP
    }
    private boolean isCancle = false;
    // 正在刷新时回调
    @Override
    protected void refreshingImpl() {
        if(translateAnimation != null && !translateAnimation.hasEnded()){
            fllowerImg.clearAnimation();
            isCancle = true;
        }

        // 播放帧动画
        mHeaderImage.setImageResource(R.anim.pulltorefresh_anim);
        animationDrawable = (AnimationDrawable) mHeaderImage.getDrawable();
        animationDrawable.start();
    }
    // 释放以刷新
    @Override
    protected void releaseToRefreshImpl() {
        // NO-OP
    }
    // 重新设置
    @Override
    protected void resetImpl() {
        mHeaderImage.setVisibility(View.VISIBLE);
        mHeaderImage.clearAnimation();
    }

    private Animation getCenter2CenterAnimation(View animView,View endView)
    {
        int[] animPosition = new int[2];
        animView.getLocationOnScreen(animPosition);
        animPosition[0] = animPosition[0] + animView.getWidth()/2;
        animPosition[1] = animPosition[1] + animView.getHeight()/2;



        int[] endPosition = new int[2];
        endView.getLocationOnScreen(endPosition);
        endPosition[0] = endPosition[0] + endView.getWidth()/2  + endView.getHeight()/ 4;
        endPosition[1] = endPosition[1] + endView.getHeight()/2;

        int[] nowPosition = new int[2];
        getLocationOnScreen(nowPosition);

        int[] starPosition = new int[2];
        starPosition[0] = endPosition[0];
        starPosition[1] = nowPosition[1];

        int starX = starPosition[0] - animPosition[0];
        int endX = endPosition[0] - animPosition[0];
        int starY = -animView.getHeight();
        int endY = endPosition[1] - animPosition[1];

        Animation animation = new TranslateAnimation(
                Animation.ABSOLUTE,starX,
                Animation.ABSOLUTE,endX,
                Animation.ABSOLUTE,starY,
                Animation.ABSOLUTE,endY);
        return animation;
    }
}