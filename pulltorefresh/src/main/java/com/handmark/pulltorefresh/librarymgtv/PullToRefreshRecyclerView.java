package com.handmark.pulltorefresh.librarymgtv;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public class PullToRefreshRecyclerView extends PullToRefreshRecyclerViewBase {

    public static interface OnScrollEdgeListener {
        void onScrollToTop();

        void onScrollToBottom();
    }


    protected OnScrollEdgeListener mScrollEdgeListener;


    public PullToRefreshRecyclerView(Context context) {
        super(context);
    }

    public PullToRefreshRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PullToRefreshRecyclerView(Context context, Mode mode) {
        super(context, mode);
    }

    public PullToRefreshRecyclerView(Context context, Mode mode, AnimationStyle animStyle) {
        super(context, mode, animStyle);
    }

    public void setScrollEdgeListener(OnScrollEdgeListener listener) {
        mScrollEdgeListener = listener;
    }

    @Override
    protected RecyclerView createRefreshableView(Context context, AttributeSet attributeSet) {
        RecyclerView recyclerView = super.createRefreshableView(context, attributeSet);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (mScrollEdgeListener == null) {
                    return;
                }

                int itemCount = getItemCount();
                if (itemCount == 0) {
                    return;
                }

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int first = mLayoutManager.findFirstCompletelyVisibleItemPosition();
                    if (first == 0) {
                        mScrollEdgeListener.onScrollToTop();
                    } else {
                        int bottom = mLayoutManager.findLastCompletelyVisibleItemPosition();
                        if (bottom == itemCount - 1) {
                            mScrollEdgeListener.onScrollToBottom();
                        }
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        return recyclerView;
    }

    @Override
    public Orientation getPullToRefreshScrollDirection() {
        return Orientation.VERTICAL;
    }

    @Override
    protected LinearLayoutManager createLayoutManager(Context context) {
        return new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
    }

    @Override
    protected boolean isPullStart(View firstVisibleChild, View parentView) {
        return firstVisibleChild.getTop() >= parentView.getTop();
    }

    @Override
    protected boolean isPullEnd(View lastVisibleChild, View parentView) {
        return lastVisibleChild.getBottom() <= parentView.getBottom();
    }
}
