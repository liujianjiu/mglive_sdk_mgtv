package com.handmark.pulltorefresh.librarymgtv.refresh;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.handmark.pulltorefresh.librarymgtv.ILoadingLayout;

public abstract class RefreshLoadingLayout extends FrameLayout
        implements ILoadingLayout {

    public RefreshLoadingLayout(Context context) {
        super(context);
        initLayout(context);
    }

    public RefreshLoadingLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public RefreshLoadingLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context);
    }

    private void initLayout(Context context) {
    }


    public abstract void pullToRefresh();

    public abstract void refreshing();

    public abstract void releaseToRefresh();

    public abstract void reset();

    public abstract void onPull(float scale);

    public abstract int getContentSize();

    public abstract void hideAllViews();

    public abstract void showInvisibleViews();


    public void setWidth(int width) {
        ViewGroup.LayoutParams lp = (ViewGroup.LayoutParams) getLayoutParams();
        lp.width = width;
        requestLayout();
    }

    public void setHeight(int height) {
        ViewGroup.LayoutParams lp = (ViewGroup.LayoutParams) getLayoutParams();
        lp.height = height;
        requestLayout();
    }

    @Override
    public void setLastUpdatedLabel(CharSequence label) {
    }

    @Override
    public void setLoadingDrawable(Drawable drawable) {
    }

    @Override
    public void setPullLabel(CharSequence pullLabel) {
    }

    @Override
    public void setRefreshingLabel(CharSequence refreshingLabel) {
    }

    @Override
    public void setReleaseLabel(CharSequence releaseLabel) {
    }

    @Override
    public void setTextTypeface(Typeface tf) {
    }
}