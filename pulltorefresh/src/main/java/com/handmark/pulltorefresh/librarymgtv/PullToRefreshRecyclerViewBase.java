package com.handmark.pulltorefresh.librarymgtv;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.handmark.pulltorefresh.library.R;

public abstract class PullToRefreshRecyclerViewBase
        extends PullToRefreshBase<RecyclerView> {


    protected RecyclerView mRecyclerView;
    protected LinearLayoutManager mLayoutManager;


    public PullToRefreshRecyclerViewBase(Context context) {
        super(context);
    }

    public PullToRefreshRecyclerViewBase(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PullToRefreshRecyclerViewBase(Context context, Mode mode) {
        super(context, mode);
    }

    public PullToRefreshRecyclerViewBase(Context context, Mode mode, AnimationStyle animStyle) {
        super(context, mode, animStyle);
    }

    protected abstract LinearLayoutManager createLayoutManager(Context context);

    protected abstract boolean isPullStart(View firstVisibleChild, View parentView);

    protected abstract boolean isPullEnd(View lastVisibleChild, View parentView);

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public LinearLayoutManager getLayoutManager() {
        return mLayoutManager;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        mRecyclerView.setAdapter(adapter);
    }

    public int getItemCount() {
        RecyclerView.Adapter adapter = mRecyclerView.getAdapter();
        return adapter != null ? adapter.getItemCount() : 0;
    }

    public int getFirstVisiblePosition() {
        return mLayoutManager.findFirstVisibleItemPosition();
    }

    public int getLastVisiblePosition() {
        return mLayoutManager.findLastVisibleItemPosition();
    }

    public View findViewByPosition(int position) {
        return mLayoutManager.findViewByPosition(position);
    }

    @Override
    protected RecyclerView createRefreshableView(Context context, AttributeSet attributeSet) {
        mLayoutManager = createLayoutManager(context);

        mRecyclerView = createRecyclerViewInternal(context, attributeSet);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setId(R.id.base_recycler_view);
        return mRecyclerView;
    }

    protected RecyclerView createRecyclerViewInternal(Context context, AttributeSet attributeSet) {
        return new RecyclerView(context, attributeSet);
    }

    @Override
    protected boolean isReadyForPullStart() {
        final RecyclerView.Adapter adapter = mRecyclerView.getAdapter();

        if (null == adapter || adapter.getItemCount() == 0) {
            return true;
        } else {
            if (mLayoutManager.findFirstVisibleItemPosition() <= 1) {
                final View firstVisibleChild = mLayoutManager.findViewByPosition(0);
                if (firstVisibleChild != null) {
                    return isPullStart(firstVisibleChild, mRecyclerView);
                }
            }
        }

        return false;
    }

    @Override
    protected boolean isReadyForPullEnd() {
        final RecyclerView.Adapter adapter = mRecyclerView.getAdapter();

        if (null == adapter || adapter.getItemCount() == 0) {
            return true;
        } else {
            final int lastItemPosition = adapter.getItemCount() - 1;
            final int lastVisiblePosition = mLayoutManager.findLastVisibleItemPosition();

            if (lastVisiblePosition >= lastItemPosition) {
                final View lastVisibleChild = mLayoutManager.findViewByPosition(lastVisiblePosition);
                if (lastVisibleChild != null) {
                    return isPullEnd(lastVisibleChild, mRecyclerView);
                }
            }
        }

        return false;
    }
}
