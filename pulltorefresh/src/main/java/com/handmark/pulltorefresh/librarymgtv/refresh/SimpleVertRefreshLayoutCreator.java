package com.handmark.pulltorefresh.librarymgtv.refresh;

import android.content.Context;
import android.util.AttributeSet;
import com.handmark.pulltorefresh.librarymgtv.PullToRefreshBase;

public abstract class SimpleVertRefreshLayoutCreator
        implements RefreshLoadingLayoutCreator {

    @Override
    public RefreshLoadingLayout createPullStartLayout(
            Context context,
            PullToRefreshBase.Orientation direction,
            AttributeSet attrs) {
        return createPullStartLayout(context);
    }

    @Override
    public RefreshLoadingLayout createPullEndLayout(
            Context context,
            PullToRefreshBase.Orientation direction,
            AttributeSet attrs) {
        return createPullEndLayout(context);
    }


    protected abstract RefreshLoadingLayout createPullStartLayout(Context context);

    protected abstract RefreshLoadingLayout createPullEndLayout(Context context);
}
