package com.handmark.pulltorefresh.librarymgtv.refresh;

import android.content.Context;
import android.util.AttributeSet;
import com.handmark.pulltorefresh.librarymgtv.PullToRefreshBase;

public interface RefreshLoadingLayoutCreator {

    RefreshLoadingLayout createPullStartLayout(
            Context context,
            PullToRefreshBase.Orientation direction,
            AttributeSet attrs);

    RefreshLoadingLayout createPullEndLayout(
            Context context,
            PullToRefreshBase.Orientation direction,
            AttributeSet attrs);
}
