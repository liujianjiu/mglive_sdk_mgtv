package com.hunantv.mglive.widget.cardstack;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.widget.media.IjkVideoView;

import java.util.ArrayList;
import java.util.HashMap;

public class ActCardAnimator {
    private static final String DEBUG_TAG = "CardAnimator";
    private static final int REMOTE_DISTANCE = 1000;
    public ArrayList<View> mCardCollection;
    private float mRotation;
    private HashMap<View, LayoutParams> mLayoutsMap;
    private LayoutParams[] mRemoteLayouts = new LayoutParams[4];
    private LayoutParams baseLayout;
    private int mStackMargin = 40;

    public ActCardAnimator(ArrayList<View> viewCollection) {
        mCardCollection = viewCollection;
        setup();
    }

    private void setup() {
        mLayoutsMap = new HashMap<View, LayoutParams>();

        for (View v : mCardCollection) {
            //setup basic layout
            LayoutParams params = (LayoutParams) v.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params.width = LayoutParams.MATCH_PARENT;
            params.height = LayoutParams.MATCH_PARENT;

            //Random rnd = new Random();
            //int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            //v.setBackgroundColor(color);

            v.setLayoutParams(params);
        }

        baseLayout = (LayoutParams) mCardCollection.get(0).getLayoutParams();
        baseLayout = copyLP(baseLayout);

        initLayout();

        for (View v : mCardCollection) {
            LayoutParams params = (LayoutParams) v.getLayoutParams();
            LayoutParams paramsCopy = copyLP(params);
            mLayoutsMap.put(v, paramsCopy);
        }

        setupRemotes();

    }

    static final int margin = Constant.toPix(15);

    public void initLayout() {
        int size = mCardCollection.size();
        for (View v : mCardCollection) {
            int index = mCardCollection.indexOf(v);
//            if (index != 0) {
//                index -= 1;
//            }
            LayoutParams params = copyLP(baseLayout);
            v.setLayoutParams(params);

//            CardUtils.scale(v, -(size - 2 - index) * margin);
//            CardUtils.move(v, (index - 1) * margin, 0);
//            v.setAlpha((index + 3.0f) / 5.0f);

            CardUtils.scale(v, (index - 1) * margin);
            CardUtils.move(v, (index - 1) * margin, 0);
            v.setAlpha((index + 4.0f) / 5.0f);

            v.setRotation(0);
        }
    }

    private void setupRemotes() {
        View topView = getTopView();
        mRemoteLayouts[0] = CardUtils.getMoveParams(topView, REMOTE_DISTANCE, -REMOTE_DISTANCE);
        mRemoteLayouts[1] = CardUtils.getMoveParams(topView, REMOTE_DISTANCE, REMOTE_DISTANCE);
        mRemoteLayouts[2] = CardUtils.getMoveParams(topView, -REMOTE_DISTANCE, -REMOTE_DISTANCE);
        mRemoteLayouts[3] = CardUtils.getMoveParams(topView, -REMOTE_DISTANCE, REMOTE_DISTANCE);

    }

    private View getTopView() {
        return mCardCollection.get(mCardCollection.size() - 1);
    }

    private void moveToBack(View child) {
        final ViewGroup parent = (ViewGroup) child.getParent();
        if (null != parent) {
            parent.removeView(child);
            parent.addView(child, 0);
        }
    }

    private void reorder() {
        View temp = getTopView();
        //RelativeLayout.LayoutParams tempLp = mLayoutsMap.get(mCardCollection.get(0));
        //mLayoutsMap.put(temp,tempLp);
        moveToBack(temp);

        for (int i = (mCardCollection.size() - 1); i > 0; i--) {
            //View next = mCardCollection.get(i);
            //RelativeLayout.LayoutParams lp = mLayoutsMap.get(next);
            //mLayoutsMap.remove(next);
            View current = mCardCollection.get(i - 1);
            //current replace next
            mCardCollection.set(i, current);
            //mLayoutsMap.put(current,lp);

        }
        mCardCollection.set(0, temp);
        temp = getTopView();
    }

    public void discard(int direction, final AnimatorListener al) {
        AnimatorSet as = new AnimatorSet();
        ArrayList<Animator> aCollection = new ArrayList<Animator>();


        final View topView = getTopView();
        LayoutParams topParams = (LayoutParams) topView.getLayoutParams();
        LayoutParams layout = copyLP(topParams);
        ValueAnimator discardAnim = ValueAnimator.ofObject(new RelativeLayoutParamsEvaluator(), layout, mRemoteLayouts[direction]);

        discardAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator value) {
                topView.setLayoutParams((LayoutParams) value.getAnimatedValue());
            }
        });
        discardAnim.setDuration(250);
        aCollection.add(discardAnim);

        ObjectAnimator rotateAnim = CardUtils.getRotateAnim(topView, direction, mRotation);
        rotateAnim.setDuration(250);
        aCollection.add(rotateAnim);

        for (int i = 0; i < mCardCollection.size(); i++) {
            final View v = mCardCollection.get(i);

            if (v == topView) continue;
            final View nv = mCardCollection.get(i + 1);
            LayoutParams layoutParams = (LayoutParams) v.getLayoutParams();
            LayoutParams endLayout = copyLP(layoutParams);
            ValueAnimator layoutAnim = ValueAnimator.ofObject(new RelativeLayoutParamsEvaluator(), endLayout, mLayoutsMap.get(nv));
            layoutAnim.setDuration(250);
            layoutAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator value) {
                    v.setLayoutParams((LayoutParams) value.getAnimatedValue());
                }
            });
            aCollection.add(layoutAnim);
        }

        as.addListener(new AnimatorListenerAdapter() {


            @Override
            public void onAnimationEnd(Animator animation) {
                reorder();
                if (al != null) {
                    al.onAnimationEnd(animation);
                }
                mLayoutsMap = new HashMap<View, LayoutParams>();
                for (View v : mCardCollection) {
                    LayoutParams params = (LayoutParams) v.getLayoutParams();
                    LayoutParams paramsCopy = copyLP(params);
                    mLayoutsMap.put(v, paramsCopy);
                }
            }
        });
        as.playTogether(aCollection);
        as.start();
    }

    public void reverse(MotionEvent e1, MotionEvent e2) {

        final View topView = getTopView();

        ValueAnimator rotationAnim = ValueAnimator.ofFloat(mRotation, 0f);
        rotationAnim.setDuration(250);
        rotationAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator v) {
                topView.setRotation(((Float) (v.getAnimatedValue())).floatValue());
            }
        });

        rotationAnim.start();

        for (final View v : mCardCollection) {
            LayoutParams layoutParams = (LayoutParams) v.getLayoutParams();
            LayoutParams endLayout = copyLP(layoutParams);
            ValueAnimator layoutAnim = ValueAnimator.ofObject(new RelativeLayoutParamsEvaluator(), endLayout, mLayoutsMap.get(v));
            layoutAnim.setDuration(250);
            layoutAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator value) {
                    v.setLayoutParams((LayoutParams) value.getAnimatedValue());
                }
            });
            layoutAnim.start();
        }

    }

    public void drag(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

        if(e1 == null || e2 == null){
            return;
        }

        View topView = getTopView();

        LayoutParams layoutParams = (LayoutParams) topView.getLayoutParams();
        LayoutParams topViewLayouts = mLayoutsMap.get(topView);
        int x_diff = (int) ((e2.getRawX() - e1.getRawX()));
        int y_diff = (int) ((e2.getRawY() - e1.getRawY()));

        layoutParams.leftMargin = topViewLayouts.leftMargin + x_diff;
        layoutParams.rightMargin = topViewLayouts.rightMargin - x_diff;
        layoutParams.topMargin = topViewLayouts.topMargin + y_diff;
        layoutParams.bottomMargin = topViewLayouts.bottomMargin - y_diff;
        topView.setLayoutParams(layoutParams);

        //animate secondary views.
        for (View v : mCardCollection) {
            int index = mCardCollection.indexOf(v);
            if (v != getTopView() && index != 0) {
                LayoutParams l = CardUtils.scaleFrom(v, mLayoutsMap.get(v), (int) (Math.abs(x_diff) * 0.05));
                CardUtils.moveFrom(v, l, 0, (int) (Math.abs(x_diff) * 0.1));
            }
        }
    }

    public void setStackMargin(int margin) {
        mStackMargin = margin;
        initLayout();
    }

    /**
     * 复制一个新的LayoutParams
     *
     * @param LP
     */
    private LayoutParams copyLP(LayoutParams LP) {
        if (null == LP) {
            return null;
        }
        LayoutParams mNewLP = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mNewLP = new LayoutParams(LP);
        } else {
            mNewLP = new LayoutParams(LP.width, LP.height);
            mNewLP.leftMargin = LP.leftMargin;
            mNewLP.topMargin = LP.topMargin;
            mNewLP.rightMargin = LP.rightMargin;
            mNewLP.bottomMargin = LP.bottomMargin;
        }
        return mNewLP;
    }
}
