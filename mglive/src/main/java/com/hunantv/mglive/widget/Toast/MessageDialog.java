package com.hunantv.mglive.widget.Toast;


import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.hunantv.mglive.R;


public class MessageDialog extends AlertDialog {

    private Context context;
    private String title;
    private String confirmButtonText;
    private ClickListenerInterface clickListenerInterface;

    public interface ClickListenerInterface {

        public void doConfirm();
    }

    public MessageDialog(Context context, String title, String confirmBtnText) {
        super(context);
        this.context = context;
        this.title = title;
        this.confirmButtonText = confirmBtnText;
    }

    public MessageDialog(Context context, int titleId, int confirmBtnTextId) {
        super(context);
        this.context = context;
        this.title = context.getString(titleId);
        this.confirmButtonText = context.getString(confirmBtnTextId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    public void init() {
        setContentView(R.layout.message_dialog);

        TextView tvTitle = (TextView) findViewById(R.id.title);
        TextView tvOK = (TextView) findViewById(R.id.tv_ok);

        tvTitle.setText(title);
        tvOK.setText(confirmButtonText);

        tvOK.setOnClickListener(new clickListener());

        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 0.6); // 高度设置为屏幕的0.6
        dialogWindow.setAttributes(lp);
    }

    public void setClicklistener(ClickListenerInterface clickListenerInterface) {
        this.clickListenerInterface = clickListenerInterface;
    }

    private class clickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.tv_ok) {
                if (clickListenerInterface != null) {
                    clickListenerInterface.doConfirm();
                } else {
                    dismiss();
                }

            }
        }

    };

}
