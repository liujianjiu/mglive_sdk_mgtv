package com.hunantv.mglive.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by qiudaaini@163.com on 16/1/9.
 */
public class MarqeeTextView extends TextView {
    public MarqeeTextView(Context context) {
        super(context);
    }

    public MarqeeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MarqeeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MarqeeTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    public boolean isFocused() {
        return true;
    }
}
