/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hunantv.mglive.widget.animation;

import android.graphics.Bitmap;

import com.sina.weibo.sdk.call.Position;

import java.util.HashMap;

/**
 * This class represents a single Droidflake, with properties representing its
 * size, rotation, location, and speed.
 */
public class Flake {

    float x, y;
    float speedY;
    float speedX;
    int width, height;

    int rotation;

    //缩放度
    float scale = 0;
    //透明度
    int alpha = 255;
    Bitmap bitmap;
    boolean isRealeyToShow = false;

    // This map stores pre-scaled bitmaps according to the width. No reason to create
    // new bitmaps for sizes we've already seen.

    /**
     * Creates a new droidflake in the given xRange and with the given bitmap. Parameters of
     * location, size, rotation, and speed are randomly determined.
     */
    static Flake createFlake(Bitmap originalBitmap) {
        Flake flake = new Flake();
        //大小
        flake.width = originalBitmap.getWidth();
        flake.height = originalBitmap.getHeight();

        //速度
        flake.speedY = -200 - (float) Math.random() * 50;
        double sX = Math.random()-0.5;
        if(sX>=0){
            flake.speedX = (float)(Math.min(0.2,sX)) * 150;
        }else{
            flake.speedX = (float)(Math.min(-0.2,sX)) * 150;
        }

        //角度
        flake.rotation = (int)(30*Math.random())-15;

        //图片
        flake.bitmap = originalBitmap;

        flake.isRealeyToShow = false;

        return flake;
    }
}
