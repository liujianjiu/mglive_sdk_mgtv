package com.hunantv.mglive.widget.cardstack;

import android.animation.TypeEvaluator;
import android.os.Build;
import android.widget.RelativeLayout.LayoutParams;

public class RelativeLayoutParamsEvaluator implements TypeEvaluator<LayoutParams> {

    @Override
    public LayoutParams evaluate(float fraction, LayoutParams start, LayoutParams end) {
        LayoutParams result = copyLP(start);
        result.leftMargin += ((end.leftMargin - start.leftMargin) * fraction);
        result.rightMargin += ((end.rightMargin - start.rightMargin) * fraction);
        result.topMargin += ((end.topMargin - start.topMargin) * fraction);
        result.bottomMargin += ((end.bottomMargin - start.bottomMargin) * fraction);
        return result;
    }

    /**
     * 复制一个新的LayoutParams
     *
     * @param LP
     */
    private LayoutParams copyLP(LayoutParams LP) {
        if (null == LP) {
            return null;
        }
        LayoutParams mNewLP = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mNewLP = new LayoutParams(LP);
        } else {
            mNewLP = new LayoutParams(LP.width, LP.height);
            mNewLP.leftMargin = LP.leftMargin;
            mNewLP.topMargin = LP.topMargin;
            mNewLP.rightMargin = LP.rightMargin;
            mNewLP.bottomMargin = LP.bottomMargin;
        }
        return mNewLP;
    }
}
