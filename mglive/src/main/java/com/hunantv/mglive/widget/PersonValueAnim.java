package com.hunantv.mglive.widget;

import android.content.Context;
import android.os.Handler;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.utils.L;

/**
 * Created by QiuDa on 15/12/18.
 */
public class PersonValueAnim {
    private static final String TAG = "PersonValueAnim";
    private Context mContext;
    public ViewGroup mViewGroup;

    public PersonValueAnim(Context mContext, ViewGroup viewGroup) {
        this.mContext = mContext;
        this.mViewGroup = viewGroup;
    }

    public void startPersonValueAnim(View view) {
//        L.d(TAG, fromx + " " + fromy + "  " + tox + "  " + toy);
        final TextView tv = new TextView(mContext);
        ViewGroup.LayoutParams pstv = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tv.setLayoutParams(pstv);
        tv.setTextColor(mContext.getResources().getColor(R.color.common_yellow));
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.text_size19dp));
        tv.setText(R.string.attention_anim_text);
        tv.setGravity(Gravity.CENTER);
        tv.setVisibility(View.INVISIBLE);
        mViewGroup.addView(tv);
        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        tv.measure(w, h);
        int viewWidth = tv.getMeasuredWidth();
        int viewHeight = tv.getMeasuredHeight();

        int[] startPosition = new int[2];
        tv.getLocationOnScreen(startPosition);
        startPosition[0] = startPosition[0] + viewWidth / 2;
        startPosition[1] = startPosition[1] + viewHeight;

        int[] endPosition = new int[2];
        view.getLocationOnScreen(endPosition);
        endPosition[0] = endPosition[0] + view.getWidth() / 2;
        endPosition[1] = endPosition[1];

        L.d("startPosition", startPosition[0] + " " + startPosition[1]);
        L.d("endPosition", endPosition[0] + " " + endPosition[1]);

        int fromx = endPosition[0] - startPosition[0];
        int fromy = endPosition[1] - startPosition[1];
        int translateY = viewHeight * 2;
        startPersonValueAnim(tv, fromx, fromy, translateY);
    }

    public void startPersonValueAnim(final TextView tv, int fromx, int fromy, int translateY) {
        final AnimationSet animationSetTv = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(Animation.ABSOLUTE, fromx, Animation.ABSOLUTE, fromx, Animation.ABSOLUTE, fromy, Animation.ABSOLUTE, fromy - translateY);
        translateAnimation.setDuration(1000);
        translateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());

        animationSetTv.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                tv.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tv.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mViewGroup.removeView(tv);

                    }
                }, 500);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        tv.clearAnimation();
        tv.startAnimation(translateAnimation);
    }
}
