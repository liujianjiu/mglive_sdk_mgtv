package com.hunantv.mglive.widget;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.StringUtil;

/**
 * Created by qiudaaini@163.com on 16/1/21.
 */
public class StarMsgView {

    private int mCountTime;
    private boolean mViewDisplay;
    private String mName;


    private View mView;
    private ImageView mIvIcon;
    private TextView mTvMsg;
    private ImageView mIvClose;

    private Context mContext;

    private Handler mHandler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            if (mCountTime >= 60) {
                disappearView(null);
            } else {
                mCountTime++;
                mHandler.sendEmptyMessageDelayed(0, 1000);
            }

        }
    };

    public StarMsgView(Context context, View mView) {
        this.mView = mView;
        this.mContext = context;
        if (mView != null) {
            mIvIcon = (ImageView) mView.findViewById(R.id.iv_star_msg_icon);
            mTvMsg = (TextView) mView.findViewById(R.id.tv_star_msg);
            mIvClose = (ImageView) mView.findViewById(R.id.iv_star_msg_close);
        }
    }

    public void initView(String image, String name) {
        if (mViewDisplay) {
            mView.setVisibility(View.GONE);
            mViewDisplay = false;
        }
        if (mIvIcon != null && mContext != null) {
            Glide.with(mContext).load(image).transform(new GlideRoundTransform(mContext, R.dimen.height_25dp)).into(mIvIcon);
        }
        mName = name + "：";
        if (mIvClose != null) {
            mIvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mView != null) {
                        mView.clearAnimation();
                        mView.setVisibility(View.GONE);
                        mViewDisplay = false;
                    }
                }
            });
        }
    }

    public void display(String msg) {
        if (mViewDisplay) {
            if (!StringUtil.isNullorEmpty(msg)) {
                disappearView(msg);
            }
        } else {
            setText(msg);
            dispalyView();
        }
    }

    private void setText(String msg) {
        int length = mName.length();
        SpannableStringBuilder style = new SpannableStringBuilder(mName + msg);
        style.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.common_yellow)), 0, length, Spannable.SPAN_EXCLUSIVE_INCLUSIVE); //设置指定位置textview的背景颜色
        style.setSpan(new ForegroundColorSpan(Color.WHITE), length, style.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE); //设置指定位置文字的颜色
        mTvMsg.setText(style);
    }

    private void disappearView(final String changeText) {
        if (mView != null) {
            mHandler.removeMessages(0);
            mView.clearAnimation();
            Animation animation = new ScaleAnimation(1f, 1f, 1f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0f);
            animation.setFillAfter(true);
            animation.setDuration(200);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (mTvMsg != null && !StringUtil.isNullorEmpty(changeText) && !StringUtil.isNullorEmpty(mName)) {
                        setText(changeText);
                        dispalyView();
                    } else {
                        mView.setVisibility(View.GONE);
                    }
                    mViewDisplay = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            mView.startAnimation(animation);
        }

    }

    private void dispalyView() {
        if (mView != null) {
            mCountTime = 0;
            mHandler.removeMessages(0);
            mHandler.sendEmptyMessageDelayed(0, 1000);
            mView.clearAnimation();
            Animation animation = new ScaleAnimation(1f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0f);
            animation.setFillAfter(true);
            animation.setDuration(200);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mView.setVisibility(View.VISIBLE);
                    mViewDisplay = true;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            mView.startAnimation(animation);
        }
    }

    private void closeView() {
        if (mView != null) {
            mView.setVisibility(View.GONE);
        }
    }

}
