package com.hunantv.mglive.widget.imagecrop;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class FixedScaleFrameLayout extends FrameLayout {

    private FixedScaleSupport mScaleSupport = new FixedScaleSupport();


    public FixedScaleFrameLayout(Context context) {
        super(context);
        initLayout(context, null);
    }

    public FixedScaleFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context, attrs);
    }

    public FixedScaleFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context, attrs);
    }

    private void initLayout(Context context, AttributeSet attrs) {
        mScaleSupport.init(this, attrs);
    }

    public FixedScaleSupport getScaleSupport() {
        return mScaleSupport;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        FixedScaleSupport.MeasureSize size = mScaleSupport.doMeasure(
                widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(size.mWidthMeasureSpec, size.mHeightMeasureSpec);
    }
}
