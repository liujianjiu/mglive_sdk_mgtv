package com.hunantv.mglive.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.utils.StringUtil;

/**
 * Created by qiuda on 16/2/22.
 */
public class GlodDialog {
    private Context mContext;
    private Dialog mDialog;
    private TextView mTvSponsor;
    private TextView mTvCoinInfon;
    private TextView mTvSucceed;
    private Handler mHandler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            try {
                if (mDialog != null && mDialog.isShowing() && mContext != null && !((Activity) mContext).isFinishing()) {
                    mDialog.dismiss();
                }
            } catch (Exception ignored) {

            }
        }
    };

    public GlodDialog(Context context) {
        this.mContext = context;

    }

    public GlodDialog creat() {
        mDialog = new Dialog(mContext, R.style.gold_window_style);
        View view = LayoutInflater.from(mContext).inflate(R.layout.gold_window_layout, null);
        mTvSponsor = (TextView) view.findViewById(R.id.tv_gold_window_sponsors);
        mTvCoinInfon = (TextView) view.findViewById(R.id.tv_gold_window_gold_num);
        mTvSucceed = (TextView) view.findViewById(R.id.tv_gold_window_succeed);
        mDialog.setContentView(view);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        return this;
    }


    public void show(String coinInfo, String sponsor) {
        if (((Activity) mContext).isFinishing() || mContext == null) {
            return;
        }
        if (mDialog != null && !mDialog.isShowing()) {
            if (!StringUtil.isNullorEmpty(coinInfo)) {
                mTvCoinInfon.setText("+" + coinInfo);
            }

            if (!StringUtil.isNullorEmpty(sponsor)) {
                mTvSponsor.setText(sponsor);
                mTvSponsor.setVisibility(View.VISIBLE);
                mTvSucceed.setVisibility(View.GONE);
            } else {
                mTvSponsor.setVisibility(View.GONE);
                mTvSucceed.setVisibility(View.VISIBLE);
            }
            if (((Activity) mContext).isFinishing()) {
                return;
            }
            mDialog.show();
            mHandler.sendEmptyMessageDelayed(0, 2000);
        }
    }

}
