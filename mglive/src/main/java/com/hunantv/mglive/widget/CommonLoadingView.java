package com.hunantv.mglive.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hunantv.mglive.R;

/**
 * 通用加载/加载错误/空页面
 * Created by liuwanfen on 15/12/29.
 */
public class CommonLoadingView {
    private Context mContext;
    private View mView;
    private ImageView mIv;
    private TextView mText;
    //private Animation mAnim;
    private PopupWindow mPopupwindow;

    private AnimationDrawable animationDrawable;

    private int mCurrentType = 0;

    private static final int MSG_SHOW = 1;

    public static final int TYPE_LOADING = 1;               /* 加载中动画 */

    public static final int TYPE_EMPTY_MSG = 10;            /* 消息列表为空 */
    public static final int TYPE_EMPTY_TAG = 11;            /* 标签为空 */
    public static final int TYPE_EMPTY_DYNAMIC = 12;        /* 广场动态空 */
    public static final int TYPE_EMPTY_LIVE_DYNAMIC = 13;   /* 直播动态空 */
    public static final int TYPE_EMPTY_CARD = 14;           /* 卡券列表空 */
    public static final int TYPE_EMPTY_GUARD = 15;          /* 守护空 */
    public static final int TYPE_EMPTY_POPULARITY = 16;     /* 人气榜空 */

    public static final int TYPE_ERROR = 20;                /* 加载失败 */


    public CommonLoadingView(Context context) {
        mContext = context;

        mView = LayoutInflater.from(mContext).inflate(R.layout.common_error_view, null);
        mIv = (ImageView) mView.findViewById(R.id.img_error);
        mText = (TextView) mView.findViewById(R.id.txt_error);

        mPopupwindow = new PopupWindow(mView,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, false);

    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what) {
                case MSG_SHOW:
                    PopupParam params = (PopupParam) msg.obj;
                    int type = msg.arg1;

                    //仍然需要显示该动画
                    if (type == mCurrentType) {
                        Activity activity = (Activity) mContext;
                        if (activity != null && !activity.isFinishing()) {
                            mPopupwindow.showAtLocation(params.getParent(), params.getGravity(), params.getX(), params.getY());
                        }
                        animationDrawable.start();
                    }

                    break;
                default:
                    break;
            }

            return false;
        }
    });

    /*
     * 设置显示图片
     */
    private void setImage(int imgId) {
        mIv.setImageResource(imgId);
        mIv.setBackgroundResource(0);
    }

    /*
     * 设置显示动画
     */
    private void setAnimation(int animId) {
        mIv.setImageDrawable(null);
        mIv.setBackgroundResource(animId);
        animationDrawable = (AnimationDrawable) mIv.getBackground();

    }

    /*
     * 设置显示文字
     */
    private void setText(String text) {
        mText.setText(text);
    }

    private void setText(int strId) {
        mText.setText(strId);
    }

    /**
     * 显示加载视图
     *
     * @param type    显示类型   TYPE_XXX
     * @param parent  父视图
     * @param gravity 对齐方式
     * @param x
     * @param y
     */
    public void showAtLocation(int type, View parent, int gravity, int x, int y) {
        mCurrentType = type;

        if (animationDrawable != null && animationDrawable.isRunning()) {
            animationDrawable.stop();
        }


        if (type == TYPE_LOADING) {
            //加载动画
            setAnimation(R.anim.app_loading_big);
            setText(R.string.loading);

            //showAtLocation在onCreate 调用出问题, 因此延迟执行
            PopupParam params = new PopupParam(parent, gravity, x, y);
            Message msg = Message.obtain();
            msg.what = MSG_SHOW;
            msg.obj = params;
            msg.arg1 = type;
            mHandler.sendMessageDelayed(msg, 200);

//            mPopupwindow.showAtLocation(parent, gravity, x, y);
//            animationDrawable.start();
        } else {
            //空页面 或者 失败

            if (type == TYPE_ERROR) {
                setImage(R.drawable.video_load_error);
                setText(R.string.load_error);
            }

            if (type == TYPE_EMPTY_MSG) {
                setImage(R.drawable.empty_msg);
                setText(R.string.empty_msg);
            }

            if (type == TYPE_EMPTY_TAG) {
                setImage(R.drawable.empty_tag);
                setText(R.string.empty_tag);
            }

            if (type == TYPE_EMPTY_DYNAMIC) {
                setImage(R.drawable.empty_dynamic);
                setText(R.string.empty_dynamic);
            }

            if (type == TYPE_EMPTY_LIVE_DYNAMIC) {
                setImage(R.drawable.empty_live_dynamic);
                setText(R.string.empty_live_dynamic);
            }

            if (type == TYPE_EMPTY_CARD) {
                setImage(R.drawable.empty_card);
                setText(R.string.empty_card);
            }

            if (type == TYPE_EMPTY_GUARD) {
                setImage(R.drawable.empty_guard);
                setText(R.string.empty_guard);
            }

            if (type == TYPE_EMPTY_POPULARITY) {
                setImage(R.drawable.empty_popularity);
                setText(R.string.empty_popularity);
            }

            Activity activity = (Activity) mContext;
            if (activity != null && !activity.isFinishing()) {
                mPopupwindow.showAtLocation(parent, gravity, x, y);
            }
        }

    }

    public void dismiss() {
        if (mPopupwindow != null) {
            mPopupwindow.dismiss();
        }

        if (animationDrawable != null && animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }

    private class PopupParam {
        private View parent;
        private int gravity;
        private int x;
        private int y;

        public PopupParam(View parent, int gravity, int x, int y) {
            this.parent = parent;
            this.gravity = gravity;
            this.x = x;
            this.y = y;
        }

        public View getParent() {
            return parent;
        }

        public void setParent(View parent) {
            this.parent = parent;
        }

        public int getGravity() {
            return gravity;
        }

        public void setGravity(int gravity) {
            this.gravity = gravity;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }
    }
}
