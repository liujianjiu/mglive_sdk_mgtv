package com.hunantv.mglive.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;


/**
 * 商品的列表
 */
public class MGridView extends GridView {
	private int mColumns;

	/**
	 * 构造方法
	 * 
	 * @param context
	 *            上下文对象
	 * @param attrs
	 *            属性
	 */
	public MGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mColumns = getAttriIntVal(attrs, "numColumns");

	}

	/**
	 * 
	 * @Method: onMeasure
	 * @param widthMeasureSpec
	 *            参数
	 * @param heightMeasureSpec
	 *            参数
	 * @see GridView#onMeasure(int, int)
	 */
	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}

	/**
	 * 
	 * @return 列数
	 */
	public int getColumns() {
		return mColumns;
	}

	private int getAttriIntVal(AttributeSet attrs, String attrName) {
		int attrCount = attrs.getAttributeCount();
		for (int i = 0; i < attrCount; ++i) {
			String name = attrs.getAttributeName(i);
			if (name.equals(attrName)) {
				return Integer.parseInt(attrs.getAttributeValue(i));
			}
		}
		return 0;
	}

}
