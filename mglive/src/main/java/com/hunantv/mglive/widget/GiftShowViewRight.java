package com.hunantv.mglive.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.GiftShowViewDataModel;
import com.hunantv.mglive.utils.GlideRoundTransform;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by QiuDa on 15/12/19.
 */
public class GiftShowViewRight {
    private static float displacement = 0.04f;
    private boolean mIsStart;
    private Context mContext;
    private ViewGroup mViewGroup;
    private LayoutInflater mInflater;
    private List<GiftShowViewDataModel> mDataList = new ArrayList<>();

    public GiftShowViewRight(Context mContext, ViewGroup mViewGroup) {
        this.mContext = mContext;
        this.mViewGroup = mViewGroup;
        mInflater = LayoutInflater.from(mContext);
    }


    public void show(GiftShowViewDataModel dataModel) {
        synchronized (mDataList) {
            if (!mIsStart && mDataList.size() <= 0) {
                showView(dataModel);
            } else {
                if (mDataList.size() < 100) {
                    mDataList.add(dataModel);
                }
            }
        }
    }

    private void showView(GiftShowViewDataModel dataModel) {
        mIsStart = true;
        final View view;
        final GSViewHolder viewHolder;
        view = mInflater.inflate(R.layout.gift_show_view_right_layout, null);
        viewHolder = new GSViewHolder();
        viewHolder.mHolderView = new ViewHodlerItem();
        viewHolder.mHolderGiftImg = new ViewHodlerItem();

        viewHolder.view = view.findViewById(R.id.icv_gift_show_view);
        viewHolder.view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.gift_show_view_right_bg));
        viewHolder.vGiftImage = view.findViewById(R.id.icv_gift_show_view_gitf_img);

        initViewHolder(viewHolder.vGiftImage, viewHolder.mHolderGiftImg);
        initViewHolder(viewHolder.view, viewHolder.mHolderView);
        if (dataModel != null) {
            setView(dataModel, viewHolder.mHolderGiftImg, true);
            setView(dataModel, viewHolder.mHolderView, false);
            setTextView(viewHolder.mHolderView);
        }
        setGiftImageDispaly(viewHolder.mHolderGiftImg);

        mViewGroup.addView(view);

        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation =
                new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
        translateAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        TranslateAnimation translateAnimationback =
                new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, displacement, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
        translateAnimationback.setDuration(300);
        translateAnimationback.setStartOffset(300);
        animationSet.addAnimation(translateAnimationback);

        animationSet.setFillBefore(true);
        animationSet.setFillAfter(true);

        final AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1);
        alphaAnimation.setDuration(300);
        alphaAnimation.setFillAfter(true);

        final TranslateAnimation translateAnimationImage;
        translateAnimationImage = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 2, Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
        translateAnimationImage.setDuration(300);
        translateAnimationImage.setFillAfter(true);

        final ScaleAnimation scaleAnimationNum = new ScaleAnimation(1.7f, 1f, 1.7f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimationNum.setDuration(300);
        scaleAnimationNum.setFillAfter(true);

        final AnimationSet animationSetAll = new AnimationSet(true);
        TranslateAnimation translateAnimationAll = new TranslateAnimation(Animation.RELATIVE_TO_SELF, displacement, Animation.RELATIVE_TO_SELF, displacement, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 3);
        translateAnimationAll.setDuration(1000);
        translateAnimationAll.setFillAfter(true);

        AlphaAnimation alphaAnimationAll = new AlphaAnimation(1, 0);
        alphaAnimationAll.setDuration(1000);
        alphaAnimationAll.setFillAfter(true);

        animationSetAll.addAnimation(translateAnimationAll);
        animationSetAll.addAnimation(alphaAnimationAll);
        animationSetAll.setFillAfter(true);


        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                viewHolder.mHolderView.llInfo.startAnimation(alphaAnimation);
                viewHolder.mHolderGiftImg.llImge.startAnimation(translateAnimationImage);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        translateAnimationImage.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                viewHolder.mHolderView.llNum.startAnimation(scaleAnimationNum);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        scaleAnimationNum.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.startAnimation(animationSetAll);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationSetAll.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mViewGroup.removeView(view);
                        synchronized (mDataList) {
                            mIsStart = false;
                            if (mDataList.size() > 0) {
                                GiftShowViewDataModel model = mDataList.get(0);
                                mDataList.remove(model);
                                showView(model);
                            }
                        }
                    }
                }, 100);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(animationSet);

    }

    private void initViewHolder(View view, ViewHodlerItem viewHodlerItem) {
        viewHodlerItem.ivIcon = (ImageView) view.findViewById(R.id.iv_gift_show_view_icon);
        viewHodlerItem.tvUserName = (TextView) view.findViewById(R.id.tv_gift_show_view_user_name);
        viewHodlerItem.tvStarName = (TextView) view.findViewById(R.id.tv_gift_show_view_star_name);
        viewHodlerItem.ivGiftImg = (ImageView) view.findViewById(R.id.iv_gift_show_gift_img);
        viewHodlerItem.tvX = (TextView) view.findViewById(R.id.tv_gift_show_view_x);
        viewHodlerItem.tvNum = (TextView) view.findViewById(R.id.tv_gift_show_view_num);
        viewHodlerItem.llInfo = (LinearLayout) view.findViewById(R.id.ll_gift_show_view_info);
        viewHodlerItem.llNum = (LinearLayout) view.findViewById(R.id.ll_gift_show_view_num);
        viewHodlerItem.llImge = (LinearLayout) view.findViewById(R.id.ll_gift_show_view_img);
    }

    private void setView(GiftShowViewDataModel dataModel, ViewHodlerItem viewItem, boolean setImage) {
        if (Util.isOnMainThread() && !((Activity) mContext).isFinishing()) {
            Glide.with(mContext).load(dataModel.getIcon()).transform(new GlideRoundTransform(mContext, R.dimen.height_30dp)).into(viewItem.ivIcon);
            viewItem.tvUserName.setText(dataModel.getUserName());
            viewItem.tvStarName.setText(mContext.getString(R.string.gift_give, dataModel.getStarName()));
            if (setImage) {
                Glide.with(mContext).load(dataModel.getImage()).into(viewItem.ivGiftImg);
            }
        }

        viewItem.tvNum.setText(dataModel.getNum() + "");
    }

    private void setGiftImageDispaly(ViewHodlerItem viewItem) {
//        ViewGroup.LayoutParams ps = viewItem.llImge.getLayoutParams();
//        ps.height = ps.width = mContext.getResources().getDimensionPixelSize(R.dimen.height_50dp);
    }

    private void setTextView(ViewHodlerItem viewItem) {
        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        viewItem.tvNum.measure(w, h);
        int viewHeight = viewItem.tvNum.getMeasuredHeight();

        Shader shaderNum = new LinearGradient(0, 0, 0, viewHeight, Color.WHITE, Color.YELLOW, Shader.TileMode.CLAMP);
        viewItem.tvNum.getPaint().setShader(shaderNum);


        w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        viewItem.tvX.measure(w, h);
        viewHeight = viewItem.tvX.getMeasuredHeight();

        Shader shaderX = new LinearGradient(0, 0, 0, viewHeight, Color.WHITE, Color.YELLOW, Shader.TileMode.CLAMP);
        viewItem.tvX.getPaint().setShader(shaderX);
    }

    class GSViewHolder {
        View view;
        View vGiftImage;
        ViewHodlerItem mHolderGiftImg;
        ViewHodlerItem mHolderView;
    }

    class ViewHodlerItem {
        ImageView ivIcon;
        TextView tvUserName;
        TextView tvStarName;
        ImageView ivGiftImg;
        TextView tvX;
        TextView tvNum;
        LinearLayout llInfo;
        LinearLayout llNum;
        LinearLayout llImge;
    }

}
