package com.hunantv.mglive.widget.Toast;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.utils.StringUtil;

/**
 * Created by admin on 2016/3/1.
 */
public class ExitLiveDialog extends AlertDialog implements View.OnClickListener {
    private OnClickListener mListener;
    private TextView mNum;
    private Button mOk;
    private Button mCancle;
    private ImageButton mClose;

    public ExitLiveDialog(Context context, OnClickListener listener) {
        super(context);
        this.mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_exit_live_dialog);
        setCanceledOnTouchOutside(true);
        initUI();

        Window window = getWindow(); //得到对话框
        window.setWindowAnimations(R.style.dialog_window_anim); //设置窗口弹出动画
        window.setBackgroundDrawableResource(R.drawable.exit_live_dialog_bg_shape); //设置对话框背景
//        window.setDimAmount(0l);

        window.setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = window.getAttributes();
        DisplayMetrics d = getContext().getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 0.72);
        window.setAttributes(lp);
    }

    public void initUI() {
        mNum = (TextView) findViewById(R.id.tv_num);
        mOk = (Button) findViewById(R.id.b_ok);
        mOk.setOnClickListener(this);
        mCancle = (Button) findViewById(R.id.b_cancle);
        mCancle.setOnClickListener(this);
        mClose = (ImageButton) findViewById(R.id.iv_close);
        mClose.setOnClickListener(this);
    }

    public void show(String onLine) {
        show();
        if (!StringUtil.isNullorEmpty(onLine) && mNum != null) {
            mNum.setText(onLine);
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.b_ok) {
            if (mListener != null) {
                mListener.ok();
            }
            dismiss();

        } else if (i == R.id.b_cancle) {
            if (mListener != null) {
                mListener.cancle();
            }
            dismiss();

        } else if (i == R.id.iv_close) {
            dismiss();

        }
    }

    public interface OnClickListener {
        public void ok();

        public void cancle();
    }
}
