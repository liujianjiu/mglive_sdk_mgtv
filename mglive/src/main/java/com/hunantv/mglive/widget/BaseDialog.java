package com.hunantv.mglive.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;

/**
 * Created by liujianjiu on 16-3-28.
 *
 * dialog基础类，当show的时候调用时先判断外部父Activity是否在运行，避免外部父Activity不在运行时的异常崩溃。
 */
public class BaseDialog extends Dialog {
    private Context mContext;

    public BaseDialog(Context context) {
        super(context);
        mContext = context;
    }

    public BaseDialog(Context context, int theme) {
        super(context, theme);
        mContext = context;
    }

    protected BaseDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        mContext = context;
    }

    @Override
    public void show() {
        if(mContext != null && mContext instanceof Activity){
            if(((Activity)mContext).isFinishing()){
                return;
            }
            if(Build.VERSION.SDK_INT>= 17 && ((Activity)mContext).isDestroyed()){
                return;
            }
        }
        super.show();
    }
}
