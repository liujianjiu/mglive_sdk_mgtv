package com.hunantv.mglive.widget.loadingView;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;

import com.hunantv.mglive.widget.loadingView.ViewCallBack;

/**
 * 添加布局界面
 * 
 * @author 孤狼
 * @since 2015-8-4
 */
public class ViewFrameLayout implements ViewCallBack {

	private ViewCallBack helper;
	private View view;

	public ViewFrameLayout(View view) {
		super();
		this.view = view;
		ViewGroup group = (ViewGroup) view.getParent();
		LayoutParams layoutParams = view.getLayoutParams();
		FrameLayout frameLayout = new FrameLayout(view.getContext());
		group.removeView(view);
		group.addView(frameLayout, layoutParams);

		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		View floatView = new View(view.getContext());
		frameLayout.addView(view, params);
		frameLayout.addView(floatView, params);
		helper = new ViewGroupLayout(floatView);
	}

	@Override
	public View getCurrentLayout() {
		return helper.getCurrentLayout();
	}

	@Override
	public void resetView() {
		helper.resetView();
	}

	@Override
	public void showLayout(View view) {
		helper.showLayout(view);
	}

	@Override
	public void showLayout(int layoutId) {
		showLayout(inflate(layoutId));
	}

	@Override
	public View inflate(int layoutId) {
		return helper.inflate(layoutId);
	}

	@Override
	public Context getContext() {
		return helper.getContext();
	}

	@Override
	public View getView() {
		return view;
	}
}
