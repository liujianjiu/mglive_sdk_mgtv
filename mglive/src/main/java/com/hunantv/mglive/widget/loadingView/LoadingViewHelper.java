package com.hunantv.mglive.widget.loadingView;

import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hunantv.mglive.R;

/**
 * 自定义布局，随意替换
 * 
 * @author 孤狼
 * @since 2015-8-4
 */
public class LoadingViewHelper {

	private ViewCallBack helper;

	private View mLayout;

	private ImageView mImage;
	private TextView  mText;
	private Button mButton;
	private AnimationDrawable animationDrawable;

	// 构造函数
	public LoadingViewHelper(View view) {
		this(new ViewGroupLayout(view));
	}

	// 构造函数
	public LoadingViewHelper(ViewCallBack helper) {
		super();
		this.helper = helper;

		mLayout = helper.inflate(R.layout.common_error_view);
		mImage = (ImageView)mLayout.findViewById(R.id.img_error);
		mText =  (TextView)mLayout.findViewById(R.id.txt_error);
		mButton = (Button)mLayout.findViewById(R.id.button_error);
	}


	private void setAnimation(int animId){
		mImage.setImageResource(animId);
		animationDrawable = (AnimationDrawable) mImage.getDrawable();
	}

	private LoadingViewHelper setImage(int redId){
		mImage.setImageResource(redId);
		return this;
	}

	private LoadingViewHelper setText(int redId){
		mText.setText(redId);
		return this;
	}

	private LoadingViewHelper setButton(int redId, OnClickListener buttonClickListener){
		mButton.setText(redId);

		if(buttonClickListener != null){
			mButton.setVisibility(View.VISIBLE);
			mButton.setOnClickListener(buttonClickListener);
		}else{
			mButton.setVisibility(View.GONE);
		}

		return this;
	}

	private LoadingViewHelper setButton(String text, OnClickListener buttonClickListener){
		mButton.setText(text);

		if(buttonClickListener != null){
			mButton.setVisibility(View.VISIBLE);
			mButton.setOnClickListener(buttonClickListener);
		}else{
			mButton.setVisibility(View.GONE);
		}

		return this;
	}

	public void show(String errorText, int errorImg, String buttonText,
			OnClickListener buttonClickListener) {
		mText.setText(errorText);
		mImage.setImageResource(errorImg);
		if(buttonText != null){
			mButton.setText(buttonText);
		}

		if(buttonClickListener != null){
			mButton.setVisibility(View.VISIBLE);
			mButton.setOnClickListener(buttonClickListener);
		}else{
			mButton.setVisibility(View.GONE);
		}

		helper.showLayout(mLayout);
	}

	/**
	 * 实现定制控件样式
	 * @param errorText
	 * @param errorImg
	 * @param buttonText
	 * @param buttonClickListener
	 * @param customListener
	 */
	public void show(String errorText, int errorImg, String buttonText,
					 OnClickListener buttonClickListener,CustomListener customListener){
		if(customListener != null)
		{
			customListener.custom(mLayout,mText,mButton);
		}
		show(errorText,errorImg,buttonText,buttonClickListener);
	}

	public void show(int errorRes, int errorImg, String buttonText,
					 OnClickListener buttonClickListener) {
		mText.setText(errorRes);
		mImage.setImageResource(errorImg);

		if(buttonText != null){
			mButton.setText(buttonText);
		}

		if(buttonClickListener != null){
			mButton.setVisibility(View.VISIBLE);
			mButton.setOnClickListener(buttonClickListener);
		}else{
			mButton.setVisibility(View.GONE);
		}

		helper.showLayout(mLayout);
	}

	public void show(){
		helper.showLayout(mLayout);
	}


	// 加载错误
	public void showError(int redId, OnClickListener buttonClickListener) {
		this.setImage(R.drawable.video_load_error)
				.setText(R.string.load_error)
				.setButton(redId, buttonClickListener)
				.show();
	}

	public void showError() {
		show(R.string.load_error, R.drawable.video_load_error, null, null);
	}


	// 数据为空
	public void showEmpty() {
		show(R.string.empty_msg, R.drawable.empty_msg, null, null);
	}

	// 数据为空
	public void showEmpty(int textResId, int imgResId) {
		show(textResId, imgResId, null, null);
	}

	// 正在加载中
	public void showLoading() {
		setAnimation(R.anim.app_loading_big);
		mText.setText(R.string.loading);
		mButton.setVisibility(View.GONE);

		helper.showLayout(mLayout);
		animationDrawable.start();
	}

	// 还原
	public void restore() {
		helper.resetView();
	}

	public interface CustomListener {
		public void custom(View view,TextView text,Button btn);
	}
}
