package com.hunantv.mglive.widget;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.hunantv.mglive.R;

/**
 * Created by qiudaaini@163.com on 15/12/27.
 */
public class LoadingView {
    private Context mContext;
    private ViewGroup mViewGroup;
    private View mView;
    private ImageView mIv;
    private Animation mAnim;
//    private PopupWindow mPopupwindow;


    public LoadingView(Context context, ViewGroup viewGroup) {
        mContext = context;
        mViewGroup = viewGroup;
        mView = LayoutInflater.from(mContext).inflate(R.layout.loading_view, null);
        mIv = (ImageView) mView.findViewById(R.id.iv_loading);
        mAnim = AnimationUtils.loadAnimation(mContext, R.anim.progress_rotate);
        mAnim.setInterpolator(new LinearInterpolator());

//        mPopupwindow = new PopupWindow(mView,
//                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, false);
    }

    public void showAtLocation() {
        if (mContext != null && !((Activity) mContext).isFinishing() && mView.getParent() == null) {
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mView.setLayoutParams(layoutParams);
            mViewGroup.addView(mView);
            mIv.startAnimation(mAnim);
        }
    }

    public void dismiss() {
        mViewGroup.removeView(mView);
        mIv.clearAnimation();
    }

//    public boolean isShow() {
//        mViewGroup.
//        if (mPopupwindow != null) {
//            return mPopupwindow.isShowing();
//        }
//        return false;
//    }
}
