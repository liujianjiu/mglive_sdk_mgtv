package com.hunantv.mglive.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.common.BuildConfig;
import com.hunantv.mglive.common.FormEncodingBuilderEx;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.ResultModel;
import com.hunantv.mglive.ui.entertainer.data.ContrData;
import com.hunantv.mglive.ui.entertainer.data.ContributionData;
import com.hunantv.mglive.ui.entertainer.data.ContributionInfoData;
import com.hunantv.mglive.ui.entertainer.data.PersonValueData;
import com.hunantv.mglive.ui.entertainer.listener.PayContributionListener;
import com.hunantv.mglive.utils.GiftUtil;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.HttpUtils;
import com.hunantv.mglive.utils.MGTVUtil;
import com.hunantv.mglive.utils.MqttChatUtils;
import com.hunantv.mglive.utils.StringUtil;
import com.hunantv.mglive.utils.Toast;
import com.hunantv.mglive.widget.Toast.ConfirmDialog;
import com.hunantv.mglive.widget.Toast.GuardDialog;

import org.json.JSONException;

import java.util.List;
import java.util.Map;

/**
 * 人气榜列表弹窗控件
 *
 * @author liujianjiu
 */
public class PersonValueListDialog extends BaseDialog implements HttpUtils.callBack{

    public static final int PAY_TYPE_BUY = 1;//购买守护
    public static final int PAY_TYPE_RENEW = 2;//续约守护

    private int payType = 1;

    private Context mContext;

    private HttpUtils mHttp;
    private int pageIndex;
    private int pageSize;
    private String uid;
    private String uName = "";

    public void setuName(String uName) {
        this.uName = uName;
        mTitleView.setText(uName+"的贡献榜");
    }

    private boolean isGoToBuy = false;
    private boolean isNeedRequestContr = true;

    private List<PersonValueData> datas;
    private List<ContributionData> guardDatas;
    private List<ContrData> contrDatas;
    private ContributionInfoData contributionInfo;

    private PersonValueAdapter adapter;

    private View rootView;
    private ImageView dissBu;
    private ListView mFansListList;
    private TextView mTitleView;

    private void assignViews() {
        mFansListList = (ListView) findViewById(R.id.fans_list_list);
        mTitleView = (TextView) findViewById(R.id.fans_list_title);
        dissBu = (ImageView)findViewById(R.id.fans_diss_bu);
        dissBu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public PersonValueListDialog(Context context, String uid) {
        super(context, R.style.dialog);
        this.mContext = context;
        this.uid = uid;

        init();

        //获取粉丝列表数据
        pageIndex = 1;
        pageSize = 20;
        getFansList(pageIndex, pageSize);

        //获取守护列表数据
        getGuardList();

        loadContrInfo();
        loadContrList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!(mContext instanceof Activity)) {
            //从非Activity的上下文中弹出，弹出系统级别的dialog
            getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

    }

    @Override
    public void show() {
        super.show();

        pageIndex = 1;
        pageSize = 20;
        //获取粉丝列表数据
        getFansList(pageIndex, pageSize);
        //获取守护列表数据
        getGuardList();

        loadContrInfo();
    }

    public void init() {

        rootView = LayoutInflater.from(mContext).inflate(R.layout.dialog_fans_list,null);
        setContentView(rootView);
        assignViews();

        rootView.setVisibility(View.GONE);

        adapter = new PersonValueAdapter(mContext);
        mFansListList.setAdapter(adapter);

        float withScale;
        float heightScale;
        Configuration newConfig = mContext.getResources().getConfiguration();
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //横屏
            withScale = 0.6f;
            heightScale = 0.95f;
        } else {
            //竖屏
            withScale = 0.9f;
            heightScale = 0.9f;
        }
        Window dialogWindow = getWindow();
        dialogWindow.setWindowAnimations(R.style.dialog_window_anim); //设置窗口弹出动画
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = mContext.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * withScale); // 宽度设置为屏幕宽度比例
        lp.height = (int) (d.heightPixels * heightScale); // 高度设置为屏幕高度比例
        dialogWindow.setAttributes(lp);
    }

    /**
     * 获取人气榜列表
     *
     * @param pageIndex 页号
     * @param pageSize  页长度
     */
    public void getFansList(int pageIndex, int pageSize) {
        Map<String, String> body = new FormEncodingBuilderEx()
                .add("page", String.valueOf(pageIndex))
                .add("pageSize", String.valueOf(pageSize))
                .add("uid", uid)
                .build();
        post(BuildConfig.URL_POPULARITY_RANK, body);
    }


    /**
     * 获取守护列表
     */
    public void getGuardList() {
        if (!StringUtil.isNullorEmpty(uid)) {
            Map<String, String> body = new FormEncodingBuilderEx()
//                    .add("page", String.valueOf(mGuardCurrentPage))
//                    .add("pageSize", String.valueOf(mGuardPageSize))
                    .add("uid", uid)
                    .build();
            post(BuildConfig.URL_GUARD_RANK, body);
        }
    }

    /**
     * 查询守护信息
     */
    private void loadContrInfo() {
        if (MaxApplication.getInstance().isLogin() && !StringUtil.isNullorEmpty(uid)) {
            Map<String, String> body = new FormEncodingBuilderEx()
                    .add("uid", MaxApplication.getInstance().getUid())
                    .add("token", MaxApplication.getInstance().getToken())
                    .add("aid", uid)
                    .build();
            post(BuildConfig.GET_CONTRIBUTION_INFO, body);
        }
    }

    /**
     * 产品守护列表
     */
    private void loadContrList() {
        if (GiftUtil.getInstance().getContrs() == null) {
            Map<String, String> body = new FormEncodingBuilderEx().add("page", "1").add("pageSize", "10").build();
            post(BuildConfig.URL_CONTR_LIST, body);
        }else {
            contrDatas = GiftUtil.getInstance().getContrs();
        }
    }

    @Override
    public boolean get(String url, Map<String, String> param) {
        return false;
    }

    @Override
    public boolean post(String url, Map<String, String> param) {
        if (mHttp == null) {
            mHttp = new HttpUtils(mContext, this);
        }
        return mHttp.post(url, param);
    }

    @Override
    public void onError(String url, Exception e) {
        if(BuildConfig.URL_POPULARITY_RANK.equals(url)) {//人气榜
            if (datas == null) {
                rootView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        return null;
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
        if(BuildConfig.URL_POPULARITY_RANK.equals(url)) {//人气榜
            if (datas == null) {
                rootView.setVisibility(View.VISIBLE);
            }
        }
        else if (BuildConfig.PAY_GIFT.equals(url) && ResultModel.ERROR_CODE_GOLD_ENOUGH.equals(resultModel.getCode())) {
            MGTVUtil.getInstance().login(MaxApplication.getAppContext());
        } else if (BuildConfig.GET_CONTRIBUTION_INFO.equals(url)) {//本人是否守护
            isNeedRequestContr = false;
            adapter.updatePay();
            if(isGoToBuy){
                showGuardBuyDialog();
                isGoToBuy = false;
                dismiss();
            }
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {

        if(BuildConfig.URL_POPULARITY_RANK.equals(url)){//人气榜
            rootView.setVisibility(View.VISIBLE);

            datas = JSON.parseArray(resultModel.getData(), PersonValueData.class);

            if(datas!=null && datas.size()>0){
                adapter.setFansDatas(datas);
//                setListViewHeightBasedOnChildren(mFansListList);
                mFansListList.setVisibility(View.VISIBLE);

            }
        }else if (url.equals(BuildConfig.URL_GUARD_RANK)) {
            rootView.setVisibility(View.VISIBLE);
            guardDatas = JSON.parseArray(resultModel.getData(), ContributionData.class);
            adapter.setGuardDatas(guardDatas);
        }
        else if (BuildConfig.GET_CONTRIBUTION_INFO.equals(url)) {//本人是否守护
            isNeedRequestContr = false;
            contributionInfo = JSON.parseObject(resultModel.getData(), ContributionInfoData.class);
            adapter.updatePay();
            if(isGoToBuy){
                showGuardBuyDialog();
                isGoToBuy = false;
                dismiss();
            }
        }
        else if (BuildConfig.URL_CONTR_LIST.equals(url)) {//守护产品种类
            GiftUtil.getInstance().setContrs(JSON.parseArray(resultModel.getData(), ContrData.class));
            contrDatas = GiftUtil.getInstance().getContrs();
        }
        else if (BuildConfig.PAY_GIFT.equals(url)) {//守护
            Toast.makeText(mContext, "守护成功", Toast.LENGTH_SHORT).show();
            if(mGuardDialog != null){
                mGuardDialog.dismiss();
            }
        }

    }

    /**
     * 显示守护购买Dialog
     */
    GuardDialog mGuardDialog;
    private void showGuardBuyDialog(){

        int height = (int)(320*mContext.getResources().getDisplayMetrics().density);
        mGuardDialog = new GuardDialog(mContext,height,payType,contrDatas,contributionInfo,new PayContributionListener() {
            @Override
            public void payContribution(ContrData contrData) {
                payContr(contrData);
            }
        });
        mGuardDialog.show();
    }

    /**
     * 发起购买
     * @param contrData
     */
    public void payContr(final ContrData contrData) {
        String title = "成为" + " "+uName+" " + "的" + contrData.getName() + "支付" + contrData.getPrice() + "金币";
        final ConfirmDialog confirmDialog = new ConfirmDialog(getContext(), title, "确认", "取消");
        confirmDialog.show();
        confirmDialog.setClicklistener(new ConfirmDialog.ClickListenerInterface() {
            @Override
            public void doConfirm() {
                FormEncodingBuilderEx body = new FormEncodingBuilderEx();
                body.add("gid", contrData.getGid());
                body.add("count", "1");
                body.add("gift", "3");
                body.add("amount", contrData.getPrice() + "");
                body.add("buid", MaxApplication.getInstance().getUid());
                body.add("cuid", uid);
                body.add("token", MaxApplication.getInstance().getToken());
                body.add("clientId", MqttChatUtils.getInstance().getClientId());
                body.add("flag", MqttChatUtils.getInstance().getFlag());
                body.add("key", MqttChatUtils.getInstance().getKey());
                body.add("tip", "");
                post(BuildConfig.PAY_GIFT, body.build());
                confirmDialog.dismiss();
            }

            @Override
            public void doCancel() {
                confirmDialog.cancel();
            }
        });

    }


//    private void setListViewHeightBasedOnChildren(ListView listView) {
//        ListAdapter listAdapter = listView.getAdapter();
//        if (listAdapter == null) {
//            return;
//        }
//
//        int maxItemHeight;
//        Configuration newConfig = mContext.getResources().getConfiguration();
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            //横屏
//            maxItemHeight = 2;
//        } else {
//            //竖屏
//            maxItemHeight = 3;
//        }
//        //一页最多显示8个，控制listView高度
//        int count;
//        if (listAdapter.getCount() > maxItemHeight) {
//            count = maxItemHeight;
//        } else {
//            count = listAdapter.getCount();
//        }
//
//        int totalHeight = 0;
//        for (int i = 0; i < count; i++) {
//            View listItem = listAdapter.getView(i, null, listView);
//            listItem.measure(0, 0);
//            totalHeight += listItem.getMeasuredHeight();
//        }
//
//        ViewGroup.LayoutParams params = listView.getLayoutParams();
//
//        params.height = totalHeight
//                + (listView.getDividerHeight() * (count - 1));
//        listView.setLayoutParams(params);
//    }

    /**
     * 跳转到登陆页面
     */
    public void jumpToLogin(String title) {
        MGTVUtil.getInstance().login(MaxApplication.getAppContext());
    }

    public class PersonValueAdapter extends BaseAdapter implements View.OnClickListener{

        private static final int ITEM_TYPE_DILI = 1;
        private static final int ITEM_TYPE_FANS_TITLE = 2;
        private static final int ITEM_TYPE_GUARD_TITLE = 3;
        private static final int ITEM_TYPE_NO_GUARD = 4;
        private static final int ITEM_TYPE_GUARD_LINE = 5;
        private static final int ITEM_TYPE_FANS_LINE = 6;
        private static final int ITEM_TYPE_MORE_GUARD = 7;

        private final int TAG_GUARD_LINE = R.layout.item_guard_line;
        private final int TAG_FANS_LINE = R.layout.layout_live_person_value_item;

        private Context mContext;

        private List<PersonValueData> datas;
        private List<ContributionData> guardDatas;
        private List<ContributionData> showGuardDatas;

        private TextView mGuardSimpleDesc;
        private TextView mGuardSimpleTitle;
        private TextView mGuardBu;
        private ImageView mGuardMoreIm;
        public PersonValueAdapter(Context context) {
            this.mContext = context;
        }

        public void setFansDatas(List<PersonValueData> datas) {
            this.datas = datas;
            notifyDataSetChanged();
        }

        public void setGuardDatas(List<ContributionData> datas) {
            this.guardDatas = datas;
            initShowGuardData();
            notifyDataSetChanged();
        }

        @Override
        public void onClick(View v) {
            if(v == mGuardSimpleDesc || v == mGuardBu){
                if(MaxApplication.getInstance().isLogin()){

                    if(contributionInfo != null || !isNeedRequestContr){
                        showGuardBuyDialog();
                        dismiss();
                    }else{
                        isGoToBuy = true;
                        loadContrInfo();
                    }
                }else{
                    jumpToLogin(mContext.getString(R.string.star_guard_login));
                }
            }else if(R.id.guard_load_lay == v.getId()){

               if(showGuardDatas.size() >= guardDatas.size()){
                   initShowGuardData();
                   notifyDataSetChanged();
                   mFansListList.setSelection(0);
               }else{
                   addShowGuardData();
                   notifyDataSetChanged();
               }

            }
        }

        /**
         * 更新购买信息
         */

        public void updatePay(){
            if(contributionInfo != null && !ContributionInfoData.GRADE_LEVEL_NO.equals(contributionInfo.getGradeName())){
                //续约
                payType = PAY_TYPE_RENEW;
                if(mGuardSimpleDesc!=null){
                    mGuardSimpleDesc.setText(R.string.star_guard_goon);
                }


            }else{
                //购买
                payType = PAY_TYPE_BUY;
                if(mGuardSimpleDesc!=null) {
                    mGuardSimpleDesc.setText(R.string.star_guard_buy);
                }
            }

        }

        /**
         * 初始化显示的守护数据
         */
        private void initShowGuardData(){
            if(guardDatas == null){
                showGuardDatas = null;
                return;
            }

            int len = guardDatas.size();
            if(len<6){
                showGuardDatas = guardDatas.subList(0,len);
            }else{
                showGuardDatas = guardDatas.subList(0,6);
            }
        }

        /**
         * 增加显示的守护数据
         */
        private void addShowGuardData(){
            if(showGuardDatas == null){
                return;
            }

            int len = guardDatas.size();
            if(len < 6+showGuardDatas.size()){
                showGuardDatas = guardDatas.subList(0,len);
            }else{
                showGuardDatas = guardDatas.subList(0,6+showGuardDatas.size());
            }
        }

        /**
         * 减少显示的守护数据
         */
        private void cutShowGuardData(){
            if(showGuardDatas == null){
                return;
            }

            int len = showGuardDatas.size();
            int cut = len%6;
            if(cut == 0){
                cut = 6;
            }

            if(len - cut < 6){
                cut = len -6;
            }

            showGuardDatas = showGuardDatas.subList(0,len-cut);
        }

        @Override
        public int getCount() {
            int count = 0;
            if(getGuardLine() >= 2 && guardDatas.size()>6 ){
                count = count + 5 + getGuardLine() + getFansLine();
            }else if(getGuardLine() > 0){
                count = count + 4 + getGuardLine() + getFansLine();
            }else{
                count = count + 5 +getFansLine();
            }
            return count;
        }

        private int getGuardLine(){
            int line = 0;
            if(showGuardDatas != null && showGuardDatas.size() > 0){
                int last = showGuardDatas.size()%3;
                if(last != 0){
                    line = showGuardDatas.size()/3 +1;
                }else{
                    line = showGuardDatas.size()/3;
                }
            }

            return line;
        }

        private int getFansLine(){
            int line = 0;
            if(datas != null && datas.size() > 0){
              line = datas.size();
            }
            return line;
        }

        @Override
        public int getItemViewType(int position) {
            int type = ITEM_TYPE_DILI;
            if(position == 0){
                type = ITEM_TYPE_DILI;
            }else if(position == 1){
                type = ITEM_TYPE_GUARD_TITLE;
            }else if(position == 2 && getGuardLine() == 0){
                type = ITEM_TYPE_NO_GUARD;
            }else if(position >= 2 && position < 2+ getGuardLine() && getGuardLine() > 0){
                type = ITEM_TYPE_GUARD_LINE;
            }else if(position == 3 && getGuardLine() == 0){
                type = ITEM_TYPE_DILI;
            }else if(position  == 2+ getGuardLine() && getGuardLine() > 0 && guardDatas.size() <= 6){
                type = ITEM_TYPE_DILI;
            }else if(position  == 2+ getGuardLine() && getGuardLine() > 0 && guardDatas.size() > 6){
                type = ITEM_TYPE_MORE_GUARD;
            }else if(position == 4 && getGuardLine() == 0){
                type = ITEM_TYPE_FANS_TITLE;
            }else if(position  == 3+ getGuardLine() && getGuardLine() > 0 && guardDatas.size() <= 6){
                type = ITEM_TYPE_FANS_TITLE;
            }else if(position  == 3+ getGuardLine() && getGuardLine() > 0 && guardDatas.size() > 6){
                type = ITEM_TYPE_DILI;
            }else if(position >= 5 && position < 5+getFansLine() && getGuardLine() == 0){
                type = ITEM_TYPE_FANS_LINE;
            }else if(position  >= 4+ getGuardLine() && position < 4+getGuardLine()+getFansLine() && getGuardLine() > 0 && guardDatas.size() <= 6){
                type = ITEM_TYPE_FANS_LINE;
            }else if(position  == 4+ getGuardLine()  && getGuardLine() > 0 && guardDatas.size() > 6){
                type = ITEM_TYPE_FANS_TITLE;
            }else if(position  >= 5+ getGuardLine() && position < 5+getGuardLine()+getFansLine() && getGuardLine() > 0 && guardDatas.size() > 6){
                type = ITEM_TYPE_FANS_LINE;
            }
            return type;
        }

        private boolean isViewTypeRight(View convertView, int type){
            int viewType = (int)convertView.getTag();
            if(viewType == type){
                return true;
            }

            return false;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            int type = getItemViewType(position);
            //实例化item
            if(convertView == null || !isViewTypeRight(convertView,type)){
                if(type == ITEM_TYPE_DILI){//分割线
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.item_diliver_view,null);

                }else if(type == ITEM_TYPE_GUARD_TITLE){
                        convertView = LayoutInflater.from(mContext).inflate(R.layout.item_guard_title_view,null);
                        mGuardSimpleDesc = (TextView) convertView.findViewById(R.id.guard_simple_desc);
                          mGuardSimpleTitle= (TextView) convertView.findViewById(R.id.guard_simple_title);
                        mGuardSimpleDesc.setOnClickListener(this);
                }else if(type == ITEM_TYPE_GUARD_LINE){
                    GuardLineHolder viewHolder = new GuardLineHolder();
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.item_guard_line,null);
                    viewHolder.child1 = convertView.findViewById(R.id.guard_item1);
                    viewHolder.child2 = convertView.findViewById(R.id.guard_item2);
                    viewHolder.child3 = convertView.findViewById(R.id.guard_item3);

                    viewHolder.childHodler1 = new GuardViewHolder(viewHolder.child1);
                    viewHolder.childHodler2 = new GuardViewHolder(viewHolder.child2);
                    viewHolder.childHodler3 = new GuardViewHolder(viewHolder.child3);
                    convertView.setTag(TAG_GUARD_LINE,viewHolder);
                }else if(type == ITEM_TYPE_NO_GUARD){
                        convertView = LayoutInflater.from(mContext).inflate(R.layout.item_guard_nodata_view,null);
                        mGuardBu = (TextView) convertView.findViewById(R.id.guard_bu);
                        mGuardBu.setOnClickListener(this);

                }else if(type == ITEM_TYPE_FANS_TITLE){
                        convertView = LayoutInflater.from(mContext).inflate(R.layout.item_fans_title_view,null);

                }else if(type == ITEM_TYPE_FANS_LINE){
                        convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_live_person_value_item, null);
                        ViewHolder viewHolder = new ViewHolder();
                        viewHolder.rankText = (TextView) convertView.findViewById(R.id.rankText);
                        viewHolder.nameText = (TextView) convertView.findViewById(R.id.nameText);
                        viewHolder.gxNumText = (TextView) convertView.findViewById(R.id.gxNumText);
                        viewHolder.gradeImg = (ImageView) convertView.findViewById(R.id.gradeImg);
                        viewHolder.rankIcon = (ImageView) convertView.findViewById(R.id.rankIcon);
                        viewHolder.personIcon = (ImageView) convertView.findViewById(R.id.personIcon);
                        convertView.setTag(TAG_FANS_LINE,viewHolder);

                }else if(type == ITEM_TYPE_MORE_GUARD){

                    convertView = LayoutInflater.from(mContext).inflate(R.layout.item_guard_load_more, null);
                    convertView.setOnClickListener(this);
                }

                convertView.setTag(type);
            }

            //填充数据
            if(type == ITEM_TYPE_MORE_GUARD){
                mGuardMoreIm = (ImageView)convertView.findViewById(R.id.guard_load_im);
                if(showGuardDatas.size() >= guardDatas.size()){
                    mGuardMoreIm.setImageResource(R.drawable.up_more);
                }else{
                    mGuardMoreIm.setImageResource(R.drawable.down_more);
                }
            }else if(type == ITEM_TYPE_GUARD_LINE){
                GuardLineHolder viewHolder = (GuardLineHolder)convertView.getTag(TAG_GUARD_LINE);
                int dataIndex = (position-2)*3;
                //数据不满个特殊控制
                if(position == 2 && showGuardDatas.size() == 1){
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)viewHolder.child1.getLayoutParams();
                    layoutParams.weight = 3;
                    viewHolder.child1.setLayoutParams(layoutParams);
                }else if(position == 2 && showGuardDatas.size() == 2){
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)viewHolder.child1.getLayoutParams();
                    layoutParams.weight = 1.5f;
                    viewHolder.child1.setLayoutParams(layoutParams);
                    LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams)viewHolder.child1.getLayoutParams();
                    layoutParams2.weight = 1.5f;
                    viewHolder.child2.setLayoutParams(layoutParams2);
                }else{
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)viewHolder.child1.getLayoutParams();
                    layoutParams.weight = 1f;
                    viewHolder.child1.setLayoutParams(layoutParams);
                    LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams)viewHolder.child1.getLayoutParams();
                    layoutParams2.weight = 1f;
                    viewHolder.child2.setLayoutParams(layoutParams2);
                }
                //填充数据
                for (int i = 0;i < 3 ;i++){
                    GuardViewHolder guardViewHolder = null;
                    View childView = null;
                    if(i == 0){
                        guardViewHolder = viewHolder.childHodler1;
                        childView = viewHolder.child1;
                    }else if(i == 1){
                        guardViewHolder = viewHolder.childHodler2;
                        childView = viewHolder.child2;
                    }else if(i == 2){
                        guardViewHolder = viewHolder.childHodler3;
                        childView = viewHolder.child3;
                    }

                    if(showGuardDatas.size() > dataIndex + i){
                        ContributionData data = showGuardDatas.get(dataIndex + i);
                        if(data != null){
                            childView.setVisibility(View.VISIBLE);
                            if (data.getGrade() >= 3) {
                                Glide.with(mContext).load(R.drawable.grade_hj).into(guardViewHolder.gradeImg);
                                guardViewHolder.gradeImg.setVisibility(View.VISIBLE);
                            } else if (data.getGrade() >= 2) {
                                Glide.with(mContext).load(R.drawable.grade_by).into(guardViewHolder.gradeImg);
                                guardViewHolder.gradeImg.setVisibility(View.VISIBLE);
                            } else if (data.getGrade() >= 1) {
                                Glide.with(mContext).load(R.drawable.grade_qt).into(guardViewHolder.gradeImg);
                                guardViewHolder.gradeImg.setVisibility(View.VISIBLE);
                            } else {
                                guardViewHolder.gradeImg.setVisibility(View.GONE);
                            }

                            Glide.with(mContext).load(StringUtil.isNullorEmpty(data.getPhoto()) ? R.drawable.default_icon : data.getPhoto())
                                    .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                                    .transform(new GlideRoundTransform(mContext, R.dimen.height_40dp))
                                    .into(guardViewHolder.personIcon);
                            guardViewHolder.nameText.setText(data.getNickName());
                            guardViewHolder.gxNumText.setText(data.getDevoteValue() + "贡献");
                        }else{
                            childView.setVisibility(View.GONE);
                        }
                    }



                }

            }else if(type == ITEM_TYPE_FANS_LINE){//粉丝列表

                ViewHolder viewHolder = (ViewHolder) convertView.getTag(TAG_FANS_LINE);

                int dataIndex = 0;
                if(getGuardLine() == 0){
                    dataIndex = position - 5;
                }else if(getGuardLine() > 0 && guardDatas.size() <= 6){
                    dataIndex = position - 4 - getGuardLine();
                }else if(getGuardLine() > 0 && guardDatas.size() > 6){
                    dataIndex = position - 5 - getGuardLine();
                }

                PersonValueData data = datas.get(dataIndex);
                if (dataIndex < 3) {
                    viewHolder.rankIcon.setVisibility(View.VISIBLE);
                    if (dataIndex == 0) {
                        viewHolder.rankIcon.setBackgroundResource(R.drawable.rank_1);
                    } else if (dataIndex == 1) {
                        viewHolder.rankIcon.setBackgroundResource(R.drawable.rank_2);
                    } else {
                        viewHolder.rankIcon.setBackgroundResource(R.drawable.rank_3);
                    }

                    viewHolder.rankText.setVisibility(View.GONE);
                } else {
                    viewHolder.rankIcon.setVisibility(View.GONE);
                    viewHolder.rankText.setVisibility(View.VISIBLE);
                    viewHolder.rankText.setText(dataIndex + 1 + "");
                }

                if (data.getGrade() >= 3) {
                    Glide.with(mContext).load(R.drawable.grade_hj).into(viewHolder.gradeImg);
                    viewHolder.gradeImg.setVisibility(View.VISIBLE);
                } else if (data.getGrade() >= 2) {
                    Glide.with(mContext).load(R.drawable.grade_by).into(viewHolder.gradeImg);
                    viewHolder.gradeImg.setVisibility(View.VISIBLE);
                } else if (data.getGrade() >= 1) {
                    Glide.with(mContext).load(R.drawable.grade_qt).into(viewHolder.gradeImg);
                    viewHolder.gradeImg.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.gradeImg.setVisibility(View.GONE);
                }

                Glide.with(mContext).load(StringUtil.isNullorEmpty(data.getPhoto()) ? R.drawable.default_icon : data.getPhoto())
                        .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                        .transform(new GlideRoundTransform(mContext, R.dimen.height_40dp))
                        .into(viewHolder.personIcon);
                viewHolder.nameText.setText(data.getNickName());
                viewHolder.gxNumText.setText(data.getDevoteValue() + "");
            }else if(type == ITEM_TYPE_GUARD_TITLE){
                if(payType == PAY_TYPE_RENEW){
                    mGuardSimpleDesc.setText(R.string.star_guard_goon);
                }else{
                    mGuardSimpleDesc.setText(R.string.star_guard_buy);
                }
                mGuardSimpleTitle.setText(uName+"的守护");
            }

            return convertView;
        }

        class ViewHolder {
            TextView rankText;
            ImageView personIcon;
            ImageView rankIcon;
            TextView nameText;
            TextView gxNumText;
            ImageView gradeImg;
        }


        class GuardViewHolder {
            ImageView personIcon;
            TextView nameText;
            TextView gxNumText;
            ImageView gradeImg;

            public GuardViewHolder(View itemView) {
                nameText = (TextView) itemView.findViewById(R.id.guard_nameText);
                gxNumText = (TextView) itemView.findViewById(R.id.guard_desc);
                gradeImg = (ImageView) itemView.findViewById(R.id.guard_gradeImg);
                personIcon = (ImageView) itemView.findViewById(R.id.guard_personIcon);
            }
        }

        class GuardLineHolder{
            View child1;
            View child2;
            View child3;

            GuardViewHolder childHodler1;
            GuardViewHolder childHodler2;
            GuardViewHolder childHodler3;
        }
    }

}