package com.hunantv.mglive.widget;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.StartActivityDelayUtil;
import com.hunantv.mglive.utils.StringUtil;

/**
 * 直播提示弹窗控件
 *
 * @author liujianjiu
 */
public class LiveRemindDialog extends BaseDialog {

    private Context mContext;
    private TextView mTitleText;
    private TextView mNameText;
    private TextView mBuText;
    private ImageView mHeadView;

    private String uid;
    private String nickName;
    private String imageUrl;

    public LiveRemindDialog(Context context, String uid, String nickName, String imageUrl) {
        super(context, R.style.dialog);
        this.mContext = context;
        this.uid = uid;
        this.nickName = nickName;
        this.imageUrl = imageUrl;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!(mContext instanceof Activity)) {
            //从非Activity的上下文中弹出，弹出系统级别的dialog
            getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }
        init();
    }

    @Override
    public void show() {
        if (StringUtil.isNullorEmpty(uid) || StringUtil.isNullorEmpty(nickName)) {
            dismiss();
        } else {
            super.show();
        }
    }

    public void init() {

        setContentView(R.layout.dialog_live_message);

        mTitleText = (TextView) findViewById(R.id.live_message_title);
        mNameText = (TextView) findViewById(R.id.live_message_name);
        mBuText = (TextView) findViewById(R.id.live_message_bu);
        mHeadView = (ImageView) findViewById(R.id.live_message_head);

        mNameText.setText(nickName);
        Glide.with(getContext()).load(imageUrl).placeholder(R.drawable.default_icon).error(R.drawable.default__img_11)
                .transform(new GlideRoundTransform(getContext(), R.dimen.height_70dp)).into(mHeadView);
        mBuText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!StringUtil.isNullorEmpty(uid)) {
                    StartActivityDelayUtil startActivityDelayUtil = StartActivityDelayUtil.instance(getContext());
                    startActivityDelayUtil.startAcitiy(uid);
                    dismiss();
                }
            }
        });

        Window dialogWindow = getWindow();
        dialogWindow.setWindowAnimations(R.style.dialog_window_anim); //设置窗口弹出动画

        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogWindow.setAttributes(lp);
    }

}