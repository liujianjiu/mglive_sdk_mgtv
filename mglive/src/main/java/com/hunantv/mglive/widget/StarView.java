package com.hunantv.mglive.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

import com.hunantv.mglive.R;
import com.hunantv.mglive.ui.adapter.StarViewAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 */
public class StarView extends ViewGroup implements View.OnClickListener {
    public static String STYLE_CENTER = "STYLE_CENTER";
    public static String STYLE_INSIDE = "STYLE_INSIDE";
    public static String STYLE_OUTSIDE = "STYLE_OUTSIDE";

    private int mCircleColor = Color.WHITE;
    private final int ITEM_ANIM_DELAYD = 120;
    private final int TRANSLATE_ANIM_DURATION = 1500;
    private final int ITEM_OFF_ANIM_DURATION = 500;

    private int mInsideTrackRadius;
    private int mOutsideTrackRadius;
    private int mRoundTrackRadius;
    private float mTextWidth;
    private float mTextHeight;
    private boolean mStartAnim = true;
    private Integer mIsStartedAnim = 0;
    private Paint mPaint;
    private View mCenterView;
    private List<View> mInsideViewList = new ArrayList<View>();
    private List<View> mOutSideViewList = new ArrayList<View>();

    private StarViewAdapter mAdapter;
    private onStarViewItemClickListener mListener;

    public StarView(Context context) {
        super(context);
        setWillNotDraw(false);
        init(null, 0);
    }

    public StarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        init(attrs, 0);
    }

    public StarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setWillNotDraw(false);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.StarView, defStyle, 0);
        mCircleColor = a.getColor(
                R.styleable.StarView_circleColor,
                mCircleColor);
        a.recycle();

        // Set up a default TextPaint object
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaint.setTextAlign(Paint.Align.LEFT);
        mPaint.setColor(mCircleColor);
        mPaint.setStrokeWidth(2);

        // Update TextPaint and text measurements from attributes
        invalidateTextPaintAndMeasurements();
    }

    private void invalidateTextPaintAndMeasurements() {
//        mTextPaint.setTextSize(mExampleDimension);
//        mTextPaint.setColor(mExampleColor);
//        mTextWidth = mTextPaint.measureText(mExampleString);
//
//        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
//        mTextHeight = fontMetrics.bottom;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int viewGroupWidth = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        int viewGroupHeight = MeasureSpec.getSize(heightMeasureSpec) - getPaddingBottom() - getPaddingTop();
        int viewCount = getChildCount();
        for (int i = 0; i < viewCount; i++) {
            View view = getChildAt(i);
            int widthSize = MeasureSpec.getSize(widthMeasureSpec);
            int heightSize = MeasureSpec.getSize(heightMeasureSpec);
            view.measure(MeasureSpec.makeMeasureSpec(widthSize, MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.AT_MOST));
        }
        setMeasuredDimension(viewGroupWidth, viewGroupHeight);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;
        int centerX = contentWidth / 2;
        int centerY = contentHeight / 2;

        int width = (int) getResources().getDimension(R.dimen.height_80dp);
        if (count > 0) {
            View view = getChildAt(0);
//            int height = view.getMeasuredHeight();
//            int width = view.getMeasuredWidth();

//            int height =  (int) getResources().getDimension(R.dimen.height_80dp);
            int height = view.getMeasuredHeight();
            view.layout(centerX - width / 2, centerY - height / 2, centerX + width / 2, centerY + view.getMeasuredHeight() - (height / 2));
            mRoundTrackRadius = (int) (width * 0.9);
        }
        mInsideTrackRadius = (int) (width * 1.3);
        mOutsideTrackRadius = (int) (width * 2.3);
        if (count > 1) {
            layoutInsideRing(centerX, centerY);
        }
        if (count > 6) {
            layoutOutsideRing(contentWidth, contentHeight);
        }
    }

    private void layoutInsideRing(int centerX, int centerY) {
        if (mInsideViewList.size() <= 0) {
            return;
        }
        int spaceAngle = 360 / mInsideViewList.size();
        for (int i = 0; i < mInsideViewList.size(); i++) {
            View viewChild = mInsideViewList.get(i);
            int height = viewChild.getMeasuredHeight();
            int width = viewChild.getMeasuredWidth();
            double angle = (i * spaceAngle + 90) % 360 * Math.PI / 180;
            int x = (int) (centerX + mInsideTrackRadius * Math.cos(angle));
            int y = (int) (centerY - mInsideTrackRadius * Math.sin(angle));
            viewChild.layout(x - width / 2, y - height / 2, x + width / 2, y + height / 2);
        }
    }


    private void layoutOutsideRing(int width, int height) {
        if (mOutSideViewList.size() <= 0) {
            return;
        }
        int centerX = width / 2;
        int centerY = height / 2;
        int viewCount = mOutSideViewList.size();
        int itemNumTop = viewCount / 2 + viewCount % 2;
        int itemNumBottom = viewCount / 2;
        int x1 = width, x3 = centerX + mOutsideTrackRadius;
        int y1, y3 = centerY;
        y1 = centerY - (int) Math.sqrt(mOutsideTrackRadius * mOutsideTrackRadius - (x1 - centerX) * (x1 - centerX));
        double topMinAngle = getAngle(centerX, centerY, x1, y1, x3, y3);
        double topMaxAngle = 180 - topMinAngle;
        double bootomMinAngle = 360 - topMaxAngle;
        double bootomMaxAngle = 360 - topMinAngle;

//        double topSpaceAngle = (topMaxAngle - topMinAngle) * 1.0f / (itemNumTop + 1f);
//        Log.d("topMinAngle=",topMinAngle+"");
//        Log.d("topSpaceAngle=",topSpaceAngle+"");
//        layoutView(0, itemNumTop, topMinAngle, topSpaceAngle);
//        double bootomSpaceAngle = (bootomMaxAngle - bootomMinAngle) * 1.0f / (itemNumBottom + 1);
////        layoutView(itemNumTop, itemNumBottom, bootomMinAngle, bootomSpaceAngle);
//        layoutView(itemNumTop, itemNumBottom);

        int inAngle = 360 / mInsideViewList.size();
        int outAngle = (180 - inAngle) / 2;
        int indexAngle = inAngle / 2;
        int size = mOutSideViewList.size() / 2;
        //上
        layoutView(0, 3, outAngle, indexAngle);
        outAngle = outAngle + 180;
        //下
        layoutView(3, 6, outAngle, indexAngle);
    }


    private void layoutView(int initIndex, int endIndex, int startAngle, int indexAngle) {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;
        int centerX = contentWidth / 2;
        int centerY = contentHeight / 2;

        int index = 0;
        for (int i = initIndex; i < endIndex && i<mOutSideViewList.size(); i++) {

            View view = mOutSideViewList.get(i);
            int height = view.getMeasuredHeight();
            int width = view.getMeasuredWidth();
            double angle = (startAngle + (indexAngle * index)) * Math.PI / 180;
            int x = (int) (centerX + mOutsideTrackRadius * Math.cos(angle));
            int y = (int) (centerY - mOutsideTrackRadius * Math.sin(angle));
            view.layout(x - width / 2, y - height / 2, x + width / 2, y + height / 2);
            index++;
        }
    }


    private void layoutView(int initIndex, int maxNum) {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;
        int centerX = contentWidth / 2;
        int centerY = contentHeight / 2;
//        for (int i = initIndex; i - initIndex < maxNum && i < mOutSideViewList.size(); i++) {
//            int spaceAngle = 360 / mOutSideViewList.size();
//            int inAngle = 360/mInsideViewList.size();
//            int startAngle = inAngle * 2;
//            int endAngle = startAngle + inAngle;
//
//
//            View view = mOutSideViewList.get(i);
////            int height = view.getMeasuredHeight();
//            int height =  (int) getResources().getDimension(R.dimen.height_48dp);
//            int width = view.getMeasuredWidth();
////            double angle = ((i-1) * spaceAngle + 90) % 360 * Math.PI / 180;
//            double angle = startAngle + inAngle *
//            int x = (int) (centerX + mOutsideTrackRadius * Math.cos(angle));
//            int y = (int) (centerY - mOutsideTrackRadius * Math.sin(angle));
////            view.layout(x - width / 2, y - height / 2, x + width / 2, y + height / 2);
//            view.layout(x - width / 2, y - height / 2, x + width / 2,  y + view.getMeasuredHeight() -(height / 2));
//        }

        int index = 0;
        for (int i = initIndex; i < mOutSideViewList.size(); i++) {
            int inAngle = 360 / mInsideViewList.size();
            int startAngle = inAngle * 2;
            int endAngle = startAngle + inAngle;

            View view = mOutSideViewList.get(i);
//            int height = view.getMeasuredHeight();
            int height = (int) getResources().getDimension(R.dimen.height_48dp);
            int width = view.getMeasuredWidth();
//            double angle = ((i-1) * spaceAngle + 90) % 360 * Math.PI / 180;
//            double angle = startAngle + ((inAngle/2) * index);
            double angle = (0) * Math.PI / 180;
            int x = (int) (centerX + mOutsideTrackRadius * Math.cos(angle));
            int y = (int) (centerY - mOutsideTrackRadius * Math.sin(angle));
//            view.layout(x - width / 2, y - height / 2, x + width / 2, y + height / 2);
            //l,t,r,b
            view.layout(x - width / 2, y - height / 2, x + width / 2, y + view.getMeasuredHeight() - (height / 2));
            index++;
        }
    }

    private void layoutView(int initIndex, int maxNum, double initialValue, double spaceAngle) {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;
        int centerX = contentWidth / 2;
        int centerY = contentHeight / 2;
        for (int i = initIndex; i - initIndex < maxNum && i < mOutSideViewList.size(); i++) {
            View view = mOutSideViewList.get(i);
            double angle = (initialValue + (i - initIndex + 1) * spaceAngle) * Math.PI / 180;
//            int height = view.getMeasuredHeight();
            int height = (int) getResources().getDimension(R.dimen.height_48dp);
            int width = view.getMeasuredWidth();
            int x = (int) (centerX + mOutsideTrackRadius * Math.cos(angle));
            int y = (int) (centerY - mOutsideTrackRadius * Math.sin(angle));
//            view.layout(x - width / 2, y - height / 2, x + width / 2, y + height / 2);
            view.layout(x - width / 2, y - height / 2, x + width / 2, y + view.getMeasuredHeight() - (height / 2));
        }
    }


    double pi180 = 180 / Math.PI;

    private double getAngle(int x1, int y1, int x2, int y2, int x3, int y3) {
        double _cos1 = getCos(x1, y1, x2, y2, x3, y3);//第一个点为顶点的角的角度的余弦值

        return Math.acos(_cos1) * pi180;
    }


    //获得三个点构成的三角形的 第一个点所在的角度的余弦值
    private double getCos(int x1, int y1, int x2, int y2, int x3, int y3) {
        double length1_2 = getLength(x1, y1, x2, y2);//获取第一个点与第2个点的距离
        double length1_3 = getLength(x1, y1, x3, y3);
        double length2_3 = getLength(x2, y2, x3, y3);

        double res = (Math.pow(length1_2, 2) + Math.pow(length1_3, 2) - Math.pow(length2_3, 2)) / (length1_2 * length1_3 * 2);//cosA=(pow(b,2)+pow(c,2)-pow(a,2))/2*b*c

        return res;
    }


    //获取坐标轴内两个点间的距离
    private double getLength(int x1, int y1, int x2, int y2) {
        double diff_x = Math.abs(x2 - x1);
        double diff_y = Math.abs(y2 - y1);
        double length_pow = Math.pow(diff_x, 2) + Math.pow(diff_y, 2);//两个点在 横纵坐标的差值与两点间的直线 构成直角三角形。length_pow等于该距离的平方
        return Math.sqrt(length_pow);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;
//        mPaint.setAlpha(50);
//        canvas.drawCircle(contentWidth / 2, contentHeight / 2, mRoundTrackRadius, mPaint);
        mPaint.setAlpha(30);
        canvas.drawCircle(contentWidth / 2, contentHeight / 2, mInsideTrackRadius, mPaint);
        mPaint.setAlpha(10);
        canvas.drawCircle(contentWidth / 2, contentHeight / 2, mOutsideTrackRadius, mPaint);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initAnim();
    }


    private synchronized void initAnim() {
        int viewCount = getChildCount();
        for (int i = 0; i < viewCount; i++) {
            View view = getChildAt(i);
            if (view != null && view.getVisibility() == View.VISIBLE) {
                view.setVisibility(View.INVISIBLE);
                view.setOnClickListener(this);
                Object tag = view.getTag();
                if (tag != null) {
                    if (tag.equals(STYLE_CENTER)) {
                        mCenterView = view;
                    } else if (tag.equals(STYLE_INSIDE)) {
                        mInsideViewList.add(view);
                    } else if (tag.equals(STYLE_OUTSIDE)) {
                        mOutSideViewList.add(view);
                    }
                }
            }
        }
        startAnim();
    }

    public void startAnim() {
        List<View> mViewList = new ArrayList<View>();
        if (mCenterView != null) {
            mViewList.add(mCenterView);
        }
        mViewList.addAll(mInsideViewList);
        mViewList.addAll(mOutSideViewList);
        for (int i = 0; i < mViewList.size(); i++) {
            final int index = i;
            final View view = mViewList.get(i);
            if (view == null) {
                continue;
            }
            synchronized (mIsStartedAnim) {
                mIsStartedAnim++;
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setDisPlayAnimation(index, view);
                }
            }, ITEM_ANIM_DELAYD * i);
        }
    }

    private void setDisPlayAnimation(final int delayed, final View view) {
        mStartAnim = true;
        ScaleAnimation animation = new ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setFillBefore(true);
        animation.setDuration(ITEM_OFF_ANIM_DURATION);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                view.setOnClickListener(StarView.this);
                if (delayed != 0) {
                    view.clearAnimation();
                    startTranslateAnimation(view);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            synchronized (mIsStartedAnim) {
                                mIsStartedAnim--;
                            }

                        }
                    }, delayed);
                } else {
                    synchronized (mIsStartedAnim) {
                        mIsStartedAnim--;
                    }
                }
            }

            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

    private void startTranslateAnimation(final View view) {
        final float toX, toY;
        Random random = new Random();
        int randomNum = Math.abs(random.nextInt()) % 8;
        switch (randomNum) {
            case 0:
                toX = 0f;
                toY = 0.025f;
                break;
            case 1:
                toX = 0;
                toY = -0.025f;
                break;
            case 2:
                toX = 0.025f;
                toY = 0;
                break;
            case 3:
                toX = -0.025f;
                toY = 0;
                break;
            case 4:
                toX = 0.025f;
                toY = 0.025f;
                break;
            case 5:
                toX = -0.025f;
                toY = 0.025f;
                break;
            case 6:
                toX = -0.025f;
                toY = -0.025f;
                break;
            case 7:
                toX = 0.025f;
                toY = 0.025f;
                break;
            default:
                toX = 0f;
                toY = 0f;
                break;
        }
        int duartion = random.nextInt(500)+3000;
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation animation1 = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, toX, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, toY);
        animation1.setDuration(duartion);
        animation1.setFillAfter(true);
        animationSet.addAnimation(animation1);
        TranslateAnimation animation2 = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -toX, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -toY);
        animation2.setDuration(duartion);
        animation2.setStartOffset(duartion);
        animation2.setFillAfter(true);
        animationSet.addAnimation(animation2);

        int scaleDuartion = duartion/3;
        int scaleAnimDuartion = scaleDuartion/2;
        for(int i=0;i<6;i++){
            final float scaleToX1, scaleToX2,scaleToY1,scaleToY2;
            switch (random.nextInt(2)){
                case  0:
                    scaleToX1 = 1.02f;
                    scaleToX2 = 0.9803921f;
                    scaleToY1 = 1.0f;
                    scaleToY2 = 1.0f;
                    break;
                case 1 :
                    scaleToX1 = 1.0f;
                    scaleToX2 = 1.0f;
                    scaleToY1 = 1.02f;
                    scaleToY2 = 0.9803921f;
                    break;
                default:
                    scaleToX1 = 1.0f;
                    scaleToX2 = 1.0f;
                    scaleToY1 = 1.0f;
                    scaleToY2 = 1.0f;
                    break;
            }
            ScaleAnimation scaleAnimX = new ScaleAnimation(1.0f, scaleToX1,1.0f,scaleToY1,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            scaleAnimX.setStartOffset(i*scaleDuartion);
            scaleAnimX.setDuration(scaleAnimDuartion);
            scaleAnimX.setFillAfter(true);
            animationSet.addAnimation(scaleAnimX);
            Animation scaleAnimY = new ScaleAnimation(1.0f,scaleToX2,1.0f, scaleToY2,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            scaleAnimY.setDuration(scaleAnimDuartion);
            scaleAnimY.setStartOffset((i*scaleDuartion) + scaleAnimDuartion);
            scaleAnimY.setFillAfter(true);
            animationSet.addAnimation(scaleAnimY);
        }

        animationSet.setFillAfter(true);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.clearAnimation();
                if (mStartAnim) {
                    startTranslateAnimation(view);
                }
//                animationSet.startNow();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animationSet);
    }


    private void setOffAnimation(final View view) {
        ScaleAnimation animation = new ScaleAnimation(1f, 0f, 1f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setFillBefore(true);
        animation.setDuration(ITEM_OFF_ANIM_DURATION);
        // TODO: 2015/12/13 只播放动画不做其他处理
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                synchronized (mIsStartedAnim) {
                    mIsStartedAnim--;
//                    if (mIsStartedAnim <= 0) {
//                        if (mAdapter != null) {
//                            mAdapter.reSetView();
//                        }
//                    }
                }
//                view.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

    public void startChangeItemAnimOut() {
        mStartAnim = false;
        List<View> mViewList = new ArrayList<View>();
        if (mCenterView != null) {
            mViewList.add(mCenterView);
        }
        mViewList.addAll(mInsideViewList);
        mViewList.addAll(mOutSideViewList);
        for (int i = 0; i < mViewList.size(); i++) {
            View view = mViewList.get(i);
            if (view == null) {
                continue;
            }
            synchronized (mIsStartedAnim) {
                mIsStartedAnim++;
            }
            setOffAnimation(view);
        }
    }


    public void startChangeItemAnimIn() {
        List<View> mViewList = new ArrayList<View>();
        if (mCenterView != null) {
            mViewList.add(mCenterView);
        } else {
            mInsideViewList.clear();
            mOutSideViewList.clear();
            int viewCount = getChildCount();
            for (int i = 0; i < viewCount; i++) {
                View view = getChildAt(i);
                Object tag = view.getTag();
                if (tag != null) {
                    if (tag.equals(STYLE_CENTER)) {
                        mCenterView = view;
                    } else if (tag.equals(STYLE_INSIDE)) {
                        mInsideViewList.add(view);
                    } else if (tag.equals(STYLE_OUTSIDE)) {
                        mOutSideViewList.add(view);
                    }
                }
            }
            mViewList.add(mCenterView);
        }
        mViewList.addAll(mInsideViewList);
        mViewList.addAll(mOutSideViewList);
        for (int i = 0; i < mViewList.size(); i++) {
            View view = mViewList.get(i);
            if (view == null) {
                continue;
            }
            view.setVisibility(View.INVISIBLE);
        }

        for (int i = 0; i < mViewList.size(); i++) {
            View view = mViewList.get(i);
            if (view == null) {
                continue;
            }
            synchronized (mIsStartedAnim) {
                mIsStartedAnim++;
            }
            view.clearAnimation();
            setDisPlayAnimation(i * ITEM_ANIM_DELAYD, view);
        }
    }

    public void stopAnimation() {
        mStartAnim = false;
    }


    @Override
    public void addView(View child) {
        Object tag = child.getTag();
        if (tag != null) {
            child.setVisibility(View.INVISIBLE);
            if (tag.equals(STYLE_CENTER)) {
                mCenterView = child;
            } else if (tag.equals(STYLE_INSIDE)) {
                mInsideViewList.add(child);
            } else if (tag.equals(STYLE_OUTSIDE)) {
                mOutSideViewList.add(child);
            }
        }
        super.addView(child);
    }

    @Override
    public void removeAllViews() {
        mCenterView = null;
        mInsideViewList.clear();
        mOutSideViewList.clear();
        super.removeAllViews();
    }

    @Override
    public void onClick(View v) {
        synchronized (mIsStartedAnim) {
            if (mIsStartedAnim > 0) {
                return;
            }
        }
        if (mListener != null) {
            Object tag = v.getTag();
            if (tag.equals(STYLE_CENTER)) {
                mListener.onItemClick(v, STYLE_CENTER, 0);
                return;
            } else if (tag.equals(STYLE_INSIDE)) {
                for (int i = 0; i < mInsideViewList.size(); i++) {
                    View viewT = mInsideViewList.get(i);
                    if (viewT == v) {
                        mListener.onItemClick(v, STYLE_INSIDE, i);
                        return;
                    }
                }
            } else if (tag.equals(STYLE_OUTSIDE)) {
                for (int i = 0; i < mOutSideViewList.size(); i++) {
                    View viewT = mOutSideViewList.get(i);
                    if (viewT == v) {
                        mListener.onItemClick(v, STYLE_OUTSIDE, i);
                        return;
                    }
                }
            }
        }
    }


    public void setAdapter(StarViewAdapter adapter) {
        this.mAdapter = adapter;
        if (mAdapter != null) {
            mAdapter.setStarView(this);
        }
    }

    public StarViewAdapter getAdapter() {
        return mAdapter;
    }


    public void setOnStarViewItemClickListener(onStarViewItemClickListener listener) {
        this.mListener = listener;
    }

    public interface onStarViewItemClickListener {

        void onItemClick(View v, String style, int index);

    }
}
