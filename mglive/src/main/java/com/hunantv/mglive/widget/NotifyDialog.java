package com.hunantv.mglive.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.utils.StringUtil;
import com.hunantv.mglive.widget.media.IjkVideoView;

/**
 * Created by qiudaaini@163.com on 15/12/28.
 */
public class NotifyDialog {
    private Context mContext;
    private View mView;
    private Dialog mDialog;
    private onButtonClickListener mListener;

    public NotifyDialog(Context context) {
        setView(context);
    }

    private void setView(Context context) {
        if(context == null){
            context = MaxApplication.getAppContext();
        }
        mDialog = new AlertDialog.Builder(context).create();
        mContext = context;
    }

    public void show() {
        show(null);
    }

    public void show(final String videoPath) {
        if (mDialog != null) {
            mDialog.show();
            mView = LayoutInflater.from(mContext).inflate(R.layout.notify_dailog_view, null);
            mView.findViewById(R.id.btn_notify_dialog_sure).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onSure(videoPath);
                    }
                    mDialog.dismiss();
                }
            });
            mView.findViewById(R.id.btn_notify_dialog_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    if (mListener != null) {
                        mListener.onCancel();
                    }
                }
            });
            mDialog.setContentView(mView);
        }
    }

    public void setOnButtonClickListener(onButtonClickListener l) {
        mListener = l;
    }

    public interface onButtonClickListener {

        void onSure(String videoPath);

        void onCancel();
    }
}
