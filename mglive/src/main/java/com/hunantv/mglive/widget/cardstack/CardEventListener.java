package com.hunantv.mglive.widget.cardstack;

/**
 * Created by guojun3 on 2016-2-3.
 */
public interface CardEventListener {
    //section
    // 0 | 1
    //--------
    // 2 | 3
    // swipe distance, most likely be used with height and width of a view ;

    public boolean swipeEnd(int section, float distance);

    public boolean swipeStart(int section, float distance);

    public boolean swipeContinue(int section, float distanceX, float distanceY);

    public void discarded(int mIndex, int direction);

    public void topCardTapped();
}
