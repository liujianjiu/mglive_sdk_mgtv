package com.hunantv.mglive.widget.imagecrop;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.hunantv.mglive.utils.DeviceInfoUtil;
import com.hunantv.mglive.utils.ImageUtils;
import com.hunantv.mglive.R;

public class ImageCropLayout extends FrameLayout {

    private static final int DEFAULT_CROP_IMAGE_SIZE = 300;


    private ScaleableImageView mImageView;
    private View mCropFrameView;
    private Bitmap mBitmap;


    public ImageCropLayout(Context context) {
        super(context);
        initLayout(context);
    }

    public ImageCropLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public ImageCropLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context);
    }

    private void initLayout(Context context) {
        View.inflate(context, R.layout.layout_image_crop, this);

        if (isInEditMode()) {
            return;
        }

        mCropFrameView = findViewById(R.id.crop_frame_view);
        mImageView = (ScaleableImageView) findViewById(R.id.crop_image_view);
        mImageView.setAlwaysCenter(false);
    }

    private Point getDecodeSize() {
        Point point = new Point();
        WindowManager manager = (WindowManager)
                getContext().getSystemService(Context.WINDOW_SERVICE);
        manager.getDefaultDisplay().getSize(point);

        return point;
    }

    public void setImagePath(String filePath) {
        Point decodeSize = getDecodeSize();
        mBitmap = ImageUtils.decodeSampledBitmapFromFile(filePath, decodeSize.x, decodeSize.y);
        mImageView.setImageBitmap(mBitmap);
    }

    public void setImagePath(Uri uri) {
        Point decodeSize = getDecodeSize();
        mBitmap = ImageUtils.decodeSampledBitmapFromUri(uri, decodeSize.x, decodeSize.y);
        mImageView.setImageBitmap(mBitmap);
    }

    public Bitmap getCropBitmap() {
        return mBitmap;
    }

    public boolean cropImage(String croppedFilePath) {
        int imgWidth = mImageView.getMeasuredWidth();
        int imgHeight = mImageView.getMeasuredHeight();

        int targetWidth = mCropFrameView.getMeasuredWidth();
        int targetHeight = mCropFrameView.getMeasuredHeight();

        Bitmap bitmap = Bitmap.createBitmap(
                imgWidth, imgHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.BLACK);
        mImageView.draw(canvas);

        bitmap = Bitmap.createBitmap(
                bitmap,
                (imgWidth - targetWidth) / 2,
                (imgHeight - targetHeight) / 2,
                targetWidth,
                targetHeight);
        int width = (int) (DeviceInfoUtil.getResolutionWidth(getContext()) * 0.8);
        bitmap = ImageUtils.resizeBitmap(bitmap, width, width);
        return ImageUtils.saveBitmapToFile(bitmap, croppedFilePath);
    }
}
