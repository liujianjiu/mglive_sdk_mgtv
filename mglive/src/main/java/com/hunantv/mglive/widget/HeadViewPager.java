package com.hunantv.mglive.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 全能Viewpager，自动滚动，循环滑动、分页指示器 更新数据必须调用getMyPagerAdapter,方法获取到的PagerAdapter
 */
public class HeadViewPager extends ViewPager implements Runnable {

    private static final int POST_DELAYED_TIME = 3000 * 2;
    // 手指是否放在上面  
    private boolean touching;
//    private TemplateBannerAdapter mHotImageAdapter;
//    private HeadViewPagerAdapter mHeadViewPagerAdapter;
//    private OnPageChangeListener mOnPageChangeListener;
//    private OnPageChangeListener mInternalPageChangeListener;
//    private List<OnPageChangeListener> mOnPageChangeListeners;

    public HeadViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        postDelayed(this, POST_DELAYED_TIME);
    }

//    public void setAdapter(TemplateBannerAdapter arg0) {
//        super.setAdapter(arg0);
//        mHotImageAdapter = arg0;
//        if (arg0 != null && arg0.getCount() != 0 && arg0.getSize() != 0) {
//            int index = Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % arg0.getSize();
//            setCurrentItem(index, false);
//        }
//        mHeadViewPagerAdapter = new HeadViewPagerAdapter();
//    }
//
//    @Override
//    public PagerAdapter getAdapter() {
//        if (mHeadViewPagerAdapter == null) {
//            mHeadViewPagerAdapter = new HeadViewPagerAdapter();
//        }
//        return mHeadViewPagerAdapter;
//    }
//
//    private void dispatchOnPageScrolled(int position, float offset, int offsetPixels) {
//        if (mHotImageAdapter != null && mHotImageAdapter.getSize() != 0) {
//            position = position % mHotImageAdapter.getSize();
//        }
//        if (mOnPageChangeListener != null) {
//            mOnPageChangeListener.onPageScrolled(position, offset, offsetPixels);
//        }
//        if (mOnPageChangeListeners != null) {
//            for (int i = 0, z = mOnPageChangeListeners.size(); i < z; i++) {
//                OnPageChangeListener listener = mOnPageChangeListeners.get(i);
//                if (listener != null) {
//                    listener.onPageScrolled(position, offset, offsetPixels);
//                }
//            }
//        }
//        if (mInternalPageChangeListener != null) {
//            mInternalPageChangeListener.onPageScrolled(position, offset, offsetPixels);
//        }
//    }
//
//    private void dispatchOnPageSelected(int position) {
//        if (mHotImageAdapter != null && mHotImageAdapter.getSize() != 0) {
//            position = position % mHotImageAdapter.getSize();
//        }
//        if (mOnPageChangeListener != null) {
//            mOnPageChangeListener.onPageSelected(position);
//        }
//        if (mOnPageChangeListeners != null) {
//            for (int i = 0, z = mOnPageChangeListeners.size(); i < z; i++) {
//                OnPageChangeListener listener = mOnPageChangeListeners.get(i);
//                if (listener != null) {
//                    listener.onPageSelected(position);
//                }
//            }
//        }
//        if (mInternalPageChangeListener != null) {
//            mInternalPageChangeListener.onPageSelected(position);
//        }
//    }
//
//    private void dispatchOnScrollStateChanged(int state) {
//        if (mHotImageAdapter != null && mHotImageAdapter.getSize() != 0) {
//            state = state % mHotImageAdapter.getSize();
//        }
//        if (mOnPageChangeListener != null) {
//            mOnPageChangeListener.onPageScrollStateChanged(state);
//        }
//        if (mOnPageChangeListeners != null) {
//            for (int i = 0, z = mOnPageChangeListeners.size(); i < z; i++) {
//                OnPageChangeListener listener = mOnPageChangeListeners.get(i);
//                if (listener != null) {
//                    listener.onPageScrollStateChanged(state);
//                }
//            }
//        }
//        if (mInternalPageChangeListener != null) {
//            mInternalPageChangeListener.onPageScrollStateChanged(state);
//        }
//    }
//
//    public void setOnPageChangeListener(OnPageChangeListener listener) {
//        mOnPageChangeListener = listener;
//    }

    @Override
    // 自动滚动关键
    public void run() {
        if (getAdapter() != null && getAdapter().getCount() > 1 && !touching) {
            int index = (getCurrentItem() + 1) % getAdapter().getCount();
            setCurrentItem(index, true);
        }
        postDelayed(this, POST_DELAYED_TIME);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            this.touching = true;
        } else if (event.getAction() == MotionEvent.ACTION_UP
                || event.getAction() == MotionEvent.ACTION_CANCEL) {
            this.touching = false;
        }

        return super.onTouchEvent(event);
    }
}  