package com.hunantv.mglive.widget.Toast;


import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.hunantv.mglive.R;


public class FullAgreeMessageDialog extends AlertDialog {
    private Context context;
    private String message;
    private TextView tvMessage;
    private static final int SET_MESSAGE = 1;

    public FullAgreeMessageDialog(Context context) {
        super(context,R.style.dialog);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        init();
    }

    public void init() {
        setContentView(R.layout.full_agree_message_dialog);
        tvMessage = (TextView) findViewById(R.id.message);

        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 0.6); // 高度设置为屏幕的0.6
        dialogWindow.setAttributes(lp);
    }

    public void setMessage(String message){
//        if(tvMessage != null)
//        {
//            tvMessage.setText(message);
//        };
        setMessage(message, true);
    }

    public void setMessage(final String message,boolean isIng){
        if(tvMessage != null)
        {
            this.message = message;
            tvMessage.setText(message);
        }
    }

    public TextView getMessageText() {
        return tvMessage;
    }

    @Override
    public void show() {
        super.show();
    }
}
