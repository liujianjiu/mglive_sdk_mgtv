package com.hunantv.mglive.widget.Toast;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import com.hunantv.mglive.utils.Toast;



public class LoadingProgressBarWithCancelController {
    private ProgressDialog pd;
    private Activity activity;

    /**
     * 普通activity创建该控制器
     *
     * @param activity
     */
    public LoadingProgressBarWithCancelController(Activity activity) {
        this.activity = activity;
    }


    public int beginLoading(String strText) {
        if (activity != null && !activity.isFinishing()) {
            if (pd == null || !pd.isShowing()) {
                pd = new ProgressDialog(activity, strText, true);
                //pd.setCanceledOnTouchOutside(true);
                pd.setOnCancelListener(mOnCancelListener);
                pd.show();
            }
        }
        return -1;
    }

    private OnCancelListener mOnCancelListener = new OnCancelListener() {

        @Override
        public void onCancel(DialogInterface dialogInterface) {

        }
    };

    public void dismiss(){
        hideProgressDialog();
    }


    private void showToast(String text) {
        if (activity != null) {
            Toast toast = Toast.makeText(activity, text, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void hideProgressDialog() {
        if (activity != null && !activity.isFinishing()) {
            if (pd != null && pd.isShowing()) {
                pd.dismiss();
            }
        }
    }

}
