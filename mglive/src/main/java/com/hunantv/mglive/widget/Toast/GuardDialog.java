package com.hunantv.mglive.widget.Toast;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.ui.adapter.ContributionPageAdapter;
import com.hunantv.mglive.ui.entertainer.data.ContrData;
import com.hunantv.mglive.ui.entertainer.data.ContributionInfoData;
import com.hunantv.mglive.ui.entertainer.listener.PayContributionListener;
import com.hunantv.mglive.utils.L;

import java.util.List;

/**
 * Created by admin on 2016/3/1.
 */
public class GuardDialog extends AlertDialog implements ContributionPageAdapter.PayLinstener {
    private Window window = null;
    private LinearLayout mView;
    private ViewPager viewPager;
    private LinearLayout dotLayout;
    private ContributionPageAdapter pageAdapter;

    private int mHeight;
    private int mPayType;
    private List<ContrData> mContrDatas;
    private ContributionInfoData mContrInfo;


    private PayContributionListener mPayContributionListener;



    public GuardDialog(Context context,int mHeight,int mPayType,List<ContrData> mContrDatas,ContributionInfoData mContrInfo,PayContributionListener payContributionListener) {
        super(context);
        this.mHeight = mHeight;
        this.mPayType = mPayType;
        this.mContrDatas = mContrDatas;
        this.mContrInfo = mContrInfo;
        this.mPayContributionListener = payContributionListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_guard_dialog);
        setCanceledOnTouchOutside(true);
        initUI();

        window = getWindow(); //得到对话框
        window.setWindowAnimations(R.style.select_dialog_window_anim); //设置窗口弹出动画
        window.setBackgroundDrawableResource(R.color.common_bg); //设置对话框背景
        window.setDimAmount(0l);

        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = window.getAttributes();
        DisplayMetrics d = getContext().getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 1);
        lp.height = mHeight;
        window.setAttributes(lp);

        ViewGroup.LayoutParams layoutParams = mView.getLayoutParams();
        layoutParams.width = (int) (d.widthPixels * 0.72);
        mView.setLayoutParams(layoutParams);
    }

    public void initUI(){
        mView =  (LinearLayout) findViewById(R.id.ll_view);
        viewPager = (ViewPager) findViewById(R.id.payViewPage);
        dotLayout = (LinearLayout) findViewById(R.id.dotLayout);
        pageAdapter =new ContributionPageAdapter(getContext());
        pageAdapter.setPayLinstener(this);
        pageAdapter.setPayType(mPayType, mContrInfo);
        pageAdapter.setDotLayout(dotLayout);
        pageAdapter.setContrDatas(mContrDatas);
        viewPager.setAdapter(pageAdapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(pageAdapter);
    }

    @Override
    public void pay(ContrData contrData) {
        mPayContributionListener.payContribution(contrData);
    }

//    public void setPayContributionListener(PayContributionListener payContributionListener) {
//        this.payContributionListener = payContributionListener;
//    }

//    public void setContrDatas(List<ContrData> contrDatas){
//        L.d("设置守护产品",contrDatas.size()+"");
//        pageAdapter.setContrDatas(contrDatas);
//    }

//    public void setPayType(int payType,ContributionInfoData contrInfo) {
//        pageAdapter.setPayType(payType, contrInfo);
//        int size = viewPager.getChildCount();
//        for(int i=0;i<size;i++){
//            View view = viewPager.getChildAt(i);
//            Object objTag = view.getTag();
//            if(objTag != null && objTag instanceof ContributionPageAdapter.ViewHold)
//            {
//                pageAdapter.initView((ContributionPageAdapter.ViewHold)objTag);
//            }
//        }
//    }

//    public void show(int height) {
//        super.show();
//    }
}
