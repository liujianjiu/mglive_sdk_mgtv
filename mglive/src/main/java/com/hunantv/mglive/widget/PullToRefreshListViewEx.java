package com.hunantv.mglive.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.handmark.pulltorefresh.librarymgtv.PullToRefreshListView;

/**
 * Created by Administrator on 2015/12/19.
 */
public class PullToRefreshListViewEx extends PullToRefreshListView {
    public PullToRefreshListViewEx(Context context) {
        super(context);
    }

    public PullToRefreshListViewEx(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PullToRefreshListViewEx(Context context, Mode mode) {
        super(context, mode);
    }

    public PullToRefreshListViewEx(Context context, Mode mode, AnimationStyle style) {
        super(context, mode, style);
    }

//    @Override
//    protected LoadingLayout createLoadingLayout(Context context, Mode mode, TypedArray attrs) {
//        LoadingLayout loadingLayout = super.createLoadingLayout(context,mode,attrs);
//        if(loadingLayout instanceof RotateLoadingLayout)
//        {
//            loadingLayout = new TweenAnimLoadingLayout(context, mode, getPullToRefreshScrollDirection(), attrs);
//        }
//
//        return loadingLayout;
//    }

}
