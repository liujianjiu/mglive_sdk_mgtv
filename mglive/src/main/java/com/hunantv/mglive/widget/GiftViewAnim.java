package com.hunantv.mglive.widget;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.hunantv.mglive.R;

/**
 * Created by QiuDa on 15/12/18.
 */
public class GiftViewAnim {
    private static final String TAG = "GiftViewAnim";
    private Context mContext;
    public ViewGroup mViewGroup;
    private int mImageDuration = 350;
    private int mTextDuration = 450;
    private int mDuration = mImageDuration + mTextDuration;

    public GiftViewAnim(Context mContext, ViewGroup viewGroup) {
        this.mContext = mContext;
        this.mViewGroup = viewGroup;
    }

    public void startGiftViewAnim(String image, String hots, int fromx, int tox, int fromy, int toy) {
        final ImageView iv = new ImageView(mContext);
        iv.setVisibility(View.INVISIBLE);
        int width = mContext.getResources().getDimensionPixelOffset(R.dimen.height_60dp);
        ViewGroup.LayoutParams ps = new ViewGroup.LayoutParams(width, width);
        iv.setLayoutParams(ps);
        if (null != mViewGroup) {
            mViewGroup.addView(iv);
        }
        if (Util.isOnMainThread() && !((Activity) mContext).isFinishing()) {
            Glide.with(mContext).load(image).into(iv);
        }

//        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
//        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
//        iv.measure(w, h);
//        int viewWidth = iv.getMeasuredWidth();
//        int viewHeight = iv.getMeasuredHeight();
        TextView tv = null;
        if (Integer.parseInt(hots) > 0) {
            tv = new TextView(mContext);
            ViewGroup.LayoutParams pstv = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, toy * 2);
            tv.setLayoutParams(pstv);
            tv.setTextColor(mContext.getResources().getColor(R.color.common_yellow));
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.text_size19dp));
            tv.setText("+" + hots + "人气");
            tv.setGravity(Gravity.CENTER);
            tv.setVisibility(View.INVISIBLE);
            if (null != mViewGroup) {
                mViewGroup.addView(tv);
            }
        }

        fromx = fromx - width / 2;
        fromy = fromy - width / 2;
        tox = tox - width / 2;
        toy = toy - width / 2;
        Log.i(TAG, fromx + " " + fromy + "  " + tox + "  " + toy);
        startGiftViewAnim(iv, tv, fromx, tox, fromy, toy);
    }

    public void startGiftViewAnim(final ImageView imageView, final TextView tv, int fromx, int tox, int fromy, int toy) {
        final AnimationSet animationSet = new AnimationSet(true);
        Animation translateAnimation = new TranslateAnimation(Animation.ABSOLUTE, fromx, Animation.ABSOLUTE, tox, Animation.ABSOLUTE, fromy, Animation.ABSOLUTE, toy);
        translateAnimation.setDuration(400);
        translateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        animationSet.addAnimation(translateAnimation);
        Animation alphaAnimation = new AlphaAnimation(1, 0);
        alphaAnimation.setDuration(100);
        alphaAnimation.setStartOffset(300);
        animationSet.addAnimation(alphaAnimation);
        final AnimationSet animationSetTv = new AnimationSet(true);
        if (tv != null) {
            {
                ScaleAnimation scaleAnimation = new ScaleAnimation(0f, 3f, 0f, 3f,
                        Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                scaleAnimation.setDuration(400);
                animationSetTv.addAnimation(scaleAnimation);
                AlphaAnimation alphaAnimationTv = new AlphaAnimation(1, 0);
                alphaAnimationTv.setDuration(150);
                alphaAnimationTv.setStartOffset(350);
                animationSetTv.addAnimation(alphaAnimationTv);
                animationSetTv.setInterpolator(new AccelerateInterpolator());
                animationSetTv.setFillAfter(true);
                animationSetTv.setDuration(500);

                animationSetTv.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        tv.setVisibility(View.GONE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (null != mViewGroup) {
                                    mViewGroup.removeView(imageView);
                                    mViewGroup.removeView(tv);
                                }
                            }
                        }, 500);

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                imageView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.GONE);
//                mViewGroup.removeView(imageView);
                if (tv != null) {
                    tv.setVisibility(View.VISIBLE);
                    tv.clearAnimation();
                    tv.startAnimation(animationSetTv);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        //此处很贱,如果不延迟10ms,在部分手机上可能会导致动画显示异常,比如左上角闪一下
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                imageView.clearAnimation();
                imageView.startAnimation(animationSet);
            }
        }, 20);
    }

    public int getAnimDuration() {
        return mDuration;
    }


}
