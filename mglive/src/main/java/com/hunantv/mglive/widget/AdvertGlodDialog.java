package com.hunantv.mglive.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.GiftingInfoModel;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.StringUtil;

/**
 * Created by qiuda on 16/2/22.
 */
public class AdvertGlodDialog implements View.OnClickListener {
    private Context mContext;
    private Dialog mDialog;
    //    private TextView mTvSponsor;
//    private TextView mTvCoinInfon;
//    private TextView mTvSucceed;
    private ImageView mAdvert;
    private ImageView mAdvertLogo;
    private TextView mAdvertTips;
    private TextView mGoleNum;
    private ImageView mClose;

    private Handler mHandler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            close();
        }
    };

    public AdvertGlodDialog(Context context) {
        this.mContext = context;

    }

    public AdvertGlodDialog creat() {
        mDialog = new Dialog(mContext, R.style.gold_advert_window_style);
        View view = LayoutInflater.from(mContext).inflate(R.layout.advert_gold_window_layout, null);
        mAdvert = (ImageView) view.findViewById(R.id.iv_advert);
        mAdvertLogo = (ImageView) view.findViewById(R.id.iv_advert_logo);
        mAdvertTips = (TextView) view.findViewById(R.id.tv_advert_tips);
        mGoleNum = (TextView) view.findViewById(R.id.tv_gold_num);
        mClose = (ImageView) view.findViewById(R.id.iv_close);
        mClose.setOnClickListener(this);

        mDialog.setContentView(view);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
//        Window window = mDialog.getWindow(); //得到对话框
//        WindowManager.LayoutParams lp = window.getAttributes();
//        DisplayMetrics d = mContext.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
//        lp.width = (int) (d.widthPixels * 0.75);
//        window.setAttributes(lp);
        return this;
    }


    public void show(GiftingInfoModel giftInfo) {
        if (((Activity) mContext).isFinishing() || giftInfo == null) {
            return;
        }
        if (mDialog != null && !mDialog.isShowing()) {
            Glide.with(mContext).load(giftInfo.getAdImage()).into(mAdvert);
            Glide.with(mContext).load(giftInfo.getLogo())
                    .placeholder(R.drawable.default_icon_preload)
                    .transform(new GlideRoundTransform(mContext, R.dimen.height_42dp))
                    .into(mAdvertLogo);
            if (!StringUtil.isNullorEmpty(giftInfo.getAdSlogan())) {
                mAdvertTips.setText(giftInfo.getAdSlogan());
            }

            if (!StringUtil.isNullorEmpty(giftInfo.getCoinInfo())) {
                mGoleNum.setText("+" + giftInfo.getCoinInfo());
            }

            if (((Activity) mContext).isFinishing()) {
                return;
            }
            mDialog.show();
            mHandler.sendEmptyMessageDelayed(0, 5000);
        }
    }

    private void close() {
        try {
            if (mDialog != null && mDialog.isShowing() && mContext != null && !((Activity) mContext).isFinishing()) {
                mDialog.dismiss();
            }
        } catch (Exception ignored) {

        }
    }

    @Override
    public void onClick(View v) {
            if(v.getId() == R.id.iv_close){
                close();
            }
    }
}
