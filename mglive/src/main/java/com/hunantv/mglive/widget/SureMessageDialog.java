package com.hunantv.mglive.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.utils.StringUtil;

/**
 * 全局的消息弹窗控件
 * @author liujianjiu
 */
public class SureMessageDialog extends BaseDialog implements View.OnClickListener {

    private Context mContext;
    private TextView mMessageText;

    private String mTitle;
    private String mMessage;
    private String mDesc;
    private String mOk;
    private OKListener mOkListener;

    public SureMessageDialog(Context context, String title, String message, String desc, String okStr) {
        super(context,R.style.dialog);
        this.mContext = context;
        this.mTitle = title;
        this.mMessage = message;
        this.mDesc = desc;
        this.mOk = okStr;
    }


    public SureMessageDialog(Context context, String title, String message, String desc) {
        super(context,R.style.dialog);
        this.mContext = context;
        this.mTitle = title;
        this.mMessage = message;
        this.mDesc = desc;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(mContext instanceof Activity){

        }else{//从非Activity的上下文中弹出，弹出系统级别的dialog
            getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }

        init();

    }

    public void init() {
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        setContentView(R.layout.dialog_message);
        TextView titleText = (TextView) findViewById(R.id.dialog_tv_title);
        if(StringUtil.isNullorEmpty(mTitle)){
            titleText.setVisibility(View.GONE);
        }else {
            titleText.setText(mTitle);
            titleText.setVisibility(View.VISIBLE);
        }

        mMessageText = (TextView) findViewById(R.id.dialog_tv_message);
        if(StringUtil.isNullorEmpty(mMessage)){
            mMessageText.setVisibility(View.GONE);
        }else {
            mMessageText.setText(mMessage);
            mMessageText.setVisibility(View.VISIBLE);
        }

        TextView descText = (TextView) findViewById(R.id.dialog_tv_desc);
        if(StringUtil.isNullorEmpty(mDesc)){
            descText.setVisibility(View.GONE);
        }else {
            descText.setText(mDesc);
            descText.setVisibility(View.VISIBLE);
        }

        TextView okBtn = (TextView)findViewById(R.id.dialog_tv_ok);
        okBtn.setOnClickListener(this);

        if(!StringUtil.isNullorEmpty(mOk)) {
            okBtn.setText(mOk);
        }

        float withScale;
        Configuration newConfig = mContext.getResources().getConfiguration();
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            //横屏
            withScale = 0.5f;
        }else{
            //竖屏
            withScale = 0.75f;
        }
        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = mContext.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * withScale); // 宽度设置为屏幕宽度比例
        dialogWindow.setAttributes(lp);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            if(mMessageText.getLineCount()>1){
                mMessageText.setGravity(Gravity.LEFT);
            }else{
                mMessageText.setGravity(Gravity.CENTER);
            }
        }
    }

    /**
     * 延迟自动关闭
     */
    public void show(long delayMillis){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, delayMillis);
        show();
    }

    @Override
    public void onClick(View v) {
        if(mOkListener != null) {
            mOkListener.onClick();
        }else {
        }
        dismiss();

    }

    public void setOkListener(OKListener okListener) {
        this.mOkListener = okListener;
    }

    public interface OKListener{
        public void onClick();
    }
}