package com.hunantv.mglive.widget.loadingView;

import android.content.Context;
import android.view.View;

/**
 * 定义实现接口
 * 
 * @author 孤狼
 * @since 2015-8-4
 */
public interface ViewCallBack {
	// 获取当前布局
	public abstract View getCurrentLayout();

	// 重置布局
	public abstract void resetView();

	// 显示布局
	public abstract void showLayout(View view);

	public abstract void showLayout(int layoutId);

	// 填充布局
	public abstract View inflate(int layoutId);

	public abstract Context getContext();

	public abstract View getView();

}