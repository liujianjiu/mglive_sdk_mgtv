package com.hunantv.mglive.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.hunantv.mglive.R;

/**
 * Created by qiudaaini@163.com on 15/12/29.
 */
public class VideoErrorView implements View.OnClickListener {

    private Context mContext;
    private View mView;
    private PopupWindow mPopupwindow;
    private onRefreshClick mListenner;


    public VideoErrorView(Context context, int width, int height) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.video_error_view, null);
        mPopupwindow = new PopupWindow(mView,
                width, height, false);
        mView.findViewById(R.id.btn_video_error_refresh).setOnClickListener(this);
    }

    public void showAtLocation(View parent, int gravity, int x, int y) {
        mPopupwindow.showAtLocation(parent, gravity, x, y);
    }

    public void dismiss() {
        mPopupwindow.dismiss();
    }

    public void setonRefreshListener(onRefreshClick l) {
        this.mListenner = l;
    }

    @Override
    public void onClick(View v) {
        if (mListenner != null) {
            mListenner.onRefreshClick();
        }
    }

    public interface onRefreshClick {

        void onRefreshClick();

    }
}
