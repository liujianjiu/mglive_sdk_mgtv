package com.hunantv.mglive.ui.discovery;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class MaxDataManager {
    private static final String SHARED_TAG = "air_sharedpreferences_tag";
    public static final String OK = "0";

    public static void putInt(Context ctx, String key, int val) {
        Editor editor = getSharedPreferencesEditor(ctx);
        editor.putInt(key, val);
        editor.commit();
    }

    public static void putLong(Context ctx, String key, long val) {
        Editor editor = getSharedPreferencesEditor(ctx);
        editor.putLong(key, val);
        editor.commit();
    }

    public static Long getLong(Context ctx, String key) {
        SharedPreferences shared = getSharedPreferences(ctx);
        return shared.getLong(key, -1);
    }

    public static void putString(Context ctx, String key, String val) {
        Editor editor = getSharedPreferencesEditor(ctx);
        editor.putString(key, val);
        editor.commit();
    }

    public static String getString(Context ctx, String key) {
        SharedPreferences shared = getSharedPreferences(ctx);
        return shared.getString(key, "");
    }

    public static int getInt(Context ctx, String key) {
        SharedPreferences shared = getSharedPreferences(ctx);
        return shared.getInt(key, -1);
    }

    /**
     * @param ctx
     * @return
     */
    public static Editor getSharedPreferencesEditor(Context ctx) {
        return getSharedPreferences(ctx).edit();
    }

    /**
     * @param ctx
     * @return
     */
    public static SharedPreferences getSharedPreferences(Context ctx) {
        return ctx.getSharedPreferences(SHARED_TAG, Activity.MODE_PRIVATE);
    }
}
