package com.hunantv.mglive.ui.adapter;

import android.app.Service;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.data.discovery.DynamicImag;
import com.hunantv.mglive.widget.SquareImageView;

import java.util.List;

/**
 * 图片显示的Adapter
 */
public class PictureAdapter extends BaseAdapter {
    private List<DynamicImag> mImgList;
    private int mColumNum = 1;
    private int width = 0;
    private Context mContext;


    public int getColumNum() {
        return this.mColumNum;
    }

    public void setColumNum(int mColumNum) {
        this.mColumNum = mColumNum;
    }

    public List<DynamicImag> getImgList() {
        return mImgList;
    }

    public void setImgList(List<DynamicImag> mImgList) {
        this.mImgList = mImgList;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    private void initWidth(final GridView gridView) {
        gridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if(Build.VERSION.SDK_INT >15){
                    gridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }

                if(width == 0){
                    width = gridView.getMeasuredWidth();
                    notifyDataSetChanged();
                }
            }
        });

    }

    public PictureAdapter(Context ctx, List<DynamicImag> mImgLis,GridView gridView) {
        setContext(ctx);
        setImgList(mImgList);
        initWidth(gridView);
    }

    @Override
    public int getCount() {
        if (null != getImgList()) {
            return getImgList().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return getImgList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (null == convertView) {
            imageView = new ImageView(getContext());
            int padding = (int)getContext().getResources().getDisplayMetrics().density*1;
            imageView.setPadding(padding, padding, padding, padding);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setLayoutParams(new GridView.LayoutParams(width/mColumNum, width/mColumNum));
        if (null != getImgList().get(position) && null != getImgList().get(position).getSmall()) {
            Glide.with(getContext()).load(getImgList().get(position).getSmall()).centerCrop().into(imageView);
        }
        imageView.setTag(R.id.star_tag, position);
        return imageView;
    }
}