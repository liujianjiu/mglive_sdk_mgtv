package com.hunantv.mglive.ui.live;

import com.hunantv.mglive.data.live.ChatData;

/**
 * Created by Qiuda on 15/12/18.
 */
public interface ChatViewCallback {

    void startAnim(String image, String text, int fromx, int fromy);

    void hasMsg(ChatData chatData);

    void changeOnline(String changeStr);

    void changeGiftView(boolean isDisplay);

    void changeStarHotValue(long hotValue);

}
