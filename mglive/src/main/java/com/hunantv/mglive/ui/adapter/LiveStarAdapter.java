package com.hunantv.mglive.ui.adapter;


import android.content.Context;
import android.media.Image;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.utils.GlideRoundTransform;

import java.text.Format;
import java.util.List;

/**
 * Created by 达 on 2015/11/30.
 */
public class LiveStarAdapter extends BaseAdapter {
    private Context mContext;
    private List<StarModel> mDataList;
    private StarModel mCheckStar;
    private boolean mShowAoutChangeTag;
    private onStarItemClickListener mListener;

    public LiveStarAdapter(Context context, List<StarModel> list) {
        this.mContext = context;
        this.mDataList = list;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public StarModel getItem(int position) {
        if (position < 0) {
            return null;
        }
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final StarModel dataModel = getItem(position);
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.live_stars_item, null);
            holder.ivIcon = (ImageView) convertView.findViewById(R.id.iv_live_stars_item_icon);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_live_stars_item_name);
            holder.mLlBg = (LinearLayout) convertView.findViewById(R.id.ll_live_stars_icon_bg);
            holder.btnAttention = (LinearLayout) convertView.findViewById(R.id.btn_live_detail_stars_attention);
            holder.appointImg = (ImageView) convertView.findViewById(R.id.iv_live_detail_stars_appointment);
            holder.appointText = (TextView) convertView.findViewById(R.id.tv_live_detail_stars_appointment);
            holder.ivAoutTag = (ImageView) convertView.findViewById(R.id.iv_live_stars_auto_change_tag);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (dataModel != null) {
            holder.tvName.setText(dataModel.getNickName());
            Glide.with(mContext).load(dataModel.getPhoto())
                    .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                    .transform(new GlideRoundTransform(mContext, R.dimen.height_35dp)).into(holder.ivIcon);
            if (mCheckStar != null && mCheckStar.getUid().equals(dataModel.getUid())) {
                holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_main));
                holder.mLlBg.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.star_icon_bg_yellow));
            } else if (mCheckStar == null && position == 0) {
                holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_main));
                holder.mLlBg.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.star_icon_bg_yellow));
            } else {
                holder.tvName.setTextColor(mContext.getResources().getColor(R.color.alpha_white));
                holder.mLlBg.setBackgroundDrawable(null);
            }
            holder.btnAttention.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onClick(dataModel, holder.btnAttention);
                    }
                }
            });

            if ("true".equals(dataModel.getIsfans())) {
                holder.btnAttention.setSelected(true);
                holder.appointImg.setImageResource(R.drawable.star_attention_yes_icon);
                holder.appointText.setText(dataModel.getFans());
            } else
            {
                holder.btnAttention.setSelected(false);
                holder.appointImg.setImageResource(R.drawable.star_attention_icon);
                holder.appointText.setText(mContext.getString(R.string.attention));
            }
            if ((position == 0 && mCheckStar == null) || (mCheckStar != null && mCheckStar.getUid().equals(dataModel.getUid()))) {
                holder.btnAttention.setVisibility(View.VISIBLE);
            } else {
                holder.btnAttention.setVisibility(View.INVISIBLE);
            }

            if (mShowAoutChangeTag && position == 0) {
                holder.ivAoutTag.setVisibility(View.VISIBLE);
            } else {
                holder.ivAoutTag.setVisibility(View.GONE);
            }
        }
        return convertView;
    }

    public void changeCheckStar(StarModel starModel) {
        if (starModel == null) {
            mCheckStar = null;
        } else {
            mCheckStar = starModel;
        }
    }

    public StarModel getCheckedStar() {
        return mCheckStar;
    }

    public void showAoutChangeTag() {
        mShowAoutChangeTag = true;
    }

    public void setOnClickListener(onStarItemClickListener l) {
        this.mListener = l;
    }

    public interface onStarItemClickListener {
        void onClick(StarModel starModel, View view);
    }

    class ViewHolder {
        LinearLayout btnAttention;
        ImageView appointImg;
        TextView appointText;
        ImageView ivIcon;
        LinearLayout mLlBg;
        TextView tvName;
        ImageView ivAoutTag;
    }
}
