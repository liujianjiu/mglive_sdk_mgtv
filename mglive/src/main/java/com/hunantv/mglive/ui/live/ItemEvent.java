package com.hunantv.mglive.ui.live;

import android.widget.AdapterView;

/**
 * Created by guojun3 on 2015-12-2.
 */
public interface ItemEvent extends AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
}
