package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.RoleUtil;
import com.hunantv.mglive.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author QiuDa
 *         <p/>
 *         listView 推荐关注
 */
public class AttentionListViewAdapter extends BaseAdapter{
    public static final int RECOMMEND_ATTENTION = 1;

    public static final int USER_ATTENTION = 2;

    private int ATTENITION_TYPE = 0;
    private Context context;

    private List<StarModel> mAttentionList = new ArrayList<StarModel>();
    private AtteListenerCallBack atteClickCallBack;

    public AttentionListViewAdapter(Context context,int type) {
        this.context = context;
        this.ATTENITION_TYPE = type;
    }

    @Override
    public int getCount() {
        return mAttentionList != null ? mAttentionList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mAttentionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            ViewHold viewHold = new ViewHold();
            convertView = LayoutInflater.from(context).inflate(R.layout.attention_list_view_item, null);
            viewHold.mFaceIcon = (ImageView) convertView.findViewById(R.id.iv_attention_list_item_icon);
            viewHold.mTipsIcon = (ImageView) convertView.findViewById(R.id.iv_attention_list_item_icon_tips);
            viewHold.mStarName = (TextView) convertView.findViewById(R.id.tv_all_star_attention_name);
            viewHold.mPersonNumText = (TextView) convertView.findViewById(R.id.tv_all_star_attention_person_num);
            viewHold.mFansNumText = (TextView) convertView.findViewById(R.id.tv_all_star_attention_fans_num);
            viewHold.mstarAttentionBtn = (LinearLayout) convertView.findViewById(R.id.tv_all_star_attention);
            viewHold.forwordImage = (ImageView) convertView.findViewById(R.id.iv_forword_image);
            convertView.setTag(viewHold);
        }
        ViewHold viewHold = (ViewHold) convertView.getTag();

        StarModel starModel = mAttentionList.get(position);
        Glide.with(context).load(StringUtil.isNullorEmpty(starModel.getPhoto()) ? R.drawable.default_icon : starModel.getPhoto())
                .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                .transform(new GlideRoundTransform(context, R.dimen.height_50dp)).into(viewHold.mFaceIcon);
        viewHold.mTipsIcon.setImageResource(RoleUtil.getRoleIcon(starModel.getRole()));
        viewHold.mStarName.setText(starModel.getNickName());
        viewHold.mPersonNumText.setText(starModel.getHots()+"");
        viewHold.mFansNumText.setText(starModel.getFans()+"");
        if(ATTENITION_TYPE == RECOMMEND_ATTENTION )
        {
            // TODO: 2016/4/29 删除粉本地存储
//            if(!MaxApplication.getApp().getFollows().contains(starModel.getUid()))
//            {
//                viewHold.forwordImage.setVisibility(View.GONE);
//                viewHold.mstarAttentionBtn.setVisibility(View.VISIBLE);
//                viewHold.mstarAttentionBtn.setOnClickListener(new AtteBtnListener(starModel,viewHold));
//                viewHold.mFaceIcon.setOnClickListener(new IconListener(starModel,viewHold));
//            }
//            else
            {
                viewHold.mstarAttentionBtn.setVisibility(View.GONE);
                viewHold.forwordImage.setVisibility(View.VISIBLE);
            }
        }else if(ATTENITION_TYPE == USER_ATTENTION)
        {
            viewHold.mstarAttentionBtn.setVisibility(View.GONE);
            viewHold.forwordImage.setVisibility(View.VISIBLE);
        }
        convertView.setTag(R.id.star_tag,starModel);
//        /fans/addFollow
        return convertView;
    }


    public class ViewHold
    {
        ImageView mFaceIcon;
        ImageView mTipsIcon;
        TextView mStarName;
        public TextView mPersonNumText;
        public TextView mFansNumText;
        LinearLayout mstarAttentionBtn;
        ImageView forwordImage;
    }

    public void setAttentionList(List<StarModel> mAttentionList) {

        if(mAttentionList != null){
            this.mAttentionList.clear();
            this.mAttentionList.addAll(mAttentionList);
        }

    }

    public void addAttentionList(List<StarModel> mAttentionList) {
        if(mAttentionList != null){
            this.mAttentionList.addAll(mAttentionList);
        }
    }

    public List<StarModel> getAttentionList() {
        return mAttentionList;
    }

    protected class IconListener implements View.OnClickListener{
        private StarModel starModel;
        private ViewHold viewHold;
        public IconListener (StarModel starModel,ViewHold viewHold)
        {
            this.starModel = starModel;
            this.viewHold = viewHold;
        }

        @Override
        public void onClick(View v) {
            if(viewHold.mstarAttentionBtn.getVisibility() == View.VISIBLE)
            {
                atteClickCallBack.onClickIcon(starModel);
            }
        }
    }

    public class AtteBtnListener implements View.OnClickListener{
        private StarModel starModel;
        private ViewHold viewHold;
        public AtteBtnListener (StarModel starModel,ViewHold viewHold)
        {
            this.starModel = starModel;
            this.viewHold = viewHold;
        }

        @Override
        public void onClick(View v) {
            boolean result = atteClickCallBack.onClickAtte(starModel,v);
            if(viewHold!= null)
            {
                viewHold.mstarAttentionBtn.setVisibility(View.GONE);
                viewHold.forwordImage.setVisibility(View.VISIBLE);
            }
        }
    }

    public interface AtteListenerCallBack {
        /**
         * 点击粉她回调
         * @param starModel
         * @return
         */
        public boolean onClickAtte(StarModel starModel,View view);

        public void onClickIcon(StarModel starModel);
    }

    public void setAtteClickCallBack(AtteListenerCallBack atteClickCallBack) {
        this.atteClickCallBack = atteClickCallBack;
    }

    public void cleanItems()
    {
        if(mAttentionList != null)
        {
            mAttentionList.clear();
            notifyDataSetChanged();
        }
    }

    /**
     * 判断当前Item是否允许点击
     * @param view
     * @return
     */
    public boolean isClickItem(View view)
    {
        Object tagObj = view.getTag();
        if(tagObj instanceof ViewHold)
        {
            ViewHold viewHold = (ViewHold) tagObj;
            if(viewHold.forwordImage.getVisibility() == View.VISIBLE)
            {
                return true;
            }
        }
        return false;
    }
}
