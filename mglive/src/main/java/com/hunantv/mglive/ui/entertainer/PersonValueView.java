package com.hunantv.mglive.ui.entertainer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.ui.entertainer.adapate.PersonValueAdapater;
import com.hunantv.mglive.ui.entertainer.data.PersonValueData;
import com.hunantv.mglive.ui.entertainer.listener.LoadmoreListener;
import com.hunantv.mglive.ui.live.detail.DetailConstants;
import com.hunantv.mglive.widget.loadingView.LoadingViewHelper;

import java.util.List;

/**
 * Created by admin on 2015/11/10.
 */
public class PersonValueView {
    private FullAgreeView parentView;
    private Context context;

    private TextView clickView;
    private LinearLayout closeView;

    private ListView personValueListView;
    private PersonValueAdapater personValueAdapter;
    private List<PersonValueData> personValueDatas;
    private int firstitem = -1;
    private boolean isClose = false;
    private LoadmoreListener mLoadmoreListener;
    private View view;
    public TextView mTitle;
    private LoadingViewHelper mLoadingViewHelper;

    public PersonValueView(Context context, FullAgreeView parentView) {
        this.parentView = parentView;
        this.context = context;
        initView();
    }

    private void initView() {
        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            view = layoutInflater.inflate(R.layout.layout_star_person_value_view, null);
            mTitle = (TextView) view.findViewById(R.id.dynamic_title);
            initView(view);
        }

    }

    public View getView() {
        if (view == null) {
            initView();
        }
        return view;
    }


    void initView(View view) {
        clickView = (TextView) view.findViewById(R.id.clickView);

        clickView.setOnTouchListener(new View.OnTouchListener() {
            int i = 200;
            float startY = 0;
            float endY = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    // 主点按下
                    case MotionEvent.ACTION_DOWN:
                        startY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        endY = event.getRawY();
                        float transY = Math.abs(endY - startY);
                        if (Math.abs(transY) > i) {
                            if (startY > endY) {
                                //下滑
                                parentView.fullAgreeBellowView();
                                clickView.setVisibility(View.GONE);
                            } else {
                                //上滑
                            }
                        }
                        return true;
                }
                return false;

            }
        });

        closeView = (LinearLayout) view.findViewById(R.id.closeView);
        closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentView.closeView();
                clickView.setVisibility(View.VISIBLE);
            }
        });

        personValueListView = (ListView) view.findViewById(R.id.personValueListView);

        personValueAdapter = new PersonValueAdapater(context);
        personValueListView.setAdapter(personValueAdapter);

        personValueListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (firstitem == 0 && personValueListView.getChildAt(0).getTop() == personValueListView.getPaddingTop()) {
                    if (isClose && scrollState == SCROLL_STATE_IDLE) {
//                        parentView.closeView();
                        parentView.closeFullArgeeView();
                        clickView.setVisibility(View.VISIBLE);
                    }
                    isClose = true;
                } else {
                    isClose = false;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                firstitem = firstVisibleItem;

                if (firstVisibleItem + visibleItemCount >
                        totalItemCount - DetailConstants.REMAIN_LOAD_MORE && totalItemCount > DetailConstants.REMAIN_LOAD_MORE && mLoadmoreListener != null) {
                    mLoadmoreListener.onLoadMore();
                }
            }
        });
        mLoadingViewHelper = new LoadingViewHelper(this.personValueListView);
        mLoadingViewHelper.showLoading();
    }

    public void setDatas(List<PersonValueData> datas) {
        personValueAdapter.setDatas(datas);

        if (mLoadingViewHelper != null) {
            mLoadingViewHelper.restore();
            if(datas == null || datas.size() == 0)
            {
                mLoadingViewHelper.showEmpty(R.string.empty_person,R.drawable.empty_live_dynamic);
            }
        }
    }

    public void addDatas(List<PersonValueData> datas) {
        personValueAdapter.addDatas(datas);
    }

    public void setLoadMoreListener(LoadmoreListener listener) {
        mLoadmoreListener = listener;
    }



    public void setClickViewVisible() {
        clickView.setVisibility(View.VISIBLE);
    }

}
