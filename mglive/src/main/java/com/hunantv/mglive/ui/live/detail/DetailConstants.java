package com.hunantv.mglive.ui.live.detail;

import com.hunantv.mglive.common.BuildConfig;

/**
 * Created by oy on 2015/12/12.
 */
public class DetailConstants {

    /**
     * 余下少于n条数据加载更多
     */
    public static final int REMAIN_LOAD_MORE = 3;

    /**
     * 默认10条
     */
    public static final int DEFAULT_PAGE_SIZE = 10;

}
