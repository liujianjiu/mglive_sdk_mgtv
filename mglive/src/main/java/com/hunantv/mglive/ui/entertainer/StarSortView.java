package com.hunantv.mglive.ui.entertainer;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.ui.entertainer.data.StarSortData;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.StringUtil;

import java.util.List;

/**
 * Created by admin on 2015/12/3.
 */
public class StarSortView extends RelativeLayout implements OnClickListener {
    private List<StarSortData> starSortDatas;
    private String textValue = null;
    private LinearLayout firstNumLayout, sencodNumLayout, thirdNumLayout;
    private ImageView advertImg;

    private RelativeLayout firstView;
    private ImageView firstIconImage;
    private TextView firstNameText;
    private TextView firstNumText;
    private ImageView firstGradeImg;

    private RelativeLayout sencodView;
    private ImageView sencodIconImage;
    private TextView sencodNameText;
    private TextView sencodNumText;
    private ImageView sencodGradeImg;

    private RelativeLayout thirdView;
    private ImageView thirdIconImage;
    private TextView thirdNameText;
    private TextView thirdNumText;
    private ImageView thirdGradeImg;

    private TextView textView1, textView2, textView3;
    private boolean isAdvert = false;
    private String advertUrl = null;

    private OnRankClickListener onRankClickListener;

    public StarSortView(Context context, List<StarSortData> starSortDatas, String textValue) {
        this(context, starSortDatas, textValue, false, null, null);
    }

    public StarSortView(Context context, List<StarSortData> starSortDatas, String textValue, boolean isAdvert, String advertUrl, OnRankClickListener onRankClickListener) {
        super(context);
        this.starSortDatas = starSortDatas;
        this.textValue = textValue;
        this.isAdvert = isAdvert;
        this.advertUrl = advertUrl;
        this.onRankClickListener = onRankClickListener;
        initView();
    }

    public void initView() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View view = layoutInflater.inflate(
                R.layout.layout_star_sort, null);
        initView(view);
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                AbsListView.LayoutParams.MATCH_PARENT,
                AbsListView.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(params);
        addView(view);
        initData();
    }

    public void initView(View view) {
        advertImg = (ImageView) view.findViewById(R.id.iv_advert);

        firstView = (RelativeLayout) view.findViewById(R.id.rl_first_view);
        firstNumLayout = (LinearLayout) view.findViewById(R.id.firstNumLayout);
        firstIconImage = (ImageView) view.findViewById(R.id.firstIconImage);
        firstNameText = (TextView) view.findViewById(R.id.firstNameText);
        firstNumText = (TextView) view.findViewById(R.id.firstNumText);
        firstGradeImg = (ImageView) view.findViewById(R.id.firstGradeImg);
        textView1 = (TextView) view.findViewById(R.id.textValue1);
        firstView.setOnClickListener(this);

        sencodView = (RelativeLayout) view.findViewById(R.id.rl_sencod_view);
        sencodNumLayout = (LinearLayout) view.findViewById(R.id.sencodNumLayout);
        sencodIconImage = (ImageView) view.findViewById(R.id.sencodIconImage);
        sencodNameText = (TextView) view.findViewById(R.id.sencodNameText);
        sencodNumText = (TextView) view.findViewById(R.id.sencodNumText);
        sencodGradeImg = (ImageView) view.findViewById(R.id.sencodGradeImg);
        textView2 = (TextView) view.findViewById(R.id.textValue2);
        sencodView.setOnClickListener(this);

        thirdView = (RelativeLayout) view.findViewById(R.id.rl_third_view);
        thirdNumLayout = (LinearLayout) view.findViewById(R.id.thirdNumLayout);
        thirdIconImage = (ImageView) view.findViewById(R.id.thirdIconImage);
        thirdNameText = (TextView) view.findViewById(R.id.thirdNameText);
        thirdNumText = (TextView) view.findViewById(R.id.thirdNumText);
        thirdGradeImg = (ImageView) view.findViewById(R.id.thirdGradeImg);
        textView3 = (TextView) view.findViewById(R.id.textValue3);
        thirdView.setOnClickListener(this);

        if (!StringUtil.isNullorEmpty(textValue)) {
            textView1.setText(textValue);
            textView2.setText(textValue);
            textView3.setText(textValue);
        }

        if (isAdvert && !StringUtil.isNullorEmpty(advertUrl)) {
            int marTop = (int) getResources().getDimension(R.dimen.margin_70dp);
            setMarginTop(firstView, marTop);
            setMarginTop(sencodView, marTop);
            setMarginTop(thirdView, marTop);
//            firstView.setBackgroundResource(R.color.white);
            advertImg.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(advertUrl).into(advertImg);
//            advertImg.setImageResource(R.drawable.advert);
        }
//        else
//        {
//            sencodView.setBackgroundColor(getResources().getColor(R.color.white));
//            thirdView.setBackgroundColor(getResources().getColor(R.color.white));
//        }
    }

    public void initData() {
        if (starSortDatas != null) {
            if (starSortDatas.size() >= 1) {
                initStarSortData(firstIconImage, firstNameText, firstNumText, firstGradeImg, 0);
                firstNumLayout.setVisibility(VISIBLE);
            }
            if (starSortDatas.size() >= 2) {
                initStarSortData(sencodIconImage, sencodNameText, sencodNumText, sencodGradeImg, 1);
                sencodNumLayout.setVisibility(VISIBLE);
            }
            if (starSortDatas.size() >= 3) {
                initStarSortData(thirdIconImage, thirdNameText, thirdNumText, thirdGradeImg, 2);
                thirdNumLayout.setVisibility(VISIBLE);
            }
        }
    }

    private void initStarSortData(ImageView iconImage, TextView nameText, TextView numText, ImageView gradeImg, int index) {
        if (starSortDatas != null && index < starSortDatas.size()) {
            StarSortData starSortData = starSortDatas.get(index);
//            iconImage.setImageResource(starSortData.getIcon());
            Glide.with(getContext()).load(StringUtil.isNullorEmpty(starSortData.getIcon()) ? R.drawable.default_icon : starSortData.getIcon())
                    .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                    .transform(new GlideRoundTransform(getContext(), R.dimen.height_70dp)).into(iconImage);
            nameText.setText(starSortData.getName());
            nameText.setTextColor(Color.parseColor("#333333"));
            numText.setText(starSortData.getNum() + "");
            if (starSortData.getGrade() >= 3) {
                Glide.with(getContext()).load(R.drawable.grade_hj).into(gradeImg);
                gradeImg.setVisibility(View.VISIBLE);
            } else if (starSortData.getGrade() >= 2) {
                Glide.with(getContext()).load(R.drawable.grade_by).into(gradeImg);
                gradeImg.setVisibility(View.VISIBLE);
            } else if (starSortData.getGrade() >= 1) {
                Glide.with(getContext()).load(R.drawable.grade_qt).into(gradeImg);
                gradeImg.setVisibility(View.VISIBLE);
            } else {
                gradeImg.setVisibility(View.GONE);
            }
        }
    }

    private void setMarginTop(View view, int marginTop) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.topMargin = layoutParams.topMargin + marginTop;
        view.setLayoutParams(layoutParams);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.rl_first_view){
            if (onRankClickListener != null) {
                onRankClickListener.onRankClick(1);
            }
        }else if(v.getId() == R.id.rl_sencod_view){
            if (onRankClickListener != null) {
                onRankClickListener.onRankClick(2);
            }
        }else if(v.getId() == R.id.rl_third_view){
            if (onRankClickListener != null) {
                onRankClickListener.onRankClick(3);
            }
        }

    }

    public interface OnRankClickListener {
        public void onRankClick(int rank);
    }

    public void setOnRankClickListener(OnRankClickListener onRankClickListener) {
        this.onRankClickListener = onRankClickListener;
    }
}
