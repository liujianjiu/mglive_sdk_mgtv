package com.hunantv.mglive.ui.live;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.gift.ClassifyGiftModel;

import java.util.List;

/**
 * Created by admin on 2016/5/16.
 */
public class GiftProgramAdapter extends BaseAdapter {
    private Context mContext;
    private List<ClassifyGiftModel> mClassifyGifts;
    private int mSelect = 0;
    private boolean mIsLang;
    //全屏235   266
    public GiftProgramAdapter(Context mContext){
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mClassifyGifts != null ? mClassifyGifts.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.layout_gift_program, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.mProgram = (ImageView) convertView.findViewById(R.id.iv_program);
            viewHolder.mLine = convertView.findViewById(R.id.v_line);
            viewHolder.mLine2 = convertView.findViewById(R.id.v_line2);
            convertView.setTag(viewHolder);
        }
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        ClassifyGiftModel model = mClassifyGifts.get(position);
        Glide.with(mContext).load(model.getClassifyIcon()).into(viewHolder.mProgram);
        viewHolder.mLine.setBackgroundColor(position == mSelect ? mContext.getResources().getColor(R.color.common_yellow) : mContext.getResources().getColor(R.color.transparent));
        //适配宽度
//        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewHolder.mLine.getLayoutParams();
//        int count = getCount();
//        int widthDp = mIsLang ? (int) UIUtil.dip2px(mContext, 235f) : DeviceInfoUtil.getResolutionWidth(mContext);
//        int otherWidth = widthDp / (mIsLang ? 4 : 5);
//        int dp75 = (int) UIUtil.dip2px(mContext, 75f);
//        if(count * dp75 < widthDp - otherWidth)
//        {
//            layoutParams.width = (widthDp - otherWidth) / count;
//        }
//        viewHolder.mLine.setLayoutParams(layoutParams);
        //隐藏分割线
        viewHolder.mLine2.setBackgroundColor(Color.parseColor(mIsLang ? "#0FFFFFFF" : "#0f000000"));
        return convertView;
    }

    private class ViewHolder{
        ImageView mProgram;
        View mLine;
        View mLine2;
    }

    public void setSelect(int mSelect) {
        this.mSelect = mSelect;
        notifyDataSetChanged();
    }

    public void setIsLang(boolean mIsLang) {
        this.mIsLang = mIsLang;
    }

    public List<ClassifyGiftModel> getClassifyGifts() {
        return mClassifyGifts;
    }

    public void setClassifyGifts(List<ClassifyGiftModel> classifyGifts) {
        this.mClassifyGifts = classifyGifts;
    }
}
