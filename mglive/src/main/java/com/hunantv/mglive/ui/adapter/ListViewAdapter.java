package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * @author Lyson
 * 
 *         listView 分类列表的adapter
 * 
 */
public class ListViewAdapter extends BaseAdapter {

	public interface ListViewAdapterDelegate {

		public abstract int getItemCount();

		public abstract View getItemViews(View convertView, int position);

		public abstract boolean isEnabled(int position);
	}


	public ListViewAdapterDelegate delegate;

	public Context m_context;

	public ListViewAdapter(ListViewAdapterDelegate myDelegate, Context context) {

		delegate = myDelegate;

		m_context = context;

	}

	@Override
	public int getCount() {
		return delegate.getItemCount();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		return delegate.getItemViews(convertView, position);
	}

	@Override
	public boolean isEnabled(int position) {
		return delegate.isEnabled(position);
	}

}
