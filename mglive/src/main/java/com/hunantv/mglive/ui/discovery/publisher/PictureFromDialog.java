package com.hunantv.mglive.ui.discovery.publisher;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import com.hunantv.mglive.R;

//maxxiang 2015-12-8
public class PictureFromDialog extends Dialog implements View.OnClickListener {
	Context mContext;

	private int mLayoutResId = R.layout.dialog_detailquan_publish_picture_from;
	// 调用本dialog的回调
	public interface PictureFromCallback {
		public void onOpenCameraCallback();
		public void onTakePictureFromAlbumCallback();
		public void onNoBackgroundCallback();
	}

	private PictureFromCallback mCallback;

	public PictureFromDialog(Context context) {
		super(context, R.style.picturefromdialog);
		mContext = context;

	}

	public void setCallback(PictureFromCallback callback) {
		mCallback = callback;
	}

	public void setLayoutResourceId(int layoutResId){
		mLayoutResId = layoutResId;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(mLayoutResId);

		initView();

		getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		WindowManager.LayoutParams wlp = getWindow().getAttributes();
		wlp.gravity = Gravity.BOTTOM;
		getWindow().setAttributes(wlp);
		getWindow().setWindowAnimations(R.style.DialogAnimation);
	}

	private void initView() {
		Button btnCamera = (Button) findViewById(R.id.picture_from_dialog_camera);
		btnCamera.setOnClickListener(this);
		Button btnAlbum = (Button) findViewById(R.id.picture_from_dialog_album);
		btnAlbum.setOnClickListener(this);
		
//		Button btnNoBkg = (Button) findViewById(R.id.no_picture);
//		if(btnNoBkg != null){
//			btnNoBkg.setOnClickListener(this);
//		}
		
		Button btnCancel = (Button) findViewById(R.id.picture_from_dialog_cancel);
		btnCancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.picture_from_dialog_camera){
				if (mCallback != null) {
					mCallback.onOpenCameraCallback();
				}
				dismiss();
			}
		else if(v.getId() == R.id.picture_from_dialog_album) {
				dismiss();
				if (mCallback != null)
					mCallback.onTakePictureFromAlbumCallback();
		}else if(v.getId() == R.id.picture_from_dialog_cancel) {
				dismiss();
		}

	}

}
