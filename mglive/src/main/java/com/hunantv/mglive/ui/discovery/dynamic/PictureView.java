package com.hunantv.mglive.ui.discovery.dynamic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.data.discovery.DynamicData;
import com.hunantv.mglive.data.discovery.DynamicImag;
import com.hunantv.mglive.ui.adapter.PictureAdapter;
import com.hunantv.mglive.ui.discovery.publisher.pic.PicPreviewActivity;
import com.hunantv.mglive.ui.discovery.publisher.pic.PublisherConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;

/**
 * Created by June Kwok on 2015-12-16.
 */
public class PictureView extends GridView {
    private List<DynamicImag> mImgList;
    private PictureAdapter mAdapter;
    private boolean isDetails = false;

    public PictureView(Context context, boolean isDetails) {
        super(context);
        this.isDetails = isDetails;
        init();
    }

    public PictureView(Context context) {
        super(context);
        this.isDetails = false;
        init();
    }

    public PictureView(Context context,AttributeSet attributes) {
        super(context,attributes);
        this.isDetails = false;
        init();
    }

    public List<DynamicImag> getImgList() {
        return mImgList;
    }

    public void setImgList(List<DynamicImag> mImgList) {
        this.mImgList = mImgList;
    }

    /**
     * 全屏浏览的时候获取大图的列表
     *
     * @return
     */
    public ArrayList<String> toStringList() {
        ArrayList<String> mList = null;
        if (null != getImgList()) {
            mList = new ArrayList<>();
            for (int i = 0; i < getImgList().size(); i++) {
                mList.add(getImgList().get(i).getBig());
            }
        }
        return mList;
    }

    private void init() {
        if (null == mImgList) {
            mImgList = new ArrayList<>();
        }
        mImgList = new ArrayList<>();
        mAdapter = new PictureAdapter(getContext(), mImgList,this);
        setAdapter(mAdapter);
        notifyChanged(mImgList);
        setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(getContext(), PicPreviewActivity.class);
                intent.putStringArrayListExtra("piclists", toStringList());
                intent.putStringArrayListExtra("picselectlists", toStringList());
                intent.putExtra("type", 1);    //当前的图片id(index，从0开始)
                intent.putExtra("position", position);    //当前的图片id(index，从0开始)
                intent.putExtra("currentMode", PicPreviewActivity.MODE_PREVIEW_ONLY);
                intent.putExtra("maxSelectCount", PublisherConstants.MAX_SELECT_PIC_COUNT);
                ((Activity) getContext()).startActivity(intent);
            }
        });
    }

    public void notifyChanged(List<DynamicImag> list) {
        mImgList.clear();
        if (null != list) {
            mImgList.addAll(list);
        }
        int n = 1;
        if (mImgList.size() == 2 || mImgList.size() == 4) {
            n = 2;
        } else if (mImgList.size() == 3 || mImgList.size() > 4) {
            n = 3;
        }
        setNumColumns(n);
        mAdapter.setColumNum(n);
        mAdapter.setImgList(mImgList);
        mAdapter.notifyDataSetChanged();
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = View.MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, View.MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
