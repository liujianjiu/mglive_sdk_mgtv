package com.hunantv.mglive.ui.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hunantv.mglive.R;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.LiveDetailModel;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.utils.GlideRoundTransform;

import java.util.List;

/**
 * 直播/点播 机位
 * Created by 达
 */
public class LiveCameraAdapter extends BaseAdapter {
    private Context mContext;
    private List<LiveDetailModel> mDataList;
    private LiveDetailModel mCheckLive;

    public LiveCameraAdapter(Context context, List<LiveDetailModel> list) {
        this.mContext = context;
        this.mDataList = list;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public LiveDetailModel getItem(int position) {
        if (position < 0) {
            return null;
        }
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LiveDetailModel dataModel = getItem(position);
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.live_camera_item, null);
            holder.ivBg = (ImageView) convertView.findViewById(R.id.iv_live_detail_camera_item_bg);
            holder.ivStoken = (ImageView) convertView.findViewById(R.id.iv_live_detail_camera_item_stroke);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_live_detail_camera_item_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (dataModel != null) {
            Glide.with(mContext).load(dataModel.getImage()).diskCacheStrategy(DiskCacheStrategy.NONE).centerCrop().into(holder.ivBg);
            holder.tvName.setText(dataModel.getTitle());
            holder.tvName.setSelected(mCheckLive == dataModel);
            holder.ivStoken.setSelected(mCheckLive == dataModel);
        }
        return convertView;
    }

    public LiveDetailModel getCheckedLive() {
        return mCheckLive;
    }

    public void setCheckedLive(LiveDetailModel liveDetailModel) {
        mCheckLive = liveDetailModel;
    }

    class ViewHolder {
        ImageView ivBg;
        ImageView ivStoken;
        TextView tvName;
    }
}
