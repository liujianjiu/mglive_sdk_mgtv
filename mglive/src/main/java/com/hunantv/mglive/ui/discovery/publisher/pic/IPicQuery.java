package com.hunantv.mglive.ui.discovery.publisher.pic;

import java.util.ArrayList;

/**
 * @author maxxiang
 * 图片查询的回调接口
 */
public interface IPicQuery {
	
	public interface IPicQueryAlbumFinished{
		public void onQueryAlbumFinished(ArrayList<AlbumItem> list);
	}
	public interface IPicQueryFinished{
		public void onQueryPicFinished(ArrayList<PicItem> list);
	}

	//查询某一相册id下的所有图片,此函数为异步
	public void queryPicList(String albumId, IPicQueryFinished callback);
}
