package com.hunantv.mglive.ui.entertainer.data;

/**
 * Created by admin on 2015/12/11.
 */
public class CenterStarData {
    private String uid;

    private String nickName;

    private String photo;

    private int role;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
