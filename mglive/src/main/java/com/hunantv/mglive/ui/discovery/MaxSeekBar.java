package com.hunantv.mglive.ui.discovery;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.hunantv.mglive.R;

/**
 * Created by June Kwok on 2015-12-27.
 */
public class MaxSeekBar extends FrameLayout implements View.OnClickListener {
    private static final int MSG_TYPE_CHECK_DISPLAY = 1;
    private static final int BTN_DISPLAY_TIME_UNIT = 1000;
    private static final int BTN_DISPLAY_TIME = 5;
    private LayoutInflater mInflater;
    public ViewHolder mSeekHolder;
    private int mCountTime;
    private Handler mHandler = new Handler() {

        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            switch (msg.what) {
                case MSG_TYPE_CHECK_DISPLAY:
                    if (mSeekHolder.mBtnPause.getVisibility() == View.VISIBLE) {
                        mCountTime++;
                        if (mCountTime >= BTN_DISPLAY_TIME) {
                            swithCtrler();
                        } else {
                            mHandler.sendEmptyMessageDelayed(MSG_TYPE_CHECK_DISPLAY, BTN_DISPLAY_TIME_UNIT);
                        }
                    }
                    break;
                default:
            }
        }
    };

    /**
     * 显示控制按钮的方案
     */
    public void swithCtrler() {
        if (mSeekHolder.mBtnPause.getVisibility() != View.VISIBLE) {
            mCountTime = 0;
            mHandler.sendEmptyMessageDelayed(MSG_TYPE_CHECK_DISPLAY, BTN_DISPLAY_TIME_UNIT);
            mSeekHolder.mBtnPause.setVisibility(View.VISIBLE);
        } else {
            mSeekHolder.mBtnPause.setVisibility(View.GONE);
        }
    }

    public MaxSeekBar(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);
        initView();
        //取消暂停功能，将点击事件抛给父控件
//        setOnClickListener(this);
    }

    /**
     * 初始化View
     */
    private void initView() {
        View view = mInflater.inflate(R.layout.layout_max_seek_bar, this, false);
        LayoutParams mViewLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        mViewLP.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        addView(view, mViewLP);
        initViewHolder(view);
    }

    /**
     * 初始化Holder
     *
     * @param view
     */
    private void initViewHolder(View view) {
        mSeekHolder = new ViewHolder();
        mSeekHolder.mBarParent = (FrameLayout) view.findViewById(R.id.ll_live_detail_bottom_seekbar);
        mSeekHolder.mBtnPause = (ImageButton) view.findViewById(R.id.ibtn_live_detail_bootm_pause);
        mSeekHolder.mSeekbar = (ProgressBar) view.findViewById(R.id.progressbar_star_detail_bottom);
    }

    @Override
    public void onClick(View v) {
        if (null != mSeekHolder.mBtnPause) {
            swithCtrler();
        }
    }

    /**
     * View Holder
     */
    public class ViewHolder {
        public FrameLayout mBarParent;
        public ImageButton mBtnPause;
        public ProgressBar mSeekbar;
    }
}
