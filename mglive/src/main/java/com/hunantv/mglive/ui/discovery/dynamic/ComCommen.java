package com.hunantv.mglive.ui.discovery.dynamic;

import android.content.Context;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hunantv.mglive.common.Constant;

/**
 * Created by guojun3 on 2015-12-16.
 */
public class ComCommen extends LinearLayout {

    TextView mNickName;
    TextView mMsgDetails;

    public ComCommen(Context context) {
        super(context);
        init();
    }

    protected void init() {
        setOrientation(HORIZONTAL);
        mNickName = new TextView(getContext());
        mNickName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        mNickName.setTextColor(0xFF56BAFC);

        mMsgDetails = new TextView(getContext());
        mMsgDetails.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        mMsgDetails.setTextColor(0xFF999999);

        addView(mNickName);
        addView(mMsgDetails);
    }

    public TextView getNickName() {
        return mNickName;
    }

    public TextView getMsgDetails() {
        return mMsgDetails;
    }
}
