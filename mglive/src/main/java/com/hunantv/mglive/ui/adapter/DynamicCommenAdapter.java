package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.discovery.CommenData;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.StringUtil;

import java.util.List;

/**
 * Created by liujianjiu on 2016-4-7
 *
 * 动态详情评论列表
 */
public class DynamicCommenAdapter extends BaseAdapter {
    public static int TYPE_TITLE = 1;
    public static int TYPE_LIST = 2;
    public static int TYPE_MORE = 3;
    public static int TOG_LIST = R.layout.item_dynamic_comment;
    private List<CommenData> mList;
    private boolean hasMoreData;
    private Context mContext;

    public void setmOnLoadMoreClickListener(OnLoadMoreClick mOnLoadMoreClickListener) {
        this.mOnLoadMoreClickListener = mOnLoadMoreClickListener;
    }

    private OnLoadMoreClick mOnLoadMoreClickListener;

    private View titleView;
    private View moreView;

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public DynamicCommenAdapter(Context ctx) {
        setContext(ctx);
    }

    @Override
    public int getCount() {
        int count = 1;
        if (null != mList) {
            count = count +  mList.size();
            if(hasMoreData){
                count++;
            }
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return TYPE_TITLE;
        }else if(position == getCount()-1 && hasMoreData){
            return TYPE_MORE;
        }else{
            return TYPE_LIST;
        }
    }

    @Override
    public Object getItem(int position) {
        if (mList != null && position>0 && position <= mList.size()) {
            return mList.get(position-1);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        if(type == TYPE_TITLE){
            if(titleView == null){
                titleView = LayoutInflater.from(mContext).inflate(R.layout.item_dynamic_head,null);
            }
            convertView = titleView;
            TextView titeText = (TextView)convertView.findViewById(R.id.item_dynamic_title);
            if(mList != null){
                titeText.setText(getContext().getString(R.string.comment_count, mList.size()));
            }else{
                titeText.setText(getContext().getString(R.string.comment_count, 0));
            }

        }else if(type == TYPE_MORE){
            if(moreView == null){
                moreView = LayoutInflater.from(mContext).inflate(R.layout.item_dynamic_more,null);
                moreView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mOnLoadMoreClickListener != null){
                            mOnLoadMoreClickListener.onLoadMore();
                        }
                    }
                });
            }

            convertView = moreView;

        }else{
            ViewHolder mHolder;
            CommenData data = (CommenData) getItem(position);
            if (null == convertView || convertView.getTag(TOG_LIST) == null || !(Boolean)convertView.getTag(TOG_LIST) ) {
                mHolder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_dynamic_comment,null);
                mHolder.mName = (TextView)convertView.findViewById(R.id.item_dynamic_comment_title);
                mHolder.mMsgDetails = (TextView)convertView.findViewById(R.id.item_dynamic_comment_content);
                mHolder.mHead = (ImageView)convertView.findViewById(R.id.item_dynamic_comment_head);
                mHolder.mTime = (TextView)convertView.findViewById(R.id.item_dynamic_comment_time);

                convertView.setTag(mHolder);
                convertView.setTag(TOG_LIST,true);
            }

            mHolder = (ViewHolder) convertView.getTag();
            //头像
            Glide.with(getContext()).load(data.getPhoto()).error(R.drawable.default_icon).placeholder(R.drawable.default_icon).transform(new GlideRoundTransform(getContext(), R.dimen.height_56dp)).into(mHolder.mHead);
            mHolder.mName.setText(data.getNickName() + "：");
            mHolder.mMsgDetails.setText(data.getContent());
            if (!StringUtil.isNullorEmpty(data.getDate())) {
                mHolder.mTime.setText(data.getDate());
            } else {
                mHolder.mTime.setText("");
            }
        }

        return convertView;
    }

    class ViewHolder {
        TextView mName;
        TextView mMsgDetails;
        TextView mTime;
        ImageView mHead;
    }

    public void setData(List<CommenData> mList,boolean hasMoreData) {
        this.mList = mList;
        this.hasMoreData = hasMoreData;
        notifyDataSetChanged();
    }

    public interface OnLoadMoreClick{
        public void onLoadMore();
    }

}
