package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.StringUtil;

import java.util.ArrayList;

/**
 * Created by admin on 2016/2/29.
 */
public class ZoneRankItemAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<StarModel> mStars;
    private int mIndex;
    private boolean mIsShowDefaultStars = false;
    private String mZoneImgUrl;
    public ZoneRankItemAdapter(Context context,ArrayList<StarModel> stars,String zoneImgUrl,int index) {
        this.mContext = context;
        this.mStars = stars;
        this.mZoneImgUrl = zoneImgUrl;
        mIndex = index;
    }

    @Override
    public int getCount() {
        if(mStars != null && mStars.size() > 0)
        {
            mIsShowDefaultStars = false;
            return mStars.size() < 10 ? 11 : 12;
        }else{
            mIsShowDefaultStars = true;
            return  4;//显示虚位以待
        }
    }

    @Override
    public Object getItem(int position) {
        if(position == 0)
        {
            return null;
        }
        if(!mIsShowDefaultStars)
        {
            if(position == getCount() - 1 && mStars.size() >= 10){
                return null;
            } else {
                int dataIndex = position - 1;
                return dataIndex < mStars.size() ? mStars.get(dataIndex) : new StarModel();
            }
        }else
        {
            return new StarModel();
        }

    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        if(position == 0)
        {
            convertView = layoutInflater.inflate(
                    R.layout.layout_zone_rank_first_item, null);
            ImageView image = (ImageView) convertView.findViewById(R.id.iv_img);
            int defaultImgId = 0;
            if(mIndex == 0)
            {
                //主战场
                defaultImgId = R.drawable.zone_zcc;
            }else if(mIndex == 1)
            {
                //地面赛区
                defaultImgId = R.drawable.zone_dmsq;
            }else if(mIndex == 2)
            {
                //直通区
                defaultImgId = R.drawable.zone_ztq;
            }
            if(!StringUtil.isNullorEmpty(mZoneImgUrl))
            {
                Glide.with(mContext).load(mZoneImgUrl).into(image);
            }else
            {
                Glide.with(mContext).load(defaultImgId).into(image);
            }

            return convertView;
        }
        if(!mIsShowDefaultStars && position ==  getCount() - 1 && mStars.size() >= 10)
        {
            //查看更多
            convertView = layoutInflater.inflate(
                    R.layout.layout_zone_rank_star_more_item, null);
            return convertView;
        }

        if(convertView == null || convertView.getTag() == null)
        {
            convertView = layoutInflater.inflate(
                    R.layout.layout_zone_rank_star_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.mRankBg = (ImageView) convertView.findViewById(R.id.iv_rank_bg);
            viewHolder.mRankNum = (TextView) convertView.findViewById(R.id.tv_rank_num);
            viewHolder.mPhoto = (ImageView) convertView.findViewById(R.id.iv_photo);
            viewHolder.mName = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.mHots = (TextView) convertView.findViewById(R.id.tv_hots);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        int dataIndex = position - 1;
        StarModel star = null;
        if(mStars != null && dataIndex < mStars.size()){
            star = mStars.get(dataIndex);
        }

        if(viewHolder != null)
        {
            if(star != null)
            {
                if(mIndex == 0 )
                {
                    viewHolder.mRankBg.setImageResource(dataIndex < 3 ? R.drawable.star_rank_bg_123 : R.drawable.star_rank_bg_other);
                    viewHolder.mRankBg.setVisibility(View.VISIBLE);
                    viewHolder.mRankNum.setText((dataIndex + 1) + "");
                    viewHolder.mRankNum.setVisibility(View.VISIBLE);
                }else {
                    viewHolder.mRankBg.setImageResource(0);
                    viewHolder.mRankBg.setVisibility(View.GONE);
                    viewHolder.mRankNum.setVisibility(View.GONE);
                }

                Glide.with(mContext).load(star.getPhoto())
                        .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                        .transform(new GlideRoundTransform(mContext, R.dimen.height_65dp))
                        .into(viewHolder.mPhoto);
                //f45ca4
                viewHolder.mName.setText(star.getNickName());
                viewHolder.mName.setTextColor(Color.parseColor("#333333"));
                String text = star.getOrigin();
                if(!StringUtil.isNullorEmpty(text)) {
                    viewHolder.mHots.setText(text);
                    viewHolder.mHots.setVisibility(View.VISIBLE);
                }else
                {
                    viewHolder.mHots.setVisibility(View.GONE);
                }
            }
            else
            {
                viewHolder.mRankBg.setImageResource(0);
                viewHolder.mRankBg.setVisibility(View.GONE);
                viewHolder.mRankNum.setVisibility(View.GONE);
                //虚位以待
                Glide.with(mContext).load(R.drawable.default_icon_disable)
                        .placeholder(R.drawable.default_icon_preload)
                        .transform(new GlideRoundTransform(mContext, R.dimen.height_65dp))
                        .into(viewHolder.mPhoto);
                viewHolder.mName.setText(R.string.empty_name);
                viewHolder.mName.setTextColor(Color.parseColor("#999999"));
                viewHolder.mHots.setVisibility(View.GONE);
            }
        }else
        {
            L.d("viewHolder=== null", viewHolder.toString());
        }
        return convertView;
    }

    public class ViewHolder{
        ImageView mRankBg;
        TextView mRankNum;
        ImageView mPhoto;
        TextView mName;
        TextView mHots;
    }
//    public void setStars(ArrayList<StarModel> stars) {
//        this.mStars = stars;
//        notifyDataSetChanged();
//    }
}
