package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.StringUtil;

import java.util.List;

/**
 * Created by admin on 2016/1/25.
 */
public class StarRankAdapter extends BaseAdapter {
    private Context mContext;
    private List<StarModel> mStars;

    public StarRankAdapter(Context context)
    {
        this.mContext = context;
    }
    @Override
    public int getCount() {
        return mStars != null ? mStars.size()+1 : 0;
    }

    @Override
    public Object getItem(int position) {
        return mStars != null && position < mStars.size() ? mStars.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        if(position > mStars.size() - 1)
        {
            convertView = layoutInflater.inflate(
                    R.layout.layout_star_rank_more_item, null);
            return convertView;
        }

        if(convertView == null || convertView.getTag() == null)
        {
            convertView = layoutInflater.inflate(
                    R.layout.layout_star_rank_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.mRankBg = (ImageView) convertView.findViewById(R.id.iv_rank_bg);
            viewHolder.mRankNum = (TextView) convertView.findViewById(R.id.tv_rank_num);
            viewHolder.mPhoto = (ImageView) convertView.findViewById(R.id.iv_photo);
            viewHolder.mName = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.mHots = (TextView) convertView.findViewById(R.id.tv_hots);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        StarModel star = mStars.get(position);
        if(viewHolder != null)
        {
//            Glide.with(mContext).load(position < 3 ? R.drawable.star_rank_bg_123 : R.drawable.star_rank_bg_other).into(viewHolder.mRankBg);
            viewHolder.mRankBg.setImageResource(position < 3 ? R.drawable.star_rank_bg_123 : R.drawable.star_rank_bg_other);
            viewHolder.mRankNum.setText((position + 1) + "");
            Glide.with(mContext).load(star.getPhoto())
                    .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                    .transform(new GlideRoundTransform(mContext, R.dimen.height_65dp))
                    .into(viewHolder.mPhoto);
            //f45ca4
            viewHolder.mName.setText(Html.fromHtml("<font color=\""+ (position < 3 ? "#f45ca4" : "#333333")+"\">" + star.getNickName() + "</font>"));
            viewHolder.mHots.setText(StringUtil.getFormatNum(star.getHots()) + "人气");
        }else
        {
            L.d("viewHolder=== null",viewHolder.toString());
        }
        return convertView;
    }

    public void setRanks(List<StarModel> stars) {
        this.mStars = stars;
        notifyDataSetChanged();
    }

    public class ViewHolder{
        ImageView mRankBg;
        TextView mRankNum;
        ImageView mPhoto;
        TextView mName;
        TextView mHots;
    }
}
