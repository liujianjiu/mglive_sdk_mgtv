package com.hunantv.mglive.ui.discovery.dynamic;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

import com.hunantv.mglive.ui.adapter.CommenAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guojun3 on 2015-12-16.
 */
public class CommentList extends ListView {
    private List<Object> mList;
    private CommenAdapter mAdapter;

    public CommentList(Context context) {
        this(context,null);
    }

    public CommentList(Context context,AttributeSet attributeSet){
        super(context,attributeSet);
        init();
        setDivider(null);
    }

    private void init() {
        if (null == getList()) {
            setList(new ArrayList<>());
        }
        mAdapter = new CommenAdapter(getContext(), getList());
        setAdapter(mAdapter);
    }

    public void notifyChanged(List<Object> list) {
        if (null != list) {
            getList().clear();
            getList().addAll(list);
        }
        mAdapter.notifyDataSetChanged();
    }

    public List<Object> getList() {
        return mList;
    }

    public void setList(List<Object> mList) {
        this.mList = mList;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, View.MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
