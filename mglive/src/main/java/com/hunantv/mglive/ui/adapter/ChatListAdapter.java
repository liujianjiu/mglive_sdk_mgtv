package com.hunantv.mglive.ui.adapter;

import android.app.ActionBar;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.ChatDataModel;
import com.hunantv.mglive.data.live.ChatData;
import com.hunantv.mglive.utils.RoleUtil;
import com.hunantv.mglive.widget.VerticalImageSpan;

import java.util.List;

/**
 * Created by 达 on 2015/11/25.
 */
public class ChatListAdapter extends BaseAdapter {
    private Context mContext;
    private boolean mIsShow;
    private List<ChatDataModel> mChatList;

    public static final int ITEM_TYPE_NULL = 0;
    public static final int ITEM_TYPE_CONTENT = 1;
    public static final int ITEM_TYPE_VIP_JOIN_IN = 2;
    public static final int ITEM_TYPE_GIFT = 3;
    public static final int ITEM_TYPE_NORMAL_JOIN_IN = 4;
    public static final int ITEM_TYPE_DEFALT = 5;

    public ChatListAdapter(List<ChatDataModel> mChatList, Context mContext) {
        this(mChatList, mContext, false);
    }


    public ChatListAdapter(List<ChatDataModel> list, Context frg, boolean isShow) {
        mContext = frg;
        mChatList = list;
        mIsShow = isShow;
    }


    @Override
    public int getItemViewType(int position) {

        int chatType = ITEM_TYPE_DEFALT;
        if(mChatList != null && mChatList.size() > position){
            chatType = mChatList.get(position).chatType;
        }
        if (position == mChatList.size() && !mIsShow) {
            return ITEM_TYPE_NULL;
        }

        if (chatType == ChatData.CHAT_TYPE_DANMO || chatType == ChatData.CHAT_TYPE_CONTENT) {
            chatType = ITEM_TYPE_CONTENT;
        } else if (chatType == ChatData.CHAT_TYPE_GIFT) {
            chatType = ITEM_TYPE_GIFT;
        } else if (chatType == ChatData.CHAT_TYPE_NORMAL_JOIN_IN) {
            chatType = ITEM_TYPE_NORMAL_JOIN_IN;
        } else if (chatType == ChatData.CHAT_TYPE_VIP_JOIN_IN
                || chatType == ChatData.CHAT_TYPE_BUY_GUARD) {
            chatType = ITEM_TYPE_VIP_JOIN_IN;
        }

        return chatType;
    }

    public boolean isItemToRightType(View contentView,int itemType) {
        ChatViewHolder holder = (ChatViewHolder) contentView.getTag();
        if(holder.viewType == itemType){
            return true;
        }

        return false;
    }

    @Override
    public int getViewTypeCount() {
        return 6;
    }

//    int totalCount = 0;
//    int reCount = 0;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ChatDataModel data = getItem(position);
        int itemType = getItemViewType(position);

//        totalCount++;
        if(convertView == null || !isItemToRightType(convertView,itemType)){
            convertView = newItemView(itemType);
        }else{
//            reCount++;
        }

        if(itemType == ITEM_TYPE_NULL || itemType == ITEM_TYPE_DEFALT){
            return convertView;
        }

        ChatViewHolder holder = (ChatViewHolder) convertView.getTag();
        if (holder.mParent != null) {
            if (holder.viewType == ITEM_TYPE_VIP_JOIN_IN) {
                if (data.grade >= 3) {
                    holder.mParent.setBackgroundResource(R.drawable.chat_list_item_go_background);
                } else if (data.grade == 2) {
                    holder.mParent.setBackgroundResource(R.drawable.chat_list_item_si_background);
                } else if (data.grade == 1) {
                    holder.mParent.setBackgroundResource(R.drawable.chat_list_item_au_background);
                }
            }
        }
        if (holder.chatName != null) {
            if (holder.viewType == ITEM_TYPE_VIP_JOIN_IN) {
                SpannableStringBuilder style = null;
                if(data.chatType == ChatData.CHAT_TYPE_VIP_JOIN_IN)
                {
                    style = getGuardJoinStyle(data);
                }else if(data.chatType == ChatData.CHAT_TYPE_BUY_GUARD)
                {
                    style = getBuyGuardStyle(data);
                }
                if(style != null)
                {
                    holder.chatName.setText(style);
                }

            } else {
//                String name = data.name1 + ((data.grade > 0 || data.role != RoleUtil.ROLE_PT) ? " " : "");
                String name = data.name1;
                if (data.grade > 0) {
                    name = name + " ";
                } else if (data.role != RoleUtil.ROLE_PT) {
                    name = " " + name;
                }
                SpannableStringBuilder style = new SpannableStringBuilder(name);
                if(data.grade > 0)
                {
                    VerticalImageSpan VerticalImageSpan = getVerticalImageSpan(data);
                    style.setSpan(VerticalImageSpan, name.length() - 1, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }else if(data.role != RoleUtil.ROLE_PT)
                {
                    VerticalImageSpan VerticalImageSpan = getVerticalImageSpan(data);
                    style.setSpan(VerticalImageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                ForegroundColorSpan colorSpan = getColorSpan(data);
                if (colorSpan != null) {
                    style.setSpan(colorSpan, 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }

                holder.chatName.setText(style);
            }
        }
        if (holder.chatContent != null) {
            if (holder.viewType == ITEM_TYPE_CONTENT) {
                String name = data.name1;
                if (data.grade > 0) {
                    name = name + " :";
                } else if (data.role != RoleUtil.ROLE_PT) {
                    name = " " + name + ":";
                } else {
                    name = name + ":";
                }
                String content = name + data.content;
                SpannableStringBuilder style = new SpannableStringBuilder(content);

                if (data.grade > 0) {
                    VerticalImageSpan VerticalImageSpan = getVerticalImageSpan(data);
                    if (VerticalImageSpan != null) {
                        style.setSpan(VerticalImageSpan, name.length() - 2, name.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    ForegroundColorSpan colorSpan = new ForegroundColorSpan(getGradeColor(data.grade));
                    style.setSpan(colorSpan, 0, content.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else if (data.role != RoleUtil.ROLE_PT) {
                    VerticalImageSpan VerticalImageSpan = getVerticalImageSpan(data);
                    if (VerticalImageSpan != null) {
                        style.setSpan(VerticalImageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    ForegroundColorSpan colorSpan = new ForegroundColorSpan(mContext.getResources().getColor(R.color.common_yellow));
                    style.setSpan(colorSpan, 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    if (mIsShow) {
                        style.setSpan(new ForegroundColorSpan(0x72000000), 0, name.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                    } else {
                        style.setSpan(new ForegroundColorSpan(0xFFC1C1C1), 0, name.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                    }
                }
                holder.chatContent.setText(style);
            } else if (holder.viewType == ITEM_TYPE_GIFT) {
                String content = " 送给";
                if (data.grade > 0) {
                    holder.chatContent.setTextColor(getGradeColor(data.grade));
                } else {
                    holder.chatContent.setTextColor(0Xb0000000);
                }
                holder.chatContent.setText(content);
            } else {
                holder.chatContent.setText(data.content);
            }
        }
        if (holder.vipName != null) {
            if (holder.viewType == ITEM_TYPE_GIFT) {
                if (data.grade > 0) {
                    holder.vipName.setTextColor(getGradeColor(data.grade));
                } else {
                    holder.vipName.setTextColor(0Xb0000000);
                }
            }

            holder.vipName.setText(data.name2);
        }
        if (holder.giftName != null) {
            holder.giftName.setText(data.giftName);
        }
        if (holder.giftNum != null) {
            if (data.giftNum > 0) {
                if (data.grade > 0) {
                    holder.giftNum.setTextColor(getGradeColor(data.grade));
                } else {
                    holder.giftNum.setTextColor(Color.parseColor("#ff7919"));
                }
                holder.giftNum.setText("X" + data.giftNum);
                holder.giftNum.setVisibility(View.VISIBLE);
            } else {
                holder.giftNum.setText("");
                holder.giftNum.setVisibility(View.GONE);
            }
        }
        if (holder.giftImage != null) {
            if (holder.viewType == ITEM_TYPE_GIFT) {
                Glide.with(mContext).load(data.giftPhoto).into(holder.giftImage);
            }
//            else if (holder.viewType == ITEM_TYPE_VIP_JOIN_IN) {
//                if (data.grade >= 3) {
//                    Glide.with(mContext).load(R.drawable.grade_hj).into(holder.giftImage);
//                } else if (data.grade == 2) {
//                    Glide.with(mContext).load(R.drawable.grade_by).into(holder.giftImage);
//                } else if (data.grade == 1) {
//                    Glide.with(mContext).load(R.drawable.grade_qt).into(holder.giftImage);
//                }
//            }
        }

//        Log.i("REUse","---->"+reCount*100/totalCount);
        return convertView;
    }

    private  SpannableStringBuilder getGuardJoinStyle(ChatDataModel data){
        StringBuilder sb = new StringBuilder();
        sb.append("欢迎");
        if (data.grade >= 3) {
            sb.append("黄金守护 " + data.name1);
        } else if (data.grade == 2) {
            sb.append("白银守护 " + data.name1);
        } else if (data.grade == 1) {
            sb.append("青铜守护 " + data.name1);
        }
        int startIndex  =sb.length();
        sb.append(" 进场");
        SpannableStringBuilder style = new SpannableStringBuilder(sb.toString());
        VerticalImageSpan VerticalImageSpan = getVerticalImageSpan(data);
        style.setSpan(VerticalImageSpan, startIndex, startIndex+1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return style;
    }

    private  SpannableStringBuilder getBuyGuardStyle(ChatDataModel data){
        StringBuilder sb = new StringBuilder();
        sb.append(data.name1).append(" 守护了").append(data.name2 + " ");
        SpannableStringBuilder style = new SpannableStringBuilder(sb.toString());
        VerticalImageSpan VerticalImageSpan = getVerticalImageSpan(data);
        style.setSpan(VerticalImageSpan, sb.length() - 1, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return style;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public int getCount() {
        if (mChatList != null) {
            if (mIsShow) {
                return mChatList.size();
            } else {
                return mChatList.size()+1;
            }
        }
        return 0;
    }

    @Override
    public ChatDataModel getItem(int position) {
        if (mChatList != null && position < mChatList.size()) {
            return mChatList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    private LinearLayout newItemView(int itemType) {
        LinearLayout rowLayout = null;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        final ChatViewHolder holder = new ChatViewHolder();
        if (itemType == ITEM_TYPE_CONTENT) {
            rowLayout = (LinearLayout) layoutInflater.inflate(R.layout.chat_list_item_content, null);
            holder.chatContent = (TextView) rowLayout.findViewById(R.id.chatContent);
            holder.viewType = ITEM_TYPE_CONTENT;
            holder.vBg = rowLayout.findViewById(R.id.ll_chat_list_item_content_bg);
            if (mIsShow) {
                holder.chatContent.setTextColor(0Xb0000000);
                Drawable drawable = holder.vBg.getBackground();
                drawable.setAlpha(178);
                holder.vBg.setBackgroundDrawable(drawable);
            }
        } else if (itemType == ITEM_TYPE_GIFT) {
            rowLayout = (LinearLayout) layoutInflater.inflate(R.layout.chat_list_item_gift, null);
            holder.giftImage = (ImageView) rowLayout.findViewById(R.id.giftImage);
            holder.chatName = (TextView) rowLayout.findViewById(R.id.chatName1);
            holder.chatContent = (TextView) rowLayout.findViewById(R.id.textview1);
            holder.vipName = (TextView) rowLayout.findViewById(R.id.textView2);
            holder.giftName = (TextView) rowLayout.findViewById(R.id.giftName);
            holder.giftNum = (TextView) rowLayout.findViewById(R.id.giftNum);
            holder.viewType = ITEM_TYPE_GIFT;
            holder.vBg = rowLayout.findViewById(R.id.ll_chat_list_item_gift_bg);
            if (mIsShow) {
                TextView tv = (TextView) rowLayout.findViewById(R.id.textview1);
                tv.setTextColor(0Xb0000000);
                holder.vipName.setTextColor(0Xb0000000);
                Drawable drawable = holder.vBg.getBackground();
                drawable.setAlpha(178);
                holder.vBg.setBackgroundDrawable(drawable);
            }
        } else if (itemType == ITEM_TYPE_NORMAL_JOIN_IN) {
            rowLayout = (LinearLayout) layoutInflater.inflate(R.layout.chat_list_item_not_vip_join_room, null);
            holder.chatName = (TextView) rowLayout.findViewById(R.id.vipName);
            holder.viewType = ITEM_TYPE_NORMAL_JOIN_IN;
        } else if (itemType == ITEM_TYPE_VIP_JOIN_IN) {
            rowLayout = (LinearLayout) layoutInflater.inflate(R.layout.chat_list_item_vip_join, null);
            holder.mParent = rowLayout.findViewById(R.id.chat_parent);
            holder.chatName = (TextView) rowLayout.findViewById(R.id.chatName);
//            holder.giftImage = (ImageView) rowLayout.findViewById(R.id.giftImage);
            holder.viewType = ITEM_TYPE_VIP_JOIN_IN;
        }
        else if(itemType == ITEM_TYPE_NULL){
            rowLayout = (LinearLayout) layoutInflater.inflate(R.layout.chat_list_item_null, null);
        }else if(itemType == ITEM_TYPE_DEFALT){
            rowLayout = new LinearLayout(mContext);
        }
        if (null == rowLayout) {
            return null;
        }
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        rowLayout.setLayoutParams(params);
        rowLayout.setTag(holder);
        return rowLayout;
    }


    class ChatViewHolder {
        //普通聊天
        TextView chatName;
        TextView chatContent;
        //送礼物
        TextView vipName;
        TextView giftNum;
        TextView giftName;
        ImageView giftImage;
        View vBg;
        View mParent;
        public int viewType;
    }

    private VerticalImageSpan getVerticalImageSpan(ChatDataModel data) {
        VerticalImageSpan imgSpan = null;
        if (data.grade > 0) {
            Drawable d = mContext.getResources().getDrawable(getGradeImg(data.grade));
            d.setBounds(0, 0, mContext.getResources().getDimensionPixelOffset(R.dimen.height_21dp), mContext.getResources().getDimensionPixelOffset(R.dimen.height_14_5dp));
            imgSpan = new VerticalImageSpan(d);
        } else if (data.role != RoleUtil.ROLE_PT) {
            Drawable d = mContext.getResources().getDrawable(R.drawable.chat_yr);
            d.setBounds(0, 0, mContext.getResources().getDimensionPixelOffset(R.dimen.height_24_5dp), mContext.getResources().getDimensionPixelOffset(R.dimen.height_13dp));
            imgSpan = new VerticalImageSpan(d);
        }
        return imgSpan;
    }

    private ForegroundColorSpan getColorSpan(ChatDataModel data) {
        ForegroundColorSpan colorSpan = null;
        if (data.grade > 0) {
            colorSpan = new ForegroundColorSpan(getGradeColor(data.grade));
        } else if (data.role != RoleUtil.ROLE_PT) {
            colorSpan = new ForegroundColorSpan(mContext.getResources().getColor(R.color.common_yellow));
        }
        return colorSpan;
    }

    private int getGradeImg(int grade) {
        if (grade == 1) {
            return R.drawable.grade_qt;
        } else if (grade == 2) {
            return R.drawable.grade_by;
        } else if (grade >= 3) {
            return R.drawable.grade_hj;
        }
        return android.R.color.transparent;
    }

    private int getGradeColor(int grade) {
        if (grade == 1) {
            return Color.parseColor("#719807");
        } else if (grade == 2) {
            return Color.parseColor("#4da4eb");
        } else if (grade >= 3) {
            return Color.parseColor("#ff7919");
        }
        return android.R.color.transparent;
    }
}
