package com.hunantv.mglive.ui.discovery.publisher.emoticon;

import java.lang.reflect.Field;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.utils.L;

public class EmoticonGridViewAdapter extends BaseAdapter {

	private Context mContext = null;
	private int mPageIndex = 0;
	private int mEmoticonIconWidth = 85;
	private int mEmoticonIconHeight = 85;
	
	
	public EmoticonGridViewAdapter(Context c ,int pageIndex) {
		// TODO Auto-generated constructor stub
		mContext = c;
		mPageIndex = pageIndex;
		
		mEmoticonIconWidth = mContext.getResources().getDimensionPixelSize(R.dimen.emoticon_item_size);
		mEmoticonIconHeight = mEmoticonIconWidth;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 21;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	protected int getResourceId(int position){
		 
		if(position == 20)
			return R.drawable.emoticon_delete;
		
		String resourceName = "emoji" + (mPageIndex *20 + position + 1);
		L.d("emoji", "resourceName:" + resourceName + "pageindex:" + mPageIndex + "postion:" + position);
		
		try {
			Field f = (Field) R.drawable.class.getDeclaredField(resourceName);
			int i = f.getInt(R.drawable.class);
			return i;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	// create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(mEmoticonIconWidth, mEmoticonIconHeight));
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

    	L.d("emoji","gridview position:"+position);
    	
        if(getResourceId(position) != -1){
        	imageView.setImageResource(getResourceId(position) );
        }
        
        return imageView;
    }

}
