package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.star.RecomTagStar;
import com.hunantv.mglive.data.star.StarDynamicModel;
import com.hunantv.mglive.data.star.TagStarModel;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.RoleUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2016/1/25.
 */
public class TagStarAdapter extends BaseAdapter {
    private Context mContext;

    private List<RecomTagStar> mRecomtagStars;
    private TagStarListener listener;

    public TagStarAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mRecomtagStars != null ? mRecomtagStars.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mRecomtagStars != null && position < mRecomtagStars.size() ? mRecomtagStars.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(
                    R.layout.layout_tag_star_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_title);
            viewHolder.mTitleBar = (RelativeLayout) convertView.findViewById(R.id.rl_title_bar);

            viewHolder.mStarView = (RelativeLayout) convertView.findViewById(R.id.rl_star_view);
            viewHolder.mPhoto = (ImageView) convertView.findViewById(R.id.iv_photo);
            viewHolder.mRole = (ImageView) convertView.findViewById(R.id.iv_role);
            viewHolder.mName = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.mHots = (TextView) convertView.findViewById(R.id.tv_hots);

            viewHolder.mCoverView1 = (RelativeLayout) convertView.findViewById(R.id.rl_cover1);
            viewHolder.mCover1 = (ImageView) convertView.findViewById(R.id.iv_cover1);
            viewHolder.mPlay1 = (ImageView) convertView.findViewById(R.id.iv_video_play1);

            viewHolder.mCoverView2 = (RelativeLayout) convertView.findViewById(R.id.rl_cover2);
            viewHolder.mCover2 = (ImageView) convertView.findViewById(R.id.iv_cover2);
            viewHolder.mPlay2 = (ImageView) convertView.findViewById(R.id.iv_video_play2);

            viewHolder.mGridStars = (GridView) convertView.findViewById(R.id.gv_star);
            convertView.setTag(viewHolder);
        }

        RecomTagStar recomTagStar = mRecomtagStars.get(position);
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.mTitle.setText(recomTagStar.getTagName());
        viewHolder.mTitleBar.setOnClickListener(new ForwordListener(recomTagStar.getTagName()));

        ArrayList<TagStarModel> tagStarModels = recomTagStar.getArtists();
        if (tagStarModels != null && tagStarModels.size() > 0) {
            TagStarModel tagStarModel = tagStarModels.get(0);
            Glide.with(mContext).load(tagStarModel.getPhoto())
                    .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                    .transform(new GlideRoundTransform(mContext, R.dimen.height_60dp))
                    .into(viewHolder.mPhoto);
            viewHolder.mPhoto.setOnClickListener(new StarListener(tagStarModel.getUid()));
            viewHolder.mRole.setImageResource(RoleUtil.getRoleIcon(tagStarModel.getRole()));
            viewHolder.mName.setText(tagStarModel.getNickName());
            viewHolder.mHots.setText(tagStarModel.getHotValue() + "人气");

            ArrayList<StarDynamicModel> starDynamicModels = tagStarModel.getDynamicInfo();
            if (starDynamicModels != null && starDynamicModels.size() > 0) {
                if (starDynamicModels.size() >= 1) {
                    viewHolder.mCoverView1.setVisibility(View.VISIBLE);
                    StarDynamicModel starDynamicModel = starDynamicModels.get(0);
                    Glide.with(mContext).load(starDynamicModel.getDynamicCover()).centerCrop()
                            .placeholder(R.drawable.default_img_43)
                            .into(viewHolder.mCover1);
                    if (StarDynamicModel.TYPE_D_IMG.equals(starDynamicModel.getdType())) {
                        viewHolder.mPlay1.setVisibility(View.GONE);
                    } else {
                        viewHolder.mPlay1.setVisibility(View.VISIBLE);
                    }
                    viewHolder.mCoverView1.setOnClickListener(new CoverListener(starDynamicModel.getDynamicId()));
                } else {
                    viewHolder.mCoverView1.setVisibility(View.GONE);
                    viewHolder.mCoverView1.setOnClickListener(null);
                }

                if (starDynamicModels.size() >= 2) {
                    viewHolder.mCoverView2.setVisibility(View.VISIBLE);
                    StarDynamicModel starDynamicModel = starDynamicModels.get(1);
                    Glide.with(mContext).load(starDynamicModel.getDynamicCover()).centerCrop()
                            .placeholder(R.drawable.default_img_43)
                            .into(viewHolder.mCover2);
                    if (StarDynamicModel.TYPE_D_IMG.equals(starDynamicModel.getdType())) {
                        viewHolder.mPlay2.setVisibility(View.GONE);
                    } else {
                        viewHolder.mPlay2.setVisibility(View.VISIBLE);
                    }
                    viewHolder.mCoverView2.setOnClickListener(new CoverListener(starDynamicModel.getDynamicId()));
                } else {
                    viewHolder.mCoverView2.setVisibility(View.GONE);
                    viewHolder.mCoverView2.setOnClickListener(null);
                }
                viewHolder.mStarView.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mStarView.setVisibility(View.GONE);
            }

            ArrayList<TagStarModel> gridData = new ArrayList<TagStarModel>();
            gridData.addAll(tagStarModels);
            gridData.remove(0);
            if (gridData.size() > 0) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewHolder.mGridStars.getLayoutParams();
                if (gridData.size() <= 3) {
                    layoutParams.height = mContext.getResources().getDimensionPixelOffset(R.dimen.height_115dp);
                } else {
                    layoutParams.height = mContext.getResources().getDimensionPixelOffset(R.dimen.height_260dp);
                }
                viewHolder.mGridStars.setLayoutParams(layoutParams);
                viewHolder.mGridStars.setAdapter(new StarGridAdapter(mContext, gridData));
                viewHolder.mGridStars.setOnItemClickListener(new StarGridItemListener(gridData));
                viewHolder.mGridStars.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mGridStars.setVisibility(View.GONE);
            }

        }

        return convertView;
    }

    public void setTagStars(List<RecomTagStar> recomTagStars) {
        this.mRecomtagStars = recomTagStars;
        notifyDataSetChanged();
    }

    public void setListener(TagStarListener listener) {
        this.listener = listener;
    }

    private class ViewHolder {
        TextView mTitle;
        RelativeLayout mTitleBar;

        RelativeLayout mStarView;
        ImageView mPhoto;
        ImageView mRole;
        TextView mName;
        TextView mHots;

        RelativeLayout mCoverView1;
        ImageView mCover1;
        ImageView mPlay1;

        RelativeLayout mCoverView2;
        ImageView mCover2;
        ImageView mPlay2;

        GridView mGridStars;
    }

    public interface TagStarListener {
        public void forword(String tagid);

        public void toDynamic(String dynamicId);

        public void toStarRoom(String uid);
    }

    public class ForwordListener implements View.OnClickListener {
        private String tagName;

        public ForwordListener(String tagName) {
            this.tagName = tagName;
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.forword(tagName);
            }
        }
    }

    public class StarListener implements View.OnClickListener {
        private String uid;

        public StarListener(String uid) {
            this.uid = uid;
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.toStarRoom(uid);
            }
        }
    }

    public class CoverListener implements View.OnClickListener {
        private String dynamicId;

        public CoverListener(String dynamicId) {
            this.dynamicId = dynamicId;
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.toDynamic(dynamicId);
            }
        }
    }

    public class StarGridItemListener implements AdapterView.OnItemClickListener {
        private ArrayList<TagStarModel> tagStarModels;

        public StarGridItemListener(ArrayList<TagStarModel> tagStarModels) {
            this.tagStarModels = tagStarModels;
        }


        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TagStarModel tagStarModel = tagStarModels.get(position);
            if (listener != null) {
                listener.toStarRoom(tagStarModel.getUid());
            }
        }
    }
}
