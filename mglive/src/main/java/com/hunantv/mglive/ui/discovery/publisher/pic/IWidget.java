package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.content.Intent;

public interface IWidget {
	public boolean onActivityResult(int code, Intent intent);
}
