package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.hunantv.mglive.R;
import com.hunantv.mglive.ui.entertainer.ContributionBottomView;
import com.hunantv.mglive.ui.entertainer.data.ContrData;
import com.hunantv.mglive.ui.entertainer.data.ContributionInfoData;
import com.hunantv.mglive.utils.L;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2015/12/16.
 */
public class ContributionPageAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener{
    private int payType;
    private ContributionInfoData contrInfo;
    private Context mContext;
    private LinearLayout dotLayout;
    private PayLinstener payLinstener;

    private List<ContrData> contrDatas;
    private List<View> viewList = new ArrayList<>();
    private List<View> dotViewList = new ArrayList<>();
    private ViewHold viewHold;
    private int oldPosition = 0;//记录上一次点的位置
    public ContributionPageAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public View getView(final ContrData contrData) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View convertView = inflater.inflate(R.layout.fragment_contribution_page, null);
        viewHold = new ViewHold();
        viewHold.iv_contr_type = (ImageView) convertView.findViewById(R.id.iv_contr_type);
        viewHold.payBtn = (Button) convertView.findViewById(R.id.payBtn);
        viewHold.tv_type = (TextView) convertView.findViewById(R.id.tv_type);
        viewHold.shNum = (TextView) convertView.findViewById(R.id.shNum);
        viewHold.shText = (TextView) convertView.findViewById(R.id.shText);
        viewHold.personText = (TextView) convertView.findViewById(R.id.personText);
        viewHold.contrData = contrData;
        initView(viewHold);
        convertView.setTag(viewHold);
        return convertView;
    }

    public void initView(ViewHold viewHold)
    {
        ContrData contrData = viewHold.contrData;
        if(payType == ContributionBottomView.PAY_TYPE_BUY)
        {
            viewHold.shText.setText("守护");
        }else if(payType == ContributionBottomView.PAY_TYPE_RENEW)
        {
            viewHold.shText.setText("升级");
        }

        viewHold.payBtn.setOnClickListener(new PayListener(contrData));

        Glide.with(mContext).load(contrData.getIcon()).asBitmap().into( viewHold.iv_contr_type);

        viewHold.shNum.setText(contrData.getCyc()+"");
        if(payType == ContributionBottomView.PAY_TYPE_BUY)
        {
            viewHold.tv_type.setText(contrData.getName());
        }else if(payType == ContributionBottomView.PAY_TYPE_RENEW)
        {
            String tyeText = contrData.getName();
            int level = Integer.parseInt(contrInfo.getGradeLevel())+contrData.getCyc();
            if(level >=3)
            {
                tyeText = "黄金守护";
            }else if(level >=2){
                tyeText = "白银守护";
            }else if(level >=1){
                tyeText = "青铜守护";
            }
            viewHold.tv_type.setText(tyeText);
        }else{
            viewHold.tv_type.setText(contrData.getName());
        }
        viewHold.payBtn.setText("仅" + contrData.getPrice() + "金币");

    }

    @Override
    public int getCount() {
        return contrDatas != null ? contrDatas.size() : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return  view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)   {
       View convertView =  container.getChildAt(position);
        container.removeView(convertView);//删除页卡
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {  //这个方法用来实例化页卡
        ContrData contrData = contrDatas.get(position);
        View convertView = null;
        if(position <= viewList.size() - 1){
            convertView = viewList.get(position);
        }else
        {
            convertView = getView(contrData);
            viewList.add(convertView);
        }
        int childViewHeight = convertView.getHeight();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, childViewHeight );//这里设置params的高度。
        container.addView(convertView, position,params);//添加页卡
        return convertView;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        int dotSize = dotLayout.getChildCount();
        if(oldPosition < dotSize  && position < dotSize)
        {
            dotViewList.get(oldPosition).setBackgroundResource(R.drawable.dot_normal);
            dotViewList.get(position).setBackgroundResource(R.drawable.dot_focused);
            oldPosition = position;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class ViewHold
    {
        ImageView iv_contr_type;
        Button payBtn;
        TextView tv_type,shNum,shText,personText;
        ContrData contrData;
    }

    private class PayListener implements View.OnClickListener{
        private ContrData contrData;

        public PayListener(ContrData contrData) {
            this.contrData = contrData;
        }

        @Override
        public void onClick(View v) {
            payLinstener.pay(contrData);
        }
    }

    public interface PayLinstener{
        public void pay(ContrData contrData);
    }

    public void setPayLinstener(PayLinstener payLinstener) {
        this.payLinstener = payLinstener;
    }

    public void setContrDatas(List<ContrData> contrDatas) {
        this.contrDatas = contrDatas;
        initDots();
        notifyDataSetChanged();
    }

    public void setDotLayout(LinearLayout dotLayout) {
        this.dotLayout = dotLayout;
    }

    private void initDots()
    {
        if(dotLayout != null && contrDatas != null)
        {
            dotLayout.removeAllViews();
            dotViewList.clear();
            for (int i=0;i<contrDatas.size();i++){
                View view = new View(mContext);
                int margin = (int) mContext.getResources().getDimension(R.dimen.margin_4dp);
                int weight = (int) mContext.getResources().getDimension(R.dimen.margin_6_5dp);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(weight,weight);
                params.setMargins(margin, 0, margin, 0);
                view.setLayoutParams(params);
                if(i == 0)
                {
                    view.setBackgroundResource(R.drawable.dot_focused);
                }else
                {
                    view.setBackgroundResource(R.drawable.dot_normal);
                }

                dotViewList.add(view);
                dotLayout.addView(view);
            }
        }
    }

    public void setPayType(int payType,ContributionInfoData contrInfo) {
        this.payType = payType;
        this.contrInfo = contrInfo;
    }
}
