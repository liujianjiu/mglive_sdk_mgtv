package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author maxxiang
 * 图片item
 */
public class PicItem implements Parcelable{
	public String mPathOrignal;	//原始图的路径
	public String mPathThumb;   //此参数暂不用
	public long mIdOrginal;
	
	public PicItem() {
		this(0,"","");
	}

	public PicItem(long id, final String strOrignal, String strThumb){
		mIdOrginal = id;
		mPathOrignal = strOrignal;
		mPathThumb = strThumb;
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mPathOrignal);
		dest.writeString(mPathThumb);
		dest.writeLong(mIdOrginal);
		
	}

}
