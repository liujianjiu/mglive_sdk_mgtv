package com.hunantv.mglive.ui.entertainer.data;

import com.hunantv.mglive.data.StarModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2015/12/11.
 */
public class StarTagData {
    private String uid;

    private String tagname;

    private String nickName;

    private String photo;

    private long count;

    private int role;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTagname() {
        return tagname;
    }

    public void setTagname(String tagname) {
        this.tagname = tagname;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public static List<StarModel> toStarModels(List<StarTagData> tags)
    {
        List<StarModel> stars = new ArrayList<StarModel>();
        if(tags != null)
        {
            for(StarTagData tag : tags){
                StarModel star = new StarModel();
                star.setUid(tag.getUid());
                star.setNickName(tag.getNickName());
                star.setPhoto(tag.getPhoto());
                star.setRole(tag.getRole());
                stars.add(star);
            }
        }
        return stars;
    }
}
