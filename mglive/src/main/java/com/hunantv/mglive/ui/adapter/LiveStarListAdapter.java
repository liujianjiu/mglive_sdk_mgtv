package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.nfc.Tag;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.RoleUtil;
import com.hunantv.mglive.utils.StringUtil;

import java.util.List;

/**
 * Created by 达 on 2015/11/13.
 */
public class LiveStarListAdapter extends BaseAdapter {
    private Context context;
    private List<StarModel> mLiveList;

    public LiveStarListAdapter(Context context) {
        this.context = context;
    }


    @Override
    public int getCount() {
        return mLiveList != null ? mLiveList.size() : 0;
    }

    @Override
    public Fragment getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHold viewHold;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.fragment_all_star_live_star_fragment, null);
            viewHold = new ViewHold();
            viewHold.mLiveIcon = (ImageView) convertView.findViewById(R.id.iv_all_star_live_item_bg);
            viewHold.mFaceIcon = (ImageView) convertView.findViewById(R.id.iv_all_star_live_star_icon);
            viewHold.mStarName = (TextView) convertView.findViewById(R.id.tv_all_star_live_star_name);
            viewHold.mTipsIcon = (ImageView) convertView.findViewById(R.id.iv_all_star_live_star_tips);
            convertView.setTag(viewHold);
        } else {
            viewHold = (ViewHold) convertView.getTag();
        }
        StarModel starData = mLiveList.get(position);
        L.d("LiveStarListAdapter_COVER", starData.getCover());
        Glide.with(context).load(starData.getCover()).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.default_img_43).into(viewHold.mLiveIcon);
        Glide.with(context).load(StringUtil.isNullorEmpty(starData.getPhoto()) ? R.drawable.default_icon : starData.getPhoto())
                .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                .transform(new GlideRoundTransform(context, R.dimen.height_27dp)).into(viewHold.mFaceIcon);
        viewHold.mStarName.setText(starData.getNickName());
        viewHold.mTipsIcon.setImageResource(RoleUtil.getRoleIcon(starData.getRole()));
        return convertView;
    }

    class ViewHold {
        ImageView mLiveIcon;
        ImageView mFaceIcon;
        TextView mStarName;
        ImageView mTipsIcon;
    }

    public void setmLiveList(List<StarModel> mLiveList) {
        this.mLiveList = mLiveList;
    }
}