package com.hunantv.mglive.ui.discovery.publisher;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import com.hunantv.mglive.common.BaseActivity;
import com.hunantv.mglive.ui.discovery.publisher.pic.PicWidget;
import com.hunantv.mglive.widget.Toast.LoadingProgressBarWithCancelController;

import java.util.ArrayList;

/**
 * @author maxxiang 发布器主界面 Create On: 2015-12-9
 */
public class PublisherActivity extends BaseActivity {
    private TableLayout mTableLayout;
    private PicWidget mPicWidget; // 图片的布局widget
    private EditText mEditText;

    private ImageButton mBtnEmoticon;
    private ImageButton mBtnKeyboard;
    private boolean mShowEmoticonPanel = false;
    private ViewPager mEmoticonViewPager;
    private View mEmoticonPanel;
    private Button mSendBtn;
    private String mCircleId = null;
    private String mLastCameraFileName = null;

    private int nSendType;        //发送类型：文字？图片？视频?


    //文件上传用
    private String strSendText; //发送的文字内容
    private String strVideoUrl; //视频url地址
    private ArrayList<String> imagePathList;   //文件名数组
    private ArrayList<String> fileKeyList;       //文件UUID
    private String bucket;
    private int mSuccessCount = 0; //上传成功的个数

    //maxxiang : 重新写了公共的Toast 如下, 这个就不用了。 private Toast mToast; //提示框 ，先写个简单的，
    LoadingProgressBarWithCancelController mToast;

    public static final int REQUESTCODE_CANCEL = 2;


}
