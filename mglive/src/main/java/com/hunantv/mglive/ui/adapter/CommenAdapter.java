package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hunantv.mglive.data.discovery.DynamicCommen;
import com.hunantv.mglive.ui.discovery.dynamic.ComCommen;

import java.util.List;

/**
 * Created by guojun3 on 2015-12-16.
 */
public class CommenAdapter extends BaseAdapter {
    private List<Object> mList;
    private Context mContext;

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public CommenAdapter(Context ctx, List<Object> mList) {
        this.mList = mList;
        setContext(ctx);
    }

    @Override
    public int getCount() {
        if (null != getList()) {
            return getList().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (getList() != null && position < getList().size()) {
            return getList().get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder mHolder;
        ComCommen mCommen;
        DynamicCommen data = (DynamicCommen) getItem(position);
        if (null == convertView || !(convertView instanceof ComCommen)) {
            mHolder = new ViewHolder();
            mCommen = new ComCommen(getContext());
            float den = getContext().getResources().getDisplayMetrics().density;
            mCommen.setPadding(0, (int)(5*den), (int)(7*den), (int)(5*den));
            mCommen.setLayoutParams(new ListView.LayoutParams(-1, -2));
            mHolder.mName = mCommen.getNickName();
            mHolder.mMsgDetails = mCommen.getMsgDetails();
        } else {
            mCommen = (ComCommen) convertView;
            mHolder = (ViewHolder) mCommen.getTag();
        }
        mHolder.mName.setText(data.getNickName() + "：");
        mHolder.mMsgDetails.setText(data.getContent());
        mCommen.setTag(mHolder);
        return mCommen;
    }

    class ViewHolder {
        TextView mName;
        TextView mMsgDetails;
        TextView mTime;
        ImageView mHead;
    }

    public List<Object> getList() {
        return mList;
    }

    public void setList(List<Object> mList) {
        this.mList = mList;
    }

}
