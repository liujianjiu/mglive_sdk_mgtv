package com.hunantv.mglive.ui.live.liveinterface;

public interface OnStarLiveCallBack {
    void onUpdatePushUrl();

    void onAuthFailure();

    void onStarLiveError();

    void onStarLiveStart();

}