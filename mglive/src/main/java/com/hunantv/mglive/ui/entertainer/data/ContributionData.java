package com.hunantv.mglive.ui.entertainer.data;

/**
 * Created by admin on 2015/12/3.
 */
public class ContributionData {
    private long gid;           //守护id
    private String uid;         //用户id
    private String nickName;    //昵称
    private String photo;       //头像
    private int grade;     //守护等级
    private String date;        //守护日期
    private long devoteValue;   //贡献值

    public long getGid() {
        return gid;
    }

    public void setGid(long gid) {
        this.gid = gid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getDevoteValue() {
        return devoteValue;
    }

    public void setDevoteValue(long devoteValue) {
        this.devoteValue = devoteValue;
    }

}
