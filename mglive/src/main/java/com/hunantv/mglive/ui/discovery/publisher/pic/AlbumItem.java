package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author maxxiang
 * 相册item, 下一界面优化选择器扩展需要，先给出定义
 * Create On: 2015-12-9
 */
public class AlbumItem implements Parcelable{
	public String mAlbumName; 
	public int mCount;

	protected AlbumItem(Parcel in) {
		mAlbumName = in.readString();
		mCount = in.readInt();
	}

	public static final Creator<AlbumItem> CREATOR = new Creator<AlbumItem>() {
		@Override
		public AlbumItem createFromParcel(Parcel in) {
			return new AlbumItem(in);
		}

		@Override
		public AlbumItem[] newArray(int size) {
			return new AlbumItem[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(mAlbumName);
		dest.writeLong(mCount);
	}

}
