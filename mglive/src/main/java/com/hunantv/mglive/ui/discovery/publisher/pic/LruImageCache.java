package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.LruCache;

import com.hunantv.mglive.utils.L;


/**
 * @author maxxiang
 * 用于bitmap的Memory Cache, 此类不对UI开放，仅提供给ImageCacheManager用
 * 此类需对外开放啦，其它地方需要用到

 */
public class LruImageCache extends LruCache<String, Bitmap>  {
	private final String TAG = this.getClass().getSimpleName();
	
	public LruImageCache(int maxSize) {
		super(maxSize);
	}

	protected int sizeOf(String key, Bitmap value) {
		return value.getRowBytes() * value.getHeight();
	}	

	public Bitmap getBitmap(String url) {
		return get(url);
	}

	public void putBitmap(String url, Bitmap bitmap) {
		int n = size();
		int j = putCount();		
		put(url, bitmap);
	}

	@Override
	protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
		
		if(evicted){
			L.d("img", "maxsize:" + maxSize() + "size:" + size() + "  removed from cache: " + key);
		}
	}
}
