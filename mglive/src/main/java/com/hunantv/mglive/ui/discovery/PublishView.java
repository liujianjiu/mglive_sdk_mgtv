package com.hunantv.mglive.ui.discovery;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.hunantv.mglive.utils.Toast;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.BaseBar;
import com.hunantv.mglive.common.Constant;

/**
 * Created by June Kwok on 2015-11-25.
 */
public class PublishView extends RelativeLayout {

    public static final int MAX_LENTH = 140;
    Bar mBar;
    ScrollView mScroll;


    public PublishView(Context context) {
        super(context);
        setBackgroundColor(0xEBF3F5F9);
        init();
    }

    private void init() {
        LayoutParams mBarLP = new LayoutParams(LayoutParams.MATCH_PARENT, Constant.toPix(128));
        mBarLP.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        mBar = new Bar(getContext());
        mBar.setLayoutParams(mBarLP);
        mBar.setId(R.id.status_bar);

        mScroll = new ScrollView(getContext());
        LayoutParams mScrollLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        mScrollLP.addRule(RelativeLayout.BELOW, R.id.status_bar);
        mScroll.setLayoutParams(mScrollLP);

        FrameLayout.LayoutParams mRcmdInfoLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        mRcmdInfoLP.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
        Content mRcmdInfo = new Content(getContext());
        mRcmdInfo.setLayoutParams(mRcmdInfoLP);
        mScroll.addView(mRcmdInfo);

        addView(mBar);
        addView(mScroll);
    }

    /**
     * 中间发布部分
     */
    class Content extends FrameLayout implements TextWatcher {
        EditText mEditTxt;
        TextView mCount;

        public Content(Context context) {
            super(context);
            init();
        }

        private void init() {
            LayoutParams mEditTxtLP = new LayoutParams(LayoutParams.MATCH_PARENT, Constant.toPix(394));
            mEditTxt = new EditText(getContext());
            mEditTxt.setHint("给大家一起分享什么…");
            mEditTxt.setHintTextColor(0xFFB7B7B7);
            mEditTxt.setSingleLine(false);
            mEditTxt.setGravity(Gravity.LEFT | Gravity.TOP);
            mEditTxt.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(30));
            mEditTxt.setBackgroundColor(0xCCFFFFFF);
            mEditTxt.setLayoutParams(mEditTxtLP);
            mEditTxt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENTH)});
            mEditTxt.setPadding(Constant.toPix(23), Constant.toPix(18), Constant.toPix(23), Constant.toPix(18));
            mEditTxt.addTextChangedListener(this);


            LayoutParams mCountLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            mCountLP.gravity = Gravity.RIGHT | Gravity.TOP;
            mCountLP.topMargin = mEditTxtLP.height + Constant.toPix(3);
            mCountLP.rightMargin = Constant.toPix(15);
            mCount = new TextView(getContext());
            mCount.setText("140");
            mCount.setTextColor(0xFF999999);
            mCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(24));
            mCount.setLayoutParams(mCountLP);

            addView(mEditTxt);
            addView(mCount);

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mCount.setText(MAX_LENTH - s.length() + "");
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    /**
     * 顶部的Bar
     */
    class Bar extends FrameLayout {
        BaseBar mBar;

        public Bar(Context context) {
            super(context);
            setBackgroundColor(0xFF242831);
            init();
        }

        private void init() {
            LayoutParams mBarLP = new LayoutParams(LayoutParams.MATCH_PARENT, Constant.toPix(70));
            mBarLP.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
            mBar = new BaseBar(getContext(), "");
            mBar.mBackIcon.setVisibility(GONE);
            mBar.mShareIcon.setVisibility(GONE);


            LayoutParams mCancleLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            mCancleLP.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
            mCancleLP.leftMargin = Constant.toPix(28);
            TextView mCancle = new TextView(getContext());
            mCancle.setText("取消");
            mCancle.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(32));
            mCancle.setTextColor(0xFFFFFFFF);
            mCancle.setLayoutParams(mCancleLP);
            mCancle.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ((Activity) Bar.this.getContext()).finish();
                    } catch (Exception e) {
                    }
                }
            });

            LayoutParams mPubLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            mPubLP.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
            mPubLP.rightMargin = Constant.toPix(28);
            TextView mPub = new TextView(getContext());
            mPub.setText("发布");
            mPub.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(32));
            mPub.setTextColor(0xFF56BAFC);
            mPub.setLayoutParams(mPubLP);

            mBar.addView(mCancle);
            mBar.addView(mPub);
            addView(mBar, mBarLP);
        }
    }

    /**
     * 推荐的内容
     */
    class RcmdInfo extends LinearLayout {
        TextView mTopTxt;


        public RcmdInfo(Context context) {
            super(context);
            setOrientation(VERTICAL);
            init();
        }

        private void init() {
            LayoutParams mTopTxtLP = new LayoutParams(Constant.toPix(300), LayoutParams.WRAP_CONTENT);
            mTopTxtLP.topMargin = Constant.toPix(40);
            mTopTxtLP.bottomMargin = Constant.toPix(40);
            mTopTxtLP.gravity = Gravity.CENTER_HORIZONTAL;
            mTopTxt = new TextView(getContext());
            mTopTxt.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(28));
            mTopTxt.setTextColor(0xFF999999);
            mTopTxt.setText("还没有关注任何艺人哦~\n" +
                    "         看看推荐吧");
            mTopTxt.setLayoutParams(mTopTxtLP);

            addView(mTopTxt);

            LayoutParams mBannerLP = new LayoutParams(LayoutParams.MATCH_PARENT, Constant.toPix(80));
            mBannerLP.gravity = Gravity.CENTER_HORIZONTAL;
            Banner mBanner = new Banner(getContext());
            addView(mBanner, mBannerLP);


            LayoutParams mItemInfoLP = new LayoutParams(LayoutParams.MATCH_PARENT, Constant.toPix(210 - 80));
            mItemInfoLP.gravity = Gravity.CENTER_HORIZONTAL;
            for (int i = 0; i < 10; i++) {
                addView(new ItemInfo(getContext()), mItemInfoLP);
            }
        }
    }

    /**
     * 标题栏
     */
    class Banner extends FrameLayout implements OnClickListener {

        ImageView mLeft;
        TextView mSugest;
        TextView mChange;
        ImageView mChangeBtn, mLine;

        public Banner(Context context) {
            super(context);
            setBackgroundColor(0xFFFFFFFF);
            setPadding(Constant.toPix(15), 0, Constant.toPix(15), 0);
            init();
        }

        void init() {
            LayoutParams mLeftLP = new LayoutParams(Constant.toPix(3), Constant.toPix(30));
            mLeftLP.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
            mLeft = new ImageView(getContext());
            mLeft.setBackgroundColor(0xFFFF7919);
            mLeft.setLayoutParams(mLeftLP);
            addView(mLeft);

            LayoutParams mChangeBtnLP = new LayoutParams(Constant.toPix(33), Constant.toPix(28));
            mChangeBtnLP.gravity = Gravity.CENTER_VERTICAL | Gravity.RIGHT;
            mChangeBtnLP.rightMargin = Constant.toPix(90);
            mChangeBtn = new ImageView(getContext());
            mChangeBtn.setImageResource(R.drawable.max_10_rccmd_refresh);
            mChangeBtn.setLayoutParams(mChangeBtnLP);
            mChangeBtn.setOnClickListener(this);
            addView(mChangeBtn);

            LayoutParams mSugestLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            mSugestLP.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
            mSugestLP.leftMargin = Constant.toPix(12);
            mSugest = new TextView(getContext());
            mSugest.setText("推荐关注");
            mSugest.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(30));
            mSugest.setTextColor(0xFF333333);
            mSugest.setLayoutParams(mSugestLP);
            addView(mSugest);

            LayoutParams mChangeLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            mChangeLP.gravity = Gravity.CENTER_VERTICAL | Gravity.RIGHT;
            mChangeLP.leftMargin = Constant.toPix(12);
            mChange = new TextView(getContext());
            mChange.setText("换一批");
            mChange.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(26));
            mChange.setTextColor(0xFF666666);
            mChange.setLayoutParams(mChangeLP);
            mChange.setOnClickListener(this);
            addView(mChange);


            LayoutParams mLineLP = new LayoutParams(LayoutParams.MATCH_PARENT, Constant.toPix(2));
            mLineLP.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
            mLine = new ImageView(getContext());
            mLine.setBackgroundColor(0xFFECECEC);
            mLine.setLayoutParams(mLineLP);
            addView(mLine);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(), "数据刷新成功!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 单挑数据
     */
    class ItemInfo extends FrameLayout implements OnClickListener {

        ImageView mHeadIcon, mStart;
        TextView mName;
        TextView mTag;
        ImageView mCheckBtn, mLine;
        boolean isChecked = true;

        public ItemInfo(Context context) {
            super(context);
            setBackgroundColor(0xFFFFFFFF);
            setPadding(Constant.toPix(15), 0, Constant.toPix(15), 0);
            init();
            setOnClickListener(this);
        }

        void init() {
            LayoutParams mHeadIconLP = new LayoutParams(Constant.toPix(100), Constant.toPix(100));
            mHeadIconLP.gravity = Gravity.CENTER;
            mHeadIcon = new ImageView(getContext());
            mHeadIcon.setImageResource(R.drawable.max_10_head);
            mHeadIcon.setLayoutParams(mHeadIconLP);

            LayoutParams mStartLP = new LayoutParams(Constant.toPix(22), Constant.toPix(22));
            mStartLP.gravity = Gravity.RIGHT | Gravity.BOTTOM;
            mStartLP.rightMargin = Constant.toPix(10);
            mStart = new ImageView(getContext());
            mStart.setImageResource(R.drawable.max_10_rcmd_start);
            mStart.setLayoutParams(mStartLP);

            LayoutParams mHeadFrameLP = new LayoutParams(Constant.toPix(100), Constant.toPix(100));
            mHeadFrameLP.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
            FrameLayout mHeadFrame = new FrameLayout(getContext());
            mHeadFrame.setLayoutParams(mHeadFrameLP);
            mHeadFrame.addView(mHeadIcon);
            mHeadFrame.addView(mStart);

            LayoutParams mCheckBtnLP = new LayoutParams(Constant.toPix(40), Constant.toPix(40));
            mCheckBtnLP.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
            mCheckBtnLP.rightMargin = Constant.toPix(15);
            mCheckBtn = new ImageView(getContext());
            mCheckBtn.setImageResource(R.drawable.max_10_rccmd_checked);
            mCheckBtn.setLayoutParams(mCheckBtnLP);
            mCheckBtn.setOnClickListener(this);

            LayoutParams mLinearNameLP = new LayoutParams(Constant.toPix(600), LayoutParams.WRAP_CONTENT);
            mLinearNameLP.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
            LinearLayout mLinearName = new LinearLayout(getContext());
            mLinearName.setOrientation(LinearLayout.VERTICAL);
            mLinearName.setLayoutParams(mLinearNameLP);


            LinearLayout.LayoutParams mNameLP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            mName = new TextView(getContext());
            mName.setText("黄婷婷");
            mName.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(28));
            mName.setTextColor(0xFF333333);
            mName.setLayoutParams(mNameLP);

            LinearLayout.LayoutParams mTagLP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            mTag = new TextView(getContext());
            mTag.setText("人气:4566 丨 粉丝:456");
            mTag.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(22));
            mTag.setTextColor(0xFF999999);
            mTag.setLayoutParams(mTagLP);

            mLinearName.addView(mName);
            mLinearName.addView(mTag);

            LayoutParams mLineLP = new LayoutParams(Constant.toPix(600), Constant.toPix(2));
            mLineLP.gravity = Gravity.RIGHT | Gravity.BOTTOM;
            mLine = new ImageView(getContext());
            mLine.setBackgroundColor(0xFFECECEC);
            mLine.setLayoutParams(mLineLP);


            addView(mHeadFrame);
            addView(mLinearName);
            addView(mCheckBtn);
            addView(mLine);
        }

        @Override
        public void onClick(View v) {
            isChecked = !isChecked;
            mCheckBtn.setImageResource(isChecked ? R.drawable.max_10_rccmd_checked : R.drawable.max_10_rccmd_uncheck);
        }
    }
}
