package com.hunantv.mglive.ui.entertainer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.BaseFragment;
import com.hunantv.mglive.ui.discovery.DynamicLayout;

/**
 * Created by admin on 2015/11/4.
 */
public class DynamicView {
    private FullAgreeView parentView;
    private Context context;
    private BaseFragment mFragment;
    private ViewGroup rootView;
    private TextView clickView;
    private LinearLayout closeView;
    private int firstitem = -1;
    private boolean isClose = false;
    public TextView mTitle;

    public DynamicLayout mDynamic = null;
    public DynamicLayout.DynamicList mDynamicList = null;

    public DynamicView(Context context, BaseFragment mFragment, FullAgreeView parentView) {
        this.mFragment = mFragment;
        this.parentView = parentView;
        this.context = context;
        initView();
    }

    private void initView() {
        if (rootView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            rootView = (ViewGroup) layoutInflater.inflate(R.layout.layout_star_dynamic_view, null);
            mTitle = (TextView) rootView.findViewById(R.id.dynamic_title);
            //导入动态View
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            layoutParams.addRule(RelativeLayout.BELOW, R.id.dunamicTopLayout);
            mDynamic = new DynamicLayout(context, null);
            rootView.addView(mDynamic, layoutParams);
            mDynamicList = mDynamic.getDynamicList();

            mDynamicList.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    if (firstitem == 0 && mDynamicList.getChildAt(0).getTop() == mDynamicList.getPaddingTop()) {
                        if (isClose && scrollState == SCROLL_STATE_IDLE) {
                            parentView.closeFullArgeeView();
                            clickView.setVisibility(View.VISIBLE);
                        }
                        isClose = true;
                    } else {
                        isClose = false;
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    firstitem = firstVisibleItem;
                }
            });

            //实现半屏状态下上滑全屏
            clickView = new TextView(context);
            RelativeLayout.LayoutParams clickViewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            clickViewLayoutParams.addRule(RelativeLayout.BELOW, R.id.dunamicTopLayout);
            clickView.setLayoutParams(clickViewLayoutParams);
            rootView.addView(clickView);
            rootView.setClickable(true);
            initView(rootView);
        }
    }

    public View getView() {
        if (rootView == null) {
            initView();
        }
        return rootView;
    }

    void initView(View view) {
//        clickView = (TextView) view.findViewById(R.id.clickView);
        clickView.setOnTouchListener(new View.OnTouchListener() {
            int i = 200;
            float startY = 0;
            float endY = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    // 主点按下
                    case MotionEvent.ACTION_DOWN:
                        startY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        endY = event.getRawY();
                        float transY = Math.abs(endY - startY);
                        if (Math.abs(transY) > i) {
                            if (startY > endY) {
                                //上滑
                                parentView.fullAgreeBellowView();
                                clickView.setVisibility(View.GONE);
                            } else {
                                //下滑
                            }
                        }
                        return true;
                }
                return false;

            }
        });

        closeView = (LinearLayout) view.findViewById(R.id.closeView);
        closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentView.closeView();
                clickView.setVisibility(View.VISIBLE);
            }
        });
    }


    public void setClickViewVisible() {
        clickView.setVisibility(View.VISIBLE);
    }
}
