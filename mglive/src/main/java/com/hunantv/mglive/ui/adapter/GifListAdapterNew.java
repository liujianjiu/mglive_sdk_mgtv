package com.hunantv.mglive.ui.adapter;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.data.GiftDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by June Kwok on 2015/11/25.
 */
public class GifListAdapterNew extends BaseAdapter {
    private Fragment mFragment;
    private List<GiftDataModel> mListData;

    public GifListAdapterNew(List<GiftDataModel> list, Fragment frg) {
        mFragment = frg;
        mListData = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        List<GiftViewHolder> giftViewHolders;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mFragment.getActivity());
            convertView = (LinearLayout) layoutInflater.inflate(
                    R.layout.gift_list_item, null);
            AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                    AbsListView.LayoutParams.MATCH_PARENT,
                    AbsListView.LayoutParams.WRAP_CONTENT);
            convertView.setLayoutParams(params);
            giftViewHolders = new ArrayList<GiftViewHolder>();
            GiftViewHolder giftViewHolder1 = new GiftViewHolder();
            giftViewHolder1.giftItemLayout = (RelativeLayout) convertView.findViewById(R.id.giftItemLayout1);
            giftViewHolder1.giftImage = (ImageView) convertView
                    .findViewById(R.id.giftImage1);
            giftViewHolder1.giftNameText = (TextView) convertView
                    .findViewById(R.id.giftNameText1);
            giftViewHolder1.giftPriceText = (TextView) convertView
                    .findViewById(R.id.giftPriceText1);
            giftViewHolder1.giftPriceLayout = (LinearLayout) convertView.findViewById(R.id.giftPriceLayout1);
            giftViewHolders.add(giftViewHolder1);

            GiftViewHolder giftViewHolder2 = new GiftViewHolder();
            giftViewHolder2.giftItemLayout = (RelativeLayout) convertView.findViewById(R.id.giftItemLayout2);
            giftViewHolder2.giftImage = (ImageView) convertView
                    .findViewById(R.id.giftImage2);
            giftViewHolder2.giftNameText = (TextView) convertView
                    .findViewById(R.id.giftNameText2);
            giftViewHolder2.giftPriceText = (TextView) convertView
                    .findViewById(R.id.giftPriceText2);
            giftViewHolder2.giftPriceLayout = (LinearLayout) convertView.findViewById(R.id.giftPriceLayout2);
            giftViewHolders.add(giftViewHolder2);

            GiftViewHolder giftViewHolder3 = new GiftViewHolder();
            giftViewHolder3.giftItemLayout = (RelativeLayout) convertView.findViewById(R.id.giftItemLayout3);
            giftViewHolder3.giftImage = (ImageView) convertView
                    .findViewById(R.id.giftImage3);
            giftViewHolder3.giftNameText = (TextView) convertView
                    .findViewById(R.id.giftNameText3);
            giftViewHolder3.giftPriceText = (TextView) convertView
                    .findViewById(R.id.giftPriceText3);
            giftViewHolder3.giftPriceLayout = (LinearLayout) convertView.findViewById(R.id.giftPriceLayout3);
            giftViewHolders.add(giftViewHolder3);

            GiftViewHolder giftViewHolder4 = new GiftViewHolder();
            giftViewHolder4.giftItemLayout = (RelativeLayout) convertView.findViewById(R.id.giftItemLayout4);
            giftViewHolder4.giftImage = (ImageView) convertView
                    .findViewById(R.id.giftImage4);
            giftViewHolder4.giftNameText = (TextView) convertView
                    .findViewById(R.id.giftNameText4);
            giftViewHolder4.giftPriceText = (TextView) convertView
                    .findViewById(R.id.giftPriceText4);
            giftViewHolder4.giftPriceLayout = (LinearLayout) convertView.findViewById(R.id.giftPriceLayout4);
            giftViewHolders.add(giftViewHolder4);
            convertView.setTag(giftViewHolders);
        } else {
            giftViewHolders = (List<GiftViewHolder>) convertView.getTag();
        }
        int index = position * giftViewHolders.size();
        for (int i = 0; i < giftViewHolders.size(); i++) {
            GiftViewHolder giftViewHolder = giftViewHolders.get(i);
            if (index <= mListData.size() - 1) {
                GiftDataModel giftData = mListData.get(index);
                initGiftHoldView(giftViewHolder, giftData);
                changeGiftHoldView(giftViewHolder, View.VISIBLE);
            } else {
                giftViewHolder.giftItemLayout.setTag(null);
                changeGiftHoldView(giftViewHolder, View.GONE);
            }
            index = index + 1;
        }
        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public int getCount() {
        int count = mListData.size() / 4;
        int size = mListData.size() % 4;
        if (size != 0) {
            count++;
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (mListData != null) {
            return mListData;
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 初始化单个礼物Item
     *
     * @param giftViewHolder
     * @param giftData
     */
    void initGiftHoldView(GiftViewHolder giftViewHolder, GiftDataModel giftData) {
//        giftViewHolder.giftImage.setImageDrawable(mFragment.getResources().getDrawable(giftData.giftImage));
//        giftViewHolder.giftNameText.setText(giftData.giftName);
//        giftViewHolder.giftPriceText.setText(giftData.giftPrice + "");
//        changeGiftHoldView(giftViewHolder, View.VISIBLE);
//        giftViewHolder.giftItemLayout.setTag(giftData);
//        giftViewHolder.giftItemLayout.setOnClickListener(giftItemClickListener);
//        giftViewHolder.giftItemLayout.setOnLongClickListener(giftItemLongClickListener);
    }

    void changeGiftHoldView(GiftViewHolder giftViewHolder, int type) {
        giftViewHolder.giftImage.setVisibility(type);
        giftViewHolder.giftNameText.setVisibility(type);
        giftViewHolder.giftPriceLayout.setVisibility(type);
    }

    class GiftViewHolder {
        public RelativeLayout giftItemLayout;
        public ImageView giftImage;
        public TextView giftNameText;
        public TextView giftPriceText;
        public LinearLayout giftPriceLayout;
    }

}
