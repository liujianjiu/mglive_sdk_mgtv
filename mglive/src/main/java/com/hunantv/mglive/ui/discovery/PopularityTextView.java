package com.hunantv.mglive.ui.discovery;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by oy on 2015/12/3.
 */
public class PopularityTextView extends TextView {

    public PopularityTextView(Context context) {
        super(context);
        init();
    }

    public PopularityTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PopularityTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
    }

    public void setPopularity(int popularity) {
        if (popularity < 0) popularity = 0;

        SpannableString ss = new SpannableString(popularity + "人气");

        ss.setSpan(new ForegroundColorSpan(Color.parseColor("#999999")), ss.length() - 2, ss.length(),
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        setText(ss);
    }

}
