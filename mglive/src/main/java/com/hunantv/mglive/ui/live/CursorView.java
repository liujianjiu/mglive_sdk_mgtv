package com.hunantv.mglive.ui.live;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.view.View;
import android.widget.FrameLayout;

import com.hunantv.mglive.common.Constant;

/**
 * @author guojun
 */
public class CursorView extends View {
    private int mWidth, mHeight;
    private Paint mPaint;
    private boolean mIsSelected = false;

    private int mDefaultColor = 0x1A000000;
    private int mSelectColor = 0xFFFF7919;

    /**
     * @param context
     */
    public CursorView(Context context) {
        super(context);
        init();
    }

    public void init() {
        mWidth = Constant.toPix(14);
        mHeight = Constant.toPix(14);
        setLayoutParams(new FrameLayout.LayoutParams(2 * mWidth, 2 * mHeight));
        setPadding(mWidth / 2, mWidth / 2, mWidth / 2, mWidth / 2);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.saveLayer(0, 0, mWidth, mHeight, null, Canvas.MATRIX_SAVE_FLAG | Canvas.CLIP_SAVE_FLAG | Canvas.HAS_ALPHA_LAYER_SAVE_FLAG | Canvas.FULL_COLOR_LAYER_SAVE_FLAG
                | Canvas.CLIP_TO_LAYER_SAVE_FLAG);
        if (mIsSelected) {
            mPaint.setColor(mSelectColor);
        } else {
            mPaint.setColor(mDefaultColor);
        }
        canvas.drawCircle(mWidth / 2, mHeight / 2, mWidth / 2, mPaint);
    }

    public void setDefaultColor(@ColorInt int color){
        mDefaultColor = color;
    }

    public void setSelectColor(@ColorInt int color){
        mSelectColor = color;
    }

    /**
     * @return the mIsSelected
     */
    public final boolean isSelected() {
        return mIsSelected;
    }

    /**
     * @param mIsSelected the mIsSelected to set
     */
    public final void setSelected(boolean mIsSelected) {
        this.mIsSelected = mIsSelected;
        invalidate();
    }
}
