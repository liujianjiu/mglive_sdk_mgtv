package com.hunantv.mglive.ui.entertainer.data;

/**
 * Created by admin on 2015/12/16.
 */
public class ContrData {
    private String gid;
    private String name;
    private String code;
    private String icon;
    private String description;
    private int cyc;
    private long price;
    private int animType;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCyc() {
        return cyc;
    }

    public void setCyc(int cyc) {
        this.cyc = cyc;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getAnimType() {
        return animType;
    }

    public void setAnimType(int animType) {
        this.animType = animType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
