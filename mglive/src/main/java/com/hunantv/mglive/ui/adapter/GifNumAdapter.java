package com.hunantv.mglive.ui.adapter;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.data.GiftNumModel;

import java.util.List;

/**
 * Created by 达 on 2015/11/25.
 */
public class GifNumAdapter extends BaseAdapter {
    private Fragment mFragment;
    private List<GiftNumModel> mDataList;

    public GifNumAdapter(Fragment frg, List<GiftNumModel> list) {
        mFragment = frg;
        mDataList = list;
    }

    @Override
    public int getCount() {
        if (mDataList != null) {
            return mDataList.size();
        }
        return 0;
    }

    @Override
    public GiftNumModel getItem(int position) {
        if (mDataList != null) {
            return mDataList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GiftNumViewHolder giftNumViewHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mFragment.getActivity());
            convertView = layoutInflater.inflate(
                    R.layout.gift_num_list_item, null);
            AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                    AbsListView.LayoutParams.MATCH_PARENT,
                    AbsListView.LayoutParams.WRAP_CONTENT);
            convertView.setLayoutParams(params);
            giftNumViewHolder = new GiftNumViewHolder();
            giftNumViewHolder.giftNum = (TextView) convertView.findViewById(R.id.giftNum);
            giftNumViewHolder.giftNumText = (TextView) convertView.findViewById(R.id.giftNumText);
            convertView.setTag(giftNumViewHolder);
        } else {
            giftNumViewHolder = (GiftNumViewHolder) convertView.getTag();
        }
        GiftNumModel giftNumData = getItem(position);
        if (giftNumData != null) {
            giftNumViewHolder.giftNum.setText(giftNumData.giftNum + "");
            giftNumViewHolder.giftNumText.setText(giftNumData.giftNumText);
        }
        return convertView;
    }


    class GiftNumViewHolder {
        public TextView giftNum;
        public TextView giftNumText;
    }
}
