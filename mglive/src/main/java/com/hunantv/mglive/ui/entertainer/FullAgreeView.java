package com.hunantv.mglive.ui.entertainer;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

import com.hunantv.mglive.R;

/**
 * Created by admin on 2015/11/10.
 */
public class FullAgreeView extends RelativeLayout {
    //是否全屏
    private boolean isFullAgree = false;
    //是否显示
    private boolean isShow = false;

    //记录半屏时坐标，位移动画用
    private float oldY = 0;

    private static final int change_view = 1;
    private onFullDisplayListener mListener;

    public FullAgreeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 显示窗口
     */
    public void showView() {
        RelativeLayout.LayoutParams labelParams = (LayoutParams) getLayoutParams();
        labelParams.addRule(RelativeLayout.ALIGN_TOP, R.id.ll_live_star_detail_centre);
        setLayoutParams(labelParams);
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_bottom_in);
        setAnimation(animation);
        setVisibility(View.VISIBLE);
        isShow = true;
    }

    /**
     * 隐藏窗口
     */
    public void closeView() {
//        RelativeLayout.LayoutParams labelParams = (LayoutParams) getLayoutParams();
//        labelParams.addRule(RelativeLayout.ALIGN_TOP, R.id.ll_live_star_detail_centre);
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_bottom_out);
        setAnimation(animation);
        setVisibility(View.GONE);
        isShow = false;
        if (mListener != null && isFullAgree) {
            mListener.onfullChange(false);
        }
        isFullAgree = false;


        /**
         * 将实现半屏状态下上滑全屏的View的显示状态设置为显示
         * 保持与右上角的X一致,不然点返回键返回就会出现滑动不了的BUG
         */
        Object tagObj = getTag();
        if (tagObj instanceof DynamicView) {
            DynamicView dynamicView = (DynamicView) tagObj;
            dynamicView.setClickViewVisible();
        } else if (tagObj instanceof PersonValueView) {
            PersonValueView personValueView = (PersonValueView) tagObj;
            personValueView.setClickViewVisible();
        } else if (tagObj instanceof ContributionView) {
            ContributionView contributionView = (ContributionView) tagObj;
            contributionView.setClickViewVisible();
        }

    }


    public void closeFullArgeeView() {
        if (isFullAgree) {
//            RelativeLayout.LayoutParams labelParams = (LayoutParams) getLayoutParams();
//            labelParams.addRule(RelativeLayout.ALIGN_TOP, R.id.ll_live_star_detail_centre);
//            setLayoutParams(labelParams);
//            isFullAgree = false;
//            WindowManager wmManager=(WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
//            DisplayMetrics dm = new DisplayMetrics();
//            wmManager.getDefaultDisplay().getMetrics(dm);
//            Animation animation = new TranslateAnimation(0, 0,-(dm.heightPixels - oldY) ,0);;
//            animation.setDuration(400);
//            setAnimation(animation);

            WindowManager wmManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            DisplayMetrics dm = new DisplayMetrics();
            wmManager.getDefaultDisplay().getMetrics(dm);
            TranslateAnimation animation = new TranslateAnimation(0, 0, 0, oldY);
            animation.setDuration(300);
            animation.setFillAfter(true);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mHandler.sendEmptyMessageDelayed(change_view, 200);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            setAnimation(animation);

            if (mListener != null && isFullAgree) {
                mListener.onfullChange(false);
            }
            isFullAgree = false;
        }
    }

    /**
     * 全屏显示
     */
    public void fullAgreeBellowView() {
        RelativeLayout.LayoutParams labelParams = (LayoutParams) getLayoutParams();
        labelParams.addRule(RelativeLayout.ALIGN_TOP, 0);
//        labelParams.removeRule(RelativeLayout.ALIGN_TOP);
//        labelParams.addRule(RelativeLayout.BELOW, R.id.inputChatLayout);
        setLayoutParams(labelParams);
        oldY = getY();
        TranslateAnimation animation = new TranslateAnimation(0, 0, getY(), 0);
        animation.setDuration(300);
        setAnimation(animation);

//        setOnTouchListener(null);
        if (mListener != null && !isFullAgree) {
            mListener.onfullChange(true);
        }
        isFullAgree = true;
    }

    public void setFullListener(onFullDisplayListener l) {
        mListener = l;
    }

    public boolean isShow() {
        return isShow;
    }

    public boolean isFullAgree() {
        return isFullAgree;
    }

    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what) {
                case change_view:
                    clearAnimation();
//                    setVisibility(VISIBLE);
                    RelativeLayout.LayoutParams labelParams = (LayoutParams) getLayoutParams();
                    labelParams.addRule(RelativeLayout.ALIGN_TOP, R.id.ll_live_star_detail_centre);
                    setLayoutParams(labelParams);
//                    if (mListener != null && isFullAgree) {
//                        mListener.onfullChange(false);
//                    }
//                    isFullAgree = false;
                    break;
                default:
                    break;
            }
            return false;
        }
    });


    public interface onFullDisplayListener {
        void onfullChange(boolean change);
    }
}
