package com.hunantv.mglive.ui.discovery;

import android.content.Context;
import android.text.InputFilter;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.utils.UIUtil;

/**
 * Created by June Kwok on 2015-12-18.
 */
public class EditBtnView extends LinearLayout {
    public EditText mEditText;
    public TextView mTxtSend;

    public EditBtnView(Context context) {
        super(context);
        initView(context);
    }

    private void initView(Context context) {
       View view = LayoutInflater.from(context).inflate(R.layout.layout_dynamic_list_bottom,null);
        addView(view);
        mEditText = (EditText)view.findViewById(R.id.bottom_edit);
        mTxtSend = (TextView)view.findViewById(R.id.bottom_send);
    }
}
