package com.hunantv.mglive.ui.entertainer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.ui.entertainer.data.ContributionInfoData;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.StringUtil;

/**
 * Created by admin on 2015/12/3.
 */
public class ContributionBottomView extends RelativeLayout implements OnClickListener{
    public static final int PAY_TYPE_BUY = 1;
    public static final int PAY_TYPE_RENEW = 2;
    private BuyCallBack buyCallBack;

    private View buyView;
    private BuyViewHolder buyViewHolder;

    private View renewView;
    private RenewViewHolder renewViewHolder;


    private ContributionInfoData mContrInfo;
    public ContributionBottomView(Context context) {
        super(context);
    }

    public ContributionBottomView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void initView()
    {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        if(buyView == null)
        {
            buyView = layoutInflater.inflate(
                    R.layout.layout_contribution_bottom_buy, null);
            initBuyView(buyView);
        }

        addView(buyView);
    }

    private void notifyContr()
    {
        removeAllViews();
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        if(mContrInfo != null && !ContributionInfoData.GRADE_LEVEL_NO.equals(mContrInfo.getGradeName()))
        {
            if(renewView == null){
                renewView = layoutInflater.inflate(
                        R.layout.layout_contribution_bottom_renew, null);
                initRenewView(renewView);
            }
            initRenewViewData();
            addView(renewView);
        }else
        {
            initView();
        }
    }

    private void initBuyView(View view)
    {
        buyViewHolder = new BuyViewHolder();
        buyViewHolder.buyBtn =  view.findViewById(R.id.buyBtn);
        buyViewHolder.buyBtn.setOnClickListener(this);
    }

    private void initRenewView(View view)
    {
        renewViewHolder = new RenewViewHolder();
        renewViewHolder.userIconImage = (ImageView) view.findViewById(R.id.userIconImage);
        renewViewHolder.userNameText = (TextView) view.findViewById(R.id.userNameText);
        renewViewHolder.contrNumText = (TextView) view.findViewById(R.id.contrNumText);
        renewViewHolder.renewBtn = (Button) view.findViewById(R.id.renewBtn);
        renewViewHolder.renewBtn.setOnClickListener(this);
    }

    private void initRenewViewData()
    {
        if(MaxApplication.getApp().isLogin())
        {
            String photo = MaxApplication.getApp().getUserInfo().getPhoto();
            Glide.with(getContext()).load(StringUtil.isNullorEmpty(photo) ? R.drawable.default_icon : photo)
                    .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                    .transform(new GlideRoundTransform(getContext(), R.dimen.height_36dp)).into(renewViewHolder.userIconImage);
            renewViewHolder.userNameText.setText(MaxApplication.getApp().getUserInfo().getNickName());
            renewViewHolder.contrNumText.setText(mContrInfo.getRemainDay()+"天");
        }
    }


    public void setContrInfo(ContributionInfoData contrInfo)
    {
        this.mContrInfo = contrInfo;
        notifyContr();
    }

    public void setBuyCallBack(BuyCallBack buyCallBack) {
        this.buyCallBack = buyCallBack;
    }

    @Override
    public void onClick(View v) {
            if(v.getId() == R.id.buyBtn){
                if(buyCallBack != null)
                {
                    buyCallBack.buyContr(PAY_TYPE_BUY,null);
                }
            }else if(v.getId() == R.id.renewBtn){
                if(buyCallBack != null)
                {
                    buyCallBack.buyContr(PAY_TYPE_RENEW,mContrInfo);
                }
            }
    }

    private class BuyViewHolder
    {
        View buyBtn;
    }

    private class RenewViewHolder
    {
        ImageView userIconImage;
        TextView userNameText;
        TextView contrNumText;
        Button renewBtn;
    }

    public interface BuyCallBack{
        public void buyContr(int payType,ContributionInfoData contrInfo);
    }


}
