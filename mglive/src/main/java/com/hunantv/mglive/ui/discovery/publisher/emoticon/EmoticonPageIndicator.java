package com.hunantv.mglive.ui.discovery.publisher.emoticon;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.hunantv.mglive.R;

public class EmoticonPageIndicator extends RadioGroup implements
		ViewPager.OnPageChangeListener {

	private ViewPager viewPager;
	private boolean isRecent;
	private int mPageCount;
	
	public static int EMOTICON_PANGE_COUNT = 5;
	
	private static final int RADIO_BUTTON_TYPE_NORMAL = 0x01;
	private static final int RADIO_BUTTON_TYPE_SYSTEM = 0x02;
	private static final int RADIO_BUTTON_TYPE_EMOJI = 0x03;

//	RadioButton systemButton, emojiButton;
	RadioButton  emojiButton;
	
	public boolean isRecent() {
		return isRecent;
	}

	public void setRecent(boolean isRecent) {
		this.isRecent = isRecent;
	}

	public ViewPager getViewPager() {
		return viewPager;
	}

	public void setViewPager(ViewPager viewPager) {
		this.viewPager = viewPager;
		if (viewPager != null)
			viewPager.setOnPageChangeListener(this);
	}

	public EmoticonPageIndicator(Context context) {
		super(context);
//		systemButton = createButton(RADIO_BUTTON_TYPE_SYSTEM);
		emojiButton = createButton(RADIO_BUTTON_TYPE_EMOJI);
	}

	public EmoticonPageIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
//		systemButton = createButton(RADIO_BUTTON_TYPE_SYSTEM);
		emojiButton = createButton(RADIO_BUTTON_TYPE_EMOJI);
	}


	public void InitUI(int pageCount){
		mPageCount = pageCount;
//		addView(emojiButton);
		for (int i = 0; i < pageCount; i++) {
			addView(createButton(RADIO_BUTTON_TYPE_NORMAL));
		}
		
		RadioButton selectButton = (RadioButton) getChildAt(0);//emojiButton getChildAt(1) else getChildAt(0)
		if (selectButton != null) {
			selectButton.setChecked(true);
		}
	}
	
	/** 记录经典tab的page选择 */
	private int lastClassicPageIndex = -1;
	
	public void reset() {
		lastClassicPageIndex = -1;
	}
	
	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
	}

	
	/** 当前在经典tab */
	private int mClassicMode;
	private static int MODE_SYSTEM = 0X01;
	private static int MODE_EMOJI = 0X02;
	@Override
	public void onPageSelected(int position) {
		if(getChildCount() > position) {
			int realPosition = position;// + 1;//for emoji button + 1 ..
//			if (mIsClassicPage) {
//				// 黄脸和Emoji合并后，指示点有特殊显示
//				int emojiPageCount = 5; //SystemAndEmojiEmoticonInfo.EMOTICON_EMOJI_PAGE_COUNT;
//				int systemPageCount = 5;//SystemAndEmojiEmoticonInfo.EMOTICON_SYSTEM_PAGE_COUNT;
//				if (lastClassicPageIndex == emojiPageCount && position == emojiPageCount - 1) {
//					// 从黄脸切到emoji
//					emojiButton.setVisibility(View.GONE);
//					systemButton.setVisibility(View.VISIBLE);
//					// emoji页面更多，隐藏掉的点显示出来
//					if (emojiPageCount > systemPageCount) {
//						for (int i = systemPageCount + 1; i <= emojiPageCount; i++) {
//							RadioButton button = (RadioButton) getChildAt(i);
//							if (button != null) {
//								button.setVisibility(View.VISIBLE);
//							}
//						}
//					} else if (emojiPageCount < systemPageCount) {
//						// 黄脸页面更多，不需要的点隐藏掉
//						for (int i = emojiPageCount + 1; i <= systemPageCount; i++) {
//							RadioButton button = (RadioButton) getChildAt(i);
//							if (button != null) {
//								button.setVisibility(View.GONE);
//							}
//						}
//					}
//					mClassicMode = MODE_EMOJI;
//				} else if ((lastClassicPageIndex == (emojiPageCount - 1) || lastClassicPageIndex == -1) && position == emojiPageCount) {
//					// 从emoji切到黄脸，或者是初始化时进入黄脸
//					emojiButton.setVisibility(View.VISIBLE);
//					systemButton.setVisibility(View.GONE);
//					// emoji页面更多，不需要的点隐藏掉
//					if (emojiPageCount > systemPageCount) {
//						for (int i = systemPageCount + 1; i <= emojiPageCount; i++) {
//							RadioButton button = (RadioButton) getChildAt(i);
//							if (button != null) {
//								button.setVisibility(View.GONE);
//							}
//						}
//					} else if (emojiPageCount < systemPageCount) {
//						// 黄脸页面更多，隐藏掉的点显示出来
//						for (int i = emojiPageCount + 1; i <= systemPageCount; i++) {
//							RadioButton button = (RadioButton) getChildAt(i);
//							if (button != null) {
//								button.setVisibility(View.VISIBLE);
//							}
//						}
//					}
//					mClassicMode = MODE_SYSTEM;
//				}
//				// 映射位置
//				if (mClassicMode == MODE_EMOJI) {
//					// emoji
//					realPosition = position + 1;
//				} else {
//					// 黄脸
//					realPosition = position - (emojiPageCount - 1);
//				}
//				lastClassicPageIndex = position;
//			} else {
//				lastClassicPageIndex = -1;
//			}
//			
			// 加一层保护，虽然这里不可能越界
			if (realPosition >= getChildCount()) {
				realPosition = getChildCount() - 1;
			}
			
			RadioButton selectButton = (RadioButton) getChildAt(realPosition);
			if (selectButton != null) {
				selectButton.setChecked(true);
			}
		}
	}

	@Override
	public void onPageScrollStateChanged(int state) {
	}

	
	/** 当前ViewPager是否显示经典表情页(黄脸+Emoji)*/
	/*private boolean mIsClassicPage;
	
	public void synButton(int count, boolean isClassic) {
		// 当前是经典tab
		if (isClassic) {
			// 如果上一次也是经典tab，用之前的button即可，不是经典tab在这里创建button
			if (!mIsClassicPage) {
				removeAllViews();// TODO 这里待优化，可以复用其他页面创建的button
				// emoji和黄脸合并为经典，这里动态控制button数量，后续可能需求删掉部分表情，页数会变化
				int mergeCount = SystemAndEmojiEmoticonInfo.EMOTICON_EMOJI_PAGE_COUNT + SystemAndEmojiEmoticonInfo.EMOTICON_SYSTEM_PAGE_COUNT;
				int maxCount = Math.max(SystemAndEmojiEmoticonInfo.EMOTICON_EMOJI_PAGE_COUNT, SystemAndEmojiEmoticonInfo.EMOTICON_SYSTEM_PAGE_COUNT);
				addView(emojiButton);
				for (int i = 0; i < maxCount; i++) {
					addView(createButton(RADIO_BUTTON_TYPE_NORMAL));
				}
				addView(systemButton);
				for (int i = 0; i < mergeCount - maxCount - 2; i++) {
					RadioButton fillButton = createButton(RADIO_BUTTON_TYPE_NORMAL);
					fillButton.setVisibility(View.GONE);
					addView(fillButton);
				}
			}
		} else {
			removeView(systemButton);
			removeView(emojiButton);
			lastClassicPageIndex = -1;
			int current = this.getChildCount();
			if (count > current) {
				for (int i = 0; i < count - current; i++) {
					this.addView(createButton(RADIO_BUTTON_TYPE_NORMAL));
				}
			}
			// 需要减少
			else if (count < current) {
				for (int i = current - 1; i >= count; i--) {
					this.removeViewAt(i);
				}
			}
			if (count > 0&&viewPager != null) {
				RadioButton firstBtn = (RadioButton) this.getChildAt(0);
				if (isRecent) {
					firstBtn.setButtonDrawable(R.drawable.emo_radio_recent_btn);
				} else {
					firstBtn.setButtonDrawable(R.drawable.emo_radio_btn);
				}
				firstBtn.setChecked(true);
			}
			for (int i = 0; i < getChildCount(); i++) {
				((RadioButton) this.getChildAt(i)).setVisibility(View.VISIBLE);
			}
		}
		
		this.mIsClassicPage = isClassic;
		
	}*/

	private RadioButton createButton(int type) {
		RadioButton button = new RadioButton(this.getContext()) {
			@Override
			public boolean performClick() {
				return true;
			}
		};
//		if (type == RADIO_BUTTON_TYPE_SYSTEM) {
//			button.setButtonDrawable(R.drawable.qvip_emoji_tab_classic_icon);
//		} else 
		
		if (type == RADIO_BUTTON_TYPE_EMOJI) {
			button.setButtonDrawable(R.drawable.qvip_emoji_tab_emoji_icon);
		} else {
			button.setButtonDrawable(R.drawable.emo_radio_btn);
		}
		button.setGravity(Gravity.CENTER);
		Resources res = getContext().getResources();
		LayoutParams params = null;
		// 通用的默认宽高
		float buttonWidthDip = 10f;
		float buttonHeightDip = 10f;
		if (type == RADIO_BUTTON_TYPE_SYSTEM) {
			buttonWidthDip = 15f;
			buttonHeightDip = 15f;
		} else if (type == RADIO_BUTTON_TYPE_EMOJI) {
			buttonWidthDip = 24f;
			buttonHeightDip = 10f;
		} 
		params = new LayoutParams((int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, buttonWidthDip, res.getDisplayMetrics()), 
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, buttonHeightDip, res.getDisplayMetrics()));
		params.gravity = Gravity.CENTER;
		int space = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 3f, res.getDisplayMetrics());
		params.leftMargin = space;
		params.rightMargin = space;
		button.setLayoutParams(params);
		button.setClickable(true);
		
//		if(AppSetting.enableTalkBack&&type != RADIO_BUTTON_TYPE_EMOJI&&type != RADIO_BUTTON_TYPE_SYSTEM){
//			button.setClickable(false);
//			button.setFocusable(false);
//		}
		
		return button;

	}
}
