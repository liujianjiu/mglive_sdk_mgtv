package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hunantv.mglive.R;

import java.util.List;

/**
 * Created by admin on 2016/1/26.
 */
public class ProvinceAndCityAdapter extends BaseAdapter {
    public static final int TYPE_PROVINCE = 1;
    public static final int TYPE_CITY = 2;
    private Context mContext;
    private List<String> mItems;
    private int mType = 1;
    private int mSelectIndex = -1;

    public ProvinceAndCityAdapter(Context context, List<String> mItems,int type){
        this.mContext = context;
        this.mItems = mItems;
        this.mType = type;
    }

    @Override
    public int getCount() {
        return mItems != null ? mItems.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mItems != null && position < mItems.size() ? mItems.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
        {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(
                    R.layout.layout_city_select_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) convertView.findViewById(R.id.tv_item);
            viewHolder.bottomLine = convertView.findViewById(R.id.v_bottom_line);
            convertView.setTag(R.id.item_text,viewHolder.text);
            convertView.setTag(viewHolder);
        }
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.text.setText(mItems.get(position));
        if(mType == TYPE_PROVINCE)
        {
            //省份
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewHolder.bottomLine.getLayoutParams();
            layoutParams.leftMargin = mContext.getResources().getDimensionPixelSize(R.dimen.margin_7_5dp);
            layoutParams.rightMargin = 0;
            viewHolder.bottomLine.setLayoutParams(layoutParams);
            if(position == mSelectIndex){
                viewHolder.text.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            }else
            {
                viewHolder.text.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
            }
        }else if(mType == TYPE_CITY)
        {
            //市
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewHolder.bottomLine.getLayoutParams();
            layoutParams.leftMargin = 0;
            layoutParams.rightMargin = mContext.getResources().getDimensionPixelSize(R.dimen.margin_7_5dp);
            viewHolder.bottomLine.setLayoutParams(layoutParams);
            if(position == mSelectIndex){
                viewHolder.text.setTextColor(Color.parseColor("#ff7919"));
            }else
            {
                viewHolder.text.setTextColor(Color.parseColor("#333333"));
            }
        }
        return convertView;
    }

    public void setItems(List<String> items) {
        this.mItems = items;
        this.mSelectIndex = -1;
        notifyDataSetChanged();
    }

    public void setSelect(int selectIndex) {
        this.mSelectIndex = selectIndex;
    }

    public class ViewHolder{
        TextView text;
        View bottomLine;
    }
}
