package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.discovery.SuperWomanData;
import com.hunantv.mglive.ui.entertainer.StarSortView;
import com.hunantv.mglive.ui.entertainer.data.StarSortData;
import com.hunantv.mglive.utils.GlideRoundTransform;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2015/12/3.
 */
public class SuperWomanRankAdapater extends BaseAdapter {
    private Context context;
    private StarSortView.OnRankClickListener onRankClickListener;
    private List<SuperWomanData> RankDatas = new ArrayList<>();

    private List<StarSortData> sortDatas;
    private String advertUrl;

    public SuperWomanRankAdapater(Context context, String advertUrl, StarSortView.OnRankClickListener onRankClickListener) {
        this.context = context;
        this.advertUrl = advertUrl;
        this.onRankClickListener = onRankClickListener;
        initStarSortDatas();
    }

    public void setDatas(List<SuperWomanData> datas) {
        RankDatas.clear();
        RankDatas.addAll(datas);
        initStarSortDatas();
        notifyDataSetChanged();
    }

    public void addDatas(List<SuperWomanData> datas) {
        RankDatas.addAll(datas);
        initStarSortDatas();
        notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public int getCount() {
        if (RankDatas != null) {
            if (RankDatas.size() - 2 > 0) {
                return RankDatas.size() - 2;
            } else {
                return 1;
            }
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return RankDatas != null && (position + 2) < RankDatas.size() ? RankDatas.get(position + 2) : null;
    }

    public Object getItemByIndex(int position) {
        return RankDatas != null && position < RankDatas.size() ? RankDatas.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (position == 0) {
            view = new StarSortView(context, sortDatas, null, true, advertUrl, onRankClickListener);
            AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                    AbsListView.LayoutParams.MATCH_PARENT,
                    AbsListView.LayoutParams.MATCH_PARENT);
            view.setLayoutParams(params);
            ViewHolder holder = new ViewHolder();
            holder.itemType = ViewHolder.ITEM_TYPE_SORT;
            view.setTag(holder);
            return view;
        }
        if (convertView == null) {
            view = getItemView();
        } else {
            ViewHolder holder = (ViewHolder) convertView.getTag();
            if (ViewHolder.ITEM_TYPE_SORT == holder.itemType) {
                view = getItemView();
            } else {
                view = convertView;
            }
        }
        final SuperWomanData data = RankDatas.get(position + 2);
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.rankText.setText((position + 3) + "");
        Glide.with(context).load(data.getPhoto())
                .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                .transform(new GlideRoundTransform(context, R.dimen.height_45dp))
                .into(holder.personIcon);
        holder.nameText.setText(data.getName());
        holder.gradeImg.setVisibility(View.GONE);
        holder.gxNumText.setText(data.getHots() + "");

        holder.rankText.setBackgroundDrawable(null);
        holder.rankText.setTextColor(context.getResources().getColor(R.color.common_gray));
        return view;
    }

    public View getItemView() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(
                R.layout.layout_star_super_woman_rank_item, null);
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                AbsListView.LayoutParams.MATCH_PARENT,
                AbsListView.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(params);

        ViewHolder holder = new ViewHolder();
        holder.itemType = ViewHolder.ITEM_TYPE;
        holder.rankText = (TextView) view
                .findViewById(R.id.rankText);
        holder.personIcon = (ImageView) view
                .findViewById(R.id.personIcon);
        holder.nameText = (TextView) view
                .findViewById(R.id.nameText);
        holder.gradeImg = (ImageView) view
                .findViewById(R.id.gradeImg);
        holder.gxNumText = (TextView) view
                .findViewById(R.id.gxNumText);
        view.setTag(holder);
        return view;
    }

    public void initStarSortDatas() {
        if (RankDatas != null) {
            sortDatas = new ArrayList<>();
            for (int i = 0; i < 3 && i < RankDatas.size(); i++) {
                SuperWomanData data = RankDatas.get(i);
                StarSortData starSortData = new StarSortData(data.getPhoto(), data.getName(),
                        data.getHots(), -1);
                sortDatas.add(starSortData);
            }
        }
    }

    class ViewHolder {
        public static final int ITEM_TYPE_SORT = 1;
        public static final int ITEM_TYPE = 2;

        TextView rankText;
        ImageView personIcon;
        TextView nameText;
        TextView gxNumText;
        ImageView gradeImg;
        int itemType;
    }
}
