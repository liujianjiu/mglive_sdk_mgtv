package com.hunantv.mglive.ui.live.liveinterface;

import tv.danmaku.ijk.media.player.IMediaPlayer;

public interface OnVideoPlayCallBack {
    void onCompletion(IMediaPlayer mp);

    boolean onError();

    void onPrepared(IMediaPlayer mp);

    void onVideoPuse();

    void onVideoResum();
}