package com.hunantv.mglive.ui.live;

import com.hunantv.mglive.data.StarModel;

/**
 * Created by admin on 2015/12/17.
 */
public interface onStarDetailVideoCallBack {
    void followStarChange(StarModel starModel);

    void showGift();
}
