package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.utils.GlideRoundTransform;

import java.util.List;

/**
 *
 *
 */
public class ChatJoinAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private List<String> mData;

    public ChatJoinAdapter(Context context, List<String> data) {
        mContext = context;
        mData = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View  itemtView = LayoutInflater.from(mContext).inflate(R.layout.chat_join_item, null);
        return  new ItemHolder(itemtView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemHolder itemHolder = (ItemHolder)holder;
        Glide.with(mContext).load(mData.get(position)).error(R.drawable.default_icon).placeholder(R.drawable.default_icon).transform(new GlideRoundTransform(mContext, R.dimen.height_35dp)).into(itemHolder.imageView);

    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }
        return mData.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        public ItemHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.iv_chat_join_item);
        }
    }

}
