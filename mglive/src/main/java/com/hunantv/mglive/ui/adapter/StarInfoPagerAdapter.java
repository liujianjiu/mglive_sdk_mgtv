package com.hunantv.mglive.ui.adapter;

import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by QiuDa on 15/11/29.
 */
public class StarInfoPagerAdapter extends BaseAdapter {
    private Fragment mFragment;
    private List<View> mViews;


    public StarInfoPagerAdapter(Fragment fm, List<View> mViews) {
        this.mViews = mViews;
        this.mFragment = fm;
    }


    @Override
    public int getCount() {
        return mViews.size();
    }

    @Override
    public Object getItem(int position) {
        return mViews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return mViews.get(position);
    }
}
