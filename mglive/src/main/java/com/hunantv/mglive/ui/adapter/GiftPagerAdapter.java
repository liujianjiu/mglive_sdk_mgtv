package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.data.GiftDataModel;
import com.hunantv.mglive.ui.live.ItemEvent;
import com.hunantv.mglive.widget.MGridView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by June Kwok on 2015/11/13.
 */
public class GiftPagerAdapter extends PagerAdapter {
    private List<GiftDataModel> mGiftList;
    private List<List<GiftDataModel>> mPagerList;
    private ItemEvent mItemEvent;
    private Context mContext;
    private int mRow = 2;
    private int mColum = 4;
    private boolean mIsLand = false;
    private boolean mIsProgram = false;

    public GiftPagerAdapter(Context context, List<GiftDataModel> list, ItemEvent mItemEvent, int row, int colum,boolean isLang,boolean isProgram) {
        mRow = row;
        mColum = colum;
        this.mGiftList = list;
        this.mItemEvent = mItemEvent;
        mContext = context;
        mIsLand = isLang;
        mIsProgram = isProgram;
        initData();
    }

    public GiftPagerAdapter(Context context,ItemEvent mItemEvent, int row, int colum,boolean isLang,boolean isProgram) {
        mRow = row;
        mColum = colum;
        this.mItemEvent = mItemEvent;
        mContext = context;
        mIsLand = isLang;
        mIsProgram = isProgram;
        initData();
    }

    public void setRowAndColum(int row, int colum) {
        mRow = row;
        mColum = colum;
        initData();
    }

    public void setLandAndProgram(boolean isLand, boolean isProgram){
        mIsLand = isLand;
        mIsProgram = isProgram;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);//删除页卡
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {  //这个方法用来实例化页卡
        View view = LayoutInflater.from(mContext).inflate(R.layout.fragment_gift_page_item, null);
        MGridView mGridView = (MGridView) view.findViewById(R.id.mgv_gift_gridview);
        if (mGiftList != null) {
            setGridView(mGridView, mColum, mPagerList.get(position), mItemEvent);
        }
        container.addView(view);//添加页卡
        return view;
    }

    @Override
    public int getCount() {
        if (mPagerList != null) {
            return mPagerList.size();
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }


    /**
     * 数据分组
     */
    public void initData() {
        if (null == mPagerList) {
            mPagerList = new ArrayList<>();
        } else {
            mPagerList.clear();
        }
        if (null != this.mGiftList && this.mGiftList.size() > 0) {
            int pageSize;
            int pageItem = mColum * mRow;
            if ((mGiftList.size() % pageItem) == 0) {
                pageSize = mGiftList.size() / pageItem;
            } else {
                pageSize = mGiftList.size() / pageItem + 1;
            }
            int index;
            for (int i = 0; i < pageSize; i++) {
                List<GiftDataModel> mList = new ArrayList<>();
                for (int j = 0; j < pageItem; j++) {
                    index = i * pageItem + j;
                    if (index < mGiftList.size()) {
                        mList.add(mGiftList.get(index));
                    } else {
                        break;
                    }
                }
                mPagerList.add(mList);
            }
        }
    }

    private void setGridView(MGridView mgr, int colum, List<GiftDataModel> mGiftList, ItemEvent mItemEvent) {
        if(mIsProgram) {
            mgr.setAdapter(new GiftProgramGridAdapter(mContext, mGiftList, mIsLand));
            mgr.setVerticalSpacing(mContext.getResources().getDimensionPixelSize(mIsLand ? R.dimen.height_10dp : R.dimen.height_5dp));
        }else {
            mgr.setAdapter(new GiftStyleGridAdapter(mContext, mGiftList, mIsLand));
        }

        mgr.setNumColumns(colum);
        mgr.setOnItemClickListener(mItemEvent);
        mgr.setOnItemLongClickListener(mItemEvent);
    }

    @Override
    public void notifyDataSetChanged() {
        initData();
        super.notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void setGiftList(List<GiftDataModel> mGiftList) {
        this.mGiftList = mGiftList;
    }
}