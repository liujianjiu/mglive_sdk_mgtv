package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.hunantv.mglive.utils.ImageUtilsEx;
import com.hunantv.mglive.utils.L;

//run in image decode thread
public class ImageDecodeRunnable implements Runnable {

	private int mImgWidth;
	private int mImgHeight;
	private ImageView mImageView;
	private int mDesiredWidth; // The max-width of the output bitmap
	private int mDesiredHeight; // The max-height of the output bitmap
	private String mImagePath;

	public ImageDecodeRunnable(int imgWidth, int imgHeight, ImageView imgView, int desiredWidth, int desiredHeight, String imagePath) {
		// TODO Auto-generated constructor stub
		mImgWidth = imgWidth;
		mImgHeight = imgHeight;
		mImageView = imgView;
		mDesiredWidth = desiredWidth;
		mDesiredHeight = desiredHeight;
		mImagePath = imagePath;

	}

	@Override
	public void run() {

		if (mImagePath == null || mDesiredWidth == 0 || mDesiredHeight == 0 || mImageView.getTag() == null) {
			return;
		}

		String imgUrl = (String) mImageView.getTag();
		if (imgUrl == null || !imgUrl.equalsIgnoreCase(mImagePath)) {// imgView请求已发生改变。。。TODO:这里线程不安全....
			return;
		}

		if (mImgWidth == 0 || mImgHeight == 0) {
			BitmapFactory.Options opts = new BitmapFactory.Options();
			opts.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(mImagePath, opts);
			mImgWidth = opts.outWidth;
			mImgHeight = opts.outHeight;
		}

		if (mImgWidth == 0 || mImgHeight == 0) {
			return;
		}

		Bitmap bmp = null;

		for (int i = 1; i <= 2; i++) {

			imgUrl = (String) mImageView.getTag();
			if (imgUrl == null || !imgUrl.equalsIgnoreCase(mImagePath)) {// imgView请求已发生改变。。。
				return;
			}

			try {
				BitmapFactory.Options opts = new BitmapFactory.Options();
				// 1. decode in sample size
				opts.inSampleSize = ImageUtilsEx.findBestSampleSize(mImgWidth, mImgHeight, mDesiredWidth, mDesiredHeight) * i;
				opts.inPurgeable = true;// If this is set to true, then the resulting bitmap will allocate its pixels such that they can be purged if the system needs to reclaim memory.
				opts.inInputShareable = true;
				Bitmap tempBitmap = BitmapFactory.decodeFile(mImagePath, opts);
				L.d("img", "decode finish path:" + mImagePath + "samplesize: " + opts.inSampleSize + "width:" + opts.outWidth + "height: " + opts.outHeight);

				// 2. scaled in special size.
				if (tempBitmap != null) {

					if (tempBitmap.getWidth() > mDesiredWidth || tempBitmap.getHeight() > mDesiredHeight) {
						// If sample size bitmap is large than desired size , scale down to the maximal desired bitmap size.
						bmp = Bitmap.createScaledBitmap(tempBitmap, mDesiredWidth, mDesiredHeight, true);
						tempBitmap.recycle();
					} else if (tempBitmap.getWidth() < mDesiredWidth && tempBitmap.getHeight() < mDesiredHeight) {
						// If bitmap is smaller than desire size , scale up possible..
						int scaledWidth, scaledHeight;
						if (tempBitmap.getWidth() > tempBitmap.getHeight()) {
							scaledWidth = mDesiredWidth;
							scaledHeight = mDesiredWidth * tempBitmap.getHeight() / tempBitmap.getWidth();
						} else {
							scaledHeight = mDesiredHeight;
							scaledWidth = mDesiredHeight * tempBitmap.getWidth() / tempBitmap.getHeight();
						}

						bmp = Bitmap.createScaledBitmap(tempBitmap, scaledWidth, scaledHeight, true);

						tempBitmap.recycle();
					} else
						bmp = tempBitmap;

				} else {
					bmp = tempBitmap;
				}
				
				if(bmp != null){
					L.d("img", "finished....bmp width:" + bmp.getWidth() + "height:" + bmp.getHeight());
				}
				else{
					L.d("img", "finish decoded bmp == null");
				}
				
			} catch (OutOfMemoryError exception) {

				L.d("img", "out of memory image path: " + mImagePath + "image size " + mImgWidth + "*" + mImgHeight);

				if (i == 2) {
					return;
				} else {
					continue;
				}
			}
			
			break;
		}

		ImgWorkThread.getInstance().getUiHandler().post(new ImageDecodeFinishedRunnable(bmp, mImagePath, mImageView, mDesiredWidth, mDesiredHeight));

	}
}
