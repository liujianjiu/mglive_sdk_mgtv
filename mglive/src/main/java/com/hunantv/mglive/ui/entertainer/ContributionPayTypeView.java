package com.hunantv.mglive.ui.entertainer;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.alibaba.fastjson.JSON;
import com.hunantv.mglive.R;
import com.hunantv.mglive.common.BuildConfig;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.ResultModel;
import com.hunantv.mglive.ui.adapter.ContributionPageAdapter;
import com.hunantv.mglive.ui.entertainer.data.ContrData;
import com.hunantv.mglive.ui.entertainer.data.ContributionInfoData;
import com.hunantv.mglive.ui.entertainer.listener.PayContributionListener;

import org.json.JSONException;

import java.util.List;

/**
 import com.hunantv.mglive.utils.HttpUtils;
 import com.hunantv.mglive.common.FormEncodingBuilderEx;
 import com.squareup.okhttp.Request;
 import com.squareup.okhttp.RequestBody;
 * Created by admin on 2015/12/3.
 */
public class ContributionPayTypeView extends RelativeLayout implements ContributionPageAdapter.PayLinstener{
    private int payType;
    private ViewPager viewPager;
    private LinearLayout dotLayout;
    private ContributionPageAdapter pageAdapter;
    private PayContributionListener payContributionListener;

    public ContributionPayTypeView(Context context) {
        super(context);
        initView();
    }

    public ContributionPayTypeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public void initView(){
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        ;
        View view = layoutInflater.inflate(
                R.layout.layout_contribution_pay_list, null);
        initView(view);
        addView(view);
    }

    private void initView(View view)
    {
        viewPager = (ViewPager) view.findViewById(R.id.payViewPage);
        dotLayout = (LinearLayout) view.findViewById(R.id.dotLayout);
        pageAdapter =new ContributionPageAdapter(getContext());
        pageAdapter.setPayLinstener(this);
        pageAdapter.setDotLayout(dotLayout);
        viewPager.setAdapter(pageAdapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(pageAdapter);
    }

    @Override
    public void pay(ContrData contrData) {
        payContributionListener.payContribution(contrData);
    }

    public void setPayContributionListener(PayContributionListener payContributionListener) {
        this.payContributionListener = payContributionListener;
    }

    public void setContrDatas(List<ContrData> contrDatas){
        pageAdapter.setContrDatas(contrDatas);
    }

    public void setPayType(int payType,ContributionInfoData contrInfo) {
        pageAdapter.setPayType(payType, contrInfo);
        int size = viewPager.getChildCount();
        for(int i=0;i<size;i++){
            View view = viewPager.getChildAt(i);
            Object objTag = view.getTag();
            if(objTag != null && objTag instanceof ContributionPageAdapter.ViewHold)
            {
                pageAdapter.initView((ContributionPageAdapter.ViewHold)objTag);
            }
        }


    }
}
