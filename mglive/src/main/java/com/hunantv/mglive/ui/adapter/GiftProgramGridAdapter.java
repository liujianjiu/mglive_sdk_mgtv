package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.aidl.FreeGiftCallBack;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.GiftDataModel;
import com.hunantv.mglive.ui.discovery.FreeGiftAnimation;

import java.util.List;

/**
 * Created by June Kwok on 2015/12/2.
 */
public class GiftProgramGridAdapter extends BaseAdapter {
    private Context mContxt;
    private List<GiftDataModel> mGiftList;
    private int mColor;
//    private int mWidth;
    private boolean mIsLang;

    public GiftProgramGridAdapter(Context context, List<GiftDataModel> mGiftList,boolean isLand) {
        this.mContxt = context;
        this.mGiftList = mGiftList;
        mIsLang = isLand;
        mColor = mIsLang ? 0xFFFFFFFF : 0xff161719;
//        mWidth = mContxt.getResources().getDimensionPixelOffset(mIsLang ? R.dimen.height_60dp : R.dimen.height_60dp);
    }

    @Override
    public int getCount() {
        if (mGiftList == null)
            return 0;
        return mGiftList.size();
    }

    @Override
    public GiftDataModel getItem(int arg0) {
        if (mGiftList == null)
            return null;
        return mGiftList.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContxt).inflate(R.layout.fragment_gift_program_item, null);
            ViewHolder viewHolder = new ViewHolder();

            viewHolder.mIcon = (ImageView) convertView.findViewById(R.id.giftImage);
            viewHolder.mIconBg = (ImageView) convertView.findViewById(R.id.gift_bg);
            viewHolder.mName = (TextView) convertView.findViewById(R.id.giftNameText);
            viewHolder.mNum = (TextView) convertView.findViewById(R.id.goldTextView);
            viewHolder.mImgParent = (FrameLayout) convertView.findViewById(R.id.giftImageParent);
            viewHolder.mName.setTextColor(mColor);

            LinearLayout.LayoutParams ps = (LinearLayout.LayoutParams) viewHolder.mImgParent.getLayoutParams();
//            ps.weight = mWidth;
//            ps.height = mWidth;
            ps.topMargin = mContxt.getResources().getDimensionPixelOffset(mIsLang ? R.dimen.height_45dp : R.dimen.height_25dp);
            viewHolder.mImgParent.setLayoutParams(ps);
//            FrameLayout.LayoutParams psI = (FrameLayout.LayoutParams) viewHolder.mIcon.getLayoutParams();
//            psI.width = mWidth;
//            psI.height = mWidth;
//            viewHolder.mIcon.setLayoutParams(psI);
            convertView.setTag(viewHolder);
        }
        final ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        GiftDataModel gift = mGiftList.get(position);
        Glide.with(mContxt).load(mIsLang ? R.drawable.gift_program_land_bg : R.drawable.gift_program_bg).into(viewHolder.mIconBg);
        Glide.with(mContxt).load(gift.getPhoto()).into(viewHolder.mIcon);
        viewHolder.mName.setText(gift.getName());
        viewHolder.mNum.setTextColor(0xff969696);
        viewHolder.mNum.setText(gift.getPrice() + "金币");
        return convertView;
    }

    public class ViewHolder {
        public ImageView mIcon;
        public ImageView mIconBg;
        public FrameLayout mImgParent;
        public TextView mName;
        public TextView mNum;
    }
}