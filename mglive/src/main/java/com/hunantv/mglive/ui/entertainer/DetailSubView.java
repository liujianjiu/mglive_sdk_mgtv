package com.hunantv.mglive.ui.entertainer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by oy on 2015/12/12.
 */
public class DetailSubView extends View {
    public DetailSubView(Context context) {
        super(context);
    }

    public DetailSubView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DetailSubView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setDatas(ArrayList<? extends Object> datas) {

    }

    public void addDatas(ArrayList<? extends Object> datas) {

    }
}
