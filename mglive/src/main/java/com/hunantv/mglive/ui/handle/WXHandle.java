package com.hunantv.mglive.ui.handle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.hunantv.mglive.utils.Toast;

import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.Util;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;


/**
 * Created by admin on 2015/12/12.
 */
public class WXHandle {

    private static WXHandle wx;
    public static IWXAPI mWXApi;
    private Context openContext;

    public static WXHandle getInstance(Context mContext) {
        if (wx == null) {
            wx = new WXHandle(mContext);
        }
        return wx;
    }

    private WXHandle(Context mContext) {
        openContext = mContext;
        mWXApi = WXAPIFactory.createWXAPI(openContext, Constant.WX_APP_ID, true);
        mWXApi.registerApp(Constant.WX_APP_ID);
    }

    public void login(Context context) {
        if (!mWXApi.isWXAppInstalled()) {
            Toast.makeText(context, "请先安装微信", Toast.LENGTH_SHORT).show();
            return;
        }
        this.openContext = context;
        final SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "wechat_sdk_demo_test";
        boolean result = mWXApi.sendReq(req);
        L.d("WXlogin_Result", result ? "true" : "false");
    }

    public void shareToWX(String title, String des, String linkurl, Bitmap image) {
        if (!mWXApi.isWXAppInstalled()) {
            Toast.makeText(openContext, "请先安装微信", Toast.LENGTH_SHORT).show();
            return;
        }
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = linkurl;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = title;
        msg.description = des;
        if (image != null) {
            msg.thumbData = Util.bmpToByteArray(image, true);
        }

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;
        mWXApi.sendReq(req);
    }


    public void shareToMoments(String title, String des, String linkurl, Bitmap image) {
        if (!mWXApi.isWXAppInstalled()) {
            Toast.makeText(openContext, "请先安装微信", Toast.LENGTH_SHORT).show();
            return;
        }
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = linkurl;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = title;
        msg.description = des;
        if (image != null) {
            msg.thumbData = Util.bmpToByteArray(image, true);
        }

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneTimeline;
        mWXApi.sendReq(req);
    }

    private String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }


    public void handleIntent(Intent intent, IWXAPIEventHandler handler) {
        mWXApi.handleIntent(intent, handler);
    }

    public void closeUi(boolean b) {
        if (openContext != null) {
            ((Activity) openContext).finish();
            openContext = null;
        }
    }
}
