package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.graphics.Bitmap;

import com.hunantv.mglive.utils.L;


/**
 * @author maxxiang
 * 此类专用于相册cache,方便统一处理相册图片的cache
 */
public class PicCache {
	
	private static PicCache mInstance;
	
	public static PicCache getInstance(){
		if (mInstance == null){
			mInstance = new PicCache();
		}
		return mInstance;
	}
	
	public Bitmap getBitmap(String url) {

		LruImageCache bmpCache = (LruImageCache) ImageCacheManager.getInstance().getImageCache();

		if (bmpCache == null) {
			L.e("error", "PicCache getBitmap ImageCacheManager ImageCache null...");
			return null;
		}

		return bmpCache.getBitmap(url);
	}

	public void putBitmap(String url, Bitmap bitmap) {
		LruImageCache bmpCache = (LruImageCache) ImageCacheManager.getInstance().getImageCache();

		if (bmpCache == null) {
			L.e("error","PicCache putBitmap ImageCacheManager ImageCache null...");
			return;
		}

		bmpCache.putBitmap(url, bitmap);
	}
}
