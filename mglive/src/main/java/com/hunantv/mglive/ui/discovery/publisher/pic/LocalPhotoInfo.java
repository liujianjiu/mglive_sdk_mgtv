package com.hunantv.mglive.ui.discovery.publisher.pic;

import java.net.URL;

public class LocalPhotoInfo {
	public final static int PHOTO_SELECTED = 1;
	public final static int PHOTO_UNSELECTED= 2;
	//数据线最后一个被发送的图片状态
	public final static int PHOTO_LAST_SELECTED = 3;
	public String path;
//	private String mName;
	public long fileSize;
	public long modifiedDate;
	public int orientation;
//	private String description;
	public int selectStatus;
	public long _id;
	public int thumbWidth;
	public int thumbHeight;
	public int index;
	
	public static String getUrl(LocalPhotoInfo photoInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append(photoInfo.path);
		sb.append("|");
		sb.append(photoInfo.thumbWidth);
		sb.append("|");
		sb.append(photoInfo.thumbHeight);
		sb.append("|");
		sb.append(photoInfo.modifiedDate);
		sb.append("|");
		sb.append(0);
		// version
		sb.append("|");
		sb.append("201403010");
//		sb.append("|");
//		sb.append(photoInfo.index);
		return sb.toString();
	}
	
	// 例如:path|width|height|lastmodify|index
	public static LocalPhotoInfo parseUrl(URL url) {
		try {
			String path = url.getFile();
			String[] args = path.split("\\|");
			LocalPhotoInfo photoInfo = new LocalPhotoInfo();
			photoInfo.path = args[0];
			photoInfo.thumbWidth = Integer.parseInt(args[1]);
			photoInfo.thumbHeight = Integer.parseInt(args[2]);
			photoInfo.modifiedDate = Long.parseLong(args[3]);
			if (args.length > 6)
				photoInfo.index = Integer.parseInt(args[6]);
			return photoInfo;
		} catch (Exception e) {
			return null;
		}
	}
}
