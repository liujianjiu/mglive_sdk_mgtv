package com.hunantv.mglive.ui.adapter;

import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.ui.entertainer.data.CenterStarData;
import com.hunantv.mglive.ui.entertainer.data.StarTagData;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.RoleUtil;
import com.hunantv.mglive.utils.StringUtil;
import com.hunantv.mglive.widget.StarView;

import java.util.List;

/**
 * Created by 达 on 2015/11/16.
 */
public class StarViewAdapter {
    private StarView mStarView;
    private Fragment mFragment;
    private CenterStarData mCenterStar;
    private List<StarTagData> mTags;
    private List<StarModel> mStars;

    public StarViewAdapter(Fragment frg) {
        this.mFragment = frg;
    }

    public void setStarView(StarView starView) {
        this.mStarView = starView;
        setView(false);
    }

    public void starView(){
        setView(false);
    }


    private void setView(boolean isReset) {
        mStarView.removeAllViews();
        addCenterView();
        addInsideView();
        addOutSideView();
        if(!isReset)
        {
            mStarView.startAnim();
        }
    }


    private void addCenterView() {
        if(mCenterStar != null)
        {
            View view = LayoutInflater.from(mFragment.getActivity()).inflate(R.layout.fragment_star_recommend_star_item, null);

            ImageView outOvalImage = (ImageView) view.findViewById(R.id.iv_fragment_star_recommend_star_out_oval_bg);
            outOvalImage.setImageResource(R.drawable.recommend_center_star_item_solid_bg);

            RelativeLayout mRlIcon = (RelativeLayout) view.findViewById(R.id.rl_fragment_star_recommend_star_item);
            LinearLayout.LayoutParams ps = (LinearLayout.LayoutParams) mRlIcon.getLayoutParams();
            ps.height = ps.width = (int) mFragment.getResources().getDimension(R.dimen.height_80dp);
            ImageView ivIcon = (ImageView) view.findViewById(R.id.iv_fragment_star_recommend_star_item_icon);
            Glide.with(mFragment).load(StringUtil.isNullorEmpty(mCenterStar.getPhoto()) ? R.drawable.default_icon : mCenterStar.getPhoto())
                    .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                    .transform(new GlideRoundTransform(mFragment.getActivity(), R.dimen.height_80dp)).into(ivIcon);
            ImageView vipIcon = (ImageView) view.findViewById(R.id.iv_fragment_star_recommend_star_item_tips);
            vipIcon.setImageResource(RoleUtil.getRoleIcon(mCenterStar.getRole()));

            TextView tvName = (TextView) view.findViewById(R.id.tv_fragment_star_recommend_star_item_name);
            tvName.setText(mCenterStar.getNickName());
            tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mFragment.getResources().getDimensionPixelOffset(R.dimen.text_size14dp));
            view.setTag(R.id.star_tag, mCenterStar);
            view.setTag(StarView.STYLE_CENTER);
            mStarView.addView(view);
        }
    }

    private void addInsideView() {
        for (int i = 0; mTags != null && i < mTags.size() && i < 5; i++) {
            StarTagData model = mTags.get(i);
            View view = LayoutInflater.from(mFragment.getActivity()).inflate(R.layout.fragment_star_recommend_styel_item, null);
            LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_fragment_star_recommend_style);
            TextView tvTitle = (TextView) view.findViewById(R.id.tv_fragment_star_recommend_style_item_title);
//            TextView tvNum = (TextView) view.findViewById(R.id.tv_fragment_star_recommend_style_item_num);
            FrameLayout.LayoutParams ps = (FrameLayout.LayoutParams) ll.getLayoutParams();
            tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, mFragment.getResources().getDimensionPixelSize(R.dimen.text_size11dp));
//            tvNum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mFragment.getResources().getDimensionPixelSize(R.dimen.text_size13dp));
            ps.width = ps.height = (int)(mFragment.getResources().getDimensionPixelSize(R.dimen.height_70dp));
            view.setLayoutParams(ps);
            tvTitle.setText(model.getTagname());
//            tvNum.setText(model.getCount() + "人");
            view.setTag(R.id.star_tag,model);
            view.setTag(StarView.STYLE_INSIDE);
            mStarView.addView(view);
        }
    }


    private void addOutSideView() {
        for (int i = 0; mStars != null && i < mStars.size() ; i++) {
            StarModel model = mStars.get(i);
            View view = LayoutInflater.from(mFragment.getActivity()).inflate(R.layout.fragment_star_recommend_star_item, null);
            RelativeLayout mRlIcon = (RelativeLayout) view.findViewById(R.id.rl_fragment_star_recommend_star_item);
            LinearLayout.LayoutParams ps = (LinearLayout.LayoutParams) mRlIcon.getLayoutParams();
            ps.height = ps.width = (int) mFragment.getResources().getDimension(R.dimen.height_60dp);
            ImageView ivIcon = (ImageView) view.findViewById(R.id.iv_fragment_star_recommend_star_item_icon);
            Glide.with(mFragment).load(StringUtil.isNullorEmpty(model.getPhoto()) ? R.drawable.default_icon : model.getPhoto())
                    .placeholder(R.drawable.default_icon_preload).transform(new GlideRoundTransform(mFragment.getActivity(), R.dimen.height_60dp))
                    .into(ivIcon);
            ImageView tipsIcon = (ImageView) view.findViewById(R.id.iv_fragment_star_recommend_star_item_tips);
            tipsIcon.setImageResource(RoleUtil.getRoleIcon(model.getRole()));
            ViewGroup.LayoutParams layoutParams = tipsIcon.getLayoutParams();
            layoutParams.width = (int)(layoutParams.width * 0.75);
            layoutParams.height = (int)(layoutParams.height * 0.75);
            tipsIcon.setLayoutParams(layoutParams);

            TextView tvName = (TextView) view.findViewById(R.id.tv_fragment_star_recommend_star_item_name);
            tvName.setText(model.getNickName());
            view.setTag(R.id.star_tag, model);
            view.setTag(StarView.STYLE_OUTSIDE);
            mStarView.addView(view);
        }
    }

    public void notifyView() {
        mStarView.startChangeItemAnimOut();
    }

    public void reSetView() {
        setView(true);
        mStarView.startChangeItemAnimIn();
    }

    public void cleanView()
    {
        mStarView.startChangeItemAnimOut();
        mStarView.removeAllViews();

    }

    public CenterStarData getCenterStarData() {
        return mCenterStar;
    }

    public void setCenterStarData(CenterStarData centerStarData) {
        this.mCenterStar = centerStarData;
    }

    public void setStarTagDatas(List<StarTagData> starTagDatas) {
        this.mTags = starTagDatas;
    }

    public void setStars(List<StarModel> mStars) {
        this.mStars = mStars;
    }
}
