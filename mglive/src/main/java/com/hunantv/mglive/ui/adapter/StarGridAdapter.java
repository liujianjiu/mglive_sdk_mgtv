package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.star.TagStarModel;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.RoleUtil;
import com.hunantv.mglive.utils.StringUtil;

import java.util.List;

/**
 * Created by 达 on 2015/11/13.
 */
public class StarGridAdapter extends BaseAdapter {
    private Context mContext;
    private List<TagStarModel> mTagStars;

    public StarGridAdapter(Context context,List<TagStarModel> tagStars) {
        this.mContext = context;
        this.mTagStars = tagStars;
    }

    @Override
    public int getCount() {
        if(mTagStars != null)
        {
            return  mTagStars.size() > 6 ? 6 : mTagStars.size() ;
        }
        return 0;
    }

    @Override
    public Fragment getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_tag_star_grid_item, null);
            ViewHolde viewHolde = new ViewHolde();
            viewHolde.mPhoto = (ImageView) convertView.findViewById(R.id.iv_photo);
            viewHolde.mRole = (ImageView) convertView.findViewById(R.id.iv_role);
            viewHolde.mName = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolde.mHots = (TextView) convertView.findViewById(R.id.tv_hots);
            convertView.setTag(viewHolde);
        }
        ViewHolde viewHolde = (ViewHolde) convertView.getTag();
        TagStarModel star = mTagStars.get(position);
        Glide.with(mContext).load(star.getPhoto())
                .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                .transform(new GlideRoundTransform(mContext, R.dimen.height_70dp)).into(viewHolde.mPhoto);
        viewHolde.mRole.setImageResource(RoleUtil.getRoleIcon(star.getRole()));
        viewHolde.mName.setText(star.getNickName());
        viewHolde.mHots.setText(StringUtil.getFormatNum(star.getHotValue()) + "人气");
        return convertView;
    }

    class ViewHolde{
        ImageView mPhoto;
        ImageView mRole;
        TextView mName;
        TextView mHots;
    }

    public void setTagStars(List<TagStarModel> tagStars) {
        this.mTagStars = tagStars;
        notifyDataSetChanged();
    }
}