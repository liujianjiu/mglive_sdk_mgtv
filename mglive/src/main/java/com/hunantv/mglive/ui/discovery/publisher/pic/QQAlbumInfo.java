package com.hunantv.mglive.ui.discovery.publisher.pic;


public class QQAlbumInfo {
	public String _id;
	public String name;

	public long coverDate;
	public LocalPhotoInfo cover;
	public int imageCount;
}
