package com.hunantv.mglive.ui.discovery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.ui.discovery.publisher.PublisherActivity;

/**
 * Created by June Kwok on 2015-11-25.
 */
public class PublishPreView extends FrameLayout implements View.OnClickListener {
    public static final int TYPE_TEXT = 0;
    public static final int TYPE_PIC = 1;
    public static final int TYPE_VIDEO = 2;

    public ImageView mHideIcon;
    public BtnView mWord, mImg, mVideo;

    public PublishPreView(Context context) {
        super(context);
        init();
    }

    public PublishPreView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PublishPreView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setBackgroundColor(0xF3F3F5F9);
        LayoutParams mWordLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mWordLP.gravity = Gravity.TOP | Gravity.RIGHT;
        mWordLP.topMargin = Constant.toPix(266);
        mWordLP.rightMargin = Constant.toPix(62);
        mWord = new BtnView(getContext(), R.drawable.max_10_pub_pre_word, "文字");
        mWord.setLayoutParams(mWordLP);
        mWord.setId(R.id.btn_text);
        mWord.setOnClickListener(this);

        LayoutParams mImgLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mImgLP.gravity = Gravity.TOP | Gravity.LEFT;
        mImgLP.topMargin = Constant.toPix(266);
        mImgLP.leftMargin = Constant.toPix(62);
        mImg = new BtnView(getContext(), R.drawable.max_10_pub_pre_img, "照片");
        mImg.setLayoutParams(mImgLP);
        mImg.setId(R.id.btn_pic);
        mImg.setOnClickListener(this);

        LayoutParams mVideoLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mVideoLP.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        mVideoLP.topMargin = Constant.toPix(266);
        mVideo = new BtnView(getContext(), R.drawable.max_10_pub_pre_video, "视频");
        mVideo.setLayoutParams(mVideoLP);
        mVideo.setId(R.id.btn_video);
        mVideo.setOnClickListener(this);

        addView(mWord);
        addView(mImg);
        addView(mVideo);

        LayoutParams mHideIconLP = new LayoutParams(Constant.toPix(47), Constant.toPix(47));
        mHideIconLP.gravity = Gravity.TOP | Gravity.RIGHT;
        mHideIconLP.topMargin = Constant.toPix(60);
        mHideIconLP.rightMargin = Constant.toPix(17);
        mHideIcon = new ImageView(getContext());
        mHideIcon.setImageResource(R.drawable.max_10_squ_del);
        mHideIcon.setLayoutParams(mHideIconLP);

        addView(mHideIcon);
        setClickable(true);
    }

    public void hideSelf() {
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_bottom_out);
        this.setAnimation(animation);
        this.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        hideSelf();
            if(v.getId() == R.id.btn_text){
                Intent mIntent = new Intent(getContext(), PublisherActivity.class);
                mIntent.putExtra("directlypickimglist", false);
                mIntent.putExtra("sendtype", TYPE_TEXT);
                ((Activity) getContext()).startActivityForResult(mIntent,0);
            }else if(v.getId() == R.id.btn_pic){
                Intent mIntent = new Intent(getContext(), PublisherActivity.class);
                mIntent.putExtra("directlypickimglist", true);
                mIntent.putExtra("sendtype", TYPE_PIC);
                ((Activity) getContext()).startActivityForResult(mIntent,0);
            }
    }

    /**
     * 每个按钮空间的类
     */
    class BtnView extends LinearLayout {

        ImageView mIcon;
        int mImgRsId;
        String mBgColor;
        TextView mTitle;

        public BtnView(Context context, int rsId, String color) {
            super(context);
            mImgRsId = rsId;
            mBgColor = color;
            setOrientation(VERTICAL);
            init();
        }

        private void init() {
            LayoutParams mIconLP = new LayoutParams(Constant.toPix(160), Constant.toPix(160));
            mIconLP.gravity = Gravity.CENTER_HORIZONTAL;
            mIcon = new ImageView(getContext());
            mIcon.setImageResource(mImgRsId);
            mIcon.setLayoutParams(mIconLP);
            addView(mIcon);

            LayoutParams mTitleLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            mTitleLP.gravity = Gravity.CENTER_HORIZONTAL;
            mTitleLP.topMargin = Constant.toPix(22);
            mTitle = new TextView(getContext());
            mTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(26));
            mTitle.setTextColor(0xFF666666);
            mTitle.setText(mBgColor);
            mTitle.setLayoutParams(mTitleLP);

            addView(mTitle);
        }
    }

}
