/**
 * maxxiang
 * 2014-7-17
 * TODO
 */
package com.hunantv.mglive.ui.discovery.publisher.pic;

/**
 * @author maxxiang
 * 把多处要使用的相册查询需要的常量，定义在这儿
 **/
public class PicQueryConstants {

	public static final String RECENT_ALBUM_ID = "$RecentAlbumId";
	public static final String RECENT_ALBUM_NAME = "最近照片";	
	
	public static final int RECENT_PHOTO_MIN_WIDTH = 100;  
	public static final int MAX_RECENT_PHOTO_NUM = 100;//最近照片加载的个数
	public static final int MAX_SINGLE_PHOTO_NUM = 800;//单个相册允许加载的最大个数,防止OOM
}
