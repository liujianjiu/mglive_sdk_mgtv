package com.hunantv.mglive.ui.entertainer.adapate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.ui.entertainer.StarSortView;
import com.hunantv.mglive.ui.entertainer.data.PersonValueData;
import com.hunantv.mglive.ui.entertainer.data.StarSortData;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2015/12/3.
 */
public class PersonValueAdapater extends BaseAdapter {
    private Context context;

    private List<PersonValueData> personValueDatas = new ArrayList<>();

    private List<StarSortData> sortDatas;

    public PersonValueAdapater(Context context) {
        this.context = context;
        initStarSortDatas();
    }

    public void setDatas(List<PersonValueData> datas) {
        personValueDatas.clear();
        personValueDatas.addAll(datas);
        initStarSortDatas();
        notifyDataSetChanged();
    }

    public void addDatas(List<PersonValueData> datas) {
        personValueDatas.addAll(datas);
        initStarSortDatas();
        notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public int getCount() {
        if(personValueDatas != null)
        {
            if(personValueDatas.size() - 2 > 0)
            {
                return personValueDatas.size() - 2;
            }else
            {
                return 1;
            }
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return personValueDatas != null ? personValueDatas.get(position + 2) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (position == 0) {
            view = new StarSortView(context, sortDatas,"贡献");
            AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                    AbsListView.LayoutParams.MATCH_PARENT,
                    AbsListView.LayoutParams.MATCH_PARENT);
            view.setLayoutParams(params);
            ViewHolder holder = new ViewHolder();
            holder.itemType = ViewHolder.ITEM_TYPE_SORT;
            view.setTag(holder);
            return view;
        }
        if (convertView == null) {
            view = getItemView();
        } else {
            ViewHolder holder = (ViewHolder) convertView.getTag();
            if (ViewHolder.ITEM_TYPE_SORT == holder.itemType) {
                view = getItemView();
            } else {
                view = convertView;
            }
        }
        if (position != 0) {
            final PersonValueData data = personValueDatas.get(position + 2);
            ViewHolder holder = (ViewHolder) view.getTag();
            holder.rankText.setText((position + 3) + "");
//            holder.personIcon(data.personIcon);
            Glide.with(context).load(StringUtil.isNullorEmpty(data.getPhoto()) ? R.drawable.default_icon : data.getPhoto())
                    .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                    .transform(new GlideRoundTransform(context, R.dimen.height_45dp))
                    .into(holder.personIcon);
            holder.nameText.setText(data.getNickName());
            if(data.getGrade() >= 3)
            {
                Glide.with(context).load(R.drawable.grade_hj).into(holder.gradeImg);
                holder.gradeImg.setVisibility(View.VISIBLE);
            }else if(data.getGrade() >= 2)
            {
                Glide.with(context).load(R.drawable.grade_by).into(holder.gradeImg);
                holder.gradeImg.setVisibility(View.VISIBLE);
            }else if(data.getGrade() >= 1)
            {
                Glide.with(context).load(R.drawable.grade_qt).into(holder.gradeImg);
                holder.gradeImg.setVisibility(View.VISIBLE);
            }else
            {
                holder.gradeImg.setVisibility(View.GONE);
            }

            holder.gxNumText.setText(data.getDevoteValue() + "");

            holder.rankText.setBackgroundDrawable(null);
            holder.rankText.setTextColor(context.getResources().getColor(R.color.common_gray));
        }
        return view;
    }

    public View getItemView() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(
                R.layout.layout_star_person_value_item, null);
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                AbsListView.LayoutParams.MATCH_PARENT,
                AbsListView.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(params);

        ViewHolder holder = new ViewHolder();
        holder.itemType = ViewHolder.ITEM_TYPE;
        holder.rankText = (TextView) view
                .findViewById(R.id.rankText);
        holder.personIcon = (ImageView) view
                .findViewById(R.id.personIcon);
        holder.nameText = (TextView) view
                .findViewById(R.id.nameText);
        holder.gradeImg = (ImageView) view
                .findViewById(R.id.gradeImg);
        holder.gxNumText = (TextView) view
                .findViewById(R.id.gxNumText);
        view.setTag(holder);
        return view;
    }

    public void initStarSortDatas() {
        if (personValueDatas != null) {
            sortDatas = new ArrayList<>();
            for (int i = 0; i < 3 && i < personValueDatas.size(); i++) {
                PersonValueData data = personValueDatas.get(i);
                StarSortData starSortData = new StarSortData(data.getPhoto(), data.getNickName(),
                        (int) data.getDevoteValue(),data.getGrade());
                sortDatas.add(starSortData);
            }
        }
    }

    class ViewHolder {
        public static final int ITEM_TYPE_SORT = 1;
        public static final int ITEM_TYPE = 2;

        TextView rankText;
        ImageView personIcon;
        TextView nameText;
        TextView gxNumText;
        ImageView gradeImg;
        int itemType;
    }
}
