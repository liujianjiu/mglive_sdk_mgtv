package com.hunantv.mglive.ui.entertainer;

/**
 * Created by admin on 2016/1/26.
 */
public interface LoadListener {
    public void onLoadSuccess(int id);

    public void onLoadError(int id);
}
