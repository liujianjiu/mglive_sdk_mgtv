package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.graphics.Bitmap;

//maxxiang
//用于发送图片时本地图片在内存中的缓存

public class SendImageCache {
	private LruImageCache mThumbCache = new LruImageCache(10 * 1024 * 1024);

	private static SendImageCache mInstance;

	public static SendImageCache getInstance() {
		if (mInstance == null) {
			mInstance = new SendImageCache();
		}
		return mInstance;
	}

	public Bitmap getBitmap(String url) {
		try {
			return mThumbCache.getBitmap(url);
		} catch (NullPointerException e) {
			throw new IllegalStateException("Image Cache Not initialized");
		}
	}

	public void putBitmap(String url, Bitmap bitmap) {
		if(url == null || bitmap == null)
			return;
		try {
			mThumbCache.putBitmap(url, bitmap);
		} catch (NullPointerException e) {
			throw new IllegalStateException("Image Cache Not initialized");
		}
	}
	
}