package com.hunantv.mglive.ui.discovery.publisher.pic;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.common.MaxApplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hunantv.mglive.R;

public class AlbumListViewAdapter extends BaseAdapter {

	private LayoutInflater mInflater = null;
	public AlbumListActivity mActivity = null;
	
	private List<QQAlbumInfo> mDataList = new ArrayList<QQAlbumInfo>();
	private ConstantState mRightArrow;
	private Drawable mRightArrowDrawable;

	public final class ItemHolder {
		public ImageView coverIcon; //todo:maxxiang CustomNetworkImageView
		public TextView descText;

		public ItemHolder() {
		}

	}
	
	public AlbumListViewAdapter(AlbumListActivity activity) {
		mInflater = LayoutInflater.from(MaxApplication.getAppContext()
				.getApplicationContext());
		mActivity = activity;
	}
	


	ColorDrawable mColorDrawable = new ColorDrawable(0x80555555);

	public AlbumListViewAdapter() {
		mRightArrowDrawable = mActivity.getResources().getDrawable(R.drawable.common_arrow_right_selector);
		mRightArrowDrawable.setBounds(0, 0, mRightArrowDrawable.getIntrinsicWidth(), mRightArrowDrawable.getIntrinsicHeight());
	}

	@Override
	public int getCount() {
		return mDataList.size();
	}

	@Override
	public QQAlbumInfo getItem(int position) {
		return mDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ItemHolder itemHolder = null;

		QQAlbumInfo info = getItem(position);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.album_list_item, parent, false);
			itemHolder = new ItemHolder();
			convertView.setTag(itemHolder);

			itemHolder.coverIcon = (ImageView) convertView
					.findViewById(R.id.covor);
			itemHolder.descText = (TextView) convertView
					.findViewById(R.id.destext);

		} else {
			itemHolder = (ItemHolder) convertView.getTag();
		}
		itemHolder.descText.setText(info.name + String.format(" (%d)", info.imageCount));
		Glide.with(mActivity).load(info.cover.path).into(itemHolder.coverIcon);
		return convertView;
	}

	public void setData(List<QQAlbumInfo> list) {
		mDataList.clear();
		if (list == null || list.size() == 0) {
			return;
		}
		mDataList.addAll(list);
	}

}
