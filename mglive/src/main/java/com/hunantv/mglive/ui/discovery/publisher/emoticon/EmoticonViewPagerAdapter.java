package com.hunantv.mglive.ui.discovery.publisher.emoticon;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.utils.L;

public class EmoticonViewPagerAdapter extends PagerAdapter{

	public static int EMOTICON_PAGE_COUNT = 5;
	public static int EMOTICON_NUM_ONE_PAGE = 20;
	private Context mContext;
	private GridView.OnItemClickListener mOnItemClickListener = null;
	public EmoticonViewPagerAdapter(GridView.OnItemClickListener onItemClickListener) {
		// TODO Auto-generated constructor stub
		mContext =  MaxApplication.getAppContext().getApplicationContext();
		mOnItemClickListener = onItemClickListener;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return EMOTICON_PAGE_COUNT;
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		// TODO Auto-generated method stub
		return arg0 == arg1;
	}
	
	@Override  
    public Object instantiateItem (ViewGroup container, int position) {  
		L.d("emoji", "instantiateItem position:" + position);

		View v = LayoutInflater.from(mContext).inflate(R.layout.emoji_gridview, null);
		GridView gridView  = (GridView)v.findViewById(R.id.emoji_gridview);
		gridView.setAdapter(new EmoticonGridViewAdapter(mContext,position));
		gridView.setOnItemClickListener(mOnItemClickListener);
		container.addView(v);

		return v;
    }  
      
    @Override  
    public void destroyItem (ViewGroup container, int position, Object object) {  
    	L.d("emoji", "destroyItem position:"+position);
        container.removeView((View)object);  
    }

}
