package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.RoleUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author QiuDa
 *         <p/>
 *         listView 推荐关注
 */
public class SearchAllStarAdapter extends BaseAdapter{
    private Context context;

    private List<StarModel> mStars = new ArrayList<StarModel>();
    private OnClickListener mClickListener;

    public SearchAllStarAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return mStars != null ? mStars.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mStars.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            ViewHold viewHold = new ViewHold();
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_search_all_star_item, null);
            viewHold.photo = (ImageView) convertView.findViewById(R.id.iv_photo);
            viewHold.roleIcon = (ImageView) convertView.findViewById(R.id.iv_role_icon);
            viewHold.name = (TextView) convertView.findViewById(R.id.tv_name);
            viewHold.hotsFans = (TextView) convertView.findViewById(R.id.tv_hots_fans);
            viewHold.attenBtn = (LinearLayout) convertView.findViewById(R.id.ll_atten_btn);
            viewHold.forwordImg = (ImageView) convertView.findViewById(R.id.iv_forword);
            convertView.setTag(viewHold);
        }
        ViewHold viewHold = (ViewHold) convertView.getTag();

        StarModel star = mStars.get(position);
        Glide.with(context).load(star.getPhoto())
                .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                .transform(new GlideRoundTransform(context, R.dimen.height_50dp)).into(viewHold.photo);
        viewHold.name.setText(star.getNickName());
        viewHold.hotsFans.setText("人气:"+star.getHotValue()+"    粉丝:"+star.getFans());
        viewHold.roleIcon.setImageResource(RoleUtil.getRoleIcon(star.getRole()));
        if(star.getRole() != RoleUtil.ROLE_PT)
        {
            if(!Boolean.parseBoolean(star.getIsfans()))
            {
                viewHold.forwordImg.setVisibility(View.GONE);
                viewHold.attenBtn.setVisibility(View.VISIBLE);
                viewHold.attenBtn.setOnClickListener(new AtteBtnListener(star,viewHold));
            }
            else
            {
                viewHold.attenBtn.setVisibility(View.GONE);
                viewHold.forwordImg.setVisibility(View.VISIBLE);
            }
            viewHold.photo.setOnClickListener(new PhotoListener(star,viewHold));
        }else {
            viewHold.attenBtn.setVisibility(View.GONE);
            viewHold.forwordImg.setVisibility(View.GONE);
        }
        convertView.setTag(R.id.star_tag,star);
        return convertView;
    }

    public class ViewHold
    {
        ImageView photo;
        ImageView roleIcon;
        TextView name;
        public TextView hotsFans;
        LinearLayout attenBtn;
        ImageView forwordImg;
    }

    public void setStarModelData(List<StarModel> starModels) {
        if(starModels != null){
            this.mStars.clear();
            this.mStars.addAll(starModels);
        }
    }

    public void addStarModelData(List<StarModel> starModels) {
        if(starModels != null){
            this.mStars.addAll(starModels);
        }
    }

    public List<StarModel> getStarModels() {
        return mStars;
    }

    protected class PhotoListener implements View.OnClickListener{
        private StarModel starModel;
        private ViewHold viewHold;
        public PhotoListener(StarModel starModel, ViewHold viewHold)
        {
            this.starModel = starModel;
            this.viewHold = viewHold;
        }

        @Override
        public void onClick(View v) {
            if(viewHold.attenBtn.getVisibility() == View.VISIBLE && mClickListener != null)
            {
                mClickListener.onClickPhoto(starModel);
            }
        }
    }

    public class AtteBtnListener implements View.OnClickListener{
        private StarModel starModel;
        private ViewHold viewHold;
        public AtteBtnListener (StarModel starModel,ViewHold viewHold)
        {
            this.starModel = starModel;
            this.viewHold = viewHold;
        }

        @Override
        public void onClick(View v) {
            if(mClickListener != null)
            {
                boolean result = mClickListener.onClickAtten(starModel, v);
                if(result && viewHold!= null)
                {
                    viewHold.attenBtn.setVisibility(View.GONE);
                    viewHold.forwordImg.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public interface OnClickListener {
        /**
         * 点击粉她回调
         * @param starModel
         * @return
         */
        public boolean onClickAtten(StarModel starModel, View view);

        public void onClickPhoto(StarModel starModel);
    }

    public void setClickListener(OnClickListener listener) {
        this.mClickListener = listener;
    }

    public void cleanItems()
    {
        if(mStars != null)
        {
            mStars.clear();
            notifyDataSetChanged();
        }
    }

    /**
     * 判断当前Item是否允许点击
     * @param view
     * @return
     */
    public boolean isClickItem(View view)
    {
        Object tagObj = view.getTag();
        if(tagObj instanceof ViewHold)
        {
            ViewHold viewHold = (ViewHold) tagObj;
            if(viewHold.forwordImg.getVisibility() == View.VISIBLE)
            {
                return true;
            }
        }
        return false;
    }
}
