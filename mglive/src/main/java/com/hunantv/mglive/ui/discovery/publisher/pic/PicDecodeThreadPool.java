package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Process;

public class PicDecodeThreadPool {

	private HandlerThread mThread[] = new HandlerThread[2];
	private Handler mHandler[] = new Handler[2];
	private Handler mUiHandler;
	private static PicDecodeThreadPool mInstance;

	private PicDecodeThreadPool() {
		// TODO Auto-generated constructor stub
	}

	public static PicDecodeThreadPool instance() {
		if (mInstance == null) {
			mInstance = new PicDecodeThreadPool();
			mInstance.init();
		}
		return mInstance;
	}

	private void init() {

		mThread[0] = new HandlerThread("picdecode", Process.THREAD_PRIORITY_BACKGROUND);// 默认优先级...
		mThread[0].start();

		mThread[1] = new HandlerThread("picdecode", Process.THREAD_PRIORITY_BACKGROUND);// 默认优先级...
		mThread[1].start();
	}

	public Handler getWorkThreadHandler(int number) {
		int i = number % 2;
		if (mHandler[i] == null) {
			mHandler[i] = new Handler(mThread[i].getLooper());
		}
		return mHandler[i];
	}

	public void postToWorkThread(Runnable r, int number) {
		if (r == null || number < 0) {
			return;
		}

		Handler handler = getWorkThreadHandler(number);
		if (handler != null) {
			handler.post(r);
		}

	}

	public Handler getUiHandler() {
		if (mUiHandler == null) {
			mUiHandler = new Handler(Looper.getMainLooper());
		}
		return mUiHandler;
	}

	public void postToMainThread(Runnable r) {
		if (r == null) {
			return;
		}
		getUiHandler().post(r);

	}

}
