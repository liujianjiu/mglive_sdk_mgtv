package com.hunantv.mglive.ui.entertainer.listener;

import com.hunantv.mglive.ui.entertainer.data.ContrData;

/**
 * Created by admin on 2015/12/17.
 */
public interface PayContributionListener {
    public void payContribution(ContrData contrData);
}
