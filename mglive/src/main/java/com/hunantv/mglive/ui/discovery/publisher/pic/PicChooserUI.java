
package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.utils.L;

import java.util.ArrayList;

/**
 * @author maxxiang 图片选择器主界面
 */
public class PicChooserUI extends Activity implements OnClickListener,
		PicChooserAdapter.PicChooseUICallback {
	private GridView mGridView;
	private PicChooserAdapter mAdapter;
	private Button mBtnSend;
	private Button mBtnPreView;
	private TextView mTitle;
	private String mTitleName;
	private int mMaxSelectCount;

	public static final int REQUESTCOD_ALBUM = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.publisher_picchooser);
		Intent intent = getIntent();
		mMaxSelectCount = intent.getIntExtra("maxSelectCount", PublisherConstants.MAX_SELECT_PIC_COUNT);
		String albumId = intent.getStringExtra("albumid");
		mTitleName = intent.getStringExtra("albumname");
		if(mTitleName == null || mTitleName.length() == 0)
			mTitleName = "最近相册";
		
		// 初始化显示缩略图的gridview控件
		mGridView = (GridView) findViewById(R.id.publisher_picchooser_gridview);
		mAdapter = new PicChooserAdapter(this, albumId, this);
		mAdapter.setMaxSelectCount(mMaxSelectCount);
		mGridView.setAdapter(mAdapter);

		mBtnSend = (Button) findViewById(R.id.btn_send);
		mBtnSend.setOnClickListener(this);
		mBtnPreView = (Button) findViewById(R.id.btn_preview);
		mBtnPreView.setOnClickListener(this);

		Button btnCancel = (Button) findViewById(R.id.btn_cancel);
		btnCancel.setOnClickListener(this);
		
		Button btnAlbum = (Button) findViewById(R.id.btn_album);
		btnAlbum.setOnClickListener(this);
		Drawable drawable = getResources().getDrawable(R.drawable.back);
		drawable.setBounds(0, 0, 70, 70); //设置边界
		btnAlbum.setCompoundDrawables(drawable, null, null, null);


		mTitle = (TextView)findViewById(R.id.title);
		mTitle.setText(mTitleName);
		
		// 多次选择图片后，再进入，需要显示上次选择的图片信息
		ArrayList<String> strList = getIntent().getStringArrayListExtra(
				"piclists");
		if (strList != null && strList.size() > 0) {
			mAdapter.setPicSelectLists(strList);
			updateSelectCount(strList.size());
		}
		else{
			updateSelectCount(0);
		}
		
		//进入相册界面调整优先级。。。
		ImgWorkThread.getInstance().setPriority((Thread.NORM_PRIORITY + Thread.MIN_PRIORITY)/2);
	}


	private void updateData(Intent intent){
		String albumId = intent.getStringExtra("albumid");
		mTitleName = intent.getStringExtra("albumname");
		if(mTitleName == null || mTitleName.length() == 0)
			mTitleName = "最近相册";
		mTitle.setText(mTitleName);
		mAdapter.setAlbumId(albumId);

	}

	@Override
	public void updateSelectCount(int count) {
		String strText = mTitleName;
		if (count>0)
			strText = "已选择 " + count + "张照片";
		mTitle.setText(strText);
		
		if (count > 0){
			
			if(!mBtnPreView.isEnabled()){
				mBtnPreView.setEnabled(true);
				mBtnPreView.setAlpha(1);
			}
			
			if(!mBtnSend.isEnabled()){
				mBtnSend.setEnabled(true);
				mBtnSend.setAlpha(1);
			}
		}
		else if(count == 0){
			
			if(mBtnPreView.isEnabled()){
				mBtnPreView.setEnabled(false);
				mBtnPreView.setAlpha(0.6f);
			}
			
			if(mBtnSend.isEnabled()){
				mBtnSend.setEnabled(false);
				mBtnSend.setAlpha(0.6f);
			}
		}
	}

	@Override
	public void goToPreviewMode(int startPosition){
		
		Intent intent = new Intent();
		intent.setClass(this, PicPreviewActivity.class);
		intent.putStringArrayListExtra("piclists", mAdapter.getPicLists());
		intent.putStringArrayListExtra("picselectlists", mAdapter.getPicSelectLists());
		intent.putExtra("position", startPosition);	//当前的图片id(index，从0开始)
		intent.putExtra("currentMode", PicPreviewActivity.MODE_SELECT_AND_PREVIEW);
		intent.putExtra("maxSelectCount", mMaxSelectCount);
		startActivityForResult(intent, PicWidget.Code_Pic_Preview);
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
			if(v.getId() == R.id.btn_send){
				PicChooserResult.getInstance().resetResult(mAdapter.getPicSelectLists());
				finish();
			}else if(v.getId() == R.id.btn_preview){
				Intent intent = new Intent();
				intent.setClass(this, PicPreviewActivity.class);
				L.d("preview", "mAdapter.getPicSelectLists" + mAdapter.getPicSelectLists().size());
				intent.putStringArrayListExtra("piclists", mAdapter.getPicSelectLists());
				intent.putStringArrayListExtra("picselectlists", mAdapter.getPicSelectLists());
				intent.putExtra("position", 0);						//当前的图片id(index，从0开始)
				intent.putExtra("currentMode", PicPreviewActivity.MODE_SELECT_AND_PREVIEW);
				startActivityForResult(intent, PicWidget.Code_Pic_Preview);
			}else if(v.getId() == R.id.btn_album){
				Intent intent = new Intent();
				intent.setClass(this, AlbumListActivity.class);
				intent.putStringArrayListExtra("piclists", mAdapter.getPicSelectLists());
				startActivityForResult(intent,REQUESTCOD_ALBUM);
			}else if(v.getId() == R.id.btn_cancel){
				onBackPressed();
			}
	}

	@Override
	public void onBackPressed() {
		PicChooserResult.getInstance().clear();
		setResult(RESULT_OK);
		super.onBackPressed();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
			case PicWidget.Code_Pic_Preview: {
				if (data != null) {
					ArrayList<String> list = null;
					
					boolean selectStateChanged = data.getBooleanExtra("selectStateChanged",false);
					if(selectStateChanged){
						list = (ArrayList<String>) data
							.getStringArrayListExtra("piclists");
						if(list != null){
							mAdapter.setPicSelectLists(list);
							mAdapter.notifyDataSetChanged();
							updateSelectCount(list.size());
						}
					}
				}
				break;
			}

			case REQUESTCOD_ALBUM:
				if(RESULT_OK==resultCode){
					updateData(data);
				}
				break;

		}
		super.onActivityResult(requestCode, resultCode, data);
	}
		
	@Override
	 protected void onDestroy() {
		super.onDestroy();
		ImgWorkThread.getInstance().setPriority(Thread.MIN_PRIORITY);
	}
	
}
