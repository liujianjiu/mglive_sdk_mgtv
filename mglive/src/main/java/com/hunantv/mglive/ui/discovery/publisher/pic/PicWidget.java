package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.hunantv.mglive.R;
import com.hunantv.mglive.ui.discovery.publisher.PictureFromDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author maxxiang
 * 发布器中图片thumb列表生成器
 */
public class PicWidget implements View.OnClickListener, IWidget {
	
	// 以下定义 activity的返回代码
	public static final int Code_Pic_Preview = 2;
		
	protected Context mContext;
	private TableLayout mTableLayout;
	public int PIC_LINENUM = 4;  //默认每行显示4个
	
	public int MAX_PIC_NUM = 9;
	
	//cache list
	private ArrayList<String> mPicList= new ArrayList<String>();
	private Map<Integer, View> mViews= new HashMap<Integer, View>();
	private Map<Integer, TableRow> mTabRows = new HashMap<Integer, TableRow>();
	
	AsyncImageLoader mImgLoader = new AsyncImageLoader();
	
	public PicWidget(Context context, TableLayout layout){
		mContext = context;
		mTableLayout = layout;
		init();
	}
	
	private void init(){

	}
	
	public void setArrayList(ArrayList<String> list){
		mPicList.clear();
		mPicList = list;
		ReBuildTableLayout();
	}
	
	public ArrayList<String> getSelectArrayList(){
		return mPicList;
	}  
	
	//根据选择的图片，重新生成tableLayout内容 
	public void ReBuildTableLayout(){
		mTableLayout.removeAllViews();
		
		int id = 0;
		int size = mPicList.size() + 1;
		for(int i=0; id<size; i++){
			TableRow tr = getTableRow(i);
			tr.removeAllViews();
			
			for(int j=0; j<PIC_LINENUM && id< size; j++, id++){
				
				if(id >= MAX_PIC_NUM){
					break;
				}
				
				View view = getView(id);
				ImageView iv = (ImageView) view.findViewById(R.id.iv);
				String strPath = "";
				if(id == size -1){  
					iv.setBackgroundResource(R.drawable.publisher_btn_add);
					iv.setImageDrawable(null);
					
					view.setTag(-1);
				}else{
					strPath = mPicList.get(id);
					iv.setBackgroundDrawable(null);
					 
					Bitmap bm= PicCache.getInstance().getBitmap(strPath);
					if (bm == null) {
						iv.setImageResource(R.drawable.publisher_defaultpic);
						mImgLoader.asyncLoadImage(iv, strPath,id);
					} else {
						iv.setImageBitmap(bm);
					}
					iv.setTag(strPath);
					view.setTag(id);
				}
				view.setOnClickListener(this);
				
				TableRow.LayoutParams p = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 
						ViewGroup.LayoutParams.WRAP_CONTENT);
				view.setLayoutParams(p);
				tr.addView(view);
			}
			
			if(tr.getChildCount()>0){
				mTableLayout.addView(tr);
			}
			
			if(id >= MAX_PIC_NUM){
				break;
			}
		}
	}
	
	private TableRow getTableRow(int i){
		TableRow tr = mTabRows.get(i);
		if (tr == null){
			tr = new TableRow(mContext);
			mTabRows.put(i, tr);
		}
		return tr;
	}
	
	private View getView(int i){
		View v = mViews.get(i);
		if(v == null){
			v = CreatePicWidget();
			mViews.put(i, v);
		}
		return v;
	}

	//create a picwidget can be override
	public View CreatePicWidget(){
		View v = View.inflate(mContext, R.layout.publisher_picwidget_item, null);
		return v;
	}
		
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getTag() == null){
			return;
		}
		int id = (Integer)v.getTag();
		if(id == -1){
//			//添加图片
//			Intent intent = new Intent();
//			intent.setClass(mContext, PicChooserUI.class);
//			intent.putStringArrayListExtra("piclists", mPicList);
//			PicChooserResult.getInstance().resetResult(mPicList);
//			((Activity)mContext).startActivity(intent);
			//进入弹出框
			if(mPicList.size()==PublisherConstants.MAX_SELECT_PIC_COUNT){
				Toast.makeText(mContext,"已经达到最大可选择的图片数量....",Toast.LENGTH_SHORT).show();
				return;
			}
			PictureFromDialog dlg = new PictureFromDialog(mContext);
			dlg.setCallback((PictureFromDialog.PictureFromCallback)mContext);
			dlg.show();
			
		}else{ //预览图片
			Intent intent = new Intent();
			intent.setClass(mContext, PicPreviewActivity.class);
			intent.putStringArrayListExtra("piclists", mPicList);
			intent.putExtra("position", id);						//当前的图片id(index，从0开始)
			((Activity)mContext).startActivityForResult(intent, Code_Pic_Preview);
		}
	}

	@Override
	public boolean onActivityResult(int code, Intent intent) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
