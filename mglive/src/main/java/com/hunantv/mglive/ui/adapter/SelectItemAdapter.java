package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hunantv.mglive.R;

import java.util.List;

/**
 * Created by admin on 2016/1/26.
 */
public class SelectItemAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mItems;
    private int mSelectIndex = -1;

    public SelectItemAdapter(Context context){
        this.mContext = context;
    }

    public SelectItemAdapter(Context context,List<String> mItems){
        this.mContext = context;
        this.mItems = mItems;
    }

    @Override
    public int getCount() {
        return mItems != null ? mItems.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return position < mItems.size() ? mItems.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
        {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(
                    R.layout.layout_select_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) convertView.findViewById(R.id.tv_item);
            viewHolder.rightLine = convertView.findViewById(R.id.v_right_line);
            viewHolder.bottomLine = convertView.findViewById(R.id.v_bottom_line);
            convertView.setTag(R.id.item_text,viewHolder.text);
            convertView.setTag(viewHolder);
        }
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.text.setText(mItems.get(position));
        if(position % 2 == 0)
        {
            //左边
            viewHolder.rightLine.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewHolder.bottomLine.getLayoutParams();
            layoutParams.leftMargin = mContext.getResources().getDimensionPixelSize(R.dimen.margin_7_5dp);
            layoutParams.rightMargin = 0;
            viewHolder.bottomLine.setLayoutParams(layoutParams);
        }else
        {
            //右边
            viewHolder.rightLine.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewHolder.bottomLine.getLayoutParams();
            layoutParams.leftMargin = 0;
            layoutParams.rightMargin = mContext.getResources().getDimensionPixelSize(R.dimen.margin_7_5dp);
            viewHolder.bottomLine.setLayoutParams(layoutParams);
        }
        if(position == mSelectIndex){
            viewHolder.text.setTextColor(Color.parseColor("#ff7919"));
        }else
        {
            viewHolder.text.setTextColor(Color.parseColor("#333333"));
        }
        return convertView;
    }

    public void setItems(List<String> items) {
        this.mItems = items;
        this.mSelectIndex = -1;
        notifyDataSetChanged();
    }

    public void setSelect(int selectIndex) {
        this.mSelectIndex = selectIndex;
    }

    public class ViewHolder{
        TextView text;
        View rightLine;
        View bottomLine;
    }


}
