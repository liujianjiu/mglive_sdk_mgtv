package com.hunantv.mglive.ui.discovery;

import android.content.Context;
import android.widget.FrameLayout;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.BaseBar;
import com.hunantv.mglive.common.Constant;

/**
 * Created by guojun3 on 2016-1-27.
 */
public class NormalHistory extends FrameLayout {

    BaseBar mBar;

    public NormalHistory(Context context) {
        super(context);
        initView();
    }

    /**
     * 初始化布局
     */
    private void initView() {
        mBar = new BaseBar(getContext(), "历史记录");
        LayoutParams mBarLP = new LayoutParams(Constant.sRealWidth, getResources().getDimensionPixelSize(R.dimen.height_44dp));
        mBarLP.topMargin = Constant.getStatusBarHeight(getContext());
        mBar.mShareIcon.setVisibility(GONE);
        addView(mBar, mBarLP);
    }
}
