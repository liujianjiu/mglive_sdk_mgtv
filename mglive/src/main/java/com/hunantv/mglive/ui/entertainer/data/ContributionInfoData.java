package com.hunantv.mglive.ui.entertainer.data;

import java.io.Serializable;

/**
 * Created by admin on 2015/12/17.
 */
public class ContributionInfoData implements Serializable {
    public static String GRADE_LEVEL_NO = "0";
    public static String GRADE_LEVEL_QT = "1";
    public static String GRADE_LEVEL_BY = "2";
    public static String GRADE_LEVEL_HJ = "3";

    private String gradeLevel;//守护等级
    private String gradeName;//守护者的等级：0 非守护者；1 铜牌守护者；2 银牌守护者；3 金牌守护者
    private String date;//守护时间（具体的月数）
    private String name;//守守护名称
    private String startTime;//开始时间
    private String endTime;//结束时间
    private String comment;//描述
    private String remainDay;//有效时间

    public String getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(String gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRemainDay() {
        return remainDay;
    }

    public void setRemainDay(String remainDay) {
        this.remainDay = remainDay;
    }
}
