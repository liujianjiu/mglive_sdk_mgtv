package com.hunantv.mglive.ui.entertainer.data;

/**
 * Created by admin on 2015/12/3.
 */
public class StarSortData {
    public StarSortData(String icon, String name, int num,int grade) {
        this.icon = icon;
        this.name = name;
        this.num = num;
        this.grade = grade;
    }

    private String icon;
    private String name;
    private int num;
    private int grade;
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
