package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.data.star.TagModel;

import java.util.List;
import java.util.Random;

/**
 * Created by admin on 2016/1/25.
 */
public class TagAdapter extends BaseAdapter {
    private Context mContext;
    private List<TagModel> mTags;
    private String[] colors = new String[]{"#6fc9b8","#e4b86f","#f37ab4","#82c6f2","#ce97d8"};
    private String[] mColors;
    private int oldColor1 = -1;
    private int oldColor2 = -1;
    public TagAdapter(Context context)
    {
        this.mContext = context;
    }
    @Override
    public int getCount() {
        return mTags != null ? mTags.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mTags != null && position < mTags.size() ? mTags.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
        {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(
                    R.layout.layout_star_tag_item, null);
            TextView tagView = (TextView) convertView.findViewById(R.id.tv_tag);
            convertView.setTag(tagView);
        }
        TextView textView = (TextView) convertView.getTag();
        TagModel tagModel = mTags.get(position);
        GradientDrawable gradientDrawable = (GradientDrawable) mContext.getResources().getDrawable(R.drawable.star_tag_bg);
        gradientDrawable.setColor(Color.parseColor(mColors[position]));
        textView.setBackgroundDrawable(gradientDrawable);
        textView.setText(tagModel.getName());
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
        if(position == mTags.size() - 1 )
        {
            layoutParams.rightMargin = mContext.getResources().getDimensionPixelSize(R.dimen.margin_7_5dp);
        }else
        {
            layoutParams.rightMargin = 0;
        }
        textView.setLayoutParams(layoutParams);
        textView.setTag(tagModel);
        return convertView;
    }

    public void setTags(List<TagModel> tags) {
        this.mTags = tags;
        initColors();
        notifyDataSetChanged();
    }

    private void initColors(){
        mColors = new String[mTags.size()];
        for (int i=0;i<mColors.length;i++){
            mColors[i] = colors[randomColor()];
        }
    }

    private int randomColor()
    {
        Random random = new Random();
        while (1 == 1){
            int index = random.nextInt(colors.length);
            if(index != oldColor1 && index != oldColor2){
                oldColor2 = oldColor1;
                oldColor1 = index;
                return index;
            }
        }
    }
}
