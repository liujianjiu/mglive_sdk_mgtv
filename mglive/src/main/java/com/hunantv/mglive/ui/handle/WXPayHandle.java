package com.hunantv.mglive.ui.handle;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.pay.WxPayModel;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.Toast;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * Created by admin on 2016/2/29.
 */
public class WXPayHandle {
    private static WXPayHandle wx;
    private static IWXAPI mWXApi;

    public static WXPayHandle getInstance() {
        if (wx == null) {
            wx = new WXPayHandle();
        }
        return wx;
    }

    private WXPayHandle() {

    }


    public boolean pay(WxPayModel payModel)
    {
        if(payModel == null)
        {
            return false;
        }
        if(mWXApi == null)
        {
            String appid =  payModel.getAppid();
            mWXApi = WXAPIFactory.createWXAPI(MaxApplication.getAppContext(), appid);
//            mWXApi.registerApp(appid);
            L.d("WXRegister","微信注册成功 appid="+appid);
        }
        if (!mWXApi.isWXAppInstalled()) {
            Toast.makeText(MaxApplication.getAppContext(), "请先安装微信", Toast.LENGTH_SHORT).show();
            return false;
        }

        PayReq request = new PayReq();
        request.appId = payModel.getAppid();
        request.partnerId = payModel.getPartnerId();
        request.prepayId = payModel.getPrepayId();
        request.packageValue = payModel.getPackageValue();
        request.nonceStr= payModel.getNonceStr();
        request.timeStamp= payModel.getTimeStamp();
        request.sign= payModel.getSign();
        mWXApi.sendReq(request);

        return true;
    }
}
