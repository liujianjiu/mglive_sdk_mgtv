package com.hunantv.mglive.ui.discovery;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.IBinder;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hunantv.mglive.aidl.FreeGiftCallBack;
import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.common.MaxApplication;

/**
 * Created by June Kwok on 2016-1-27.
 */
public class NormalFlower extends FrameLayout implements FreeGiftCallBack {
    ImageView mBg;
    ImageView mFlower;
    TextView mCount;
    Paint mPaint;
    Paint mPaintOne;
    FreeGiftAnimation mGift;
    TextView mFlowerTxt;
    String mFlowerName = "鲜花";

    public String getFlowerName() {
        return mFlowerName;
    }

    public void setFlowerName(String mFlowerName) {
        this.mFlowerName = mFlowerName;
    }

    public NormalFlower(Context context) {
        super(context);
        init();
    }

    private void init() {
        final int mWidth = Constant.toPixByHeight(156);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(0x19000000);

        mPaintOne = new Paint();
        mPaintOne.setAntiAlias(true);
        mPaintOne.setColor(0xFFFF7919);
        mPaintOne.setStyle(Paint.Style.STROKE);
        mPaintOne.setStrokeWidth(Constant.toPix(4));

        LayoutParams mBgLP = new LayoutParams(mWidth, mWidth);
        LayoutParams mFlowerLP = new LayoutParams(Constant.toPixByHeight(120), Constant.toPixByHeight(120));
        mFlowerLP.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
        mFlowerLP.topMargin = Constant.toPix(10);

        LayoutParams mCountLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mCountLP.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
        mCountLP.topMargin = mWidth / 2 - Constant.toPix(6);

        mBg = new ImageView(getContext()) {
            @Override
            protected void onDraw(Canvas canvas) {
                super.onDraw(canvas);
                canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 2, mPaint);
            }
        };
        addView(mBg, mBgLP);

        mFlower = new ImageView(getContext());
        addView(mFlower, mFlowerLP);


        mGift = new FreeGiftAnimation(getContext(), mWidth);
        addView(mGift, mBgLP);
        mGift.setCallBack(this);

        mCount = new TextView(getContext());
        mCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(32));
        mCount.setTextColor(0xFFFE7A1A);
        mCount.setText("180秒");
        mCount.setVisibility(GONE);
        addView(mCount, mCountLP);
        mFlowerTxt = new TextView(getContext());

        int freeGiftCount = MaxApplication.getInstance().getFreeGiftCount();
        mFlowerTxt.setText(getFlowerName() + "(" + freeGiftCount + ")");
        mFlowerTxt.setTag(freeGiftCount);
        mFlowerTxt.setTextSize(TypedValue.COMPLEX_UNIT_PX, Constant.toPix(24));
        mFlowerTxt.setTextColor(0xFF999999);
        LayoutParams mFlowerTxtLP = new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mFlowerTxtLP.gravity = Gravity.CENTER_HORIZONTAL;
        mFlowerTxtLP.topMargin = mBgLP.height + Constant.toPixByHeight(2);
        mFlowerTxt.setLayoutParams(mFlowerTxtLP);
        addView(mFlowerTxt, mFlowerTxtLP);
    }

    @Override
    public void onTimeLeft(int left) {
        int freeGiftCount = MaxApplication.getInstance().getFreeGiftCount();
        mCount.setVisibility(freeGiftCount == 0 ? VISIBLE : GONE);
        mCount.setText(left + "秒");
    }

    @Override
    public void onNumAdd(int num) {
        int freeGiftCount = MaxApplication.getInstance().getFreeGiftCount();
        if (freeGiftCount >= 5) {
            mCount.setVisibility(GONE);
        }
        mFlowerTxt.setText(getFlowerName() + "(" + freeGiftCount + ")");
    }

    @Override
    public IBinder asBinder() {
        return null;
    }
}
