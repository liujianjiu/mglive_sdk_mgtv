package com.hunantv.mglive.ui.live;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.common.BaseActivity;
import com.hunantv.mglive.common.BuildConfig;
import com.hunantv.mglive.common.FormEncodingBuilderEx;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.ResultModel;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.RoleUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class StarLiveEndActivity extends BaseActivity implements View.OnClickListener {
    public static final String JUMP_STAR_LIVE_END_ID = "JUMP_STAR_LIVE_END_ID";
    public static final String JUMP_STAR_LIVE_END_ICON = "JUMP_STAR_LIVE_END_ICON";
    public static final String JUMP_STAR_LIVE_END_NAME = "JUMP_STAR_LIVE_END_NAME";
    public static final String JUMP_STAR_LIVE_END_ROLE = "JUMP_STAR_LIVE_END_ROLE";
    public static final String JUMP_STAR_LIVE_END_COUNT = "JUMP_STAR_LIVE_END_COUNT";
    public static final String JUMP_STAR_LIVE_END_HOT = "JUMP_STAR_LIVE_END_HOT";
    public static final String JUMP_STAR_LIVE_END_DURATION = "JUMP_STAR_LIVE_END_DURATION";
    public static final String JUMP_STAR_LIVE_TPYE = "JUMP_STAR_LIVE_TPYE";

    private int mRole;
    private boolean mType;
    private String mUid;
    private String mIcon;
    private String mName;
    private String mCount;
    private String mHot;
    private String mDuration;
    private Button mBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_star_live_end);
        Intent intent = getIntent();
        mUid = intent.getStringExtra(JUMP_STAR_LIVE_END_ID);
        mIcon = intent.getStringExtra(JUMP_STAR_LIVE_END_ICON);
        mName = intent.getStringExtra(JUMP_STAR_LIVE_END_NAME);
        mRole = intent.getIntExtra(JUMP_STAR_LIVE_END_ROLE, 0);
        mCount = intent.getStringExtra(JUMP_STAR_LIVE_END_COUNT);
        mHot = intent.getStringExtra(JUMP_STAR_LIVE_END_HOT);
        mDuration = intent.getStringExtra(JUMP_STAR_LIVE_END_DURATION);
        mType = intent.getBooleanExtra(JUMP_STAR_LIVE_TPYE, false);
        setView();
    }


    private void setView() {
        ImageView icon = (ImageView) findViewById(R.id.iv_star_live_end_icon);
        Glide.with(this).load(mIcon).transform(new GlideRoundTransform(this, R.dimen.height_55dp)).error(R.drawable.default_icon).into(icon);
        ImageView tag = (ImageView) findViewById(R.id.iv_star_live_end_tag);
        int roleId = RoleUtil.getRoleIcon(mRole);
        Glide.with(this).load(roleId).into(tag);
        TextView tvName = (TextView) findViewById(R.id.tv_star_live_end_name);
        tvName.setText(mName);
        TextView tvCount = (TextView) findViewById(R.id.tv_star_live_end_count);
        tvCount.setText(mCount);
        TextView tvHot = (TextView) findViewById(R.id.tv_star_live_end_hot);
        tvHot.setText(mHot);
        TextView tvDuration = (TextView) findViewById(R.id.tv_star_live_end_duration);
        tvDuration.setText(mDuration);
        mBtn = (Button) findViewById(R.id.btn_star_live_end);
        mBtn.setOnClickListener(this);
        if (mType) {
            mBtn.setText(getString(R.string.star_live_go_back_home));
        } else {
            if (isLogin()) {
                getIsFollowStatus(mUid);
            } else {
                mBtn.setText(getString(R.string.attention));
                mBtn.setSelected(false);
            }
        }
        ImageButton close = (ImageButton) findViewById(R.id.ibtn_star_live_end_close);
        close.setOnClickListener(this);
    }

    private void followStar(String starId) {
        Map<String, String> body = new FormEncodingBuilderEx()
                .add("uid", getUid())
                .add("token", getToken())
                .add("followid", starId)
                .build();
        post(BuildConfig.URL_ADD_FOLLOW, body);
    }

    private void getIsFollowStatus(String starId){
        if(isLogin()){
            Map<String, String> param = new FormEncodingBuilderEx()
                    .add("uid", MaxApplication.getInstance().getUid())
                    .add("token", MaxApplication.getInstance().getToken())
                    .add("artistId", starId != null ? starId : "").build();
            post(BuildConfig.URL_GET_IS_FOLLOWED, param);
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.ibtn_star_live_end_close) {
            finish();

        } else if (i == R.id.btn_star_live_end) {
            if (mType) {
                finish();
            } else {
                if (!isLogin()) {
                    jumpToLogin("登录以后才可以粉TA哦~");
                    return;
                }
                if (!v.isSelected()) {
                    followStar(mUid);
                }
            }

        }
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        return super.asyncExecute(url, resultModel);
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException {
        super.onSucceed(url, resultModel);
        if (BuildConfig.URL_ADD_FOLLOW.contains(url)) {
            mBtn.setSelected(true);
            mBtn.setText(getString(R.string.star_live_attentioned));
        }else if(BuildConfig.URL_GET_IS_FOLLOWED.equals(url))
        {
            try {
                JSONObject jsonData = new JSONObject(resultModel.getData());
                boolean isFollow = jsonData.getBoolean("isFollowed");
                if(isFollow)
                {
                    mBtn.setText(getString(R.string.star_live_attentioned));
                    mBtn.setSelected(true);
                }else
                {
                    mBtn.setText(getString(R.string.attention));
                    mBtn.setSelected(false);
                }
            }catch (Exception e){

            }
        }
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
        super.onFailure(url, resultModel);
    }
}
