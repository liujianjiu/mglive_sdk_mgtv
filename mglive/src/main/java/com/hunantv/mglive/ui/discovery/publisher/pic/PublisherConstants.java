package com.hunantv.mglive.ui.discovery.publisher.pic;

import android.os.Environment;

/**
 * @author maxxiang
 * 所有发布器的可能被重用的常量都定义在这儿，减少单独定义，从而出错的可能
 */
public class PublisherConstants {

	public static final int REQ_CAMERA_DATA = 1;

	public static final int MAX_SELECT_PIC_COUNT = 6;//最大可选择的图片个数

	//文件路径/存储相关
	public static String ROOT_APP_PATH = "/data/data/com.hunantv.mglive/";   //程序的根路径，防止以后万一改了包名，在程序初始化时重新赋值

	//sdcard
	public static final String ROOT_SDCARD = Environment.getExternalStorageDirectory().getAbsolutePath();
	public static final String ROOT_SDCARD_DATA_PATH = ROOT_SDCARD +"/mglivedata/";
	
	//camera/pic
	/*拍照的照片存储位置*/        
	public static final String PHOTO_DIR_Path = Environment.getExternalStorageDirectory() + "/DCIM/Camera/";
	
	//activity 通用参数部分
	public static final int RequestCode_Jump = 0;  //表示跳过前一个activity

	
	//activity之间 intent的通用参数
	public static final String Activity_Result_value = "result_value"; //取消或确定
	public static final String Activity_From = "activity_from";  //表示来自哪一个activity
	
	
	//设置相关
	public final static String APP_SETTING_ID = "circle_app_setting";
	public final static String SETTING_ENVIRONMENT = "release_server";
}
