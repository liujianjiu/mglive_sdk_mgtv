package com.hunantv.mglive.ui.adapter;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.LiveDataModel;
import com.hunantv.mglive.utils.StringUtil;

import java.util.List;

/**
 * Created by 达 on 2015/11/30.
 */
public class LiveListAdapter extends BaseAdapter {
    private Fragment mFragment;
    private List<LiveDataModel> mDataList;
    private int mHeight;
    private IAppointmentClickListener onListener;

    public LiveListAdapter(Fragment fg, List<LiveDataModel> list, int width) {
        this.mFragment = fg;
        this.mDataList = list;
        double scale = 750.0f / 420;
        this.mHeight = (int) (width / scale);
    }

    @Override
    public int getCount() {
//        return mDataList.size()+1;
        return mDataList.size();
    }

    @Override
    public LiveDataModel getItem(int position) {
        if (position < 0) {
            return null;
        }
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LiveDataModel dataModel = getItem(position);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mFragment.getActivity());
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(
                    R.layout.live_list_item_layout, null);
            holder.ivBg = (ImageView) convertView.findViewById(R.id.iv_live_list_item_bg);
            RelativeLayout.LayoutParams ps = (RelativeLayout.LayoutParams) holder.ivBg.getLayoutParams();
            ps.height = mHeight;
            holder.tvTag = (TextView) convertView.findViewById(R.id.tv_live_list_item_tag);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_live_list_item_name);
            holder.tvDes = (TextView) convertView.findViewById(R.id.tv_live_list_des);
            holder.btnAppointment = (LinearLayout) convertView.findViewById(R.id.btn_live_list_item_appointment);
            holder.appointImg = (ImageView) convertView.findViewById(R.id.iv_live_list_item_appointment);
            holder.appointText = (TextView) convertView.findViewById(R.id.tv_live_list_item_appointment);
            holder.viewDivider = convertView.findViewById(R.id.v_live_list_item_divider);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (position < mDataList.size() - 1) {
            holder.viewDivider.setVisibility(View.VISIBLE);
        } else {
            holder.viewDivider.setVisibility(View.GONE);
        }
        if (dataModel != null) {
            holder.tvName.setText(dataModel.getTitle());
            Glide.with(mFragment).load(dataModel.getImage()).placeholder(R.drawable.default_img_16_9).into(holder.ivBg);
            if (StringUtil.isNullorEmpty(dataModel.getLiveTag())) {
                holder.tvTag.setVisibility(View.GONE);
            } else {
                holder.tvTag.setVisibility(View.VISIBLE);
                holder.tvTag.setText(dataModel.getLiveTag());
            }
            if ("off".equals(dataModel.getLiveStatus())) {
                holder.tvTag.setBackgroundResource(R.drawable.live_bg);
                holder.tvDes.setVisibility(View.GONE);
                holder.btnAppointment.setVisibility(View.VISIBLE);
                holder.btnAppointment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onListener != null) {
                            onListener.onAppointmentClick(dataModel);
                        }
                    }
                });
            } else if (LiveDataModel.TYPE_BACK.equals(dataModel.getType())) {
                holder.tvTag.setBackgroundResource(R.drawable.main_list_item6);
                holder.tvTag.setTextColor(mFragment.getResources().getColor(R.color.white));
                holder.tvDes.setText(mFragment.getString(R.string.live_playing_times, dataModel.getCount()));
                if (LiveDataModel.SHOW_COUNT_SHOW.equals(dataModel.getShowCount())) {
                    holder.tvDes.setVisibility(View.VISIBLE);
                } else {
                    holder.tvDes.setVisibility(View.GONE);
                }
                holder.tvDes.setCompoundDrawablesWithIntrinsicBounds(mFragment.getResources().getDrawable(R.drawable.main_list_item7), null, null, null);
                holder.btnAppointment.setVisibility(View.GONE);
            } else {
                holder.tvTag.setBackgroundResource(R.drawable.live_bg);
                holder.tvTag.setTextColor(mFragment.getResources().getColor(R.color.white));
                holder.tvDes.setText(mFragment.getString(R.string.live_online_home, dataModel.getCount()));
                if (LiveDataModel.SHOW_COUNT_SHOW.equals(dataModel.getShowCount())) {
                    holder.tvDes.setVisibility(View.VISIBLE);
                } else {
                    holder.tvDes.setVisibility(View.GONE);
                }
                holder.tvDes.setCompoundDrawablesWithIntrinsicBounds(mFragment.getResources().getDrawable(R.drawable.main_list_item8), null, null, null);
                holder.btnAppointment.setVisibility(View.GONE);
            }
        }
        return convertView;
    }


    public void setAppointmentClickListener(IAppointmentClickListener l) {
        this.onListener = l;
    }

    public interface IAppointmentClickListener {
        void onAppointmentClick(LiveDataModel dataModel);
    }


    class ViewHolder {
        ImageView ivBg;
        TextView tvTag;
        TextView tvName;
        TextView tvDes;
        LinearLayout btnAppointment;
        ImageView appointImg;
        TextView appointText;
        View viewDivider;
    }

}
