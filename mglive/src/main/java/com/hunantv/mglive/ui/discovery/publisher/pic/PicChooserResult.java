package com.hunantv.mglive.ui.discovery.publisher.pic;

import java.util.ArrayList;


public class PicChooserResult {
	private static PicChooserResult mInstance;
	private ArrayList<String> mResultList = null;
	public boolean mSetResult = false;
	
	public static PicChooserResult getInstance() {
		
		if (mInstance == null) {
				mInstance = new PicChooserResult();
				mInstance.mResultList = new ArrayList<String>();
		}
		
		return mInstance;
	}
	
	public void clear() {
		mResultList.clear();
		mSetResult = false;
	}
	
	public void resetResult(ArrayList<String> list) {
		mResultList.clear();
		if(list != null && list.size() >= 0){
			mResultList.addAll(list);
		}
		mSetResult = true;
	}
	
	public ArrayList<String> getResult() {
		return mResultList;
	}
	
	public boolean hasBeenSetResult(){
		return mSetResult;
	}
}
