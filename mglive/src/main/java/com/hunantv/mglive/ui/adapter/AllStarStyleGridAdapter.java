package com.hunantv.mglive.ui.adapter;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.RoleUtil;
import com.hunantv.mglive.utils.StringUtil;

import java.util.List;

/**
 * Created by 达 on 2015/11/13.
 */
public class AllStarStyleGridAdapter extends BaseAdapter {
    private Fragment mFragment;
    private List<StarModel> mStyleStarList;

    public AllStarStyleGridAdapter(Fragment frg) {
        this.mFragment = frg;
    }


    @Override
    public int getCount() {
        return mStyleStarList != null ? mStyleStarList.size() : 0 ;
    }

    @Override
    public Fragment getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mFragment.getActivity()).inflate(R.layout.fragment_all_star_style_star_item, null);
            ViewHolde viewHolde = new ViewHolde();
            viewHolde.mStyleStarFaceIcon = (ImageView) convertView.findViewById(R.id.style_star_face_icon);
            viewHolde.mStyleStarName = (TextView) convertView.findViewById(R.id.style_star_name);
            viewHolde.mStyleStarPersonNum = (TextView) convertView.findViewById(R.id.style_star_person_num);
            viewHolde.mTipsIcon = (ImageView) convertView.findViewById(R.id.style_star_star_tips);
            convertView.setTag(viewHolde);
        }
        ViewHolde viewHolde = (ViewHolde) convertView.getTag();
        StarModel starData = mStyleStarList.get(position);
        Glide.with(mFragment).load(StringUtil.isNullorEmpty(starData.getPhoto()) ? R.drawable.default_icon : starData.getPhoto())
                .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                .transform(new GlideRoundTransform(mFragment.getActivity(), R.dimen.height_70dp)).into(viewHolde.mStyleStarFaceIcon);
        viewHolde.mStyleStarName.setText(starData.getNickName());
        viewHolde.mStyleStarPersonNum.setText(("null".equals(starData.getHots()) && StringUtil.isNullorEmpty(starData.getHots())) ? 0+"" : starData.getHots()+"人气");
        viewHolde.mTipsIcon.setImageResource(RoleUtil.getRoleIcon(starData.getRole()));
        return convertView;
    }



    class ViewHolde{
        ImageView mStyleStarFaceIcon;
        TextView mStyleStarName;
        TextView mStyleStarPersonNum;
        ImageView mTipsIcon;
    }

    public void setmStyleStarList(List<StarModel> mStyleStarList) {
        this.mStyleStarList = mStyleStarList;
    }
}