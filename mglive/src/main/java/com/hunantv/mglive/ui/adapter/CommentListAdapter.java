package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.user.DynamicMessageData;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.RoleUtil;
import com.hunantv.mglive.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2016/1/19.
 */
public class CommentListAdapter extends BaseAdapter {
    private List<DynamicMessageData> listMessage = new ArrayList<DynamicMessageData>();
    private Context mContext;
    private OnClickListener mClickListener;
    public CommentListAdapter(Context mContext){
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return listMessage != null ? listMessage.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return position < listMessage.size() ? listMessage.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
        {
            ViewHolder viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.layout_comment_item, null);
            viewHolder.photo = (ImageView) convertView.findViewById(R.id.iv_photo);
            viewHolder.roleIcon = (ImageView) convertView.findViewById(R.id.iv_role_icon);
            viewHolder.praiseNum = (TextView) convertView.findViewById(R.id.tv_praise_num);
            viewHolder.praiseImg = (ImageView) convertView.findViewById(R.id.iv_praise_img);
            viewHolder.content = (TextView) convertView.findViewById(R.id.tv_content);
            viewHolder.date = (TextView) convertView.findViewById(R.id.tv_date);
            viewHolder.dynamicContext = (TextView) convertView.findViewById(R.id.tv_dynamic_context);
            viewHolder.videoImg = (ImageView) convertView.findViewById(R.id.iv_video_img);
            viewHolder.videoPlay = (ImageView) convertView.findViewById(R.id.iv_video_play);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        DynamicMessageData dynamicData =  listMessage.get(position);
        Glide.with(mContext).load(dynamicData.getPhoto())
                .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                .transform(new GlideRoundTransform(mContext, R.dimen.height_40dp))
                .into(viewHolder.photo);
        viewHolder.roleIcon.setImageResource(RoleUtil.getRoleIcon(dynamicData.getRole()));
        viewHolder.date.setText(dynamicData.getDateTime());

        if(DynamicMessageData.TYPE_D_TEXT.equals(dynamicData.getdType()))
        {
            //文字
            viewHolder.dynamicContext.setVisibility(View.VISIBLE);
            viewHolder.videoImg.setVisibility(View.GONE);
            viewHolder.videoPlay.setVisibility(View.GONE);
            viewHolder.dynamicContext.setText(dynamicData.getdTitle());
        }else if(DynamicMessageData.TYPE_D_IMG.equals(dynamicData.getdType()))
        {
            //图片
            viewHolder.dynamicContext.setVisibility(View.GONE);
            viewHolder.videoImg.setVisibility(View.VISIBLE);
            viewHolder.videoPlay.setVisibility(View.GONE);
            Glide.with(mContext).load(dynamicData.getMessageCover())
                    .placeholder(R.drawable.default_img_43)
                    .into(viewHolder.videoImg);
        }else if(DynamicMessageData.TYPE_D_VIDEO.equals(dynamicData.getdType())){
            //视频
            viewHolder.dynamicContext.setVisibility(View.GONE);
            viewHolder.videoImg.setVisibility(View.VISIBLE);
            viewHolder.videoPlay.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(dynamicData.getMessageCover())
                    .placeholder(R.drawable.default_img_43)
                    .into(viewHolder.videoImg);
        }
        else {
            //默认为文字
            viewHolder.dynamicContext.setVisibility(View.VISIBLE);
            viewHolder.videoImg.setVisibility(View.GONE);
            viewHolder.videoPlay.setVisibility(View.GONE);
            viewHolder.dynamicContext.setText(dynamicData.getContent());
        }

        String name = dynamicData.getNickName();
        if(dynamicData.getRole() != RoleUtil.ROLE_PT)
        {
            name = "<font color=\"#ff7919\">"+name+"</font>";
        }

        if(DynamicMessageData.TYPE_M_COMMENT.equals(dynamicData.getmType()))
        {
            //回复
            viewHolder.praiseNum.setVisibility(View.GONE);
            viewHolder.praiseImg.setVisibility(View.GONE);
            viewHolder.date.setVisibility(View.VISIBLE);
            viewHolder.content.setText(Html.fromHtml(name + ":" + dynamicData.getContent()));
        }else if(DynamicMessageData.TYPE_M_PRAISE.equals(dynamicData.getmType()))
        {
            //点赞
            viewHolder.praiseNum.setVisibility(View.VISIBLE);
            if(!StringUtil.isNullorEmpty(dynamicData.getContent()))
            {
                viewHolder.praiseNum.setText("+"+dynamicData.getContent());
            }else {
                viewHolder.praiseNum.setText("+1");
            }
            viewHolder.praiseImg.setVisibility(View.VISIBLE);
            viewHolder.date.setVisibility(View.GONE);
            viewHolder.content.setText(dynamicData.getdTitle());
//            viewHolder.content.setText(Html.fromHtml(name + "赞了动态"));
        }else if(DynamicMessageData.TYPE_M_REPLY.equals(dynamicData.getmType()))
        {
            //评论
            viewHolder.praiseNum.setVisibility(View.GONE);
            viewHolder.praiseImg.setVisibility(View.GONE);
            viewHolder.date.setVisibility(View.VISIBLE);
            viewHolder.content.setText(Html.fromHtml(name + ":" + dynamicData.getContent()));
        }else{
            //默认为回复
            viewHolder.praiseNum.setVisibility(View.GONE);
            viewHolder.praiseImg.setVisibility(View.GONE);
            viewHolder.date.setVisibility(View.VISIBLE);
            viewHolder.content.setText(Html.fromHtml(name + ":" + dynamicData.getContent()));
        }

        //监听器
        viewHolder.photo.setOnClickListener(new PhotoClickListener(dynamicData));

        return convertView;
    }

    public void setListMessage(List<DynamicMessageData> listMessage) {
        this.listMessage.clear();
        this.listMessage.addAll(listMessage);
        notifyDataSetChanged();
    }

    public void addListMessage(List<DynamicMessageData> listMessage) {
        this.listMessage.addAll(listMessage);
        notifyDataSetChanged();
    }

    public List<DynamicMessageData> getListMessage() {
        return listMessage;
    }

    public void setOnClickListener(OnClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    private class ViewHolder{
        ImageView photo;
        ImageView roleIcon;
        ImageView praiseImg;
        TextView praiseNum;

        TextView content;
        TextView date;

        TextView dynamicContext;
        ImageView videoImg;
        ImageView videoPlay;
    }

    public interface OnClickListener{
        public void onClickPhoto(DynamicMessageData messageData);
    }

    private class PhotoClickListener implements View.OnClickListener{
        private DynamicMessageData dynamicData;
        public PhotoClickListener(DynamicMessageData dynamicData){
            this.dynamicData = dynamicData;
        }

        @Override
        public void onClick(View v) {
            if(mClickListener != null)
            {
                mClickListener.onClickPhoto(dynamicData);
            }
        }
    }


}
