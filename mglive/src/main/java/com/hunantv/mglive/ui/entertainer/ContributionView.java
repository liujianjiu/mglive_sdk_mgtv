package com.hunantv.mglive.ui.entertainer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.ui.entertainer.adapate.ContributionAdapater;
import com.hunantv.mglive.ui.entertainer.data.ContrData;
import com.hunantv.mglive.ui.entertainer.data.ContributionData;
import com.hunantv.mglive.ui.entertainer.data.ContributionInfoData;
import com.hunantv.mglive.ui.entertainer.listener.LoadmoreListener;
import com.hunantv.mglive.ui.entertainer.listener.PayContributionListener;
import com.hunantv.mglive.ui.live.detail.DetailConstants;
import com.hunantv.mglive.utils.MGTVUtil;
import com.hunantv.mglive.widget.Toast.GuardDialog;
import com.hunantv.mglive.widget.loadingView.LoadingViewHelper;

import java.util.List;

/**
 * Created by admin on 2015/11/10.
 */
public class ContributionView
        implements View.OnClickListener, AbsListView.OnScrollListener, OnTouchListener, ContributionBottomView.BuyCallBack {
    private FullAgreeView parentView;
    private Context context;

    private View clickView;
    private LinearLayout closeView;

    private ListView contributionListView;
    private ContributionAdapater contributionAdapter;

    private ContributionBottomView contributionBottomView;
    private int firstitem = -1;
    private boolean isClose = false;
    private LoadmoreListener mLoadmoreListener;
    private View view, contrView;
    private boolean isShowDefault = true;
    private RelativeLayout mBar;
    public TextView mTitle;
    private LoadingViewHelper mLoadingViewHelper;

    //dialog相关参数
    private PayContributionListener payContributionListener;
    private List<ContrData> contrDatas;
    private int payType;
    private ContributionInfoData contrInfo;

    public ContributionView(Context context, FullAgreeView parentView) {
        this.parentView = parentView;
        this.context = context;
        initView();
    }

    private void initView() {
        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            view = layoutInflater.inflate(R.layout.layout_star_contribution_view, null);
            mTitle = (TextView) view.findViewById(R.id.dynamic_title);
            initView(view);
        }
    }

    public View getView() {
        if (view == null) {
            initView();
        }
        return view;
    }

    void initView(final View view) {
        mBar = (RelativeLayout) view.findViewById(R.id.contributionTopLayout);
        contrView = view.findViewById(R.id.rl_contr_view);

        clickView = view.findViewById(R.id.clickView);
        clickView.setOnTouchListener(this);
        clickView.setOnClickListener(this);

        closeView = (LinearLayout) view.findViewById(R.id.closeView);
        closeView.setOnClickListener(this);

        contributionBottomView =
                (ContributionBottomView) view.findViewById(R.id.contributionBottomView);
        contributionBottomView.setBuyCallBack(this);
        contributionBottomView.initView();

        contributionListView = (ListView) view.findViewById(R.id.contributionListView);
        contributionAdapter = new ContributionAdapater(context);
        contributionListView.setAdapter(contributionAdapter);
        contributionListView.setOnScrollListener(this);

        mLoadingViewHelper = new LoadingViewHelper(contrView);
        mLoadingViewHelper.showLoading();
    }

    @Override
    public void onClick(View v) {
            if(v.getId() == R.id.closeView){
                parentView.closeView();
                clickView.setVisibility(View.VISIBLE);
            }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (firstitem == 0 && contributionListView.getChildAt(0).getTop() ==
                contributionListView.getPaddingTop()) {
            if (isClose && scrollState == SCROLL_STATE_IDLE) {
                parentView.closeFullArgeeView();
                clickView.setVisibility(View.VISIBLE);
            }
            isClose = true;
        } else {
            isClose = false;
        }

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount) {
        firstitem = firstVisibleItem;

        if (firstVisibleItem + visibleItemCount >
                totalItemCount - DetailConstants.REMAIN_LOAD_MORE && totalItemCount > DetailConstants.REMAIN_LOAD_MORE && mLoadmoreListener != null) {
            mLoadmoreListener.onLoadMore();
        }
    }

    private void changeView() {
        if (isShowDefault) {
            clickView.setVisibility(View.GONE);
            mLoadingViewHelper.show(context.getResources().getString(R.string.empty_guard), R.drawable.empty_guard, context.getResources().getString(R.string.me_guard), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showGuardDialog();
                }
            }, new LoadingViewHelper.CustomListener() {
                @Override
                public void custom(View view, TextView text, Button btn) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) btn.getLayoutParams();
                    layoutParams.width = context.getResources().getDimensionPixelOffset(R.dimen.height_140dp);
                    layoutParams.height = context.getResources().getDimensionPixelOffset(R.dimen.height_36dp);
                    layoutParams.topMargin = context.getResources().getDimensionPixelOffset(R.dimen.margin_18dp);
                    btn.setBackgroundResource(R.drawable.star_default_conbribution_btn_bg);
                    btn.setTextColor(Color.parseColor("#ffffff"));
                    btn.setTextSize(TypedValue.COMPLEX_UNIT_PX,context.getResources().getDimensionPixelOffset(R.dimen.text_size15dp));
                }
            });
        } else {
            contrView.setVisibility(View.VISIBLE);
            clickView.setVisibility(View.VISIBLE);
            mLoadingViewHelper.restore();
        }
    }

    public void setDatas(List<ContributionData> datas) {
        if (datas.size() > 0) {
            isShowDefault = false;
        } else {
            isShowDefault = true;
        }
        changeView();
        contributionAdapter.setDatas(datas);
    }

    public void addDatas(List<ContributionData> datas) {
        contributionAdapter.addDatas(datas);
    }

    public void setLoadMoreListener(LoadmoreListener listener) {
        mLoadmoreListener = listener;
    }

    public void setPayContributionListener(PayContributionListener payContributionListener) {
        this.payContributionListener = payContributionListener;
    }

    public void setContrDatas(List<ContrData> contrDatas) {
        this.contrDatas = contrDatas;
    }

    public void setContrInfo(ContributionInfoData contrInfo) {
        contributionBottomView.setContrInfo(contrInfo);
    }

    int i = 200;
    float startY = 0;
    float endY = 0;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        {
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                // 主点按下
                case MotionEvent.ACTION_DOWN:
                    startY = event.getRawY();
                    return true;
                case MotionEvent.ACTION_UP:
                    endY = event.getRawY();
                    float transY = Math.abs(endY - startY);
                    if (Math.abs(transY) > i) {
                        if (startY > endY) {
                            if (v.getId() == R.id.clickView) {
                                //上滑
                                parentView.fullAgreeBellowView();
                                clickView.setVisibility(View.GONE);
                            }
                        }
                    }
                    return true;
            }
            return false;
        }
    }

    public void setShowDefault(boolean isShowDefault) {
        this.isShowDefault = isShowDefault;
        changeView();
    }

    @Override
    public void buyContr(int payType, ContributionInfoData contrInfo) {
        if (MaxApplication.getApp().isLogin()) {
            this.payType = payType;
            this.contrInfo = contrInfo;

            showGuardDialog();
            clickView.setVisibility(View.VISIBLE);
        } else {
            MGTVUtil.getInstance().login(MaxApplication.getAppContext());
        }
    }

    private void showGuardDialog(){
        if(contrDatas != null && contrDatas.size() > 0)
        {
            int[] position = new int[2];
            mBar.getLocationOnScreen(position);
            Rect outRect = new Rect();
            ((Activity)context).getWindow().getDecorView().getWindowVisibleDisplayFrame(outRect);
            DisplayMetrics d = new DisplayMetrics();
            ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(d);
            int height = (int) ((d.widthPixels * 0.75) + mBar.getMeasuredHeight() + outRect.top);
            new GuardDialog(context,d.heightPixels - height,payType,contrDatas,contrInfo,payContributionListener).show();

            if(parentView.isFullAgree())
            {
                parentView.closeFullArgeeView();
            }
        }
    }



    public void setClickViewVisible() {
        clickView.setVisibility(View.VISIBLE);
    }


    public void resetSelection(){
        if(contributionListView != null){
            contributionListView.setSelection(0);
        }
    }
//    private void dismissDialog(){
//        if(mGuardDialog != null && mGuardDialog.isShowing())
//        {
//            mGuardDialog.dismiss();
//        }
//    }
//
//    private boolean isShowDialog(){
//        return mGuardDialog != null && mGuardDialog.isShowing() ? true : false;
//    }
}
