package com.hunantv.mglive.ui.discovery;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.hunantv.mglive.R;
import com.hunantv.mglive.common.BaseActivity;
import com.hunantv.mglive.common.BaseBar;
import com.hunantv.mglive.common.BuildConfig;
import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.common.DiscoveryConstants;
import com.hunantv.mglive.common.FormEncodingBuilderEx;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.ResultModel;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.data.user.NoticeMessage;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.StringUtil;

import org.json.JSONException;

import java.util.Map;

/**
 * 发现_广场页面
 * @author liujianjiu
 *
 */
public class SquareActivity extends BaseActivity implements View.OnClickListener {
    public static final String KEY_SQUARE_MESSAGE_NEW_NUM = "KEY_SQUARE_MESSAGE_NEW_NUM";
    public static final int RESULT_PUBLISH_RETURN = 10;

    public static final String KEY_DYANIMC_MODEL = "isSquareModle";
    public static final String KEY_DYANIMC_COUNT = "dynamic_count";

    /** 标题栏*/
    private BaseBar mBar;
    /** 发布布局*/
    private PublishPreView mPreView;
    /** 动态列表布局*/
    private DynamicLayout mDynamicLayout;

    /** 动态列表子布局*/
    private ListView mDynamicListView;
    private View mDynamicHeadView;

    /**消息布局 */
    private RelativeLayout mMessageView;
    private RelativeLayout mMessageNewView;
    private TextView mMsessageNewNum;

    String mTitle = "";
    String mCount = "";
    int mNewNum = 0;

    /**
     * true:广场模式，现在没有广场模式了
     * false:艺人空间模式/自己空间模式
     */
    private boolean isSquareModle = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setContentView(R.layout.activity_square);

        initInComeData();
        initBarView();
        initHeadView();
        initView();

        mDynamicLayout.setIsSquareModle(isSquareModle);//是否广场模式
        if(!isSquareModle){//个人空间，传入用户信息
            StarModel starModel = new StarModel();
            starModel.setUid(MaxApplication.getInstance().getUid());
            mDynamicLayout.setStarInfo(starModel);
        }
        mDynamicLayout.loadData();//加载数据
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isLogin()) {
            loadUserMessage();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDynamicLayout.onDestroy();
    }

    /**
     * 顶部的Bar初始化
     */
    private void initBarView() {
        mBar = (BaseBar) findViewById(R.id.squ_bar);

        mBar.mTitle.setText(mTitle + mCount);
        mBar.mShareIcon.setImageResource(R.drawable.max_10_squ_add_icon);
        mBar.mShareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo:maxxiang 暂时注册掉，不然你们普通用户发不了贴了，嘿嘿
                int role = MaxApplication.getInstance().getUserInfo().getRole();
                if (role <= 0) {   //0 普通用户, 1 艺人, 3 明星, 4 系统用户   普通用户引到报名，通过审核后，才能发贴。
                    return;
                }

                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_bottom_in);
                mPreView.setAnimation(animation);
                mPreView.setVisibility(View.VISIBLE);
            }
        });

    }


    /**
     * 页面初始化
     */
    private void initView() {
        mPreView = (PublishPreView) findViewById(R.id.squ_publishpreView);
        mPreView.mHideIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_bottom_out);
                mPreView.setAnimation(animation);
                mPreView.setVisibility(View.GONE);
            }
        });
        mDynamicLayout = (DynamicLayout)findViewById(R.id.squ_dynamic_lay);
        mDynamicListView = mDynamicLayout.getDynamicList().getRefreshableView();
        mDynamicListView.addHeaderView(mDynamicHeadView);
    }

    /**
     * 初始化新消息头部View
     */
    private void initHeadView() {
        LayoutInflater inflater = getLayoutInflater();
        mDynamicHeadView = inflater.inflate(R.layout.layout_dynamic_message_clock, null);
        mMessageView = (RelativeLayout) mDynamicHeadView.findViewById(R.id.rl_message_view);
        mMessageNewView = (RelativeLayout) mDynamicHeadView.findViewById(R.id.rl_message_new_view);
        mMsessageNewNum = (TextView) mDynamicHeadView.findViewById(R.id.tv_message_update_num);
    }

    /**
     * 设置消息数
     */
    private void setNewMessageView() {
        if (mNewNum > 0) {
            if (mMessageView.getVisibility() == View.GONE) {
                mMessageView.setVisibility(View.VISIBLE);
            }
            mMsessageNewNum.setText(Html.fromHtml("<font color=\"#ff7919\">" + mNewNum + "</font>条新消息"));
            mMessageNewView.setOnClickListener(this);

        } else {
            if (mMessageView.getVisibility() == View.VISIBLE) {
                mMessageView.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 初始化传入参数
     */
    private void initInComeData(){
        boolean bol = true;
        try {
            String s = getIntent().getStringExtra(KEY_DYANIMC_MODEL);
            if (!StringUtil.isNullorEmpty(s) && s.equals("0")) {
                bol = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String str = "";
        try {
            str = getIntent().getStringExtra(DiscoveryConstants.KEY_TITLE);
        } catch (Exception e) {
            L.e(TAG, e.getMessage());
        }
        if (!StringUtil.isNullorEmpty(str)) {
            mTitle = str;
        }
        isSquareModle = bol;
        if (!isSquareModle) {
            try {
                int ct = getIntent().getIntExtra(KEY_DYANIMC_COUNT, 0);
                mCount = "(" + ct + ")";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mNewNum = getIntent().getIntExtra(KEY_SQUARE_MESSAGE_NEW_NUM, 0);
    }

    /**
     * 获取用户消息数据
     */
    private void loadUserMessage() {
        //获取用户消息数据
        Map<String, String> param = new FormEncodingBuilderEx()
                .add("uid", getUid())
                .add("token", getToken())
                .build();
        post(BuildConfig.URL_MESSAGE_MYNOTICE, param);
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException {
        super.onSucceed(url, resultModel);
        if (BuildConfig.URL_MESSAGE_MYNOTICE.equals(url)) {
            NoticeMessage mNoticeMessage = JSON.parseObject(resultModel.getData(), NoticeMessage.class);
            if (!isSquareModle) {
                mCount = "(" + mNoticeMessage.getDyCount() + ")";
                mBar.mTitle.setText(mTitle + mCount);
            }
            mNewNum = mNoticeMessage.getDyMsgCount();
            setNewMessageView();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_PUBLISH_RETURN) {
            mDynamicLayout.loadData();  //刷新数据
        }
    }

    @Override
    public void onClick(View v) {
            if(v.getId() == R.id.rl_message_new_view){
            }
    }
}