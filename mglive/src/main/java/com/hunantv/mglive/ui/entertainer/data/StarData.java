package com.hunantv.mglive.ui.entertainer.data;

/**
 * Created by admin on 2015/11/18.
 */
public class StarData {
    //live
    private int liveIcon;
    private int faceIcon;
    private String name;

    //推荐
    private int personNum;
    private int fasonNum;

    public int getLiveIcon() {
        return liveIcon;
    }

    public void setLiveIcon(int liveIcon) {
        this.liveIcon = liveIcon;
    }

    public int getFaceIcon() {
        return faceIcon;
    }

    public void setFaceIcon(int faceIcon) {
        this.faceIcon = faceIcon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPersonNum() {
        return personNum;
    }

    public void setPersonNum(int personNum) {
        this.personNum = personNum;
    }

    public int getFasonNum() {
        return fasonNum;
    }

    public void setFasonNum(int fasonNum) {
        this.fasonNum = fasonNum;
    }
}
