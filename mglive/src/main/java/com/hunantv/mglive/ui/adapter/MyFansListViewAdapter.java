package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.user.MyFansData;
import com.hunantv.mglive.utils.GlideRoundTransform;
import com.hunantv.mglive.utils.RoleUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author QiuDa
 *         <p/>
 *         listView 推荐关注
 */
public class MyFansListViewAdapter extends BaseAdapter{
    private Context context;
    private int mNewFansNum = 0;

    private List<MyFansData> mFansDatas = new ArrayList<MyFansData>();
    private OnClickListener mClickListener;

    public MyFansListViewAdapter(Context context, int newFansNum) {
        this.context = context;
        this.mNewFansNum = newFansNum;
    }

    @Override
    public int getCount() {
        return mFansDatas != null ? mFansDatas.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mFansDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            ViewHold viewHold = new ViewHold();
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_my_fans_item, null);
            viewHold.photo = (ImageView) convertView.findViewById(R.id.iv_fans_photo);
            viewHold.roleIcon = (ImageView) convertView.findViewById(R.id.iv_role_icon);
            viewHold.newFansIcon = (ImageView) convertView.findViewById(R.id.iv_new_fans);
            viewHold.name = (TextView) convertView.findViewById(R.id.tv_name);
            viewHold.attenBtn = (LinearLayout) convertView.findViewById(R.id.ll_atten_btn);
            viewHold.forwordImg = (ImageView) convertView.findViewById(R.id.iv_forword);
            convertView.setTag(viewHold);
        }
        ViewHold viewHold = (ViewHold) convertView.getTag();

        MyFansData fansData = mFansDatas.get(position);
        Glide.with(context).load(fansData.getPhoto())
                .placeholder(R.drawable.default_icon_preload).error(R.drawable.default_icon)
                .transform(new GlideRoundTransform(context, R.dimen.height_50dp)).into(viewHold.photo);
        viewHold.name.setText(fansData.getNickName());
        viewHold.roleIcon.setImageResource(RoleUtil.getRoleIcon(fansData.getRole()));
        if(position < mNewFansNum)
        {
            viewHold.newFansIcon.setVisibility(View.VISIBLE);
        }else
        {
            viewHold.newFansIcon.setVisibility(View.GONE);
        }
        if(fansData.getRole() != RoleUtil.ROLE_PT)
        {
//            if(!Boolean.parseBoolean(fansData.getIsfans()))
//            {
//                viewHold.forwordImg.setVisibility(View.GONE);
//                viewHold.attenBtn.setVisibility(View.VISIBLE);
//                viewHold.attenBtn.setOnClickListener(new AtteBtnListener(fansData,viewHold));
//            }
//            else
//            {
                viewHold.attenBtn.setVisibility(View.GONE);
                viewHold.forwordImg.setVisibility(View.VISIBLE);
//            }
            viewHold.photo.setOnClickListener(new PhotoListener(fansData,viewHold));
        }else {
            viewHold.attenBtn.setVisibility(View.GONE);
            viewHold.forwordImg.setVisibility(View.GONE);
        }
        return convertView;
    }

    public class ViewHold
    {
        ImageView photo;
        ImageView roleIcon;
        ImageView newFansIcon;
        TextView name;
        LinearLayout attenBtn;
        ImageView forwordImg;
    }

    public void setFansData(List<MyFansData> fans) {

        if(fans != null){
            this.mFansDatas.clear();
            this.mFansDatas.addAll(fans);
        }

    }

    public void addFansData(List<MyFansData> fans) {
        if(fans != null){
            this.mFansDatas.addAll(fans);
        }
    }

    public List<MyFansData> getFansData() {
        return mFansDatas;
    }

    protected class PhotoListener implements View.OnClickListener{
        private MyFansData fansData;
        private ViewHold viewHold;
        public PhotoListener(MyFansData fansData, ViewHold viewHold)
        {
            this.fansData = fansData;
            this.viewHold = viewHold;
        }

        @Override
        public void onClick(View v) {
            if(viewHold.attenBtn.getVisibility() == View.VISIBLE && mClickListener != null)
            {
                mClickListener.onClickPhoto(fansData);
            }
        }
    }

    public class AtteBtnListener implements View.OnClickListener{
        private MyFansData starModel;
        private ViewHold viewHold;
        public AtteBtnListener (MyFansData starModel,ViewHold viewHold)
        {
            this.starModel = starModel;
            this.viewHold = viewHold;
        }

        @Override
        public void onClick(View v) {
            if(mClickListener != null)
            {
                boolean result = mClickListener.onClickAtten(starModel, v);
                if(result && viewHold!= null)
                {
                    viewHold.attenBtn.setVisibility(View.GONE);
                    viewHold.forwordImg.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public interface OnClickListener {
        /**
         * 点击粉她回调
         * @param fansData
         * @return
         */
        public boolean onClickAtten(MyFansData fansData, View view);

        public void onClickPhoto(MyFansData fansData);
    }

    public void setClickListener(OnClickListener listener) {
        this.mClickListener = listener;
    }

    public void cleanItems()
    {
        if(mFansDatas != null)
        {
            mFansDatas.clear();
            notifyDataSetChanged();
        }
    }

    /**
     * 判断当前Item是否允许点击
     * @param view
     * @return
     */
    public boolean isClickItem(View view)
    {
        Object tagObj = view.getTag();
        if(tagObj instanceof ViewHold)
        {
            ViewHold viewHold = (ViewHold) tagObj;
            if(viewHold.forwordImg.getVisibility() == View.VISIBLE)
            {
                return true;
            }
        }
        return false;
    }
}
