package com.hunantv.mglive.ui.handle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.BuildConfig;
import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.common.FormEncodingBuilderEx;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.ResultModel;
import com.hunantv.mglive.utils.HttpUtils;
import com.tencent.connect.share.QQShare;
import com.tencent.connect.share.QzoneShare;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by admin on 2015/12/12.
 */
public class TencentHandle implements IUiListener, HttpUtils.callBack {
    public static final String PLATFORM_TYPE_SDK_QQ = "2";

    private Context mContext;
    private HttpUtils http;
    private Tencent mTencent;

    public TencentHandle(Context context) {
        this.mContext = context;
        http = new HttpUtils(context, this);
        mTencent = Tencent.createInstance(Constant.QQ_APP_ID, MaxApplication.getAppContext());
    }

    public void login() {
        if (!mTencent.isSessionValid()) {
            mTencent.login((Activity) mContext, "get_simple_userinfo", this);
        }
    }

    public void shareToQQ(String title, String des, String linkurl, String imageUrl) {
        final Bundle params = new Bundle();
        params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_DEFAULT);
        params.putString(QQShare.SHARE_TO_QQ_TITLE, title);
        params.putString(QQShare.SHARE_TO_QQ_SUMMARY, des);
        params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, linkurl);
        params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL, imageUrl);
        params.putString(QQShare.SHARE_TO_QQ_APP_NAME, mContext.getString(R.string.app_name));
        params.putInt(QQShare.SHARE_TO_QQ_EXT_INT, 0);
        mTencent.shareToQQ((Activity) mContext, params, this);
    }


    public void shareToQzone(String title, String des, String linkurl, String imageUrl) {
        ArrayList<String> imageList = new ArrayList<>();
        imageList.add(imageUrl);
        final Bundle params = new Bundle();
        //分享类型
        params.putInt(QzoneShare.SHARE_TO_QZONE_KEY_TYPE, QzoneShare.SHARE_TO_QZONE_TYPE_IMAGE_TEXT);
        params.putString(QzoneShare.SHARE_TO_QQ_TITLE, title);//必填
        params.putString(QzoneShare.SHARE_TO_QQ_SUMMARY, des);//选填
        params.putString(QzoneShare.SHARE_TO_QQ_TARGET_URL, linkurl);//必填
        params.putStringArrayList(QzoneShare.SHARE_TO_QQ_IMAGE_URL, imageList);
        mTencent.shareToQzone((Activity) mContext, params, this);
    }


    /**
     * qq登录成功回调
     *
     * @param o
     */
    @Override
    public void onComplete(Object o) {
        try {
            JSONObject json = new JSONObject(o.toString());
            if (!json.isNull("access_token") && !json.isNull("openid") && !json.isNull("expires_in")) {
                String accessToken = json.getString("access_token");
                String openId = json.getString("openid");
                String expireTime = json.getString("expires_in");
                Map<String, String> param = new FormEncodingBuilderEx()
                        .add("code", "")
                        .add("accessToken", accessToken)
                        .add("openId", openId)
                        .add("expireTime", expireTime)
                        .add("platform", PLATFORM_TYPE_SDK_QQ)
                        .build();
//                http.post(BuildConfig.URL_THIRD_LOGIN_TOKEN, param);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("onComplete:", o.toString());
    }

    /**
     * qq登录失败回调
     *
     * @param e
     */
    @Override
    public void onError(UiError e) {
        Log.d("onError:", "code:" + e.errorCode + ", msg:"
                + e.errorMessage + ", detail:" + e.errorDetail);
    }

    /**
     * qq取消登录回调
     */
    @Override
    public void onCancel() {
        Log.d("onCancel", "onCancel");
    }

    /**
     * activity回调
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResultData(int requestCode, int resultCode, Intent data) {
        Tencent.onActivityResultData(requestCode, resultCode, data, this);
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException {
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
    }


    @Override
    public boolean get(String url, Map<String, String> param) {
        return http.get(url, param);
    }

    @Override
    public boolean post(String url, Map<String, String> param) {
        return http.post(url, param);
    }

    @Override
    public void onError(String url, Exception e) {

    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        return null;
    }
}
