package com.hunantv.mglive.ui.discovery;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;

import com.hunantv.mglive.aidl.FreeGiftCallBack;
import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.utils.FreeGiftUtils;
import com.hunantv.mglive.utils.L;

/**
 * Created by June Kwok on 2015-12-28.
 */
public class FreeGiftAnimation extends View implements FreeGiftCallBack {

    private static final int STOK_WIDTH = Constant.toPix(5);//画笔的宽度
    private int mWidth;
    private int mHeight;
    private int mRadius;
    private Paint mPaint, mpaintTxt, mPaintBg;
    private RectF mRect;
    private int mEndAngle = 0;
    private static final int COLOR_1 = 0xFFFF7919;
    private FreeGiftCallBack mCallBack;

    public FreeGiftCallBack getCallBack() {
        return mCallBack;
    }

    public void setCallBack(FreeGiftCallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    public FreeGiftAnimation(Context context, int mWidth) {
        super(context);
        this.mWidth = mWidth;
        this.mHeight = mWidth;
        init();
    }

    private void init() {
        FreeGiftUtils.getInstance().startService();
        mPaint = new Paint();
        mPaint.setColor(COLOR_1);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(STOK_WIDTH);
        mPaint.setAntiAlias(true); //去锯齿

        mpaintTxt = new Paint();
        mpaintTxt.setColor(COLOR_1);
        mpaintTxt.setTextSize(Constant.toPix(30));
        mpaintTxt.setAntiAlias(true); //去锯齿

        mPaintBg = new Paint();
        mPaintBg.setAntiAlias(true);
        mPaintBg.setColor(0x7FFFFFFF);

        mRect = new RectF();
        initLength();
    }

    private void initLength() {
        mWidth = getWidth();
        mHeight = getHeight();
        int mXCenter = mWidth / 2;
        int mYCenter = mHeight / 2;
        mRadius = mWidth / 2;
        int tempRadius = mRadius - STOK_WIDTH / 2;
        mRect.left = (mXCenter - tempRadius);
        mRect.top = (mYCenter - tempRadius);
        mRect.right = tempRadius * 2 + (mXCenter - tempRadius);
        mRect.bottom = tempRadius * 2 + (mYCenter - tempRadius);
    }

    boolean isNeedShow = false;//黄色的圈圈
    boolean isBackShow = false;//阴影的背景
    public boolean isTimeLeftShow = false;//是否在花上面显示倒计时

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isBackShow) {
            canvas.drawCircle(mRadius, mRadius, mRadius, mPaintBg);
        }
        if (isNeedShow) {
            initLength();
            canvas.drawArc(mRect, mEndAngle - 90, 360 - mEndAngle, false, mPaint);
            if (isTimeLeftShow) {
                String str = (360 - mEndAngle) / 2 + "";
                canvas.drawText(str, mRadius - mpaintTxt.measureText(str) / 2, mRadius + Constant.toPix(15), mpaintTxt);
            }
        }
    }

    public void setStartAngle(int time) {
        isNeedShow = true;
        mEndAngle = 360 - 2 * time;
        postInvalidate();
    }

    public void showCount(boolean isShow) {
        isNeedShow = isShow;
        invalidate();
    }

    public void showBg(boolean isShow) {
        isBackShow = isShow;
        invalidate();
    }

    /**
     * 清除一切
     */
    public void reset() {
        isNeedShow = false;
        isBackShow = false;
        invalidate();
    }

    public void addCall() {
        if (null != FreeGiftUtils.getInstance().getService()) {
            FreeGiftUtils.getInstance().addCallback(this);
        }
    }

    public void startAnima() {
        if (null != FreeGiftUtils.getInstance().getService()) {
            addCall();
//            try {
//                FreeGiftUtils.getInstance().getService().startAction(MaxBGService.ACTION_TYPE_GIFT);
//            } catch (RemoteException e) {
//                L.e(FreeGiftAnimation.class.getName(), e);
//            }
        }
    }

    @Override
    public void onTimeLeft(int left) {
        if (null != getCallBack()) {
            try {
                getCallBack().onTimeLeft(left);
            } catch (RemoteException e) {
                L.e(FreeGiftAnimation.class.getName(), e);
            }
        }
        setStartAngle(left);
        int freeGiftCount = MaxApplication.getInstance().getFreeGiftCount();
        if (freeGiftCount == 0) {
            showBg(true);
            showCount(true);
        } else {
            showBg(false);
            showCount(false);
        }
    }

    @Override
    public void onNumAdd(int num) {
        int freeGiftCount = MaxApplication.getInstance().getFreeGiftCount();
        if (freeGiftCount >= 5) {
            reset();
        }
        if (null != getCallBack()) {
            try {
                getCallBack().onNumAdd(num);
            } catch (RemoteException e) {
                L.e(FreeGiftAnimation.class.getName(), e);
            }
        }
    }

    public void onDestory() {
        FreeGiftUtils.getInstance().setCallback(null);
        FreeGiftUtils.getInstance().removeAllCallback();
    }

    @Override
    public IBinder asBinder() {
        return null;
    }
}
