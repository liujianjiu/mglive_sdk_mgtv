package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import mgtvcom.devsmart.android.ui.HorizontalListView;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.data.star.ZoneRankModel;
import com.hunantv.mglive.ui.live.StarDetailActivity;
import com.hunantv.mglive.utils.StringUtil;

import java.util.List;

/**
 * Created by admin on 2016/2/29.
 */
public class ZoneRankAdapter extends BaseAdapter{
    private Context mContext;
    private List<ZoneRankModel> mZoneRanks;

    public ZoneRankAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        if(mZoneRanks != null)
        {
            return mZoneRanks.size() > 3 ? 3 : mZoneRanks.size();
        }else
        {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
        {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(
                    R.layout.layout_zone_rank_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.mBar = (RelativeLayout) convertView.findViewById(R.id.rl_title_bar);
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_title);
            viewHolder.mDesc = (TextView) convertView.findViewById(R.id.tv_desc);
            viewHolder.mRankView = (HorizontalListView) convertView.findViewById(R.id.hls_rank_view);
            convertView.setTag(viewHolder);
        }
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        ZoneRankModel zoneRankModel = mZoneRanks.get(position);
        viewHolder.mBar.setOnClickListener(new ForwordListener(zoneRankModel.getUrl()));
        viewHolder.mTitle.setText(zoneRankModel.getRankingName());
        viewHolder.mDesc.setText(zoneRankModel.getDes());
//        viewHolder.mRankView.disableParentTouch(true);
        viewHolder.mRankView.setAdapter(new ZoneRankItemAdapter(mContext, zoneRankModel.getStars(),zoneRankModel.getImage(), position));
        viewHolder.mRankView.setOnItemClickListener(new ZoneStarListener(zoneRankModel.getUrl()));
        return convertView;
    }

    public void setZoneRanks(List<ZoneRankModel> zoneRanks) {
        this.mZoneRanks = zoneRanks;
        notifyDataSetChanged();
    }

    private class ForwordListener implements View.OnClickListener{
        private String url;

        public ForwordListener(String url) {
            this.url = url;
        }

        @Override
        public void onClick(View v) {
//            forword(url);
        }
    }

    private class ZoneStarListener implements OnItemClickListener{
        private String url;

        public ZoneStarListener(String url) {
            this.url = url;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            StarModel star = (StarModel) parent.getAdapter().getItem(position);
            if (star != null) {
                if(!StringUtil.isNullorEmpty(star.getUid()))
                {
                    Intent intent = new Intent(mContext, StarDetailActivity.class);
                    intent.putExtra(StarDetailActivity.KEY_STAR_ID, star.getUid());
                    mContext.startActivity(intent);
                }
            } else {
//                forword(url);
            }
        }
    }

    private class ViewHolder{
        RelativeLayout mBar;
        TextView mTitle;
        TextView mDesc;
        HorizontalListView mRankView;
    }
}
