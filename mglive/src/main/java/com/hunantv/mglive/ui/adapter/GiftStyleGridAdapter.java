package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.aidl.FreeGiftCallBack;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.GiftDataModel;
import com.hunantv.mglive.ui.discovery.FreeGiftAnimation;
import com.hunantv.mglive.utils.L;

import java.util.List;

/**
 * Created by June Kwok on 2015/12/2.
 */
public class GiftStyleGridAdapter extends BaseAdapter {
    private Context mContxt;
    private List<GiftDataModel> mGiftList;
    private int mColor;
    private int mWidth;
    private ViewHolder mFreeViewHolder;
    private boolean mIsLang;

    public GiftStyleGridAdapter(Context context, List<GiftDataModel> mGiftList,boolean  isLang) {
        this.mContxt = context;
        this.mGiftList = mGiftList;
        mIsLang = isLang;
        mColor = mIsLang ? 0xFFFFFFFF : 0xff161719;
        mWidth = mContxt.getResources().getDimensionPixelOffset(mIsLang ? R.dimen.height_40dp : R.dimen.height_58dp);
    }

    @Override
    public int getCount() {
        if (mGiftList == null)
            return 0;
        return mGiftList.size();
    }

    @Override
    public GiftDataModel getItem(int arg0) {
        if (mGiftList == null)
            return null;
        return mGiftList.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContxt).inflate(R.layout.fragment_gift_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.mIcon = (ImageView) convertView.findViewById(R.id.giftImage4);
            viewHolder.mName = (TextView) convertView.findViewById(R.id.giftNameText4);
            viewHolder.mNum = (TextView) convertView.findViewById(R.id.goldTextView4);
            viewHolder.mImgParent = (FrameLayout) convertView.findViewById(R.id.giftImageParent);
            viewHolder.mName.setTextColor(this.mColor);
            LinearLayout.LayoutParams ps = (LinearLayout.LayoutParams) viewHolder.mImgParent.getLayoutParams();
            ps.weight = mWidth;
            ps.height = mWidth;
            ps.topMargin = mContxt.getResources().getDimensionPixelOffset(mIsLang ? R.dimen.height_6dp : R.dimen.height_10dp);
            viewHolder.mImgParent.setLayoutParams(ps);
            FrameLayout.LayoutParams psI = (FrameLayout.LayoutParams) viewHolder.mIcon.getLayoutParams();
            psI.width = mWidth;
            psI.height = mWidth;
            viewHolder.mIcon.setLayoutParams(psI);
            convertView.setTag(viewHolder);
        }
        final ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        GiftDataModel gift = mGiftList.get(position);
        Glide.with(mContxt).load(gift.getPhoto()).into(viewHolder.mIcon);
        viewHolder.mName.setText(gift.getName());
        if (gift.getPrice() == 0) {
            mFreeViewHolder = viewHolder;
            if (null != viewHolder.mAnima) {
                viewHolder.mAnima.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mAnima = new FreeGiftAnimation(mContxt, 0);
                viewHolder.mAnima.isTimeLeftShow = true;
                viewHolder.mAnima.setCallBack(new FreeGiftCallBack() {
                    @Override
                    public IBinder asBinder() {
                        return null;
                    }

                    @Override
                    public void onTimeLeft(int left) {
                    }

                    @Override
                    public void onNumAdd(int num) {
                        int number = MaxApplication.getInstance().getFreeGiftCount();
                        viewHolder.mNum.setText("免费(" + number + ")");
                    }
                });
                FrameLayout.LayoutParams mLP = new FrameLayout.LayoutParams(mWidth, mWidth);
                viewHolder.mImgParent.addView(viewHolder.mAnima, mLP);
            }
            viewHolder.mNum.setTextColor(0xFFFF7919);
            int number = MaxApplication.getInstance().getFreeGiftCount();
            viewHolder.mNum.setText("免费(" + number + ")");
            viewHolder.mAnima.showBg(number == 0);
            viewHolder.mAnima.addCall();
            if (number == 0) {
                viewHolder.mAnima.startAnima();
            }
        } else if (gift.getPrice() >= 0) {
            if (null != viewHolder.mAnima) {
                viewHolder.mAnima.setVisibility(View.GONE);
            }
            viewHolder.mNum.setTextColor(0xff969696);
            viewHolder.mNum.setText(gift.getPrice() + "金币");
        }
        return convertView;
    }

    public void onDestory(){
        if(mFreeViewHolder != null)
        {
            mFreeViewHolder.mAnima.onDestory();
        }
    }

    /**
     *
     */

    public class ViewHolder {
        public ImageView mIcon;
        public FrameLayout mImgParent;
        public FreeGiftAnimation mAnima;
        public TextView mName;
        public TextView mNum;
    }
}