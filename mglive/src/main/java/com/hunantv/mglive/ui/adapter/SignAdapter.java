package com.hunantv.mglive.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.sign.SignInfoModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2016/2/18.
 */
public class SignAdapter extends BaseAdapter {
    private Context mContext;
    private List<SignInfoModel> mSignInfos;

    public SignAdapter(Context contex){
        this.mContext = contex;
    }
    @Override
    public int getCount() {
        if(mSignInfos != null)
        {
            return mSignInfos.size() < 3 ? mSignInfos.size() : 3;
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_sign_item, null);
            ViewHolde viewHolde = new ViewHolde();

            viewHolde.mTitle = (TextView) convertView.findViewById(R.id.tv_title);
            viewHolde.mBg = (ImageView) convertView.findViewById(R.id.iv_bg);
            viewHolde.mGoldView = (RelativeLayout) convertView.findViewById(R.id.rl_gold);
            viewHolde.mGoldImg = (ImageView) convertView.findViewById(R.id.iv_sign_gold);
            viewHolde.mGoldNum = (TextView) convertView.findViewById(R.id.tv_gold_num);
            viewHolde.mReceiveImg = (ImageView) convertView.findViewById(R.id.iv_receive);
            convertView.setTag(viewHolde);
        }
        ViewHolde viewHolde = (ViewHolde) convertView.getTag();
        SignInfoModel signInfo = mSignInfos.get(position);
        viewHolde.mTitle.setText(signInfo.getDateInfo());
        viewHolde.mGoldNum.setText(signInfo.getCoinInfo()+"金币");
        if("1".equals(signInfo.getIsReceive()))
        {
            //已领
            Glide.with(mContext).load(R.drawable.sign_receive).into(viewHolde.mBg);
            viewHolde.mGoldView.setBackgroundResource(R.drawable.sign_item_bg_shape);
            Glide.with(mContext).load(R.drawable.sign_gold).into(viewHolde.mGoldImg);
            viewHolde.mGoldNum.setTextColor(Color.parseColor("#ff7919"));
            viewHolde.mReceiveImg.setVisibility(View.VISIBLE);
            viewHolde.isSign = false;
        }else {
            //未领
            if("1".equals(signInfo.getEnable())){
                //可领
                Glide.with(mContext).load(R.drawable.sign_receive).into(viewHolde.mBg);
                viewHolde.mGoldView.setBackgroundResource(R.drawable.sign_item_bg_disable_shape);
                Glide.with(mContext).load(R.drawable.sign_gold).into(viewHolde.mGoldImg);
                viewHolde.mGoldNum.setTextColor(Color.parseColor("#ff7919"));
                viewHolde.mReceiveImg.setVisibility(View.GONE);
                viewHolde.isSign = true;
            }else{
                //不可领
                Glide.with(mContext).load(R.drawable.sign_receive_disable).into(viewHolde.mBg);
                viewHolde.mGoldView.setBackgroundResource(R.drawable.sign_item_bg_shape);
                Glide.with(mContext).load(R.drawable.sign_gold_disable).into(viewHolde.mGoldImg);
                viewHolde.mGoldNum.setTextColor(Color.parseColor("#666666"));
                viewHolde.mReceiveImg.setVisibility(View.GONE);
                viewHolde.isSign = false;
            }
        }

        return convertView;
    }

    public void setSignInfos(List<SignInfoModel> signInfos) {
        this.mSignInfos = signInfos;
        notifyDataSetChanged();
    }

    public class ViewHolde{
        TextView mTitle;
        ImageView mBg;
        RelativeLayout mGoldView;
        ImageView mGoldImg;
        TextView mGoldNum;
        public ImageView mReceiveImg;
        public boolean isSign;
    }
}
