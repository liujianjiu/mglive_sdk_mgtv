package com.hunantv.mglive.common;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * <br>类描述:公用的屏幕像素处理工具
 * <br>功能详细描述:初始化之后可以拿到一些常用的变量
 *
 * @author June Kwok
 * @date [2015-11-7]
 */
public class Constant {
    //城市JSON
    public static final String CITY_FILE_NAME = "citys.json";
    //免费礼物的开始数
    public static int FREE_NORMAL_GIFT_TIME_COUNT = 180;
    //免费礼物的倒计时数
    public static long FREE_GITL_ANIMA_START_TIME = -1;
    //RSA公钥
    public static final String RSA_FILE_NAME = "rsa_pub.pem";

    //第三方登录配置
    public static final String QQ_APP_ID = "100994359";//QQid
    public static final String WX_APP_ID = "wxbbc6e0adf8944632";//微信id
    public static final String WEIXIN_SECRET_KEY = "388b8be98cdbc649b7290fbc671e59dc";
    public static final String WB_APP_ID = "885156017";//微博id

    //maxxiang 头像裁剪用到
    public static final String SRC_IMAGE_PATH = "src_image_path";
    public static final String DST_IMAGE_PATH = "dst_image_path";
    public static final String SRC_IMAGE_URI = "src_image_uri";
    public static final String IS_SUCCESS = "is_success";


    public static final int REQUEST_CHOOSE_PORTRAIT = 1101;
    public static final int REQUEST_CROP_PORTRAIT = 1102;

    //后台协议部分用到的参数名称,统一定义在这里
    public static final String KEY_PAGE = "page";
    public static final String KEY_PAGE_SIZE = "pageSize";
    public static final String KEY_VERSION = "version";
    public static final String KEY_UID = "uid";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_LID = "lid";
    public static final String KEY_TYPE = "type";
    public static final String KEY_CHANNELID = "channelId";
    public static final String KEY_MEDIAID = "mediaId";
    public static final String KEY_RATE = "rate";
    public static final String DE_RATE = "SD";
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_NICK_NAME = "nickName";
    public static final String KEY_DEVICE_ID = "deviceId";
    public static final String KEY_APPVERSION = "appVersion";
    public static final String KEY_ENDTYPE = "endType";
    public static final String KEY_CUSTOMINFO = "customInfo";

    public static final Integer PAGE_SIZE = 20;

    public static final String MGLIVE_URL = "mglive";
    public static final String SQUARE_URL = "mglive://mglive/feeds";//艺人动态
    public static final String NORMAL_URL = "mglive://mglive/artistvote";//大众评审
    public static final String UPLOAD_VIDEO_URL = "mglive://mglive/upload";//作品上传
    public static final String SUPER_WOMAN_RANK_URL = "mglive://mglive/top";//超女排行榜
    public static final String MGLIVE_URL_HOME = "mglive://mglive/home";//芒果直播首页
    public static final String MGLIVE_URL_LIVE = "mglive://mglive/play";//直播间
    public static final String MGLIVE_URL_artistpace = "mglive://mglive/artistpace";//艺人空间
    public static final String MGLIVE_URL_coupon = "mglive://mglive/coupon";//我的卡券
    public static final String MGLIVE_URL_PAY = "mglive://mglive/pay";//我的金币
    public static final String MGLIVE_URL_Dynamic_Detail = "mglive://mglive/feeddetail";//动态详情
    public static final String MGLIVE_URL_REDIRECT = "mglive://mglive/redirect";//自测用，胡松需要
    public static final String MGLIVE_URL_RECOM = "mglive://mglive/recommend";//猜你喜欢
    public static final String MGLIVE_URL_ACT = "mglive://mglive/act";//活动
    public static final String MGLIVE_URL_TEMPLATE = "mglive://mglive/channel";//模板
    public static final String MGLIVE_URL_STAR_LIVE = "mglive://mglive/plive";//秀场直播
    public static final String MGLIVE_URL_STAR_PLAY = "mglive://mglive/play";//场景直播
    public static final String MGLIVE_URL_WEB = "mglive://mglive/webview";//webview

    public static final String URL_BEARTIST = "http://help.max.mgtv.com/beartist";//超女报名


    //********** 二维码  begin  **********
    public static final int QR_CODE_SCAN_RESULT = 60;//二维码扫描返回码
    public static final String QR_CODE_SCAN_KEY = "QR_CODE_SCAN_KEY";//二维码扫描结果

    public static final String QR_HOST = ".mgtv.com";//login 登录path

    public static final String QR_PATH_LOGIN = "/app";//网页登录
    public static final String QR_PATH_PLIVE = "/plive";//个人直播
    public static final String QR_PATH_ARTIST = "/artist/space";//艺人空间
    public static final String QR_PATH_DYNAMIC_DETAIL= "/feeddetail";//动态详情
    public static final String QR_PATH_SCENE_LIVE= "/artist/live";//场景直播

    public static final String QR_ACTION = "max_qr_action";//表示该二维码的用途   login 登录
    public static final String QR_ACTION_LOGIN = "login";//表示该二维码的用途   login 登录
    public static final String QR_CODE = "max_qr_code";// 二维码的唯一标记



    //********** 二维码  end  **********

    //********** 免费花  begin  **********
    public static final String REQEUST_HEADER = "mgtv-agent";
    public static final String APP_KEY = "B51E3F8CA40E5878FAEB76161FCFE5FF";
    public static final String REQEUST_HEADER_SIGN = "mgtv-max-sig";
    //********** 免费花  end  **********

    public static final String RECORDVIDEO_FROM = "from";
    public static final String RECORDVIDEO_columnId = "columnId";  //栏目ID
    public static final String RECORDVIDEO_aId = "aId";  //活动ID
    public static final String RECORDVIDEO_topic = "topic";  //活动主题
    public static final String RECORDVIDEO_title = "title";  //标题
    public static final String FROMWEBVIEW = "fromWebView";  //从哪个界面来

    public static final int RESULT_COMMON = 100; //通用的返回状态
    public static final int RECORDVIDEO_FROM_DYNAMIC = 0;  //从广场调用录制视频界面
    public static final int RECORDVIDEO_FROM_BAOMING = 1;  //从超女报名调用录制视频界面
    public static final int RECORDVIDEO_FROM_PK = 2;  //从PK赛调用录制视频界面

    //活动页面key
    public static final String ACT_KEY_TITLE = "KEY_TITLE";
    public static final String ACT_KEY_ACT_ID = "KEY_ACT_ID";
    public static final String ACT_KEY_STAGE_ID = "KEY_STAGE_ID";
    public static final String ACT_KEY_PK_INFO = "KEY_PK_INFO";
    public static final String ACT_KEY_TIPS = "ACT_KEY_TIPS";
    public static final String ACT_KEY_SATGE_ACTION = "ACT_KEY_SATGE_ACTION";


    public static final String JUMP_INTENT_DATA = "JUMP_INTENT_DATA";
    public static final String JUMP_INTENT_STAR_VIDEO_TITLE = "JUMP_INTENT_STAR_VIDEO_DATA";
    public static final String JUMP_INTENT_STAR_VIDEO_STAR = "JUMP_INTENT_STAR_VIDEO_STAR";
    public static final String JUMP_INTENT_VIDEO_PATH = "JUMP_INTENT_VIDEO_PATH";
    public static final String JUMP_INTENT_VIDEO_PLAYED = "JUMP_INTENT_VIDEO_PLAYED";
    public static final String JUMP_INTENT_VIDEO_PROGRESS = "JUMP_INTENT_VIDEO_PROGRESS";
    public static final String JUMP_INTENT_VIDEO_POSITION = "JUMP_INTENT_VIDEO_POSITION";
    public static final String JUMP_INTENT_DYNAMIC_VIDEO_ID = "JUMP_INTENT_DYNAMIC_VIDEO_ID";

    public static final String CHANNEL_FIRST_TAG = "_first";//首发标记

    //绑定手机-是否跳转到签到页
    public static final String KEY_TO_SIGN = "KEY_TO_SIGN";
    /**
     * sDateFormat
     */
    public static String sDateFormat = "yyyy年MM月dd日";
    /**
     * 默认屏幕的宽度
     */
    public static final int S_DEFAULT_WIDTH = 750;
    /**
     * 默认屏幕的高度
     */
    public static final int S_DEFAULT_HEIGHT = 1334;
    /**
     * 状态栏的高度
     */
    public static int sStatusBarHeight = 0;
    /**
     * 任务栏的高度
     */
    public static int sNavBarHeight = 0;
    /**
     * 屏幕宽度 单位 像素
     */
    public static int sRealWidth = S_DEFAULT_WIDTH;
    /**
     * 屏幕高度 单位 像素
     */
    public static int sRealHeight = S_DEFAULT_HEIGHT;

    /**
     * 宽度屏幕比例
     */
    public static float sScaleX = 1.0f;
    /**
     * 高度屏幕比例
     */
    public static float sScaleY = 1.0f;

    /**
     * 时间字体
     */
    public static Typeface sTypefaceTime;
    /**
     * 普通公共字体
     */
    public static Typeface sTypeface;
    /**
     * 数字字体
     */
    public static Typeface sTypefaceNum;
    /**
     * App是否全屏
     */
    public static boolean mIsFullScreen = false;
    /**
     * 跳转直播/点播的Lid参数
     */
    public static final String JUMP_INTENT_LID = "JUMP_INTENT_LID";
    /**
     * 跳转直播/点播的Uid参数
     */
    public static final String JUMP_INTENT_UID = "JUMP_INTENT_UID";
    /**
     * 跳转直播/点播的类型参数
     */
    public static final String JUMP_INTENT_TYPE = "JUMP_INTENT_TYPE";

    /**
     * 默认最小时长
     */
    public static float DEFAULT_MIN_DURATION_LIMIT = 8;
    /**
     * 默认时长
     */
    public static float DEFAULT_MAX_DURATION_LIMIT = 300;
    /**
     * 视频录制的宽
     */
    public static int DEFAULT_WIDTH = 480;
    /**
     * 视频录制的高
     */
    public static int DEFAULT_HEIGHT = 480;
    /**
     * 默认码率
     */
    public static int DEFAULT_BITRATE = 800 * 1000;
    /**
     * 默认Video保存路径，请开发者自行指定
     */
    public static String VIDEOPATH = FileUtils.newOutgoingFilePath();
    /**
     * 默认缩略图保存路径，请开发者自行指定
     */
    public static String THUMBPATH = VIDEOPATH + ".jpg";
    /**
     * 水印本地路径，文件必须为rgba格式的PNG图片
     */
    public static String WATER_MARK_PATH = "assets://Qupai/watermark/qupai-logo.png";


    /**
     * 获取实际像素值根据宽度比例
     *
     * @param n
     * @return
     */
    public static float toPixf(int n) {
        return n * sRealWidth / S_DEFAULT_WIDTH;
    }

    /**
     * 获取实际像素值根据宽度比例
     *
     * @param n
     * @return
     */
    public static int toPix(int n) {
        return n * sRealWidth / S_DEFAULT_WIDTH;
    }

    /**
     * 获取实际像素值根据宽度比例
     *
     * @param n
     * @return
     */
    public static int toPix(boolean b, int n) {
        return b ? n * sRealWidth / S_DEFAULT_WIDTH : toPixByHeight(n);
    }

    /**
     * 获取实际像素值根据长度比例
     *
     * @param n
     * @return
     */
    public static int toPixByHeight(int n) {
        return n * sRealHeight / S_DEFAULT_HEIGHT;
    }

    /**
     * 获取实际像素值根据长度比例
     *
     * @param n
     * @return
     */
    public static float toPixByHeightf(int n) {
        return n * sRealHeight / S_DEFAULT_HEIGHT;
    }

    /**
     * <br>功能简述:初始化屏幕信息
     * <br>功能详细描述:
     * <br>注意:
     *
     * @param ctx
     */
    public static void initMetrics(Context ctx) {
        DisplayMetrics mDm = ctx.getResources().getDisplayMetrics();
        sRealWidth = mDm.widthPixels;
        sRealHeight = mDm.heightPixels;
        getStatusBarHeight(ctx);
        getNavBarHeight(ctx);
        sRealHeight = sRealHeight - sStatusBarHeight;
    }

    /**
     * <br>功能简述:初始化状态栏的高度
     * <br>功能详细描述:
     * <br>注意:
     *
     * @param ctx
     * @return
     */
    public static int getStatusBarHeight(Context ctx) {
        if (sStatusBarHeight > 0) {
            return sStatusBarHeight;
        }
        Class<?> c = null;
        Object obj = null;
        Field field = null;
        int x = 0;
        try {
            c = Class.forName("com.android.internal.R$dimen");
            obj = c.newInstance();
            field = c.getField("status_bar_height");
            x = Integer.parseInt(field.get(obj).toString());
            sStatusBarHeight = ctx.getResources().getDimensionPixelSize(x);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return sStatusBarHeight;
    }

    /**
     * 初始化状透明任务栏的高度
     *
     * @param ctx
     */
    public static int getNavBarHeight(Context ctx) {
        if (sNavBarHeight > 0) {
            return sNavBarHeight;
        }
        if (ctx != null) {
            WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Class<?> sClass = null;
            try {
                if (sClass == null) {
                    sClass = Class.forName("android.view.Display");
                }
                Point realSize = new Point();
                Method method = sClass.getMethod("getRealSize", Point.class);
                method.invoke(display, realSize);
                sNavBarHeight = realSize.y - sRealHeight;
            } catch (Exception e) {
                sNavBarHeight = 0;
            }
        }
        return sNavBarHeight;
    }

    /**
     * 获取ActionBar距离上面的距离
     *
     * @return
     */
    public static int getTopMargin() {
        return mIsFullScreen ? sStatusBarHeight : 0;
    }

    /**
     * 数据初始化
     *
     * @param mCtx
     */
    public static void initData(Context mCtx, boolean sIsFull) {
        mIsFullScreen = sIsFull;
        initMetrics(mCtx);
    }
}
