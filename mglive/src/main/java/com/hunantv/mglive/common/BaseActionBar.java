package com.hunantv.mglive.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.hunantv.mglive.R;

/**
 * Created by max on 2015/11/13.
 */
    public class BaseActionBar extends FrameLayout {

        public BaseActionBar(Context context) {
            super(context);
            initLayout(context);
        }

        public BaseActionBar(Context context, AttributeSet attrs) {
            super(context, attrs);
            initLayout(context);
        }

        public BaseActionBar(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            initLayout(context);
        }

    private void initLayout(Context context) {
        View.inflate(context, R.layout.layout_base_action_bar, this);
    }

    public FrameLayout getRightExtraContainer() {
        return (FrameLayout) findViewById(R.id.right_extra_container);
    }
    }