package com.hunantv.mglive.common;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;

import com.hunantv.mglive.data.ResultModel;
import com.hunantv.mglive.data.config.ConfigModel;
import com.hunantv.mglive.data.login.UserInfoData;
import com.hunantv.mglive.utils.DeviceInfoUtil;
import com.hunantv.mglive.utils.FreeGiftUtils;
import com.hunantv.mglive.utils.HttpUtils;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.SharePreferenceUtils;
import com.hunantv.mglive.utils.StringUtil;
import com.hunantv.mglive.utils.TokenPreference;

import java.util.Map;

/**
 * Created by 达 on 2015/11/11.
 */
public class MaxApplication {
    private  Application mApp;
    private  static MaxApplication mMaxApp;
    private UserInfoData mUserInfo;
    private final Object obj = new Object();


    private String clientId;//个推clientId

    private ConfigModel configModel;
    private int freeGiftCount = 0;          //免费礼物数量

    public boolean isSaveToken = false;

    public synchronized static MaxApplication getInstance() {
        if(mMaxApp == null){
            mMaxApp = new MaxApplication();
        }
        return mMaxApp;
    }

    private MaxApplication(){};

    /**
     * 初始化芒果直播功能
     */
    public boolean initMGLive(Application app) {
        try {
            mApp = app;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

        Log.i(this.getClass().toString(),"initSucces");
        return true;

    }

    public void initUserInfo() {
        synchronized (obj) {
            mUserInfo = TokenPreference.getInstance(mApp).getUserInfo();
            if (mUserInfo != null) {
                isSaveToken = true;
            }
        }
    }

    public static Context getAppContext() {
        return getInstance().mApp;
    }

    public static MaxApplication getApp() {
        return getInstance();
    }

    public UserInfoData getUserInfo() {
        if (mUserInfo == null) {
            initUserInfo();
        }
        return mUserInfo;
    }

    public void setUserInfo(UserInfoData userInfo) {
        synchronized (obj) {
            this.mUserInfo = userInfo;
            //持久化用户登录信息
            saveUserInfo();
        }
    }

    /**
     * 刷新用户信息,不刷新token
     */
    public void refreshUserInfo(UserInfoData userInfo) {
        if (mUserInfo == null) {
            initUserInfo();
        }
        if (this.mUserInfo != null) {
            String token = this.mUserInfo.getToken();
            this.mUserInfo = userInfo;
            this.mUserInfo.setToken(token);
            saveUserInfo();
        }
    }

    /**
     * 刷新Token,不刷新用户信息
     */
    public void refreshToken(String token) {
        if (!StringUtil.isNullorEmpty(token) && mUserInfo != null) {
            mUserInfo.setToken(token);
            saveUserInfo();
        } else if (mUserInfo == null) {
            initUserInfo();
            saveUserInfo();
        }
    }


    /**
     * 获取用户Token
     */
    public String getToken() {
        if (mUserInfo == null) {
            initUserInfo();
        }
        if (mUserInfo != null && !StringUtil.isNullorEmpty(mUserInfo.getToken())) {
            return mUserInfo.getToken();
        }
        return "";
    }

    public void saveUserInfo() {
        SharePreferenceUtils sharePreferenceUtils = new SharePreferenceUtils(mApp, SharePreferenceUtils.FILE_KEY_HINT_DATA);
        sharePreferenceUtils.updateSaveTokenTime();
        TokenPreference.getInstance(mApp).saveToken(getUserInfo());
    }

    /**
     * 获取用户id
     */
    public String getUid() {
        if (mUserInfo == null) {
            initUserInfo();
        }
        if (mUserInfo != null && !StringUtil.isNullorEmpty(mUserInfo.getUid())) {
            return mUserInfo.getUid();
        }
        return "";
    }

    /**
     * 登出
     */
    public void logout() {
        this.mUserInfo = null;
        TokenPreference.getInstance(mApp).removeToken();
        new SharePreferenceUtils(mApp, SharePreferenceUtils.FILE_KEY_ZAN_DATA).clearZanDynamic();
    }

    public String webViewAppendString() {
        return "MGlive-aphone-" + DeviceInfoUtil.getVersionName(getAppContext());
    }

    /**
     * 新增视频上传回调
     * 是否登录
     */
    public boolean isLogin() {
        if (mUserInfo == null) {
            initUserInfo();
        }
        return mUserInfo != null && !StringUtil.isNullorEmpty(mUserInfo.getToken());
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setConfigModel(ConfigModel configModel) {
        this.configModel = configModel;
    }

    public int getFreeGiftCount() {
        return this.freeGiftCount;
    }

    public void setFreeGiftCount(int count) {
        this.freeGiftCount = count;
        //为0 重新调用免费礼物初始化,防止app挂掉后免费花服务未启动
        if (freeGiftCount == 0) {
            FreeGiftUtils.getInstance();
        }
    }

    /**
     * 获取application中指定的meta-data
     *
     * @return 如果没有获取成功(没有对应值或者异常)，则返回值为空
     */
    public static String getAppMetaData(Context ctx, String key) {
        if (ctx == null || TextUtils.isEmpty(key)) {
            return null;
        }
        String resultData = null;
        try {
            PackageManager packageManager = ctx.getPackageManager();
            if (packageManager != null) {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(ctx.getPackageName(),
                        PackageManager.GET_META_DATA);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        resultData = applicationInfo.metaData.getString(key);
                    }
                }

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public String getSignFill0() {
        StringBuilder signFill0 = new StringBuilder();
        //默认使用3个0加密
        signFill0.append("000");
        return signFill0.toString();
    }

}
