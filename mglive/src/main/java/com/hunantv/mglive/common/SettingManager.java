package com.hunantv.mglive.common;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 2015/12/19.
 */
public class SettingManager {
    private static final String TAG = "SettingManager";
    private static SettingManager instance;
    private static boolean bWifiCache;


    public static SettingManager getInstance(){
        if (instance == null){
            instance = new SettingManager();
            instance.init();
        }
        return instance;
    }

    private static void init() {
        //读取设置里的push是否打开
        SharedPreferences sp =MaxApplication.getAppContext().getSharedPreferences("MGLiveSetting", Context.MODE_PRIVATE);
        bWifiCache = sp.getBoolean("bWifiCache", true);
    }

    public static boolean getWifiCache(){
        return bWifiCache;
    }

    public static void setWifiCache(boolean b){
        bWifiCache = b;

        SharedPreferences sp =MaxApplication.getAppContext().getSharedPreferences("MGLiveSetting", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();
        editor.putBoolean("bWifiCache", bWifiCache);
        editor.commit();
    }
}
