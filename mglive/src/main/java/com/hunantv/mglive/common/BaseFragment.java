package com.hunantv.mglive.common;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;
import com.hunantv.mglive.data.ResultModel;
import com.hunantv.mglive.data.login.UserInfoData;
import com.hunantv.mglive.utils.HttpUtils;
import com.hunantv.mglive.utils.MGTVUtil;
import com.hunantv.mglive.utils.MqttChatUtils;

import java.util.Map;

public class BaseFragment extends Fragment implements HttpUtils.callBack {
    public final String TAG = this.getClass().getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private HttpUtils mHttp;


    private String mParam1;
    private String mParam2;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BaseFragment.
     */
    public static BaseFragment newInstance(String param1, String param2) {
        BaseFragment fragment = new BaseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        checkHttpUtil();
    }

    private void checkHttpUtil(){
        if(mHttp == null){
            mHttp = new HttpUtils(getContext(), this);
        }
    }

    public Context getContext(){
        return MaxApplication.getAppContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_base_fragmetn, container, false);
    }


    @Override
    public boolean get(String url, Map<String, String> param) {
        checkHttpUtil();
        return mHttp.get(url, param);
    }

    @Override
    public boolean post(String url, Map<String, String> param) {
        checkHttpUtil();
        return mHttp.post(url, param);
    }

    public boolean post(String url, Map<String, String> param, Map<String, String> header) {
        checkHttpUtil();
        return mHttp.post(url, param, header);
    }

    public boolean post(String url, Map<String, String> param, Object tag) {
        checkHttpUtil();
        return mHttp.post(url, param, tag);
    }

    @Override
    public void onError(String url, Exception e) {

    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        return null;
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {

    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) {

    }

    /**
     * 获取用户信息
     *
     * @return
     */
    public UserInfoData getUserInfo() {
        return MaxApplication.getInstance().getUserInfo();
    }

    /**
     * 获取用户Token
     *
     * @return
     */
    public String getToken() {
        return MaxApplication.getInstance().getToken();
    }

    /**
     * 获取用户id
     *
     * @return
     */
    public String getUid() {
        return MaxApplication.getInstance().getUid();
    }

    /**
     * 登出
     */
    public void logout() {
        MaxApplication.getInstance().logout();
    }

    /**
     * 是否登录
     *
     * @return
     */
    public boolean isLogin() {
        return MaxApplication.getInstance().isLogin();
    }

    /**
     * 获取个推clientId
     *
     * @return
     */
    public String getClientId() {
        return MaxApplication.getInstance().getClientId();
    }

    /**
     * 无参数页面跳转
     *
     * @param cls
     */
    public void navigate(Class cls) {
        Intent intent = new Intent(getActivity(), cls);
        startActivity(intent);
    }

    /**
     * 跳转到登陆页面
     */
    public void jumpToLogin(String title) {
        MGTVUtil.getInstance().login(MaxApplication.getAppContext());
    }

    /**
     * 跳转到登陆页面
     */
    public void jumpToLogin(String title, Intent intent) {
        MGTVUtil.getInstance().login(MaxApplication.getAppContext());
    }

    /**
     * 跳转到登陆页面
     */
    public void jumpToLogin(Intent intent) {
        MGTVUtil.getInstance().login(MaxApplication.getAppContext());
    }

    protected MqttChatUtils getMqtt(){
        return MqttChatUtils.getInstance();
    }

    protected String getMqttFlag() {
        return getMqtt().getFlag();
    }

    protected String getMqttKey() {
        return getMqtt().getKey();
    }

    protected String getMqttClientId() {
        return getMqtt().getClientId();
    }

    @Override
    public void onDestroy() {
        if (mHttp != null) {
            mHttp.cancelAll();
        }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.with(this).onLowMemory();
    }

}
