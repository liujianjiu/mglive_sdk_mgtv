package com.hunantv.mglive.common;


import android.util.Log;

import com.hunantv.mglive.utils.ChannelUtil;
import com.hunantv.mglive.utils.DeviceInfoUtil;
import com.hunantv.mglive.utils.StringUtil;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;


public class FormEncodingBuilderEx {
    private SortedMap<String, String> mParam;

    public FormEncodingBuilderEx() {
        mParam = new TreeMap<>();

        String cid = DeviceInfoUtil.getCid();
        if (!StringUtil.isNullorEmpty(cid)) {
            mParam.put("gtCid", cid);
        }
        String osVersion = DeviceInfoUtil.getOSVersion();
        if (!StringUtil.isNullorEmpty(osVersion)) {
            mParam.put("osVersion", osVersion);
        }
        String ostype = DeviceInfoUtil.getOSType();
        if (!StringUtil.isNullorEmpty(ostype)) {
            mParam.put("osType", ostype);
        }
        String device = DeviceInfoUtil.getDevice();
        if (!StringUtil.isNullorEmpty(device)) {
            mParam.put("device", device);
        }
        String endType = DeviceInfoUtil.getEndType();
        if (!StringUtil.isNullorEmpty(endType)) {
            mParam.put("endType", endType);
        }
        String version = DeviceInfoUtil.getVersionName(MaxApplication.getAppContext());
        if (!StringUtil.isNullorEmpty(version)) {
            mParam.put("version", version);
        }
        String appVersion = DeviceInfoUtil.getVersionName(MaxApplication.getAppContext());
        if (!StringUtil.isNullorEmpty(appVersion)) {
            mParam.put("appVersion", appVersion);
        }
        String deviceId = DeviceInfoUtil.getDeviceId(MaxApplication.getAppContext());
        if (!StringUtil.isNullorEmpty(deviceId)) {
            mParam.put("deviceId", deviceId);
        }
        String token = MaxApplication.getInstance().getToken();
        if (!StringUtil.isNullorEmpty(token)) {
            mParam.put("token", token);
        }
        String ch = ChannelUtil.getChannel(MaxApplication.getAppContext());
        if (!StringUtil.isNullorEmpty(ch)) {
            mParam.put("ch", ch);
            Log.i("youmeng_ch", ch);
        }
        mParam.put("seqId", getSeqId());
    }


    private String getSeqId() {
        int value = (int) ((Math.random() * 9 + 1) * 100000);
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new java.util.Date()) + value;
    }


    public FormEncodingBuilderEx add(String name, String value) {
        if (name != null && value != null) {
            mParam.put(name, value);
        }
        return this;
    }

    public Map<String, String> build() {
        return mParam;
    }
}