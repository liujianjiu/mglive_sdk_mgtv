package com.hunantv.mglive.common;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.R;

/**
 * Created by max on 2015/11/13.
 */
public class BaseActionActivity extends BaseActivity {
    private boolean mOverLayActionBar = false;
    private BaseActionBar mCustomizedActionBar;
    private View mBackBtn;
    private FrameLayout mRightBtn;
    private Button mRightBtnText;
    private FrameLayout mRightImgBtn;
    private ImageView mRightBtnImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (mOverLayActionBar) {
            getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        }
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        super.onCreate(savedInstanceState);

        initActionBar();

        mBackBtn = mCustomizedActionBar.findViewById(R.id.actionbar_back_btn);
        mBackBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackClick(v);
                    }
                });

        mRightBtn = (FrameLayout) mCustomizedActionBar.findViewById(R.id.right_extra_container);
        mRightBtnText = (Button) findViewById(R.id.right_extra_button);
        mRightBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onRightBtnClick(v);
                    }
                });

        mRightImgBtn = (FrameLayout) mCustomizedActionBar.findViewById(R.id.fl_right_img_btn);
        mRightBtnImg = (ImageView) findViewById(R.id.b_right_img_btn);
        mRightImgBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onRightImgBtnClick(v);
                    }
                });
    }

    protected void onRightBtnClick(View v) {

    }

    protected void onRightImgBtnClick(View v) {

    }

    protected BaseActionBar createCustomizedActionBar() {
        mCustomizedActionBar = new BaseActionBar(this);
        return mCustomizedActionBar;
    }

    private void initActionBar() {
        ActionBar actionBar = this.getSupportActionBar();// getActionBar();
        //ActionBar actionBar = null; //TODO

        mCustomizedActionBar = createCustomizedActionBar();
        if (actionBar == null) {
            return;
        }

        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);

        if (mCustomizedActionBar != null) {
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


            actionBar.setCustomView(mCustomizedActionBar, params);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setDisplayShowCustomEnabled(true);
        }

        if (mOverLayActionBar) {
            actionBar.setBackgroundDrawable(new ColorDrawable(
                    getResources().getColor(android.R.color.transparent)));
        }
    }

    public BaseActionBar getCustomizedActionBar() {
        return mCustomizedActionBar;
    }

    public void setOverLayActionBar(boolean overLay) {
        mOverLayActionBar = overLay;
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        TextView titleView = (TextView) mCustomizedActionBar.findViewById(R.id.actionbar_title);
        if (titleView != null) {
            titleView.setText(title);
        }
    }

    protected void showBackIcon(boolean show) {
        mBackBtn.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public FrameLayout getRightExtraContainer() {
        return mCustomizedActionBar.getRightExtraContainer();
    }


    public void onBackClick(View v) {
        onBackPressed();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.with(this).onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Glide.with(this).onTrimMemory(level);
    }

    public void setRightBtnText(String text){
        if(mRightBtnText != null)
        {
            mRightBtnText.setText(text);
            mRightBtn.setVisibility(View.VISIBLE);
        }
    }

    public void setRightImageBtnText(@DrawableRes int img){
        if(mRightBtnImg != null){
            mRightBtnImg.setImageResource(img);
            mRightImgBtn.setVisibility(View.VISIBLE);
        }
    }

    public ImageView getRightBtnImg() {
        return mRightBtnImg;
    }

    public Button getRightBtnText() {
        return mRightBtnText;
    }
}