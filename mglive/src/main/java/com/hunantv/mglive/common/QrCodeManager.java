package com.hunantv.mglive.common;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.hunantv.mglive.ui.discovery.DetailsActivity;
import com.hunantv.mglive.ui.live.LiveDetailActivity;
import com.hunantv.mglive.ui.live.StarDetailActivity;
import com.hunantv.mglive.utils.MGTVUtil;
import com.hunantv.mglive.utils.StartActivityDelayUtil;
import com.hunantv.mglive.utils.StringUtil;

/**
 * Created by admin on 2016/4/26.
 */
public class QrCodeManager {
    /**
     * 二维码跳转
     *
     * @param context
     * @param url
     */
    public static void jumpToActivity(Context context, String url) {
        if (!StringUtil.isNullorEmpty(url) && (url.startsWith("http") || url.startsWith("https"))) {
            Uri uri = Uri.parse(url);
            String host = uri.getHost();
            String path = uri.getPath();
            if (!StringUtil.isNullorEmpty(host)) {
                if (host.endsWith(Constant.QR_HOST)) {
                    if(!StringUtil.isNullorEmpty(path) && path.endsWith("/"))
                    {
                        //兼容参数前带斜杠url
                        path = path.substring(0,path.length() - 1);
                    }
                    if (Constant.QR_PATH_LOGIN.equals(path)) {
                        jumpToConfirmLogin(context, uri);
                    } else if (Constant.QR_PATH_PLIVE.equals(path)) {
                        //个人直播
                        jumpToStarLive(context, uri);
                    } else if (Constant.QR_PATH_ARTIST.equals(path)) {
                        //艺人空间
                        jumpToStarRoom(context, uri);
                    } else if (Constant.QR_PATH_DYNAMIC_DETAIL.equals(path)) {
                        //动态详情
                        jumpToDynamicDetail(context, uri);
                    } else if(Constant.QR_PATH_SCENE_LIVE.equals(path)){
                        //场景直播
                        jumpToSceneLive(context,uri);
                    }else {
//                        jumpToWeb(context, url);
                    }
                } else {
//                    jumpToWeb(context, url);
                }
            }
        }
    }

    /**
     * 网页确认登录框
     *
     * @param context
     * @param uri
     */
    private static void jumpToConfirmLogin(Context context, Uri uri) {
        String action = uri.getQueryParameter(Constant.QR_ACTION);
        String code = uri.getQueryParameter(Constant.QR_CODE);
        //网页登录
        if (Constant.QR_ACTION_LOGIN.equals(action)) {
            MGTVUtil.getInstance().login(MaxApplication.getAppContext());
        }
    }

    /**
     * 艺人直播
     *
     * @param context
     * @param uri
     */
    private static void jumpToStarLive(Context context, Uri uri) {
        String auid = uri.getQueryParameter("auid");
        StartActivityDelayUtil startActivityDelayUtil = StartActivityDelayUtil.instance(context);
        startActivityDelayUtil.startAcitiy(auid);
    }

    /**
     * 艺人空间
     *
     * @param context
     * @param uri
     */
    private static void jumpToStarRoom(Context context, Uri uri) {
        String auid = uri.getQueryParameter("auid");
        Intent intent = new Intent(context, StarDetailActivity.class);
        intent.putExtra(StarDetailActivity.KEY_STAR_ID, auid);
        context.startActivity(intent);

    }

    /**
     * 动态详情
     *
     * @param context
     * @param uri
     */
    private static void jumpToDynamicDetail(Context context, Uri uri) {
        String fid = uri.getQueryParameter("fid");
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_DYNAMIC_ID, fid);
        context.startActivity(intent);
    }

    /**
     * 场景直播
     *
     * @param context
     */
    private static void jumpToSceneLive(Context context,Uri uri) {
        String lid = uri.getQueryParameter("lid");
        String type = uri.getQueryParameter("type");
        Intent intent = new Intent(context, LiveDetailActivity.class);
        intent.putExtra(Constant.JUMP_INTENT_LID, lid);
        intent.putExtra(Constant.JUMP_INTENT_TYPE, type);
        context.startActivity(intent);
    }
}
