package com.hunantv.mglive.common;

/**
 * Created by admin on 2015/12/1.
 */
public class DiscoveryConstants extends Constant {
    /**
     * 跳转页面是要带title过去  所以要这个KEY
     */
    public static final String KEY_TITLE = "TITLE_KEY";
    /**
     * 删除动态的Action
     */
    public static final String ACTION_DEL_DYNAMIC = "ACTION_DEL_DYNAMIC";
    /**
     * 删除动态 用户广播的Key
     */
    public static final String KEY_DEL_DYNAMIC = "KEY_DEL_DYNAMIC";
    /**
     * 更新动态的Action
     */
    public static final String ACTION_UPDATE_DYNAMIC = "ACTION_UPDATE_DYNAMIC";
}
