package com.hunantv.mglive.common;


import com.hunantv.mglive.utils.ChannelUtil;
import com.hunantv.mglive.utils.DeviceInfoUtil;
import com.hunantv.mglive.utils.NetworkUtils;
import com.hunantv.mglive.utils.StringUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;


public class FormVVReportBuilder {
    public static String SID;
    private SortedMap<String, String> mParam;

    public FormVVReportBuilder() {
        if (StringUtil.isNullorEmpty(SID)) {
            SID = DeviceInfoUtil.getDeviceId(MaxApplication.getAppContext()) + System.currentTimeMillis();
        }
        mParam = new TreeMap<>();
        DateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
        Date d = new Date();
        mParam.put("t", f.format(d));
        mParam.put("sid", SID);
        mParam.put("did", DeviceInfoUtil.getDeviceId(MaxApplication.getAppContext()));
        mParam.put("uuid", MaxApplication.getApp().getUid());
        mParam.put("net", getNetWorkType());
        mParam.put("mf", DeviceInfoUtil.getManufacturer());
        mParam.put("mod", DeviceInfoUtil.getDevice());
        String viersion = DeviceInfoUtil.getOSVersion();
        if (DeviceInfoUtil.isTablet(MaxApplication.getAppContext())) {
            mParam.put("sver", "apad-" + viersion);
        } else {
            mParam.put("sver", "aphone-" + viersion);
        }
        mParam.put("aver", DeviceInfoUtil.getVersionName(MaxApplication.getAppContext()) + "." + DeviceInfoUtil.getVersionCode(MaxApplication.getAppContext()));
        mParam.put("ch", ChannelUtil.getChannel(MaxApplication.getAppContext()));
    }


    private String getNetWorkType() {
        NetworkUtils.NetType netType = NetworkUtils.getNetworkType();
        if (netType == NetworkUtils.NetType.Wifi) {
            return "1";
        } else if (netType == NetworkUtils.NetType.G2) {
            return "4";
        } else if (netType == NetworkUtils.NetType.G3) {
            return "2";
        } else if (netType == NetworkUtils.NetType.G4) {
            return "3";
        }
        return "0";
    }

    public FormVVReportBuilder add(String name, String value) {
        if (name != null && value != null) {
            mParam.put(name, value);
        }
        return this;
    }

    public FormVVReportBuilder add(Map<String, String> params) {
        for (String key : params.keySet()) {
            mParam.put(key, params.get(key));
        }
        return this;
    }

    public Map<String, String> build() {
        return mParam;
    }
}