package com.hunantv.mglive.common;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;

import com.bumptech.glide.Glide;
import com.hunantv.mglive.data.ResultModel;
import com.hunantv.mglive.data.login.UserInfoData;
import com.hunantv.mglive.utils.HttpUtils;
import com.hunantv.mglive.utils.MGTVUtil;
import com.hunantv.mglive.utils.MqttChatUtils;
import com.hunantv.mglive.utils.PromoteGoldUtil;

import org.json.JSONException;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BaseActivity extends AppCompatActivity implements HttpUtils.callBack {
    public final String TAG = this.getClass().getSimpleName();
//    private ReceiveBroadCast mReceiveBroadCast = new ReceiveBroadCast();
//    private IntentFilter mFilter;
    private PromoteGoldUtil mPromoteGoldUtil;

    //maxxiang added 2015/11/30
    private static enum LifeChangeType {
        None,
        Create,
        Start,
        Resume,
        Pause,
        Stop,
        Destroy,
    }


    private List<OnActivityResultListener> mActivityResultListener =
            new LinkedList<>();

    public static void startActivityInternal(
            Context context, Class<? extends Activity> clazz) {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }


    public static interface OnActivityResultListener {
        void onActivityResult(BaseActivity activity, int requestCode,
                              int resultCode, Intent data);
    }


    public static abstract class OnSingleActivityResultListener
            implements OnActivityResultListener {

        @Override
        public void onActivityResult(final BaseActivity activity, int requestCode,
                                     int resultCode, Intent data) {
            handleActivityResult(activity, requestCode, resultCode, data);

            ThreadManager.getInstance().postOnUIHandler(new Runnable() {
                @Override
                public void run() {
                    activity.removeActivityResultListener(
                            OnSingleActivityResultListener.this);
                }
            });
        }

        protected abstract void handleActivityResult(
                BaseActivity activity, int requestCode,
                int resultCode, Intent data);
    }

    public void addActivityResultListener(OnActivityResultListener listener) {
        mActivityResultListener.add(listener);
    }

    public void removeActivityResultListener(OnActivityResultListener listener) {
        mActivityResultListener.remove(listener);
    }

    public Context getContext() {
        return this;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (OnActivityResultListener listener : mActivityResultListener) {
            listener.onActivityResult(this, requestCode, resultCode, data);
        }
    }
    //maxxiang added end 2015/11/30

    private HttpUtils mHttp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHttp = new HttpUtils(this, this);
        if (Build.VERSION.SDK_INT >= 19) {
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        //setContentView(R.layout.activity_base_actvity);
//        mFilter = new IntentFilter();


        mPromoteGoldUtil = new PromoteGoldUtil(getContext());
    }


    @Override
    protected void onDestroy() {
        if (mHttp != null) {
            mHttp.cancelAll();
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPromoteGoldUtil.regirsterActivity();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPromoteGoldUtil.unRegirsterAcivity();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mPromoteGoldUtil.hideGfitView();
    }

    @Override
    public boolean get(String url, Map<String, String> param) {
        return mHttp.get(url, param);
    }

    @Override
    public boolean post(String url, Map<String, String> param) {
        return mHttp.post(url, param);
    }

    public boolean post(String url, Map<String, String> param, Map<String, String> header) {
        return mHttp.post(url, param, header);
    }

    @Override
    public void onError(String url, Exception e) {

    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        return null;
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {

    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException {

    }

    /**
     * 获取用户信息
     *
     * @return
     */
    public UserInfoData getUserInfo() {
        return MaxApplication.getInstance().getUserInfo();
    }

    /**
     * 获取用户Token
     *
     * @return
     */
    public String getToken() {
        return MaxApplication.getInstance().getToken();
    }

    /**
     * 获取用户id
     *
     * @return
     */
    public String getUid() {
        return MaxApplication.getInstance().getUid();
    }

    /**
     * 登出
     */
    public void logout() {
        MaxApplication.getInstance().logout();
    }

    /**
     * 是否登录
     *
     * @return
     */
    public boolean isLogin() {
        return MaxApplication.getInstance().isLogin();
    }

    /**
     * 获取个推clientId
     *
     * @return
     */
    public String getClientId() {
        return MaxApplication.getInstance().getClientId();
    }

    /**
     * 无参数页面跳转
     *
     * @param cls
     */
    public void navigate(Class cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

    /**
     * 跳转到登陆页面
     */
    public void jumpToLogin(String title) {
        MGTVUtil.getInstance().login(this);
    }

    /**
     * 跳转到登陆页面
     */
    public void jumpToLogin(String title, Intent intent) {
        MGTVUtil.getInstance().login(this);
    }

    /**
     * 跳转到登陆页面
     */
    public void jumpToLogin(Intent intent) {
        MGTVUtil.getInstance().login(this);

    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

//
//    public class ReceiveBroadCast extends BroadcastReceiver {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            //得到广播中得到的数据，并显示出来
//            if (intent != null) {
//                String lid = intent.getStringExtra(Constant.JUMP_INTENT_LID);
//                String type = intent.getStringExtra(Constant.JUMP_INTENT_TYPE);
//                if (!StringUtil.isNullorEmpty(lid) && !StringUtil.isNullorEmpty(type)) {
//                    Intent intentJump = new Intent(BaseActivity.this, LiveDetailActivity.class);
//                    intentJump.putExtra(Constant.JUMP_INTENT_LID, lid);
//                    intentJump.putExtra(Constant.JUMP_INTENT_TYPE, type);
//                    startActivity(intentJump);
//                }
//            }
//        }
//
//    }

    protected MqttChatUtils getMqtt() {
        return MqttChatUtils.getInstance();
    }

    protected String getMqttFlag() {
        return getMqtt().getFlag();
    }

    protected String getMqttKey() {
        return getMqtt().getKey();
    }

    protected String getMqttClientId() {
        return getMqtt().getClientId();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.with(this).onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Glide.with(this).onTrimMemory(level);
    }
}
