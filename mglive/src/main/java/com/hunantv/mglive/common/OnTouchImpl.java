/**
 * 
 */
package com.hunantv.mglive.common;

import android.content.Context;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

/**
 * @author June Kwok
 *
 */
public class OnTouchImpl extends SimpleOnGestureListener implements OnTouchListener {

	private GestureDetector mDetector;
	private View mView;
	private Context mContext;

	public OnTouchImpl(Context context) {
		mContext = context;
		mDetector = new GestureDetector(getContext(), this);
		mDetector.setIsLongpressEnabled(false);
	}
	@Override
	public boolean onTouch(View view, MotionEvent event) {
		if (view != null && view != mView) {
			mView = view;
		}
		if (event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP) {
			onUp(event);
		}
		return mDetector.onTouchEvent(event);
	}
	public Context getContext() {
		return mContext;
	}
	@Override
	public boolean onDown(MotionEvent e) {
		return true;
	}
	public boolean onUp(MotionEvent e) {
		return true;
	}
	@Override
	public void onShowPress(MotionEvent e) {
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return true;
	}
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		return true;
	}
	@Override
	public void onLongPress(MotionEvent e) {
	}
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		return false;
	}
	@Override
	public boolean onDoubleTap(MotionEvent e) {
		return false;
	}
	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		return false;
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		return false;
	}
	public final View getCurView() {
		return mView;
	}
}
