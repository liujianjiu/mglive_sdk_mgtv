package com.hunantv.mglive.common;


/**
 * Created by maxxiang on 2015/12/14.
 */
public class BuildConfig {


    //NEED
    public static String http_msg = "http://open.action.api.max.mgtv.com";  //消息/message
    public static String http_pay = "http://open.dy.api.max.mgtv.com";  //账户/account 卡券/coupon 礼物/gift 支付/pay
    public static String http_live = "http://open.content.api.max.mgtv.com";//直播
    public static String http_action = "http://open.action.api.max.mgtv.com";//个人空间
    public static String http_user = "http://open.artist.api.max.mgtv.com"; //用户
    public static String http_cms = "http://open.content.api.max.mgtv.com";   //内容
    public static String http_promote = "http://open.dy.api.max.mgtv.com";//金币
    public static String BASE_URL_MQTT_RELEASE = "http://provider.barrage.hunantv.com";//MQTT
    public static String BASE_URL_ADVERT_RELEASE = "http://x.da.hunantv.com";//广告

    //获取分享配置信息
    public static String URL_GET_SHARE_INFO = "/global/shareConfig";
    //手机直播举报
    public static String URL_LIVE_REPORT = "/live/reported";

    //礼物
    //产品 守护列表
    public static String URL_CONTR_LIST = "/gift/getGuards";
    //分类礼物列表
    public static String GET_CLASSIFY_GIFT_LIST = "/gift/getClassifyGifts";
    //快捷
    public static String GET_SHORTCUT_GIFT = "/gift/getShortcutGift";
    //免费礼物相关
    public static String GET_FREE_GIFT_NUM = "/pay/getFreeGiftCount";
    //喊话
    public static String GET_SHOUT_GIFT = "/gift/getShoutGift";
    //礼物消费
    public static String PAY_GIFT = "/pay/v2/giftPayment";
    //芒果币余额
    public static String URL_USER_MG_MONEY = "/account/balance";
    //获取守护信息
    public static String GET_CONTRIBUTION_INFO = "/gift/getUserGuardByUId";

    //聊天
    public static String SEND_GUARD_MSG = "/gift/sendGuardMessage";
    //刚刚进入聊天室的时候获取最近的20条聊天记录
    public static String CHAT_GET_LAST_MSG_PATH = "/chat/getLastMsg";//TODO 返回字段

    //直播
    //获取秀场直播拉流地址
    public static String URL_LIVE_GET_URL = "/live/getPlayUrl";
    //通过uid获取lid
    public static String URL_GET_LIVE_INFO_BY_UID = "/live/getLiveInfoByuid";
    //直播详情
    public static String URL_GET_DETAIL = "/content/getLiveDetail";
    //场景直播获取地址
    public static String URL_GET_LIVE_PLAY_URL = "/live/getLivePlayURL";
    //是否有开启直播
    public static String URL_LIVE_IS_OPEN = "/live/isLiveshowByUid";

    //个人空间
    //人气
    public static String URL_POPULARITY_RANK = "/fans/getPopularityRank";
    //添加关注
    public static String URL_ADD_FOLLOW = "/fans/addFollow";
    //删除关注
    public static String URL_REMOVE_FOLLOW = "/fans/removeFollow";
    //守护
    public static String URL_GUARD_RANK = "/fans/getGuardRank";
    //添加标签
    public static String URL_ADD_TAGS = "/tag/addTag";
    //获取热门标签
    public static String URL_HOT_TAGS = "/tag/getHotTags";
    //添加赞
    public static String URL_DYNAMIC_ADD_PRAISE = "/dynamic/addPraise";
    //删除赞
    public static String URL_DYNAMIC_DEL_PRAISE = "/dynamic/removePraise";
    //艺人资料获取
    public static String URL_STAR_INFO = "/artist/getArtistInfo";
    //获取推荐关注的艺人列表
    public static String URL_RCMD_STAR_LIST = "/artist/getRecommendCollectArtist";
    //广场动态请求地址
    public static String URL_MY_DYNAMIC = "/dynamic/getDynamicList";//TODO 字段
    //艺人动态列表请求地址
    public static String URL_STAR_DYNAMIC = "/dynamic/getOwndynamic";
    //添加评论
    public static String URL_DYNAMIC_ADD_COMMEN = "/dynamic/addComment";
    //我的消息通知，包含全部count
    public static String URL_MESSAGE_MYNOTICE = "/message/getMyNotice";
    //获取动态详情
    public static String URL_DYNAMIC_GET_DETAIL = "/dynamic/getDynamicDetail";
    //是否关注了某人
    public static String URL_GET_IS_FOLLOWED = "/artist/isfollowed";//TODO 地址确认
    //某动态的所有赞请求地址
    public static String URL_DYNAMIC_PRAISE_LIST = "/dynamic/getPraises";
    //某动态的所有评论请求地址
    public static String URL_DYNAMIC_COMMEN_LIST = "/dynamic/getComments";
    //获取点播url  新接口  老接口为/content/getVODURL
    public static String URL_GET_VIDEO_URL = "/vod/getVideoURL";
    //发表动态
    public static String URL_DYNAMIC_POST = "/dynamic/createDynamic";//no use

    // 金币
    //获取随机领金币信息_V2
    public static String URL_GET_GIFTING_INFO_V2 = "/promote/v2/getGiftingInfo";
    //用户领取金币_V2
    public static String URL_GET_GIFTING_V2 = "/promote/v2/getGifting";
    //签到_V2
    public static String URL_SIGN_V2 = "/v2/promote/sign";
    //获取签到信息_V2
    public static String URL_SIGN_INFO_V2 = "/v2/promote/getSignInfo";
    //是否签到_V2
    public static String URL_IS_SIGN_V2 = "/v2/promote/isSign";

    //MQTT
    //获取聊天室token
    public static String CHAT_REG_PATH = "/provider/chat/v1/token";
    //发送聊天内容
    public static String CHAT_ONLINE_PATH = "/chat/getLiveOnlineCout";
    //发送消息,vision2-增加守护字段
    public static String CHAT_SEND_PATH_V2 = "/chat/v2/sendUgcMsg";


    //广告
    public static String URL_APP_START_ADVERT = "/json/app/boot";


    static {

        //NEED

        //礼物
        URL_CONTR_LIST = http_pay + URL_CONTR_LIST;
        GET_CLASSIFY_GIFT_LIST = http_pay + GET_CLASSIFY_GIFT_LIST;
        GET_SHORTCUT_GIFT = http_pay + GET_SHORTCUT_GIFT;
        GET_FREE_GIFT_NUM = http_pay + GET_FREE_GIFT_NUM;
        GET_SHOUT_GIFT = http_pay + GET_SHOUT_GIFT;
        PAY_GIFT = http_pay + PAY_GIFT;
        URL_USER_MG_MONEY = http_pay + URL_USER_MG_MONEY;
        GET_CONTRIBUTION_INFO = http_pay + GET_CONTRIBUTION_INFO;

        //聊天
//        CHAT_SEND_PATH = http_msg + CHAT_SEND_PATH;
        SEND_GUARD_MSG = http_pay + SEND_GUARD_MSG;
        CHAT_GET_LAST_MSG_PATH = http_action + CHAT_GET_LAST_MSG_PATH;

        //直播
        URL_LIVE_GET_URL = http_live + URL_LIVE_GET_URL;
        URL_GET_LIVE_INFO_BY_UID = http_live + URL_GET_LIVE_INFO_BY_UID;
        URL_GET_DETAIL = http_live + URL_GET_DETAIL;
        URL_GET_LIVE_PLAY_URL = http_live + URL_GET_LIVE_PLAY_URL;
        URL_LIVE_IS_OPEN = http_live + URL_LIVE_IS_OPEN;

        //个人空间
        URL_POPULARITY_RANK = http_action + URL_POPULARITY_RANK;
        URL_ADD_FOLLOW = http_action + URL_ADD_FOLLOW;
        URL_REMOVE_FOLLOW = http_action + URL_REMOVE_FOLLOW;
        URL_GUARD_RANK = http_action + URL_GUARD_RANK;
        URL_ADD_TAGS = http_action + URL_ADD_TAGS;
        URL_HOT_TAGS = http_action + URL_HOT_TAGS;
        URL_DYNAMIC_ADD_PRAISE = http_action + URL_DYNAMIC_ADD_PRAISE;
        URL_DYNAMIC_DEL_PRAISE = http_action + URL_DYNAMIC_DEL_PRAISE;
        URL_STAR_INFO = http_user + URL_STAR_INFO;
//        URL_USER_STARS = http_action + URL_USER_STARS;
        URL_RCMD_STAR_LIST = http_action + URL_RCMD_STAR_LIST;
        URL_MY_DYNAMIC = http_action + URL_MY_DYNAMIC;
        URL_STAR_DYNAMIC = http_action + URL_STAR_DYNAMIC;
        URL_DYNAMIC_ADD_COMMEN = http_action + URL_DYNAMIC_ADD_COMMEN;
        URL_MESSAGE_MYNOTICE = http_action + URL_MESSAGE_MYNOTICE;
//        URL_DYNAMIC_GET_MESSAGE_LIST = http_action + URL_DYNAMIC_GET_MESSAGE_LIST;
        URL_DYNAMIC_GET_DETAIL = http_action + URL_DYNAMIC_GET_DETAIL;
        URL_GET_IS_FOLLOWED = http_action + URL_GET_IS_FOLLOWED;
        URL_DYNAMIC_PRAISE_LIST = http_action + URL_DYNAMIC_PRAISE_LIST;
        URL_DYNAMIC_COMMEN_LIST = http_action + URL_DYNAMIC_COMMEN_LIST;
        URL_GET_VIDEO_URL = http_cms + URL_GET_VIDEO_URL;
        URL_DYNAMIC_POST = http_action + URL_DYNAMIC_POST;

        //金币
        URL_GET_GIFTING_INFO_V2 = http_promote + URL_GET_GIFTING_INFO_V2;
        URL_GET_GIFTING_V2 = http_promote + URL_GET_GIFTING_V2;
        URL_SIGN_V2 = http_promote + URL_SIGN_V2;
        URL_SIGN_INFO_V2 = http_promote + URL_SIGN_INFO_V2;
        URL_IS_SIGN_V2 = http_promote + URL_IS_SIGN_V2;

        //mqtt
        CHAT_REG_PATH = BASE_URL_MQTT_RELEASE + CHAT_REG_PATH;
        CHAT_ONLINE_PATH = http_msg + CHAT_ONLINE_PATH;
        CHAT_SEND_PATH_V2 = http_msg + CHAT_SEND_PATH_V2;

        URL_GET_SHARE_INFO = http_action + URL_GET_SHARE_INFO;
        URL_LIVE_REPORT = http_cms + URL_LIVE_REPORT;

        //************广告  BEGIN********************
        URL_APP_START_ADVERT = BASE_URL_ADVERT_RELEASE + URL_APP_START_ADVERT;
        //************广告  END**********************

    }
}
