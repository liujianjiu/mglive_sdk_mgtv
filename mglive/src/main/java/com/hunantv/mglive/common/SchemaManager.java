package com.hunantv.mglive.common;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.hunantv.mglive.ui.discovery.DetailsActivity;
import com.hunantv.mglive.ui.discovery.SquareActivity;
import com.hunantv.mglive.ui.live.LiveDetailActivity;
import com.hunantv.mglive.ui.live.StarDetailActivity;
import com.hunantv.mglive.ui.live.StarLiveActivity;
import com.hunantv.mglive.utils.StartActivityDelayUtil;
import com.hunantv.mglive.utils.StringUtil;

/**
 * Created by maxxiang on 2015/12/19.
 */
public class SchemaManager {
    private static final String TAG = "SchemaManager";
    private static SchemaManager instance;

    public static SchemaManager getInstance() {
        if (instance == null) {
            instance = new SchemaManager();
        }
        return instance;
    }

    public boolean jumpToActivity(Context c, String url, boolean bDefault) {
        if (c == null || url == null) {
            return false;
        }
        return jumpToActivity(c, url, bDefault, false);
    }

    //返回值： 为true表示要finish当前界面。
    public boolean jumpToActivity(Context c, String url, boolean bDefault, boolean isNewTask) {
        if (url.startsWith(Constant.SQUARE_URL)) {     //艺人动态
            navigateSquare(c, url, SquareActivity.class, isNewTask);
            return false;
        } else if (url.startsWith(Constant.MGLIVE_URL_Dynamic_Detail)) {     //动态详情
            navigateDynamicDetail(c, url, DetailsActivity.class, isNewTask);
            return false;
        } else if (url.startsWith(Constant.MGLIVE_URL_artistpace)) { //艺人空间
            navigateArtistpace(c, url, StarDetailActivity.class, isNewTask);
            return false;
        }else if (url.startsWith(Constant.MGLIVE_URL_LIVE)) {      //直播间
            navigateLiveDetail(c, url, LiveDetailActivity.class, isNewTask);
            return false;
        }else if (url.startsWith(Constant.MGLIVE_URL_STAR_LIVE)) {
            navigateStarLive(c, url, StarLiveActivity.class, isNewTask);
            return false;
        }else if (url.startsWith(Constant.MGLIVE_URL_STAR_PLAY)) {
            navigateStarPlay(c, url, StarLiveActivity.class, isNewTask);
            return false;
        }
        return true;
    }

    private void navigate(Context c, Class cls, boolean isNewTask) {
        Intent intent = new Intent(c, cls);
        if (isNewTask) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        c.startActivity(intent);
    }


    private void navigateSquare(Context c, String url, Class cls, boolean isNewTask) {
        String title = "";

        try {
            title = Uri.parse(url).getQueryParameter("title");

            Intent i = new Intent(c, cls);
            i.putExtra(DiscoveryConstants.KEY_TITLE, title);
            if (isNewTask) {
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            c.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //mglive://mglive/feeddetail?fid=    //动态详情（fid为动态id）
    private void navigateDynamicDetail(Context c, String url, Class cls, boolean isNewTask) {
        String fid = "";

        try {
            fid = Uri.parse(url).getQueryParameter("fid");

            Intent i = new Intent(c, cls);
            i.putExtra(DetailsActivity.KEY_DYNAMIC_ID, fid);
            if (isNewTask) {
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            c.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void navigateArtistpace(Context c, String url, Class cls, boolean isNewTask) {
        String uid = "";
        String fid = "";
        try {
            uid = Uri.parse(url).getQueryParameter("uid");
            fid = Uri.parse(url).getQueryParameter("fid");
            Intent i = new Intent(c, cls);
            i.putExtra(StarDetailActivity.KEY_STAR_ID, uid);
            if (!StringUtil.isNullorEmpty(fid)) {
                i.putExtra(StarDetailActivity.KEY_DYNAMIC_VIDEO_ID, fid);
            }
            if (isNewTask) {
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            c.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 秀场直播
     *
     * @param c
     * @param url
     * @param cls
     * @param isNewTask
     */
    private void navigateStarLive(Context c, String url, Class cls, boolean isNewTask) {
        String cid = "";
        try {
            cid = Uri.parse(url).getQueryParameter("auid");
            StartActivityDelayUtil startActivityDelayUtil = StartActivityDelayUtil.instance(c);
            startActivityDelayUtil.startAcitiy(cid, isNewTask);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 场景直播
     *
     * @param c
     * @param url
     * @param cls
     * @param isNewTask
     */
    private void navigateStarPlay(Context c, String url, Class cls, boolean isNewTask) {
        try {
            Intent i = new Intent(c, cls);
            String live = Uri.parse(url).getQueryParameter("live");
            String type = Uri.parse(url).getQueryParameter("type");
            i.putExtra(Constant.JUMP_INTENT_LID, live);
            i.putExtra(Constant.JUMP_INTENT_TYPE, type);
            if (isNewTask) {
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            c.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void navigateStarDetail(Context c, String url, Class cls, boolean isNewTask) {
        String uid = "";
        String title = "";
        try {
            uid = Uri.parse(url).getQueryParameter("uid");
            title = Uri.parse(url).getQueryParameter("title");
            if (!StringUtil.isNullorEmpty(title) && title.length() > 15)
                title = title.substring(0, 15);        //怕后台传的太长了，只取最多15个字
        } catch (Exception e) {
            e.printStackTrace();
            uid = "";
            title = "";
        }

        Intent i = new Intent(c, cls);
        i.putExtra(StarDetailActivity.KEY_STAR_ID, uid);
        i.putExtra(StarDetailActivity.KEY_TITLE, title);
        if (isNewTask) {
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        c.startActivity(i);

    }

    //直播间
    private void navigateLiveDetail(Context c, String url, Class cls, boolean isNewTask) {
        String live = "0";
        String type = "0";
        String title = "";
        try {
            live = Uri.parse(url).getQueryParameter("live");
            type = Uri.parse(url).getQueryParameter("type");
            title = Uri.parse(url).getQueryParameter("title");
            if (!StringUtil.isNullorEmpty(title) && title.length() > 15)
                title = title.substring(0, 15);        //怕后台传的太长了，只取最多15个字
        } catch (Exception e) {
            e.printStackTrace();
            live = "0";
            type = "0";
            title = "";
        }

        Intent i = new Intent(c, cls);
        i.putExtra(Constant.JUMP_INTENT_LID, live);
        i.putExtra(Constant.JUMP_INTENT_TYPE, type);
        i.putExtra(LiveDetailActivity.JUMP_INTENT_TITLE, title);
        if (isNewTask) {
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        c.startActivity(i);

    }

    //mglive://mglive/coupon?cid=15&aid=1&title=%E6%88%91%E7%9A%84%E5%8D%A1%E5%88%B8
    private void navigateUserCard(Context c, String url, Class cls, boolean isNewTask) {
        String title = "";
        try {
            title = Uri.parse(url).getQueryParameter("title");
            if (!StringUtil.isNullorEmpty(title) && title.length() > 15)
                title = title.substring(0, 15);        //怕后台传的太长了，只取最多15个字

        } catch (Exception e) {
            e.printStackTrace();

            title = "";
        }


        Intent i = new Intent(c, cls);
        i.putExtra(Constant.RECORDVIDEO_title, title);
        if (isNewTask) {
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        c.startActivity(i);

    }

    private void navigatePay(Context c, String url, Class cls, boolean isNewTask) {
        String title = "";
        try {
            title = Uri.parse(url).getQueryParameter("title");
            if (!StringUtil.isNullorEmpty(title) && title.length() > 15)
                title = title.substring(0, 15);        //怕后台传的太长了，只取最多15个字

        } catch (Exception e) {
            e.printStackTrace();
            title = "";
        }

        Intent i = new Intent(c, cls);
        i.putExtra(Constant.RECORDVIDEO_title, title);
        if (isNewTask) {
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        c.startActivity(i);

    }


}