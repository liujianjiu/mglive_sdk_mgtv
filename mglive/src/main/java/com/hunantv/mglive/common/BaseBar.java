package com.hunantv.mglive.common;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.hunantv.mglive.R;
import com.hunantv.mglive.utils.UIUtil;

/**
 * Created by June Kwok on 2015-11-12.
 */
public class BaseBar extends FrameLayout {

    public ImageView mBackIcon;
    public ImageView mShareIcon;
    public TextView mTvRight;
    public TextView mTitle;
    public String mTxt = "";
    public String mTxtRight = "";

    public BaseBar(Context context, String str) {
        super(context);
        mTxt = str;
        init();
    }

    public BaseBar(Context context) {
        super(context);
        init();
    }

    public BaseBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setRightText(String text) {
        mTxtRight = text;
        mTvRight.setText(mTxtRight);
    }

    public void setTitle(String title){
        mTxt = title;
        mTitle.setText(mTxt);
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_titlebar,null);
        addView(view);
        mBackIcon = (ImageView)view.findViewById(R.id.title_back_bar);
        mShareIcon = (ImageView)view.findViewById(R.id.title_share_bar);
        mTitle = (TextView)view.findViewById(R.id.title_center_text);
        mTvRight = (TextView)view.findViewById(R.id.title_right_text);
        mTitle.setText(mTxt);
        mTvRight.setText(mTxtRight);
        mBackIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ((Activity) BaseBar.this.getContext()).finish();
                } catch (Exception e) {
                }
            }
        });
    }
}
