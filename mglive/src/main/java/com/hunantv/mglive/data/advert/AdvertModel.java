package com.hunantv.mglive.data.advert;

import java.util.ArrayList;

/**
 * Created by admin on 2016/2/19.
 *
 * - type： 固定值1
 - url: 本次开机大图展示素材地址
 - backup_url: 本次开机大图备选开机大图地址，可以有多个或者没有
 - impression: 印象上报地址，0个或者多个
 - click: 点击上报地址，0个或者多个
 - title: 广告名字
 - duration: 素材展示时长，单位s
 - jump_type: 素材跳转类型：
 * 点播：videoplayer
 * 专题：subjectpage
 * 片库：videolibrary
 * 内置浏览器：webview
 * 外置浏览器：browser
 * 频道页：channel
 * 跳转其他App：otherapp
 - jump_val:素材跳转值，数组，通常只有一个值，数组是为了后续扩展预留空间
 * 点播：videoid1需有值（用videoid填充），url1需有值（指定升级页地址），其它字段可为空字符
 串
 * 专题：videoid1需有值（用subjectid填充），url1需有值（指定升级页地址），其它字 段可为空
 字符串
 * 片库：videoid1需有值（用libid填充），url1需有值（指定升级页地址），其它字段可为空字符串
 * 内置浏览器：url1需有值（必须为http://开头），其它字段可为空字符串
 * 外置浏览器：url1需有值（必须为http://开头），其它字段可为空字符串
 * 频道页：videoid1需有值（用channelid填充），url1需有值（指定升级页地址），其它字段可为空
 字符串
 * 跳转其他App：url1需有值（必须为http://开头），url2需有值（指定升级页地址）
 */
public class AdvertModel {
    //内部浏览器跳转
    public static final String JUMP_TYPE_WEBVIEW = "webview";
    //外部浏览器跳转
    public static final String JUMP_TYPE_BROWSER = "browser";
    /**
     * 无用
     */
    private String type;

    /**
     * 本次开机大图素材地址
     */
    private String url;
    /**
     * 备选有效开机大图素材地址
     */
    private ArrayList<String> backup_url;
    /**
     * 印象监测地址列表
     */
    private ArrayList<String> impression;
    /**
     * 点击上报地址
     */
    private ArrayList<String> click;
    /**
     * 广告素材名字
     */
    private String title;
    /**
     * 图片显示时长
     */
    private int duration;
    /**
     * 跳转类型
     */
    private String jump_type;
    /**
     * 跳转值
     */
    private ArrayList<String> jump_val;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<String> getBackup_url() {
        return backup_url;
    }

    public void setBackup_url(ArrayList<String> backup_url) {
        this.backup_url = backup_url;
    }

    public ArrayList<String> getImpression() {
        return impression;
    }

    public void setImpression(ArrayList<String> impression) {
        this.impression = impression;
    }

    public ArrayList<String> getClick() {
        return click;
    }

    public void setClick(ArrayList<String> click) {
        this.click = click;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getJump_type() {
        return jump_type;
    }

    public void setJump_type(String jump_type) {
        this.jump_type = jump_type;
    }

    public ArrayList<String> getJump_val() {
        return jump_val;
    }

    public void setJump_val(ArrayList<String> jump_val) {
        this.jump_val = jump_val;
    }
}
