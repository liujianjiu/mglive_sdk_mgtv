package com.hunantv.mglive.data.template;

import java.io.Serializable;

/**
 * Created by admin on 2016/3/15.
 */
public class MenuModel implements Serializable {
    /**
     * 菜单ID
     */
    private String menuID;
    /**
     * 菜单名称
     */
    private String name;
    /**
     * 模板对象
     */
    private TemplateModel templet;

    public String getMenuID() {
        return menuID;
    }

    public void setMenuID(String menuID) {
        this.menuID = menuID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TemplateModel getTemplet() {
        return templet;
    }

    public void setTemplet(TemplateModel templet) {
        this.templet = templet;
    }
}
