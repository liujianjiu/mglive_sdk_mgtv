package com.hunantv.mglive.data.user;

/**
 * Created by admin on 2016/1/20.
 */
public class MyFansData {
    /**
     * 用户id
     */
    private String uid;
    /**
     * 用户角色
     */
    private int role;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 照片
     */
    private String photo;
    /**
     * 关注时间
     */
    private String DateTime;
    /**
     * 是否互粉
     */
    private String isfans;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public String getIsfans() {
        return isfans;
    }

    public void setIsfans(String isfans) {
        this.isfans = isfans;
    }
}
