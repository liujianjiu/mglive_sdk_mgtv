package com.hunantv.mglive.data;

/**
 * Created by 达 on 2015/11/24.
 */
public class ChatDataModel {

    public String uuid;
    public int chatType;
    //发送者
    public String name1;
    //给谁发
    public String name2;
    //内容
    public String content;
    //礼物名
    public String giftName;
    //礼物数量
    public Long giftNum;
    //礼物ID
    public long giftId;
    //礼物图片
    public String giftPhoto;
    //
    public int grade;

    public int role;
}
