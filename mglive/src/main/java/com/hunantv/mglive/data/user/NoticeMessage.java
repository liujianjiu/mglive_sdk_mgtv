package com.hunantv.mglive.data.user;

/**
 * Created by admin on 2016/1/19.
 */
public class NoticeMessage {
    /**
     * 未读消息量
     */
    private int msgCount;

    /**
     * 卡券数量
     */
    private int cardCount;
    /**
     * 粉丝数量
     */
    private int fansCount;
    /**
     * 新增粉丝量
     */
    private int newFansCount;
    /**
     * 我的关注数量
     */
    private int followCount;
    /**
     * 动态数量
     */
    private int dyCount;
    /**
     * 动态消息数量
     */
    private int dyMsgCount;
    /**
     * 意见消息数量
     */
    private int feedbackCount;

    public int getFeedbackCount() {
        return feedbackCount;
    }

    public void setFeedbackCount(int feedbackCount) {
        this.feedbackCount = feedbackCount;
    }

    public int getMsgCount() {
        return msgCount;
    }

    public void setMsgCount(int msgCount) {
        this.msgCount = msgCount;
    }

    public int getCardCount() {
        return cardCount;
    }

    public void setCardCount(int cardCount) {
        this.cardCount = cardCount;
    }

    public int getFansCount() {
        return fansCount;
    }

    public void setFansCount(int fansCount) {
        this.fansCount = fansCount;
    }

    public int getNewFansCount() {
        return newFansCount;
    }

    public void setNewFansCount(int newFansCount) {
        this.newFansCount = newFansCount;
    }

    public int getFollowCount() {
        return followCount;
    }

    public void setFollowCount(int followCount) {
        this.followCount = followCount;
    }

    public int getDyCount() {
        return dyCount;
    }

    public void setDyCount(int dyCount) {
        this.dyCount = dyCount;
    }

    public int getDyMsgCount() {
        return dyMsgCount;
    }

    public void setDyMsgCount(int dyMsgCount) {
        this.dyMsgCount = dyMsgCount;
    }
}
