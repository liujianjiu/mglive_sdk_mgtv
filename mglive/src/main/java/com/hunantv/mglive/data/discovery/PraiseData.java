package com.hunantv.mglive.data.discovery;

/**
 * 赞
 * Created by guojun3 on 2015-12-16.
 */
public class PraiseData {

    //{"dynamicId":"f4977d420fe743968684d3998139c08e","nickName":"max","photo":"http://p1.hunantv.com/1/ava1_TwmJtXeLIgDkflSaI952TE4OeNkzs2VR.jpg","uid":"eb5e2ee98c109a3f3bf7069c040e10f3"
    /**
     * uid	String	是		用户id
     * dynamicId	Long	是		动态id
     * photo	String	是		头像
     * nickName	String	是		昵称
     */

    private String uid;
    private String dynamicId;
    private String photo;
    private String nickName;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDynamicId() {
        return dynamicId;
    }

    public void setDynamicId(String dynamicId) {
        this.dynamicId = dynamicId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
