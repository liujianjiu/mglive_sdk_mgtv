package com.hunantv.mglive.data.discovery;

/**
 * Created by admin on 2015/12/15.
 */
public class MenuData {
    private String mId;
    private String target;
    private String icon;
    private String tips;
    private String tipsUrl;
    private String menuName;

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getTipsUrl() {
        return tipsUrl;
    }

    public void setTipsUrl(String tipsUrl) {
        this.tipsUrl = tipsUrl;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

}
