package com.hunantv.mglive.data.user;

/**
 * Created by admin on 2015/12/10.
 */
public class PayData {
    /**
     * 订单id
     */
    private String oid;
    /**
     * 支付类型
     * qrcode：二维码支付（微信）
     * location：跳转支付
     */
    private String paymentType;

    /**
     * 支付URL
     * 二维码类型：放好二维码url
     * 跳转支付类型：返回跳转url
     */
    private String paymentUrl;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public void setPaymentUrl(String paymentUrl) {
        this.paymentUrl = paymentUrl;
    }
}
