package com.hunantv.mglive.data.sign;

/**
 * Created by admin on 2016/2/18.
 */
public class SignAdvertModel {
    /**
     * 图片URL
     */
    private String image;

    /**
     * 图片链接（为空时不做跳转）
     */
    private String url;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
