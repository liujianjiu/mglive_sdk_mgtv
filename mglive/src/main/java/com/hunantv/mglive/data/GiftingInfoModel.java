package com.hunantv.mglive.data;

/**
 * Created by qiuda on 16/2/22.
 */
public class GiftingInfoModel {
    public static final String TYPE_SURPRISE_BIG = "1";//大惊喜
    public static final String TYPE_SURPRISE_SMALL = "0";//小惊喜

    /**
     * 状态
     */
    private String status;
    /**
     * 活动时间有效
     */
    private String isActive;
    /**
     * 赞助商
     */
    private String sponsor;
    /**
     * 金币数量
     */
    private String coinInfo;
    /**
     * 距离小人显示的时间间隔
     */
    private String remainderTime;
    /**
     * 未点击领取，10s消失后，下一次的显示时间间隔
     */
    private String nextTime;
    /**
     * 显示的图片
     */
    private String image;

    /**
     * 0 小惊喜  1大惊喜
     */
    private String type;
    /**
     * 图标（大惊喜领取页面使用）
     */
    private String logo;
    /**
     * 广告图（大惊喜领取页面使用）
     */
    private String adImage;
    /**
     * 广告语（大惊喜领取页面使用）
     */
    private String adSlogan;
    /**
     * 32位票据
     */
    private String ticket;

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getCoinInfo() {
        return coinInfo;
    }

    public void setCoinInfo(String coinInfo) {
        this.coinInfo = coinInfo;
    }

    public String getNextTime() {
        return nextTime;
    }

    public void setNextTime(String nextTime) {
        this.nextTime = nextTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemainderTime() {
        return remainderTime;
    }

    public void setRemainderTime(String remainderTime) {
        this.remainderTime = remainderTime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAdImage() {
        return adImage;
    }

    public void setAdImage(String adImage) {
        this.adImage = adImage;
    }

    public String getAdSlogan() {
        return adSlogan;
    }

    public void setAdSlogan(String adSlogan) {
        this.adSlogan = adSlogan;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }
}
