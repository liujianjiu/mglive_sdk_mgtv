package com.hunantv.mglive.data.user;

/**
 * 意见数据
 */
public class OpinionData {


    /**
     * id
     */
    private String feedbackId;

    /**
     * 0:问题，1：回复
     */
    private int feedbackType;

    /**
     * 回复时间(yyyy-MM-dd HH:mm:ss)
     */
    private String dateTime;

    /**
     * 0文本1图片
     */
    private int type;

    /**
     * 文字内容
     */
    private String content;

    /**
     * 图片内容
     */
    private String image;

    /**
     * 文件路径key
     */
    private String fileKey;

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public int getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(int feedbackType) {
        this.feedbackType = feedbackType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(String feedbackId) {
        this.feedbackId = feedbackId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
