package com.hunantv.mglive.data.config;

/**
 * Created by admin on 2015/12/10.
 * 启动广告
 */
public class AdModel {
    /**
     * Launch开屏广告，pullrefresh下拉广告
     */
    private String type;
    /**
     * 广告跳转地址,http://跳转到网页，app://跳转到native页面
     */
    private String url;
    /**
     * 图片宽度，缺省为0
     */
    private String width;
    /**
     * 图片高度，缺省为0
     */
    private String height;
    /**
     * 图片地址
     */
    private String image;
    /**
     * 广告播放时间，单位为秒，缺省为0
     */
    private int showtime;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getShowtime() {
        return showtime;
    }

    public void setShowtime(int showtime) {
        this.showtime = showtime;
    }
}
