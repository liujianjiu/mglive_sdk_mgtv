package com.hunantv.mglive.data;

import java.io.Serializable;

/**
 * Created by 达 on 2015/11/25.
 */
public class GiftDataModel implements Serializable {
    /**
     * 动画类别
     */
    private int animType;
    /**
     * 产品编码
     */
    private String code;
    /**
     * 礼物id
     */
    private long gid;
    /**
     * 图标
     */
    private String icon;
    /**
     * 名称
     */
    private String name;
    /**
     * 大图
     */
    private String photo;
    /**
     * 价格，20芒果币
     */
    private long price;
    /**
     * 标签
     */
    private String tag;
    /**
     * 增加的人气值
     */
    private String hots;

    public int getAnimType() {
        return animType;
    }

    public void setAnimType(int animType) {
        this.animType = animType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getGid() {
        return gid;
    }

    public void setGid(long gid) {
        this.gid = gid;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getHots() {
        return hots;
    }

    public void setHots(String hots) {
        this.hots = hots;
    }
}
