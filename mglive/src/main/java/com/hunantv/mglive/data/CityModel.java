package com.hunantv.mglive.data;

import java.util.ArrayList;

/**
 * Created by admin on 2016/1/26.
 */
public class CityModel {
    private String province;
    private ArrayList<String> cities;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public ArrayList<String> getCities() {
        return cities;
    }

    public void setCities(ArrayList<String> cities) {
        this.cities = cities;
    }
}
