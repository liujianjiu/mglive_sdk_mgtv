package com.hunantv.mglive.data.common;

/**
 * Created by admin on 2016/5/23.
 */
public class EncodeData {
    public EncodeData(String encode, String time, String salt) {
        this.encode = encode;
        this.time = time;
        this.salt = salt;
    }

    /**
     * md5编码
     */
    private String encode;
    /**
     * MD5编码时间戳
     */
    private String time;
    /**
     * MD5变量N
     */
    private String salt;

    public String getEncode() {
        return encode;
    }

    public void setEncode(String encode) {
        this.encode = encode;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
