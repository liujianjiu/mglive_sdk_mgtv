package com.hunantv.mglive.data.template;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2016/3/15.
 */
public class ElementModel implements Serializable {
    public static final String TYPE_BANNER = "e_banner";//banner
    public static final String TYPE_NOTICE = "e_notice";//公告栏
    public static final String TYPE_NAVIGATION = "e_navigation";//导航
    public static final String TYPE_H5 = "e_h5";//H5
    public static final String TYPE_TOPLIST = "e_topList";//榜单
    public static final String TYPE_VIDEO = "e_video";//视频
    public static final String TYPE_USER = "e_user";//用户
    public static final String TYPE_DYNAMIC = "e_dynamic";//动态
    public static final String TYPE_USERLIVE = "e_userLive";//用户直播
    public static final String TYPE_USERLIVE_1 = "e_userLive_1";//个人直播单行滚动

    //app自定义模板类型
    public static final String TYPE_CUSTOM_TITLEBAR = "c_titlebar";//标题栏
    public static final String TYPE_CUSTOM_VIDEO_1 = "c_video_1";//视频一行一个
    public static final String TYPE_CUSTOM_VIDEO_2 = "c_video_2";//视频一行两个
    public static final String TYPE_CUSTOM_DYNAMIC = "c_dynamic";//动态
    public static final String TYPE_CUSTOM_LIVE = "c_live";//直播一行一个
    public static final String TYPE_CUSTOM_MORE = "c_more";//更多
    public static final String TYPE_CUSTOM_EMPTY = "c_empty";//空行

    /**
     * 元素类型
     */
    private String type;
    /**
     * TITLE链接
     */
    private String linkUrl;
    /**
     * TITLE
     */
    private String title;
    /**
     * TITLE 提示
     */
    private String tips;
    /**
     * 是否显示TITLE
     */
    private String titleIsShow;
    /**
     * 数据接口地址
     */
    private String url;
    /**
     * 接口参数
     */
    private String parameter;
    /**
     * 显示高度
     */
    private String height;
    /**
     * 首位图标url
     */
    private String firstIcon;
    /**
     * 排名角标url
     */
    private String rankIcon;
    /**
     * 是否可分页 0\1
     */
    private String turnPage;
    /**
     * 列数
     */
    private String col;
    /**
     * 行数
     */
    private String row;
    /**
     * 首行大图  0、1
     */
    private String FBshow;
    /**
     * 数据跳转地址
     */
    private String dataLinkUrl;
    /**
     * 数据跳转参数
     */
    private String dataLinkPar;
    /**
     * 底部标示
     */
    private String footer;
    /**
     * 数据
     */
    private Object data;
    /**
     * 数据起始下标
     */
    private int dataStartIndex;

    /**
     * 数据结束下标
     */
    private int dataEndIndex;
    /**
     * 自定义高度
     */
    private int cusHeight;

    /**
     * 统一色值
     */
    private String colour;
    /**
     * 统一图标
     */
    private String icon;

    private RequestData requestData;

    /**
     * 是否显示分割线,只对titlebar节点有效
     */
    private boolean  isShowLine;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getTitleIsShow() {
        return titleIsShow;
    }

    public void setTitleIsShow(String titleIsShow) {
        this.titleIsShow = titleIsShow;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getFirstIcon() {
        return firstIcon;
    }

    public void setFirstIcon(String firstIcon) {
        this.firstIcon = firstIcon;
    }

    public String getRankIcon() {
        return rankIcon;
    }

    public void setRankIcon(String rankIcon) {
        this.rankIcon = rankIcon;
    }

    public String getTurnPage() {
        return turnPage;
    }

    public void setTurnPage(String turnPage) {
        this.turnPage = turnPage;
    }

    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getFBshow() {
        return FBshow;
    }

    public void setFBshow(String FBshow) {
        this.FBshow = FBshow;
    }

    public String getDataLinkUrl() {
        return dataLinkUrl;
    }

    public void setDataLinkUrl(String dataLinkUrl) {
        this.dataLinkUrl = dataLinkUrl;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getDataLinkPar() {
        return dataLinkPar;
    }

    public void setDataLinkPar(String dataLinkPar) {
        this.dataLinkPar = dataLinkPar;
    }

    public int getCusHeight() {
        return cusHeight;
    }

    public void setCusHeight(int cusHeight) {
        this.cusHeight = cusHeight;
    }

    public int getDataEndIndex() {
        return dataEndIndex;
    }

    public void setDataEndIndex(int dataEndIndex) {
        this.dataEndIndex = dataEndIndex;
    }

    public int getDataStartIndex() {
        return dataStartIndex;
    }

    public void setDataStartIndex(int dataStartIndex) {
        this.dataStartIndex = dataStartIndex;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public boolean isShowLine() {
        return isShowLine;
    }

    public void setIsShowLine(boolean isShowLine) {
        this.isShowLine = isShowLine;
    }

    public RequestData getRequestData() {
        return requestData;
    }

    public void setRequestData(RequestData requestData) {
        this.requestData = requestData;
    }

    public ElementModel clone(){
        ElementModel element = new ElementModel();
        element.setType(type);
        element.setLinkUrl(linkUrl);
        element.setTitle(title);
        element.setTips(tips);
        element.setTitleIsShow(titleIsShow);
        element.setUrl(url);
        element.setParameter(parameter);
        element.setHeight(height);
        element.setFirstIcon(firstIcon);
        element.setRankIcon(rankIcon);
        element.setTurnPage(turnPage);
        element.setCol(col);
        element.setRow(row);
        element.setFBshow(FBshow);
        element.setDataLinkUrl(dataLinkUrl);
        element.setDataLinkPar(dataLinkPar);
        element.setFooter(footer);
        element.setData(data);
        element.setDataStartIndex(dataStartIndex);
        element.setDataEndIndex(dataEndIndex);
        element.setCusHeight(cusHeight);
        element.setColour(colour);
        element.setIcon(icon);
        element.setRequestData(requestData);
        element.setIsShowLine(isShowLine);
        return element;
    }

    public boolean isData()
    {
        if(TYPE_CUSTOM_VIDEO_1.equals(type) || TYPE_CUSTOM_VIDEO_2.equals(type)
            ||TYPE_CUSTOM_DYNAMIC.equals(type) || TYPE_CUSTOM_LIVE.equals(type))
        {
                //array
            if(data != null && data instanceof List)
            {
                List list = (List) data;
                if(list.size() > dataStartIndex)
                {
                    return true;
                }
            }
            return false;
        }else
        {
            return data != null;
        }
    }
}
