package com.hunantv.mglive.data;


/**
 * Created by oy on 2015/12/12.
 */
public class SearchResultModel {
    private String title;       //昵称/标题
    private String cid;         //视频id/用户id
    private String tags;        //多个用”,”隔开
    private String cover;       //封面/头像
    private int type;           //1艺人/2视频
    private long hots;          //人气
    private String time;        //直播时间/视频时长
    private int videotype;      //0直播中，1预约直播，2回看，3专题
    private String date;        //创建时间

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getHots() {
        return hots;
    }

    public void setHots(long hots) {
        this.hots = hots;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getVideotype() {
        return videotype;
    }

    public void setVideotype(int videotype) {
        this.videotype = videotype;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
