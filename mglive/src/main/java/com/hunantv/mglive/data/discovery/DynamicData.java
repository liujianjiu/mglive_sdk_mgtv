package com.hunantv.mglive.data.discovery;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015-12-12.
 */
public class DynamicData implements Serializable {
    //type :0，文本  1，图片  2，视频 3,直播
    //{"commentCount":0,"content":"方便年纪","date":"24分钟前","dynamicId":"74834c68fb08454e8b04b0bd6c05e29c","images":[{"big":"http://timelinepic.cdn.max.mgtv.com/2015122118224183205_mgtv.jpg@0o_1l_640w.png","small":"http://timelinepic.cdn.max.mgtv.com/2015122118224183205_mgtv.jpg@2o_1l_300w.png"}],"nickName":"张玮","photo":"http://pic-mgtv-max-cms.oss-cn-beijing.aliyuncs.com/BB8FA385-C5F6-4C69-868D-D1CF0D2107DE","praiseCount":0,"type":1,"uid":"e61822f24dfe73b9d6820361bd4f6e03","video":{"cover":"","url":""}}
    private String uid;
    private String dynamicId;
    private String nickName;
    private String photo;
    private String cover;
    private long praiseCount;
    private long commentCount;
    private int type;
    private String date;
    private String content;
    private List<DynamicImag> images;
    private DynamicVideo video;
    private List<DynamicCommen> comments;

    private boolean isPraised;

    public boolean isPraised() {
        return isPraised;
    }

    public void setIsPraised(boolean isPraised) {
        this.isPraised = isPraised;
    }

    public DynamicCommen getNewCommen(String uid, String nickName, String content) {
        DynamicCommen com = new DynamicCommen();
        com.setContent(content);
        com.setUid(uid);
        com.setNickName(nickName);
        if (null == getComments()) {
            setComments(new ArrayList<DynamicCommen>());
        }
        getComments().add(0, com);
        return com;
    }


    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDynamicId() {
        return dynamicId;
    }

    public void setDynamicId(String dynamicId) {
        this.dynamicId = dynamicId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public long getPraiseCount() {
        return praiseCount;
    }

    public void setPraiseCount(long praiseCount) {
        this.praiseCount = praiseCount;
    }

    public long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(long commentCount) {
        this.commentCount = commentCount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<DynamicImag> getImages() {
        return images;
    }

    public void setImages(List<DynamicImag> images) {
        this.images = images;
    }

    public DynamicVideo getVideo() {
        return video;
    }

    public void setVideo(DynamicVideo video) {
        this.video = video;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Object> getObjectCommentsList() {
        List<Object> list = new ArrayList<>();
        if(getComments() != null)
        {
            int size = getComments().size();
            for (int i = 0; i < size; i++) {
                list.add(getComments().get(i));
                if (i > 3) {
                    break;
                }
            }
        }
        return list;
    }

    public List<DynamicCommen> getComments() {
        return comments;
    }

    public void setComments(List<DynamicCommen> comments) {
        this.comments = comments;
    }

}