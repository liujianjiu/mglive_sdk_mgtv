package com.hunantv.mglive.data;

/**
 * Created by qiuda on 16/3/25.
 */
public class LiveUrlModel {
    private String format;
    private String url;

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
