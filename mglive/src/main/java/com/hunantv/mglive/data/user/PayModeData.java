package com.hunantv.mglive.data.user;

import com.hunantv.mglive.utils.L;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2015/12/1.
 */
public class PayModeData {
    public static final String ALI_SDK="ali-sdk";
    public static final String WX_SDK="weixin-sdk";
    public static final String BAIDU_SDK="baidu-sdk";

    private String pid;

    private String name;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
