package com.hunantv.mglive.data.user;

import java.util.ArrayList;

/**
 * 意见基本数据
 */
public class OpinionBasicData {


    /**
     * 回复人员名称
     */
    private String resperName;

    /**
     *回复人员头像
     */
    private String resperAvatar;

    /**
     * 回复内容
     */
    private String content;

    /**
     * H5问题分类列表顶部tips
     */
    private String h5Tips;

    /**
     * H5问题分类列表
     */
    private ArrayList<OpinionCategry> h5QuesCategory;

    public ArrayList<OpinionCategry> getH5QuesCategory() {
        return h5QuesCategory;
    }

    public void setH5QuesCategory(ArrayList<OpinionCategry> h5QuesCategory) {
        this.h5QuesCategory = h5QuesCategory;
    }

    public String getResperName() {
        return resperName;
    }

    public void setResperName(String resperName) {
        this.resperName = resperName;
    }

    public String getResperAvatar() {
        return resperAvatar;
    }

    public void setResperAvatar(String resperAvatar) {
        this.resperAvatar = resperAvatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getH5Tips() {
        return h5Tips;
    }

    public void setH5Tips(String h5Tips) {
        this.h5Tips = h5Tips;
    }

    public class OpinionCategry{

        /**
         * 问题分类名称
         */
        private String categoryName;

        public ArrayList<Opinion> getQuestions() {
            return questions;
        }

        public void setQuestions(ArrayList<Opinion> questions) {
            this.questions = questions;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        /**
         * 问题列表
         */
        private ArrayList<Opinion> questions;


        public class Opinion{

            /**
             * 问题title
             */
            private String questionTitle;

            public String getLinks() {
                return links;
            }

            public void setLinks(String links) {
                this.links = links;
            }

            public String getQuestionTitle() {
                return questionTitle;
            }

            public void setQuestionTitle(String questionTitle) {
                this.questionTitle = questionTitle;
            }

            /**
             * 链接地址
             */
            private String links;
        }
    }
}
