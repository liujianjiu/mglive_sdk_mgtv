package com.hunantv.mglive.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 达 on 2015/11/16.
 */
public class StarModel implements Serializable {
    /**
     * 用户id
     */
    private String uid;
    /**
     * 用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 性别，0女，1男
     */
    private String sex;
    /**
     * 头像
     */
    private String photo;
    /**
     * 普通用户/艺人/明星/官方
     */
    private int role;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 简介
     */
    private String intr;
    /**
     * 人气
     */
    private String hotValue;
    /**
     * 排行
     */
    private String rank;
    /**
     * 粉丝数
     */
    private String fans;
    /**
     * 视频地址/key
     */
    private String video;
    /**
     * 封面
     */
    private String cover;
    /**
     * 标签多个用”,”g隔开
     */
    private ArrayList<String> tags;
    /**
     * 人气值
     */
    private String hots;
    /**
     * 星座
     */
    private String xingzuo;
    /**
     * 城市
     */
    private String city;
    /**
     * 年纪
     */
    private String age;

    /**
     * 是否互粉
     */
    private String isfans;

    private String seq;

    private String ranking;

    //艺人来源
    private String origin;

    public StarModel(){

    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getIntr() {
        return intr;
    }

    public void setIntr(String intr) {
        this.intr = intr;
    }

    public String getHotValue() {
        return hotValue;
    }

    public void setHotValue(String hotValue) {
        this.hotValue = hotValue;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public String getHots() {
        return hots;
    }

    public void setHots(String hots) {
        this.hots = hots;
    }

    public String getFans() {
        return fans;
    }

    public void setFans(String fans) {
        this.fans = fans;
    }


    public String getXingzuo() {
        return xingzuo;
    }

    public void setXingzuo(String xingzuo) {
        this.xingzuo = xingzuo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIsfans() {
        return isfans;
    }

    public void setIsfans(String isfans) {
        this.isfans = isfans;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    //
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    private StarModel(Parcel in){
//        uid = in.readString();
//        username = in.readString();
//        nickName = in.readString();
//        sex = in.readString();
//        photo = in.readString();
//        role = in.readString();
//        birthday = in.readString();
//        intr = in.readString();
//        hotValue = in.readString();
//        rank = in.readString();
//        fansCount = in.readString();
//        video = in.readString();
//        cover = in.readString();
//        hots= in.readString();
////        tags = in.readStringArray();
//        fans= in.readString();
//        xingzuo= in.readString();
//        city= in.readString();
//        age= in.readString();
//        String tagStr = in.readString();
//        if(!StringUtil.isNullorEmpty(tagStr))
//        {
//            String[] tags = tagStr.split(",");
//            setTags(Arrays.asList(tags));
//        }
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(uid);
//        dest.writeString(username);
//        dest.writeString(nickName);
//        dest.writeString(sex);
//        dest.writeString(photo);
//        dest.writeString(role);
//        dest.writeString(birthday);
//        dest.writeString(intr);
//        dest.writeString(hotValue);
//        dest.writeString(rank);
//        dest.writeString(fansCount);
//        dest.writeString(video);
//        dest.writeString(cover);
//        dest.writeString(hots);
//        dest.writeString(fans);
//        dest.writeString(xingzuo);
//        dest.writeString(city);
//        dest.writeString(age);
//        StringBuilder sb = new StringBuilder();
//        List<String> tags = getTags();
//        for (int i=0;tags != null && i<tags.size();i++)
//        {
//            sb.append(tags.get(i));
//            if(i != tags.size() - 1)
//            {
//               sb.append(",");
//            }
//        }
//        dest.writeString(sb.toString());
//    }
//
//    public static final Parcelable.Creator<StarModel> CREATOR = new Parcelable.Creator<StarModel>() {
//
//        @Override
//        public StarModel createFromParcel(Parcel source) {
//            return new StarModel(source);
//        }
//
//        @Override
//        public StarModel[] newArray(int size) {
//            return new StarModel[size];
//        }
//    };
}
