package com.hunantv.mglive.data.live;

import com.hunantv.mglive.data.ChatDataModel;
import com.hunantv.mglive.data.GiftDataModel;
import com.hunantv.mglive.data.GiftShowViewDataModel;
import com.hunantv.mglive.data.StarModel;
import com.hunantv.mglive.data.login.UserInfoData;
import com.hunantv.mglive.utils.GiftUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2015/12/4.
 */
public class ChatData {

    /**
     1、 没有type字段， 表示普通消息
     2、 ==1 表示普通消息
     3、 ==2 带有身份的用户进入直播间
     4、 ==3 表示送礼消息，需要累加人气值
     5、 ==4 预定义 广告
     */
    /**
     * 普通消息(弹幕)
     */
    /**
     * 普通消息(弹幕)
     */
    public static final int CHAT_TYPE_DANMO = 0;
    /**
     * 表示普通消息
     */
    public static final int CHAT_TYPE_CONTENT = 1;
    /**
     * 带有身份的用户进入直播间
     */
    public static final int CHAT_TYPE_VIP_JOIN_IN = 2;
    /**
     * 表示送礼消息，需要累加人气值
     */
    public static final int CHAT_TYPE_GIFT = 3;
    /**
     * 预定义 广告
     */
    public static final int CHAT_TYPE_AD = 4;
    /**
     * 普通用户进场
     */
    public static final int CHAT_TYPE_NORMAL_JOIN_IN = 5;

    /**
     * 直播艺人变化
     */
    public static final int CHAT_TYPE_STAR_CHANGE = 6;
    /**
     * 关闭秀场直播
     */
    public static final int CHAT_TYPE_LIVE_END = 7;
    /**
     * 秀场直播暂停
     */
    public static final int CHAT_TYPE_LIVE_PAUSE = 8;
    /**
     * 直播开启消息
     */
    public static final int CHAT_TYPE_LIVE_MESSAGE = 9;
    /**
     * 用户进场头像
     */
    public static final int CHAT_TYPE_JOIN_IN_ICON = 50;
    /**
     * 系统通知消息
     */
    public static final int CHAT_TYPE_SYSTEM_MSG = 30;
    /**
     * 购买守护
     */
    public static final int CHAT_TYPE_BUY_GUARD = 333;

    /**
     * 1、 没有type字段， 表示普通消息
     * 2、 ==1 表示普通消息
     * 3、 ==2 带有身份的用户进入直播间
     * 4、 ==3 表示送礼消息，需要累加人气值
     * 5、 ==4 预定义 广告
     * 6、 ==5 普通用户进场
     */
    private int type;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 用户id
     */
    private String uuid;

    /**
     * 产生消息的用户头像
     */
    private String avatar;

    /**
     * 消息内容
     */
    private String barrageContent;

    /**
     * 守护等级
     * 1：铜牌
     * 2：银牌
     * 3：金牌
     */
    private int grade;

    /**
     * 用户角色
     */
    private int role;

    /**
     * 礼物id
     */
    private long productId;

    /**
     * 礼物个数，如果是守护表示几个月
     */
    private long count;

    /**
     * 如果是喊话表示喊话内容
     */
    private String tip;

    /**
     * 接收礼物的用户（艺人）id
     */
    private String targetUuid;

    /**
     * 广告商logo
     */
    private String bossLogo;

    /**
     * 广告商名称
     */
    private String bossName;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 产品图标
     */
    private String productIcon;

    /**
     * 点击链接
     */
    private String link;
    /**
     * 点击链接
     */
    private int time;
    /**
     * 展示方式
     */
    private int show;
    /**
     * 直播间id
     */
    private String roomId;
    /**
     * 镜头id
     */
    private String channelId;

    /**
     * 镜头名称
     */
    private String channelName;

    /**
     * 系统消息内容
     */
    private String content;

    /**
     * 人气,守护消息用 type=333
     */
    private String hotValue;

    /**
     * 正在直播的艺人(只有uid)
     */
    private List<UserInfoData> showOwners = new ArrayList<>();

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBarrageContent() {
        return barrageContent;
    }

    public void setBarrageContent(String barrageContent) {
        this.barrageContent = barrageContent;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getTargetUuid() {
        return targetUuid;
    }

    public void setTargetUuid(String targetUuid) {
        this.targetUuid = targetUuid;
    }

    public String getBossLogo() {
        return bossLogo;
    }

    public void setBossLogo(String bossLogo) {
        this.bossLogo = bossLogo;
    }

    public String getBossName() {
        return bossName;
    }

    public void setBossName(String bossName) {
        this.bossName = bossName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductIcon() {
        return productIcon;
    }

    public void setProductIcon(String productIcon) {
        this.productIcon = productIcon;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getShow() {
        return show;
    }

    public void setShow(int show) {
        this.show = show;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public List<UserInfoData> getShowOwners() {
        return showOwners;
    }

    public void setShowOwners(List<UserInfoData> showOwners) {
        this.showOwners = showOwners;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getHotValue() {
        return hotValue;
    }

    public void setHotValue(String hotValue) {
        this.hotValue = hotValue;
    }

    public long getHots(GiftDataModel mShortCutGiftData) {
        long hot = 0;
        if (mShortCutGiftData != null && String.valueOf(productId).equals(String.valueOf(mShortCutGiftData.getGid()))) {
            hot = Long.valueOf(mShortCutGiftData.getHots());
            return hot;
        }

        GiftDataModel giftModel = GiftUtil.getInstance().getGiftModel(String.valueOf(productId));
        if(giftModel != null)
        {
            hot = Long.valueOf(giftModel.getHots());
        }
        return hot;
    }

    public ChatDataModel toChatModle(GiftDataModel mShortCutGiftData, String name2) {
        ChatDataModel modle = toChatModle(mShortCutGiftData);
        modle.name2 = name2;
        return modle;
    }

    private ChatDataModel toChatModle(GiftDataModel mShortCutGiftData) {
        String name = "";
        ChatDataModel modle = null;
        if (null != mShortCutGiftData && String.valueOf(mShortCutGiftData.getGid()).equals(String.valueOf(getProductId()))) {
            name = mShortCutGiftData.getName();
            modle = toChatModle();
            modle.giftName = name;
            modle.giftPhoto = mShortCutGiftData.getPhoto();
        } else {
            modle = toChatDataModel();
        }
        return modle;
    }

    private ChatDataModel toChatDataModel() {
        ChatDataModel modle = toChatModle();
        GiftDataModel giftModel = GiftUtil.getInstance().getGiftModel(String.valueOf(getProductId()));
        if(giftModel != null)
        {
            modle.giftName = giftModel.getName();
            modle.giftPhoto = giftModel.getPhoto();
        }
        return modle;
    }

    public GiftShowViewDataModel getGiftShowDataModel(GiftDataModel giftDataModel, StarModel starModel) {
        if (giftDataModel == null || starModel == null) {
            return null;
        }
        if (giftDataModel.getGid() == getProductId() && giftDataModel.getAnimType() != 0) {
            GiftShowViewDataModel dataModel = new GiftShowViewDataModel();
            dataModel.setIcon(getAvatar());
            dataModel.setImage(giftDataModel.getPhoto());
            dataModel.setNum(getCount());
            dataModel.setStarName(starModel.getNickName());
            dataModel.setUserName(getNickname());
            return dataModel;
        }
        return null;
    }


    public GiftShowViewDataModel getGiftShowDataModel(StarModel starModel) {
        if (starModel == null) {
            return null;
        }

        GiftDataModel giftModel = GiftUtil.getInstance().getGiftModel(String.valueOf(getProductId()));
        return getGiftShowDataModel(giftModel, starModel);
    }

    public ChatDataModel toCharModelByGrund(String starName){
        ChatDataModel model = toChatModle();
        model.name2 = starName;
        return model;
    }

    public ChatDataModel toChatModle() {
        ChatDataModel modle = new ChatDataModel();
        modle.chatType = getType();
        modle.content = getBarrageContent();
        modle.name1 = getNickname();
        modle.giftId = getProductId();
        modle.giftNum = getCount();
        modle.grade = getGrade();
        modle.uuid = getUuid();
        modle.role = getRole();
        return modle;
    }
}
