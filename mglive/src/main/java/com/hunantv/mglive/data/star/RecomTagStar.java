package com.hunantv.mglive.data.star;

import java.util.ArrayList;

/**
 * Created by admin on 2016/1/25.
 */
public class RecomTagStar {
    private String tagName;

    private String tagId;

    private ArrayList<TagStarModel> artists;

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public ArrayList<TagStarModel> getArtists() {
        return artists;
    }

    public void setArtists(ArrayList<TagStarModel> artists) {
        this.artists = artists;
    }
}
