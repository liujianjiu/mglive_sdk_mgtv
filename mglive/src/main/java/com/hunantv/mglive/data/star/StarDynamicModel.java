package com.hunantv.mglive.data.star;

/**
 * Created by admin on 2016/1/25.
 */
public class StarDynamicModel {
    public static final String TYPE_D_IMG = "1";
    public static final String TYPE_D_VIDEO = "2";

    private String dynamicId;

    private String dType;

    private String dynamicCover;

    public String getDynamicId() {
        return dynamicId;
    }

    public void setDynamicId(String dynamicId) {
        this.dynamicId = dynamicId;
    }

    public String getdType() {
        return dType;
    }

    public void setdType(String dType) {
        this.dType = dType;
    }

    public String getDynamicCover() {
        return dynamicCover;
    }

    public void setDynamicCover(String dynamicCover) {
        this.dynamicCover = dynamicCover;
    }
}
