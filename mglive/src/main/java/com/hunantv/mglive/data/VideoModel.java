package com.hunantv.mglive.data;

import java.io.Serializable;

/**
 * Created by qiudaaini@163.com on 16/1/8.
 */
public class VideoModel implements Serializable {

    private String cover;
    private String url;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
