package com.hunantv.mglive.data.config;

/**
 * Created by admin on 2015/12/10.
 */
public class MenuModel {
    /**
     * 菜单名称
     */
    private String name;
    /**
     * 菜单id
     */
    private long mid;
    /**
     * 菜单跳转路径，http://跳转到网页，app://跳转到native页面
     */
    private String target;
    /**
     * 图标地址, http:// 网络图标，app://内置图标
     */
    private String icon;
    /**
     * 菜单tips
     */
    private String tips;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }
}
