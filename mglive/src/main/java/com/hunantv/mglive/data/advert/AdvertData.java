package com.hunantv.mglive.data.advert;

/**
 * Created by admin on 2016/2/22.
 */
public class AdvertData {
    private String url;
    private byte[] bytes;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
