package com.hunantv.mglive.data.version;

/**
 * Created by admin on 2016/6/13.
 */
public class PatchModel {
    /**
     * 补丁版本号
     */
    private String patchVersion;
    /**
     * 升级说明
     */
    private String updateRemark;
    /**
     * 补丁下载地址
     */
    private String downloadUrl;

    public String getPatchVersion() {
        return patchVersion;
    }

    public void setPatchVersion(String patchVersion) {
        this.patchVersion = patchVersion;
    }

    public String getUpdateRemark() {
        return updateRemark;
    }

    public void setUpdateRemark(String updateRemark) {
        this.updateRemark = updateRemark;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
}
