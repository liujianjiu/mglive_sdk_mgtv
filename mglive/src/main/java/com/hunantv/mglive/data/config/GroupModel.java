package com.hunantv.mglive.data.config;

import java.util.List;

/**
 * Created by admin on 2015/12/10.
 */
public class GroupModel {
    /**
     * 菜单组名，默认空
     */
    private String name;
    /**
     * 菜单
     */
    private List<MenuModel> menus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MenuModel> getMenus() {
        return menus;
    }

    public void setMenus(List<MenuModel> menus) {
        this.menus = menus;
    }
}
