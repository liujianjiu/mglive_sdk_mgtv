package com.hunantv.mglive.data.act;

/**
 * Created by admin on 2016/2/1.
 */
public class ActStageModel {
    public static final String STAGE_INITIAL = "initial";
    public static final String STAGE_START = "start";
    public static final String STAGE_RUN = "run";
    public static final String STAGE_STOP = "stop";
    /**
     * 阶段id
     */
    private String id;
    /**
     * 阶段标题
     */
    private String title;
    /**
     * 阶段主题
     */
    private String topic;
    /**
     * 阶段图片
     */
    private String image;
    /**
     * 阶段状态 initial、start、run、stop
     */
    private String status;
    /**
     * 该阶段参与的人员数量
     */
    private String partiTotal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPartiTotal() {
        return partiTotal;
    }

    public void setPartiTotal(String partiTotal) {
        this.partiTotal = partiTotal;
    }
}
