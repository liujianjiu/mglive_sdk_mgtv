package com.hunantv.mglive.data.discovery;

/**
 * 看视频找妹子的数据结构
 * <p/>
 * "age":"27",
 * "city":"上海",
 * "cover":"http://pic-mgtv-max-cms.oss-cn-beijing.aliyuncs.com/BB8FA385-C5F6-4C69-868D-D1CF0D2107DE",
 * "fansCount":"57",
 * "hots":"76102583",
 * "nickName":"张玮",
 * "photo":"http://pic-mgtv-max-cms.oss-cn-beijing.aliyuncs.com/BB8FA385-C5F6-4C69-868D-D1CF0D2107DE",
 * "rank":"0",
 * "role":1,
 * "tags":Array[10],
 * "uid":"e61822f24dfe73b9d6820361bd4f6e03",
 * "video":"",
 * "xingzuo":"狮子座"
 * Created by June Kwok on 2015-12-12.
 */
public class WatchMMData {
    private String age;
    private String city;
    private String fansCount;
    private String hots;
    private String rank;
    private int role;
    private String xingzuo;
    private String uid;
    private String nickName;
    //    private List<WatchMMTag> tags;
    private String photo;
    private String cover;
    private String video;
    private String intr;
    private String isFollowed;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFansCount() {
        return fansCount;
    }

    public void setFansCount(String fansCount) {
        this.fansCount = fansCount;
    }

    public String getHots() {
        return hots;
    }

    public void setHots(String hots) {
        this.hots = hots;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getXingzuo() {
        return xingzuo;
    }

    public void setXingzuo(String xingzuo) {
        this.xingzuo = xingzuo;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

//    public List<WatchMMTag> getTags() {
//        return tags;
//    }
//
//    public void setTags(List<WatchMMTag> tags) {
//        this.tags = tags;
//    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getIntr() {
        return intr;
    }

    public void setIntr(String intr) {
        this.intr = intr;
    }

    public String getIsFollowed() {
        return isFollowed;
    }

    public void setIsFollowed(String isFollowed) {
        this.isFollowed = isFollowed;
    }

    public boolean isFollowed() {
        return Boolean.valueOf(getIsFollowed());
    }

    public void setIsFollowed(boolean isFollowed) {
        setIsFollowed(String.valueOf(isFollowed));
    }
}



