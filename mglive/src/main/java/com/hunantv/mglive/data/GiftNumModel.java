package com.hunantv.mglive.data;

/**
 * Created by 达 on 2015/11/25.
 */
public class GiftNumModel {
    public GiftNumModel(){

    }

    public GiftNumModel(int giftNum){
        this.giftNum = giftNum;
    }

    public int giftNum;
    public String giftNumText;

    public int getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(int giftNum) {
        this.giftNum = giftNum;
    }

    public String getGiftNumText() {
        return giftNumText;
    }

    public void setGiftNumText(String giftNumText) {
        this.giftNumText = giftNumText;
    }
}
