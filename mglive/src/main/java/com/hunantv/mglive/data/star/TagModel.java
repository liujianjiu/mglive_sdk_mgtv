package com.hunantv.mglive.data.star;

/**
 * Created by admin on 2016/1/25.
 */
public class TagModel {
    private String tid;

    private String name;

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
