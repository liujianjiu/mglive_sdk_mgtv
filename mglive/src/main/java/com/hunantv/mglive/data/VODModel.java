package com.hunantv.mglive.data;

/**
 * Created by admin on 2016/6/20.
 */
public class VODModel {
    public static final String TYPE_MGTV = "MGTV";
    /**
     * M3U8地址
     */
    private String playUrl;
    /**
     * MP4地址
     */
    private String mp4Url;
    /**
     * MGTV、UGC、PGC
     * 为mgtv时返回的是调度地址,需get请求后使用info字段播放
     */
    private String vodType;

    public String getPlayUrl() {
        return playUrl;
    }

    public void setPlayUrl(String playUrl) {
        this.playUrl = playUrl;
    }

    public String getMp4Url() {
        return mp4Url;
    }

    public void setMp4Url(String mp4Url) {
        this.mp4Url = mp4Url;
    }

    public String getVodType() {
        return vodType;
    }

    public void setVodType(String vodType) {
        this.vodType = vodType;
    }

    public boolean isMgtvType(){
        return TYPE_MGTV.equals(vodType);
    }
}
