package com.hunantv.mglive.data;

/**
 * Created by qiuda on 16/3/16.
 */
public class SuperWomenRankModel {
    //   用户id
    private String uid;
    //    人气值
    private String hotValue;
    //   昵称
    private String nickName;
    //  排名
    private String ranking;
    //   头像
    private String photo;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getHotValue() {
        return hotValue;
    }

    public void setHotValue(String hotValue) {
        this.hotValue = hotValue;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
