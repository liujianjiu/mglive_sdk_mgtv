package com.hunantv.mglive.data.star;

import com.hunantv.mglive.data.StarModel;

import java.util.ArrayList;

/**
 * Created by admin on 2016/1/25.
 */
public class StarRankModel {
    private String title;

    private String cid;

    private String aid;

    private ArrayList<StarModel> artistRanking;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<StarModel> getArtistRanking() {
        return artistRanking;
    }

    public void setArtistRanking(ArrayList<StarModel> artistRanking) {
        this.artistRanking = artistRanking;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }
}
