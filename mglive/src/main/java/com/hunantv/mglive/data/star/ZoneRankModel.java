package com.hunantv.mglive.data.star;

import com.hunantv.mglive.data.StarModel;

import java.util.ArrayList;

/**
 * Created by admin on 2016/2/29.
 */
public class ZoneRankModel {
    /**
     * 榜单名称
     */
    private String rankingName;
    /**
     * 描述
     */
    private String des;
    /**
     * 跳转地址URL
     */
    private String url;
    /**
     * 赛区图片
     */
    private String image;
    /**
     * 艺人列表
     */
    private ArrayList<StarModel> stars;

    public String getRankingName() {
        return rankingName;
    }

    public void setRankingName(String rankingName) {
        this.rankingName = rankingName;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<StarModel> getStars() {
        return stars;
    }

    public void setStars(ArrayList<StarModel> stars) {
        this.stars = stars;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
