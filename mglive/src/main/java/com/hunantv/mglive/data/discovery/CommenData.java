package com.hunantv.mglive.data.discovery;

/**
 * 评论
 * Created by guojun3 on 2015-12-16.
 */
public class CommenData {

    //  "commentId":202,"content":"快来公司","date":"12-20 16:22","dynamicId":"f4977d420fe743968684d3998139c08e","nickName":"max","photo":"http://p1.hunantv.com/1/ava1_TwmJtXeLIgDkflSaI952TE4OeNkzs2VR.jpg","uid":"eb5e2ee98c109a3f3bf7069c040e10f3"
    /**
     * uid	String	是		用户id
     * commentId	Long	是		评论id
     * nickName	String	是		昵称
     * photo	String	是		头像
     * content	String	是		评论内容
     * date	String	是		评论时间
     * dynamicId	Long	是		动态id
     */
    private String uid;
    private long commentId;
    private String nickName;
    private String photo;
    private String content;
    private String date;
    private String dynamicId;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDynamicId() {
        return dynamicId;
    }

    public void setDynamicId(String dynamicId) {
        this.dynamicId = dynamicId;
    }
}
