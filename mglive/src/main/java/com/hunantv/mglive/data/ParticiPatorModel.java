package com.hunantv.mglive.data;

import java.io.Serializable;

/**
 * Created by qiudaaini@163.com on 16/2/3.
 */
public class ParticiPatorModel  implements Serializable{
    /**
     * 排行榜序号
     */
    private String rank;
    /**
     * 艺人id
     */
    private String uid;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 头像
     */
    private String photo;
    /**
     * 活动的参与ID
     */
    private String partiId;
    /**
     * 参与形式类型，当前需求只有视频动态，取值：vdy
     */
    private String linkType;
    /**
     * 关联的参与ID，当前需求，是视频动态的ID
     */
    private String linkId;
    /**
     * 动态视频的id
     */
    private String videoId;
    /**
     * 动态视频的图片
     */
    private String videoImage;
    /**
     * 票数
     */
    private long poll;
    /**
     * 是否粉丝  1：0
     */
    private String isfans;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPartiId() {
        return partiId;
    }

    public void setPartiId(String partiId) {
        this.partiId = partiId;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoImage() {
        return videoImage;
    }

    public void setVideoImage(String videoImage) {
        this.videoImage = videoImage;
    }

    public long getPoll() {
        return poll;
    }

    public void setPoll(long poll) {
        this.poll = poll;
    }

    public String getIsfans() {
        return isfans;
    }

    public void setIsfans(String isfans) {
        this.isfans = isfans;
    }
}
