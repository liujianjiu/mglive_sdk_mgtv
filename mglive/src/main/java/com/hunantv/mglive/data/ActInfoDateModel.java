package com.hunantv.mglive.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qiudaaini@163.com on 16/2/3.
 */
public class ActInfoDateModel implements Serializable {
    /**
     * 序号
     */
    private String seq;
    /**
     * 校验密钥
     */
    private String secret;
    /**
     * 有效时间，单位：分钟
     */
    private int duration;
    /**
     * 参与pk的参与者
     */
    private List<ParticiPatorModel> pkArray = new ArrayList<>();

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<ParticiPatorModel> getPkArray() {
        return pkArray;
    }

    public void setPkArray(List<ParticiPatorModel> pkArray) {
        this.pkArray = pkArray;
    }
}
