package com.hunantv.mglive.data;

/**
 * Created by qiudaaini@163.com on 16/2/3.
 */
public class VoteResultModel {
    /**
     * 是否正确
     */
    private String result;
    /**
     * 猜中的次数
     */
    private int rightNum;
    /**
     * 猜错的次数
     */
    private int wrongNum;
    /**
     * 之前的星探值
     */
    private double value;
    /**
     * 增长的星探值
     */
    private double addValue;
    /**
     * A的票数
     */
    private long pollA;
    /**
     * B的票数
     */
    private long pollB;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getRightNum() {
        return rightNum;
    }

    public void setRightNum(int rightNum) {
        this.rightNum = rightNum;
    }

    public int getWrongNum() {
        return wrongNum;
    }

    public void setWrongNum(int wrongNum) {
        this.wrongNum = wrongNum;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getAddValue() {
        return addValue;
    }

    public void setAddValue(double addValue) {
        this.addValue = addValue;
    }

    public long getPollA() {
        return pollA;
    }

    public void setPollA(long pollA) {
        this.pollA = pollA;
    }

    public long getPollB() {
        return pollB;
    }

    public void setPollB(long pollB) {
        this.pollB = pollB;
    }
}
