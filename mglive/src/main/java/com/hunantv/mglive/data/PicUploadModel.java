package com.hunantv.mglive.data;

/**
 * Created by admin on 2015/12/9.
 */
public class PicUploadModel {
    /**
     * 阿里
     */
    public static final String FLAG_ALI = "0";

    /**
     * 腾讯
     */
    public static final String FLAG_TENCENT = "0";

    private String accessid;

    /**
     * 目录
     */
    private String policy;

    /**
     * 验证串
     */
    private String signature;

    /**
     * 过期时间
     */
    private String expire;

    /**
     * 云标准(0:阿里，1:腾讯)
     */
    private String yunFlag;

    public String getAccessid() {
        return accessid;
    }

    public void setAccessid(String accessid) {
        this.accessid = accessid;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getYunFlag() {
        return yunFlag;
    }

    public void setYunFlag(String yunFlag) {
        this.yunFlag = yunFlag;
    }
}
