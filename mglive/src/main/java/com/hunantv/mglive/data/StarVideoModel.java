package com.hunantv.mglive.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qiudaaini@163.com on 16/1/8.
 */
public class StarVideoModel  implements Serializable{

    private String uid;
    private String dynamicId;
//    private String nickName;
//    private String photo;
//    private long praiseCount;
//    private long commentCount;
//    private String date;
    private String content;
//    private List<ImageModel> images = new ArrayList<>();
    private VideoModel video;
//    private List<CommentModel> comments = new ArrayList<>();
//    private String type;


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDynamicId() {
        return dynamicId;
    }

    public void setDynamicId(String dynamicId) {
        this.dynamicId = dynamicId;
    }

//    public String getNickName() {
//        return nickName;
//    }

//    public void setNickName(String nickName) {
//        this.nickName = nickName;
//    }

//    public String getPhoto() {
//        return photo;
//    }

//    public void setPhoto(String photo) {
//        this.photo = photo;
//    }

//    public long getPraiseCount() {
//        return praiseCount;
//    }

//    public void setPraiseCount(long praiseCount) {
//        this.praiseCount = praiseCount;
//    }

//    public long getCommentCount() {
//        return commentCount;
//    }

//    public void setCommentCount(long commentCount) {
//        this.commentCount = commentCount;
//    }

//    public String getDate() {
//        return date;
//    }

//    public void setDate(String date) {
//        this.date = date;
//    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

//    public List<ImageModel> getImages() {
//        return images;
//    }

//    public void setImages(List<ImageModel> images) {
//        this.images = images;
//    }

    public VideoModel getVideo() {
        return video;
    }

    public void setVideo(VideoModel video) {
        this.video = video;
    }

//    public List<CommentModel> getComments() {
//        return comments;
//    }

//    public void setComments(List<CommentModel> comments) {
//        this.comments = comments;
//    }

//    public String getType() {
//        return type;
//    }

//    public void setType(String type) {
//        this.type = type;
//    }
}
