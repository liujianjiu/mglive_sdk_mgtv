package com.hunantv.mglive.data.template;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by admin on 2016/3/15.
 */
public class TemplateModel implements Serializable {
    /**
     * 模板ID
     */
    private String templetID;
    /**
     * 统一色值
     */
    private String colour;
    /**
     * 统一图标
     */
    private String icon;
    /**
     * 标题
     */
    private String title;
    /**
     * 模板元素集合
     */
    private ArrayList<ElementModel> elements;

    public String getTempletID() {
        return templetID;
    }

    public void setTempletID(String templetID) {
        this.templetID = templetID;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public ArrayList<ElementModel> getElements() {
        return elements;
    }

    public void setElements(ArrayList<ElementModel> elements) {
        this.elements = elements;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
