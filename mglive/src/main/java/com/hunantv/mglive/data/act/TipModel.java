package com.hunantv.mglive.data.act;

import java.io.Serializable;

/**
 * Created by admin on 2016/2/2.
 */
public class TipModel implements Serializable {
    /**
     * 投票时候的提示
     */
    private String tip1;
    /**
     * 投票时候的提示
     */
    private String tip2;
    /**
     * 投票答对的提示
     */
    private String tip3;
    /**
     * 投票答对的提示
     */
    private String tip4;
    /**
     * 投票答错的提示
     */
    private String tip5;
    /**
     * 投票答错的提示
     */
    private String tip6;
    /**
     * 上传成功提示
     */
    private String uploadSuccess;

    public String getTip1() {
        return tip1;
    }

    public void setTip1(String tip1) {
        this.tip1 = tip1;
    }

    public String getTip2() {
        return tip2;
    }

    public void setTip2(String tip2) {
        this.tip2 = tip2;
    }

    public String getTip3() {
        return tip3;
    }

    public void setTip3(String tip3) {
        this.tip3 = tip3;
    }

    public String getTip4() {
        return tip4;
    }

    public void setTip4(String tip4) {
        this.tip4 = tip4;
    }

    public String getTip5() {
        return tip5;
    }

    public void setTip5(String tip5) {
        this.tip5 = tip5;
    }

    public String getTip6() {
        return tip6;
    }

    public void setTip6(String tip6) {
        this.tip6 = tip6;
    }

    public String getUploadSuccess() {
        return uploadSuccess;
    }

    public void setUploadSuccess(String uploadSuccess) {
        this.uploadSuccess = uploadSuccess;
    }
}
