package com.hunantv.mglive.data.discovery;

import java.io.Serializable;

/**
 * Created by guojun3 on 2016-1-15.
 */
public class DynamicCommen implements Serializable {

    private String uid;
    private String nickName;
    private String content;
    private String Date;

    public DynamicCommen() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}

