package com.hunantv.mglive.data.sign;

/**
 * Created by admin on 2016/2/18.
 */
public class SignInfoModel {
    /**
     * 日期（根据用户领取的情况返回不同的三组信息）
     */
    private String dateInfo;
    /**
     * 金币数
     */
    private String coinInfo;
    /**
     * 1已领取  0未领取
     */
    private String isReceive;
    /**
     * 1可领  0不可领
     */
    private String enable;

    public String getDateInfo() {
        return dateInfo;
    }

    public void setDateInfo(String dateInfo) {
        this.dateInfo = dateInfo;
    }

    public String getCoinInfo() {
        return coinInfo;
    }

    public void setCoinInfo(String coinInfo) {
        this.coinInfo = coinInfo;
    }

    public String getIsReceive() {
        return isReceive;
    }

    public void setIsReceive(String isReceive) {
        this.isReceive = isReceive;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }
}
