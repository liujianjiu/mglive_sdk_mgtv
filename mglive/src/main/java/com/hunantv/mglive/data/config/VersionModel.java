package com.hunantv.mglive.data.config;

import com.alibaba.fastjson.JSON;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2015/12/9.
 */
public class VersionModel {
    private String alert;      // 1/0   // 1是强制更新
    private String title;       //标题
    private String version;      //最新版本
    private String desc;       //描述
    private String appUrl;      //升级的url
    private String review;      //1 审核中 0已上线

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
