package com.hunantv.mglive.data.discovery;

/**
 * Created by admin on 2015/12/15.
 */
public class MenuTipsData {
    private String ids;
    private String text;
    private String icon;
    private String tips;
    private String tipsUrl;

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getTipsUrl() {
        return tipsUrl;
    }

    public void setTipsUrl(String tipsUrl) {
        this.tipsUrl = tipsUrl;
    }
}
