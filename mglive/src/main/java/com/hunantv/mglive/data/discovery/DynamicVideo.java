package com.hunantv.mglive.data.discovery;

import java.io.Serializable;

/**
 * Created by guojun3 on 2016-1-15.
 */
public class DynamicVideo implements Serializable {

    private String cover;
    private String url;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
