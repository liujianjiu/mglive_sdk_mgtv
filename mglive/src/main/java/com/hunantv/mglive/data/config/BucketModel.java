package com.hunantv.mglive.data.config;

/**
 * Created by admin on 2015/12/9.
 */
public class BucketModel {
    private String moduleName;
    private String bucketName;

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
