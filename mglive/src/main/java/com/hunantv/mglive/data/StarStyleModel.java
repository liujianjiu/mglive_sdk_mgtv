package com.hunantv.mglive.data;

import java.util.List;

/**
 * Created by 达 on 2015/11/16.
 */
public class StarStyleModel {
    private String name;
    private List<StarModel> starModels;

    public StarStyleModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StarModel> getStarModels() {
        return starModels;
    }

    public void setStarModels(List<StarModel> starModels) {
        this.starModels = starModels;
    }
}
