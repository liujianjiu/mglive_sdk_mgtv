package com.hunantv.mglive.data.template;

import java.io.Serializable;

/**
 * Created by admin on 2016/3/26.
 */
public class RequestData implements Serializable {
    public String type;
    public int startIndex;
    public int endIndex;
}
