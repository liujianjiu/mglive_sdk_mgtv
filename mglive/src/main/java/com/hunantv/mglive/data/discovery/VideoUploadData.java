package com.hunantv.mglive.data.discovery;

/**
 * Created by admin on 2015/12/1.
 */
public class VideoUploadData {
    private String sdkFlag;

    private String bucket;

    private String token;

    public String getSdkFlag() {
        return sdkFlag;
    }

    public void setSdkFlag(String sdkFlag) {
        this.sdkFlag = sdkFlag;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
