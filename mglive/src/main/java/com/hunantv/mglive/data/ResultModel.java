package com.hunantv.mglive.data;

import java.util.Map;

/**
 * Created by QiuDa on 15/11/30.
 */
public class ResultModel {

    private String url;
    private String code;
    private String msg;
    private String data;
    private Object dataModel;

    private String responseCode;
    /**
     * 请求的tag（预留）
     */
    private Object tag;
    /**
     * 请求的参数
     */
    private Map<String, String> param;

    /**
     * mqtt 返回的code
     */
    private String err_code;
    /**
     * mqtt 返回的msg
     */

    private String err_msg;

    private Exception e;

    public ResultModel() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getDataModel() {
        return dataModel;
    }

    public void setDataModel(Object dataModel) {
        this.dataModel = dataModel;
    }

    public String getErr_code() {
        return err_code;
    }

    public void setErr_code(String err_code) {
        this.err_code = err_code;
    }

    public String getErr_msg() {
        return err_msg;
    }

    public void setErr_msg(String err_msg) {
        this.err_msg = err_msg;
    }

    public Exception getE() {
        return e;
    }

    public void setE(Exception e) {
        this.e = e;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public Map<String, String> getParam() {
        return param;
    }

    public void setParam(Map<String, String> param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return "ResultModel{" +
                "url='" + url + '\'' +
                ", code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data='" + data + '\'' +
                ", dataModel=" + dataModel +
                '}';
    }


    public static final String ERROR_CODE_REQUEST = "101";
    public static final String ERROR_CODE_PARSE = "102000";
    public static final String ERROR_CODE_CONNECT = "103000";
    public static final String ERROR_CODE_RESULT = "104000";
    public static final String ERROR_CODE_SHOW_MESSAGE = "1509";
    public static final String ERROR_CODE_GOLD_ENOUGH= "2201";
}
