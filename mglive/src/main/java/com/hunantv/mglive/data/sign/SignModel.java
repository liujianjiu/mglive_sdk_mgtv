package com.hunantv.mglive.data.sign;

import java.util.ArrayList;

/**
 * Created by admin on 2016/2/18.
 */
public class SignModel {
    private SignAdvertModel signAD;

    private ArrayList<SignInfoModel> signInfo;

    public SignAdvertModel getSignAD() {
        return signAD;
    }

    public void setSignAD(SignAdvertModel signAD) {
        this.signAD = signAD;
    }

    public ArrayList<SignInfoModel> getSignInfo() {
        return signInfo;
    }

    public void setSignInfo(ArrayList<SignInfoModel> signInfo) {
        this.signInfo = signInfo;
    }
}
