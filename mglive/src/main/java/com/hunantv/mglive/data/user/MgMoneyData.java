package com.hunantv.mglive.data.user;

import com.hunantv.mglive.utils.L;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2015/11/19.
 */
public class MgMoneyData {

    public MgMoneyData(JSONObject jsonObj) {
        parserData(jsonObj);
    }

    private String pid;

    private String name;

    private String count;

    private String price;

    private String origin;

    private String tag;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void parserData(JSONObject jsonObj) {
        try {
            if (!jsonObj.isNull("pid")) {
                pid = jsonObj.getString("pid");
            }
            if (!jsonObj.isNull("name")) {
                name = jsonObj.getString("name");
            }
            if (!jsonObj.isNull("count")) {
                count = jsonObj.getString("count");
            }
            if (!jsonObj.isNull("price")) {
                price = jsonObj.getString("price");
            }
            if (!jsonObj.isNull("origin")) {
                origin = jsonObj.getString("origin");
            }

            if (!jsonObj.isNull("tag")) {
                tag = jsonObj.getString("tag");
            }
        } catch (Exception e) {
            L.e(MgMoneyData.class.toString(), e);
        }
    }

    public static List<MgMoneyData> toArray(JSONArray jsonArray) throws JSONException {
        List<MgMoneyData> list = new ArrayList<MgMoneyData>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
            MgMoneyData data = new MgMoneyData(jsonObj);
            list.add(data);
        }
        return list;
    }
}
