package com.hunantv.mglive.data.gift;

import com.hunantv.mglive.data.GiftDataModel;

import java.util.ArrayList;

/**
 * Created by admin on 2016/5/26.
 */
public class ClassifyGiftModel {
    /**
     * 分类ID
     */
    private String classifyId;
    /**
     * 分类名称
     */
    private String classifyName;
    /**
     * 分类Icon
     */
    private String classifyIcon;
    /**
     * 礼物列表
     */
    private ArrayList<GiftDataModel> gifts;
    /**
     * 礼物顶部标题
     */
    private String giftsTitle;
    /**
     * 0/1 否/是,是否是默认分类
     */
    private String isDefault;

    public String getClassifyId() {
        return classifyId;
    }

    public void setClassifyId(String classifyId) {
        this.classifyId = classifyId;
    }

    public String getClassifyName() {
        return classifyName;
    }

    public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }

    public String getClassifyIcon() {
        return classifyIcon;
    }

    public void setClassifyIcon(String classifyIcon) {
        this.classifyIcon = classifyIcon;
    }

    public ArrayList<GiftDataModel> getGifts() {
        return gifts;
    }

    public void setGifts(ArrayList<GiftDataModel> gifts) {
        this.gifts = gifts;
    }

    public String getGiftsTitle() {
        return giftsTitle;
    }

    public void setGiftsTitle(String giftsTitle) {
        this.giftsTitle = giftsTitle;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }
}
