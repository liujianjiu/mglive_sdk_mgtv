package com.hunantv.mglive.data;

import java.io.Serializable;

/**
 * 分享配置信息
 */
public class ShareConfigData  implements Serializable {
        private String title;
        private String desc;
        private String url;
        private String img;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }
    }