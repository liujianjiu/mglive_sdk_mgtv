package com.hunantv.mglive.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 达 on 2015/11/30.
 */
public class LiveDataModel {
    public static final String TYPE_LIVE = "0";
    public static final String TYPE_BACK = "1";
    public static final String TYPE_LINK = "2";
    public static final String TYPE_MARK = "3";
    public static final String TYPE_AD = "4";

    public static final String SHOW_COUNT_SHOW = "1";
    public static final String SHOW_COUNT_HIDE = "0";

    private String lId;
    private String title;
    private String liveStatus;
    private String liveTag;
    private String type;
    private String count;
    private String image;
    private String target;
    private String showCount = "1";

    public String getlId() {
        return lId;
    }

    public void setlId(String lId) {
        this.lId = lId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLiveStatus() {
        return liveStatus;
    }

    public void setLiveStatus(String liveStatus) {
        this.liveStatus = liveStatus;
    }

    public String getLiveTag() {
        return liveTag;
    }

    public void setLiveTag(String liveTag) {
        this.liveTag = liveTag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getShowCount() {
        return showCount;
    }

    public void setShowCount(String showCount) {
        this.showCount = showCount;
    }
}
