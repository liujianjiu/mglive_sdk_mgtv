package com.hunantv.mglive.data.user;

/**
 * Created by admin on 2016/1/19.
 */
public class DynamicMessageData {
    public static final String TYPE_M_COMMENT = "0";
    public static final String TYPE_M_PRAISE = "1";
    public static final String TYPE_M_REPLY = "2";

    public static final String TYPE_D_TEXT = "0";
    public static final String TYPE_D_IMG = "1";
    public static final String TYPE_D_VIDEO = "2";
    /**
     * 消息类型 0评论 1点赞 2回复x
     */
    private String mType;
    /**
     * 动态ID
     */
    private String dynamicId;
    /**
     * 动态类型 0文本 1图片 2视频
     */
    private String dType;

    /**
     * 动态标题
     */
    private String dTitle;
    /**
     * 消息内容  mtype为1时显示星星
     */
    private String content;
    /**
     * 消息发生时间
     */
    private String dateTime;
    /**
     * 可能为空
     */
    private String messageCover;
    /**
     * 用户ID
     */
    private String uid;
    /**
     * 用户头像
     */
    private String photo;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 角色
     */
    private int role;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getDynamicId() {
        return dynamicId;
    }

    public void setDynamicId(String dynamicId) {
        this.dynamicId = dynamicId;
    }

    public String getdTitle() {
        return dTitle;
    }

    public void setdTitle(String dTitle) {
        this.dTitle = dTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getMessageCover() {
        return messageCover;
    }

    public void setMessageCover(String messageCover) {
        this.messageCover = messageCover;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getdType() {
        return dType;
    }

    public void setdType(String dType) {
        this.dType = dType;
    }
}
