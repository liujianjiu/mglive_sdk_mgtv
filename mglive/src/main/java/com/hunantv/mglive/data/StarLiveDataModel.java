package com.hunantv.mglive.data;

import java.io.Serializable;

/**
 * Created by qiuda on 16/3/25.
 */
public class StarLiveDataModel implements Serializable {
    private String lId;
    private String title;
    private String count;
    private String uid;
    private int role;
    private String nickName;
    private String photo;
    private String hotValue;
    private String flag;
    private String key;
    private String historyCount;
    private String pushUrl;
    private String dynamicCount;
    private String fansCount;
    private String followCount;
    private String userDesc;
    private String liveStatus;
    private String showHotValue;
    private String giftInvalid;
    /**
     * 视频码率
     */
    private int videoBitRate;
    /**
     * FPS
     */
    private int videoFPS;
    /**
     * 音频码率
     */
    private int audioBitRate;
    /**
     * 音频采样率
     */
    private int audioSampleRate;
    /**
     * 最大码率
     */
    private int maxVideoBitRate;
    /**
     * 最小码率
     */
    private int minVideoBitRate;
    /**
     * 关键帧时间间隔
     */
    private int iFrameInterval;

    public String getlId() {
        return lId;
    }

    public void setlId(String lId) {
        this.lId = lId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getHotValue() {
        return hotValue;
    }

    public void setHotValue(String hotValue) {
        this.hotValue = hotValue;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getHistoryCount() {
        return historyCount;
    }

    public void setHistoryCount(String historyCount) {
        this.historyCount = historyCount;
    }

    public String getPushUrl() {
        return pushUrl;
    }

    public void setPushUrl(String pushUrl) {
        this.pushUrl = pushUrl;
    }

    public String getDynamicCount() {
        return dynamicCount;
    }

    public void setDynamicCount(String dynamicCount) {
        this.dynamicCount = dynamicCount;
    }

    public String getFansCount() {
        return fansCount;
    }

    public void setFansCount(String fansCount) {
        this.fansCount = fansCount;
    }

    public String getFollowCount() {
        return followCount;
    }

    public void setFollowCount(String followCount) {
        this.followCount = followCount;
    }

    public String getUserDesc() {
        return userDesc;
    }

    public void setUserDesc(String userDesc) {
        this.userDesc = userDesc;
    }

    public String getLiveStatus() {
        return liveStatus;
    }

    public void setLiveStatus(String liveStatus) {
        this.liveStatus = liveStatus;
    }

    public String getShowHotValue() {
        return showHotValue;
    }

    public void setShowHotValue(String showHotValue) {
        this.showHotValue = showHotValue;
    }

    public String getGiftInvalid() {
        return giftInvalid;
    }

    public void setGiftInvalid(String giftInvalid) {
        this.giftInvalid = giftInvalid;
    }

    public int getVideoBitRate() {
        return videoBitRate;
    }

    public void setVideoBitRate(int videoBitRate) {
        this.videoBitRate = videoBitRate;
    }

    public int getVideoFPS() {
        return videoFPS;
    }

    public void setVideoFPS(int videoFPS) {
        this.videoFPS = videoFPS;
    }

    public int getAudioBitRate() {
        return audioBitRate;
    }

    public void setAudioBitRate(int audioBitRate) {
        this.audioBitRate = audioBitRate;
    }

    public int getAudioSampleRate() {
        return audioSampleRate;
    }

    public void setAudioSampleRate(int audioSampleRate) {
        this.audioSampleRate = audioSampleRate;
    }

    public int getMaxVideoBitRate() {
        return maxVideoBitRate;
    }

    public void setMaxVideoBitRate(int maxVideoBitRate) {
        this.maxVideoBitRate = maxVideoBitRate;
    }

    public int getMinVideoBitRate() {
        return minVideoBitRate;
    }

    public void setMinVideoBitRate(int minVideoBitRate) {
        this.minVideoBitRate = minVideoBitRate;
    }

    public int getiFrameInterval() {
        return iFrameInterval;
    }

    public void setiFrameInterval(int iFrameInterval) {
        this.iFrameInterval = iFrameInterval;
    }

    public StarModel getStarModel() {
        StarModel model = new StarModel();
        model.setUid(uid);
        model.setPhoto(photo);
        model.setRole(role);
        model.setHotValue(hotValue);
        model.setNickName(nickName);
        return model;
    }
}
