package com.hunantv.mglive.data.config;

import com.alibaba.fastjson.JSON;
import com.hunantv.mglive.utils.L;
import com.hunantv.mglive.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2015/12/9.
 */
public class ConfigModel {
    /**
     * 头像
     */
    public static final String BUCKET_USER = "user";
    /**
     * 动态
     */
    public static final String BUCKET_TIME_LINE = "timeline";

    /**
     * 配置信息版本号
     */
    private String version;
    /**
     * 注册协议链接
     */
    private String protocol_reg;

    /**
     * 支付协议链接
     */
    private String protocol_pay;
    /**
     * 充值记录h5链接
     */
    private String recharge_recode_url;
    /**
     * 首页标题
     */
    private String liveTabs;

    /**
     * 启动广告
     */
    private List<AdModel> ads;
    /**
     * 发现菜单
     */
    private List<GroupModel> findmenus;
    /**
     * 礼物购买量提示
     */
    private List<BuyModel> giftsbuy;
    /**
     * 录制最长时间
     */
    private String takeMaxDur;
    /**
     * 录制最短时间
     */
    private String takeMinDur;
    /**
     * 视频大小
     */
    private String viedeoSize;
    /**
     * 视频比例
     */
    private String takeRate;
    /**
     * 二级Tab的ID
     */
    private String tabTemplateId;
    /**
     * bucket
     */
    private List<BucketModel> buckets = new ArrayList<>();

    /**
     * 视频码率
     */
    private int videoBitRate;
    /**
     * FPS
     */
    private int videoFPS;
    /**
     * 音频码率
     */
    private int audioBitRate;
    /**
     * 音频采样率
     */
    private int audioSampleRate;
    /**
     * 最大码率
     */
    private int maxVideoBitRate;
    /**
     * 最小码率
     */
    private int minVideoBitRate;
    /**
     * 秀场直播上报周期
     */
    private int liveReportCycle;
    /**
     * 关键帧时间间隔
     */
    private int iFrameInterval;

    /**
     * 签名计算N值
     */
    private int sigFill0Width;

    /**
     * IP测速超时时间ms
     */
    private int testIpTime;
    /**
     * 解析直播域名超时时间ms
     */
    private int parseDomainTime;

    /**
     * 百度支付优惠活动提示文字
     */
    private String baiduPayTopTip;

    /**
     * 百度支付优惠活动图标
     */
    private String baiduPayBarTipimg;


    public void setTestIpTime(int testIpTime) {
        this.testIpTime = testIpTime;
    }

    public void setParseDomainTime(int parseDomainTime) {
        this.parseDomainTime = parseDomainTime;
    }


    public int getParseDomainTime() {
        return parseDomainTime;
    }

    public int getTestIpTime() {
        return testIpTime;
    }


    public String getBaiduPayBarTipimg() {
        return baiduPayBarTipimg;
    }

    public void setBaiduPayBarTipimg(String baiduPayBarTipimg) {
        this.baiduPayBarTipimg = baiduPayBarTipimg;
    }

    public String getBaiduPayTopTip() {
        return baiduPayTopTip;
    }

    public void setBaiduPayTopTip(String baiduPayTopTip) {
        this.baiduPayTopTip = baiduPayTopTip;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getProtocol_reg() {
        return protocol_reg;
    }

    public void setProtocol_reg(String protocol_reg) {
        this.protocol_reg = protocol_reg;
    }

    public String getProtocol_pay() {
        return protocol_pay;
    }

    public void setProtocol_pay(String protocol_pay) {
        this.protocol_pay = protocol_pay;
    }

    public String getRecharge_recode_url() {
        return recharge_recode_url;
    }

    public void setRecharge_recode_url(String recharge_recode_url) {
        this.recharge_recode_url = recharge_recode_url;
    }

    public String getLiveTabs() {
        return liveTabs;
    }

    public void setLiveTabs(String liveTabs) {
        this.liveTabs = liveTabs;
    }

    public List<AdModel> getAds() {
        return ads;
    }

    public void setAds(List<AdModel> ads) {
        this.ads = ads;
    }

    public List<GroupModel> getFindmenus() {
        return findmenus;
    }

    public void setFindmenus(List<GroupModel> findmenus) {
        this.findmenus = findmenus;
    }

    public List<BuyModel> getGiftsbuy() {
        return giftsbuy;
    }

    public void setGiftsbuy(List<BuyModel> giftsbuy) {
        this.giftsbuy = giftsbuy;
    }

    public List<BucketModel> getBuckets() {
        return buckets;
    }

    public void setBuckets(List<BucketModel> buckets) {
        this.buckets = buckets;
    }

    public String getTabTemplateId() {
        return tabTemplateId;
    }

    public void setTabTemplateId(String tabTemplateId) {
        this.tabTemplateId = tabTemplateId;
    }

    public String getTakeMaxDur() {
        return takeMaxDur;
    }

    public void setTakeMaxDur(String takeMaxDur) {
        this.takeMaxDur = takeMaxDur;
    }

    public String getTakeMinDur() {
        return takeMinDur;
    }

    public void setTakeMinDur(String takeMinDur) {
        this.takeMinDur = takeMinDur;
    }

    public String getViedeoSize() {
        return viedeoSize;
    }

    public void setViedeoSize(String viedeoSize) {
        this.viedeoSize = viedeoSize;
    }

    public String getTakeRate() {
        return takeRate;
    }

    public void setTakeRate(String takeRate) {
        this.takeRate = takeRate;
    }

    public int getVideoBitRate() {
        return videoBitRate;
    }

    public void setVideoBitRate(int videoBitRate) {
        this.videoBitRate = videoBitRate;
    }

    public int getVideoFPS() {
        return videoFPS;
    }

    public void setVideoFPS(int videoFPS) {
        this.videoFPS = videoFPS;
    }

    public int getAudioBitRate() {
        return audioBitRate;
    }

    public void setAudioBitRate(int audioBitRate) {
        this.audioBitRate = audioBitRate;
    }

    public int getAudioSampleRate() {
        return audioSampleRate;
    }

    public void setAudioSampleRate(int audioSampleRate) {
        this.audioSampleRate = audioSampleRate;
    }

    public int getMaxVideoBitRate() {
        return maxVideoBitRate;
    }

    public void setMaxVideoBitRate(int maxVideoBitRate) {
        this.maxVideoBitRate = maxVideoBitRate;
    }

    public int getMinVideoBitRate() {
        return minVideoBitRate;
    }

    public void setMinVideoBitRate(int minVideoBitRate) {
        this.minVideoBitRate = minVideoBitRate;
    }

    public int getLiveReportCycle() {
        return liveReportCycle;
    }

    public void setLiveReportCycle(int liveReportCycle) {
        this.liveReportCycle = liveReportCycle;
    }

    public int getiFrameInterval() {
        return iFrameInterval;
    }

    public void setiFrameInterval(int iFrameInterval) {
        this.iFrameInterval = iFrameInterval;
    }

    public int getSigFill0Width() {
        return sigFill0Width;
    }

    public void setSigFill0Width(int sigFill0Width) {
        this.sigFill0Width = sigFill0Width;
    }

    public String getBuckets(String moduleName) {
        if (buckets != null) {
            for (int i = 0; i < buckets.size(); i++) {
                BucketModel model = buckets.get(i);
                if (model.getModuleName().equals(moduleName)) {
                    return model.getBucketName();
                }
            }
        }
        return null;
    }
}
