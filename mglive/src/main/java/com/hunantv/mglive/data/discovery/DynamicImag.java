package com.hunantv.mglive.data.discovery;

import java.io.Serializable;

/**
 * Created by guojun3 on 2016-1-15.
 */
public class DynamicImag implements Serializable {

    private String small;
    private String big;

    public String getBig() {
        return big;
    }

    public void setBig(String big) {
        this.big = big;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

}
