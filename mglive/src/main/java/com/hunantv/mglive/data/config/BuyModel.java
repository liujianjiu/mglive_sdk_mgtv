package com.hunantv.mglive.data.config;

/**
 * Created by admin on 2015/12/10.
 * 礼物购买量提示
 */
public class BuyModel {
    /**
     * 购买名称
     */
    private String name;
    /**
     * 购买数量
     */
    private long count;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
