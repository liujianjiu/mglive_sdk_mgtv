package com.hunantv.mglive.data;

/**
 * Created by admin on 2016/2/22.
 */
public class CaptchaModel {
    /**
     * 验证码ID，第一次可为空，后续传入返回结果中的captchaId
     */
    private String captchaId;
    /**
     * 图片的base64编码
     */
    private String image;
    /**
     * 验证码ID是否在有效期
     */
    private String validated;

    public String getCaptchaId() {
        return captchaId;
    }

    public void setCaptchaId(String captchaId) {
        this.captchaId = captchaId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getValidated() {
        return validated;
    }

    public void setValidated(String validated) {
        this.validated = validated;
    }
}
