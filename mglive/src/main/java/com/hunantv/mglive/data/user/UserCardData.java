package com.hunantv.mglive.data.user;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 2015/11/19.
 */
public class UserCardData implements Parcelable {

    public UserCardData(String imageBg, String logoImage, String title, String condition, int price, String company, String overdue, int status, int useStatus) {
        this.imageBg = imageBg;
        this.companyLogo = logoImage;
        this.name = title;
        this.require = condition;
        this.price = price;
        this.company = company;
        this.validate = overdue;
        this.status = status;
        this.useStatus = useStatus;
    }

    public UserCardData(){

    }
    /**
     * 产品id
     */
    private String pid;

    /**
     * 产品编码
     */
    private String code;

    /**
     * 产品名称
     */
    private String name;

    /**
     * 使用条件
     */
    private String require;

    /**
     * 使用说明
     */
    private String useDes;

    /**
     * 面值
     */
    private double price;

    /**
     * 赞助企业
     */
    private String company;

    /**
     * 企业logo
     */
    private String companyLogo;

    /**
     * 获得日期
     */
    private String date;

    /**
     * 有效期
     */
    private String validate;

    /**
     * 0未过期，1已过期，2无效
     */
    private int status;

    /**
     * 0未使用，1已使用
     */
    private int useStatus;

    /**
     * 未过期
     */
    public static final int STATUS_OVERDUE_NO = 0;

    /**
     * 已过期
     */
    public static final int STATUS_OVERDUE_YES = 1;

    /**
     * 无效
     */
    public static final int STATUS_INVALID = 2;


    /**
     * 未使用
     */
    public static final int USE_STATUS_NO = 0;
    /**
     * 已使用
     */
    public static final int USE_STATUS_YES = 1;

    /**
     * 跳转链接
     */
    private String link;

    private String imageBg;

    public String getImageBg() {
        return imageBg == null ? "#ff6700" : imageBg;
    }

    public void setImageBg(String imageBg) {
        this.imageBg = imageBg;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRequire() {
        return require;
    }

    public void setRequire(String require) {
        this.require = require;
    }

    public String getUseDes() {
        return useDes;
    }

    public void setUseDes(String useDes) {
        this.useDes = useDes;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getValidate() {
        return validate;
    }

    public void setValidate(String validate) {
        this.validate = validate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUseStatus() {
        return useStatus;
    }

    public void setUseStatus(int useStatus) {
        this.useStatus = useStatus;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    private UserCardData(Parcel in){
        pid = in.readString();
        code = in.readString();
        name = in.readString();
        require = in.readString();
        useDes = in.readString();
        price = in.readDouble();
        company = in.readString();
        companyLogo = in.readString();
        date = in.readString();
        validate = in.readString();
        status = in.readInt();
        useStatus = in.readInt();
        link = in.readString();
        imageBg= in.readString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pid);
        dest.writeString(code);
        dest.writeString(name);
        dest.writeString(require);
        dest.writeString(useDes);
        dest.writeDouble(price);
        dest.writeString(company);
        dest.writeString(companyLogo);
        dest.writeString(date);
        dest.writeString(validate);
        dest.writeInt(status);
        dest.writeInt(useStatus);
        dest.writeString(link);
        dest.writeString(imageBg);
    }

    public static final Parcelable.Creator<UserCardData> CREATOR = new Parcelable.Creator<UserCardData>() {

        @Override
        public UserCardData createFromParcel(Parcel source) {
            return new UserCardData(source);
        }

        @Override
        public UserCardData[] newArray(int size) {
            return new UserCardData[size];
        }
    };
}
