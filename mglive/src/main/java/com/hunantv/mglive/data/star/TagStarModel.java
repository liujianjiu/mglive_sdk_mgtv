package com.hunantv.mglive.data.star;

import java.util.ArrayList;

/**
 * Created by admin on 2016/1/25.
 */
public class TagStarModel {
    private String uid;

    private int role;

    private String nickName;

    private String photo;

    private String hotValue;

    private ArrayList<StarDynamicModel> dynamicInfo;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getHotValue() {
        return hotValue;
    }

    public void setHotValue(String hotValue) {
        this.hotValue = hotValue;
    }

    public ArrayList<StarDynamicModel> getDynamicInfo() {
        return dynamicInfo;
    }

    public void setDynamicInfo(ArrayList<StarDynamicModel> dynamicInfo) {
        this.dynamicInfo = dynamicInfo;
    }
}
