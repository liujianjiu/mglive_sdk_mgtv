package com.hunantv.mglive.data.act;

import java.util.ArrayList;

/**
 * Created by admin on 2016/2/1.
 */
public class ActDetailModel {
    /**
     * 活动的id
     */
    private String actId;
    /**
     * 活动的类型
     */
    private String actType;
    /**
     * 活动的标题
     */
    private String title;
    /**
     * 活动的图片
     */
    private String image;
    /**
     * 活动的描述
     */
    private String desc;
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 活动当前的阶段
     */
    private String stageId;
    /**
     * 活动当前的阶段标题
     */
    private String stageTitle;
    /**
     * 活动当前阶段图片
     */
    private String stageImage;
    /**
     * 活动当前的阶段主题
     */
    private String stageTopic;
    /**
     * 活动当前阶段的说明
     */
    private String stageDesc;
    /**
     * 活动当前的阶段状态
     */
    private String stageStatus;
    /**
     * 活动当前的阶段的参与总人数
     */
    private String stagePartiTotal;
    /**
     * 活动当前的阶段行为
     */
    private String stageAction;
    /**
     * 活动当前的阶段的参与总人数
     */
    private ArrayList<StarActModel> artArray;
    /**
     * 活动的各个阶段
     */
    private ArrayList<ActStageModel> stageArray;
    /**
     * tips说明，包含：投票时候的提示，投票答对的提示，投票答错
     */
    private TipModel tips;

    public String getActId() {
        return actId;
    }

    public void setActId(String actId) {
        this.actId = actId;
    }

    public String getActType() {
        return actType;
    }

    public void setActType(String actType) {
        this.actType = actType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getStageTitle() {
        return stageTitle;
    }

    public void setStageTitle(String stageTitle) {
        this.stageTitle = stageTitle;
    }

    public String getStageTopic() {
        return stageTopic;
    }

    public void setStageTopic(String stageTopic) {
        this.stageTopic = stageTopic;
    }

    public String getStageDesc() {
        return stageDesc;
    }

    public void setStageDesc(String stageDesc) {
        this.stageDesc = stageDesc;
    }

    public String getStageStatus() {
        return stageStatus;
    }

    public void setStageStatus(String stageStatus) {
        this.stageStatus = stageStatus;
    }

    public String getStagePartiTotal() {
        return stagePartiTotal;
    }

    public void setStagePartiTotal(String stagePartiTotal) {
        this.stagePartiTotal = stagePartiTotal;
    }

    public ArrayList<StarActModel> getArtArray() {
        return artArray;
    }

    public void setArtArray(ArrayList<StarActModel> artArray) {
        this.artArray = artArray;
    }

    public ArrayList<ActStageModel> getStageArray() {
        return stageArray;
    }

    public void setStageArray(ArrayList<ActStageModel> stageArray) {
        this.stageArray = stageArray;
    }

    public TipModel getTips() {
        return tips;
    }

    public void setTips(TipModel tips) {
        this.tips = tips;
    }

    public String getStageAction() {
        return stageAction;
    }

    public void setStageAction(String stageAction) {
        this.stageAction = stageAction;
    }

    public String getStageImage() {
        return stageImage;
    }

    public void setStageImage(String stageImage) {
        this.stageImage = stageImage;
    }
}
