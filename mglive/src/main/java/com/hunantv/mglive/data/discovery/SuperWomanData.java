package com.hunantv.mglive.data.discovery;

import java.util.ArrayList;

/**
 * Created by admin on 2015/12/28.
 */
public class SuperWomanData {
    private String uid;
    private String name;
    private String photo;
    private int hots;
    private String fans;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getHots() {
        return hots;
    }

    public void setHots(int hots) {
        this.hots = hots;
    }

    public String getFans() {
        return fans;
    }

    public void setFans(String fans) {
        this.fans = fans;
    }
}
