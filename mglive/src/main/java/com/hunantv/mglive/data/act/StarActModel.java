package com.hunantv.mglive.data.act;

/**
 * Created by admin on 2016/2/1.
 */
public class StarActModel {
    public static final String LINK_TYPE = "vdy";

    /**
     * 排行榜序号
     */
    private String rank;
    /**
     * uid
     */
    private String uid;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 头像
     */
    private String photo;
    /**
     * 参与形式类型，当前需求只有视频动态，取值：vdy
     */
    private String linkType;
    /**
     * 关联的参与ID，当前需求，是视频动态的ID
     */
    private String linkId;
    /**
     * 得票数
     */
    private String vote;
    /**
     * 活动结束的时候，艺人得到赠送的人气值
     */
    private String hotValue;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public String getHotValue() {
        return hotValue;
    }

    public void setHotValue(String hotValue) {
        this.hotValue = hotValue;
    }
}
