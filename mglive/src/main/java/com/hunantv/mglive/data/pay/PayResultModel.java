package com.hunantv.mglive.data.pay;

/**
 * Created by admin on 2015/12/12.
 */
public class PayResultModel {
    /**
     * 订单不存在
     */
    public static final String STATUS_NO = "None";

    /**
     * 正在支付
     */
    public static final String STATUS_PAYING = "Ready";

    /**
     * 支付成功
     */
    public static final String STATUS_PAY_SUCCESS = "Finish";

    /**
     * 支付失败
     */
    public static final String STATUS_PAY_FAILE = "Bad";

    private  String oid;
    private String status;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
