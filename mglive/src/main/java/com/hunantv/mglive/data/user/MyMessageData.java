package com.hunantv.mglive.data.user;

/**
 * Created by max on 2015/12/15.
 * 我的消息结构
 */
public class MyMessageData {
    private String mId;  //消息id
    private int type; //消息类型，0系统，1订单，2其他
    private String content; //消息内容
    private String date;  //消息日期

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
