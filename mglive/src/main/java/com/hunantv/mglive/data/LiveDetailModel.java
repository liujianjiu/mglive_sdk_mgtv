package com.hunantv.mglive.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 达 on 2015/12/4.
 */
public class LiveDetailModel implements Serializable {
    public static final String TYPE_LIVE = "0";
    public static final String TYPE_BACK = "1";

    private String lId;
    private String type;
    private String title;
    private String image;
    private String video;
    private long count;
    private String chatFlag;
    private String chatKey;
    private String videoPath;
    private String showCount = "1";
    private ArrayList<CameraModel> cameras = new ArrayList<>();
    private ArrayList<StarModel> users = new ArrayList<>();
    private ArrayList<LiveDetailModel> relationLives = new ArrayList<>();

    public String getlId() {
        return lId;
    }

    public void setlId(String lId) {
        this.lId = lId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public List<CameraModel> getCameras() {
        return cameras;
    }

    public void setCameras(ArrayList<CameraModel> cameras) {
        this.cameras = cameras;
    }

    public List<StarModel> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<StarModel> users) {
        this.users = users;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getChatFlag() {
        return chatFlag;
    }

    public void setChatFlag(String chatFlag) {
        this.chatFlag = chatFlag;
    }

    public String getChatKey() {
        return chatKey;
    }

    public void setChatKey(String chatKey) {
        this.chatKey = chatKey;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getShowCount() {
        return showCount;
    }

    public void setShowCount(String showCount) {
        this.showCount = showCount;
    }

    public ArrayList<LiveDetailModel> getRelationLives() {
        return relationLives;
    }

    public void setRelationLives(ArrayList<LiveDetailModel> relationLives) {
        this.relationLives = relationLives;
    }
}
