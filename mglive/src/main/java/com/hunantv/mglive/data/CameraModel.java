package com.hunantv.mglive.data;

import java.io.Serializable;

/**
 * Created by 达 on 2015/12/4.
 */
public class CameraModel implements Serializable{
    private String positionName;
    private String photo;
    private String channleId;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getChannleId() {
        return channleId;
    }

    public void setChannleId(String channleId) {
        this.channleId = channleId;
    }
}
