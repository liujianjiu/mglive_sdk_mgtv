package com.hunantv.mglive.data.discovery;

/**
 * Created by Administrator on 2015-12-12.
 */
public class RcmdStarsData {

    /**
     * uid	String	是		用户id
     * video	String	是		视频地址/key
     * nickName	String	是		昵称
     * photo	String	是		照片
     * cover	String	是		封面
     * hots	long	是		人气值
     * fans	Long	是		粉丝数
     */

    private String uid;
    private String video;
    private String nickName;
    private String photo;
    private String cover;
    private long hots;
    private Long fans;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public long getHots() {
        return hots;
    }

    public void setHots(long hots) {
        this.hots = hots;
    }

    public Long getFans() {
        return fans;
    }

    public void setFans(Long fans) {
        this.fans = fans;
    }
}
