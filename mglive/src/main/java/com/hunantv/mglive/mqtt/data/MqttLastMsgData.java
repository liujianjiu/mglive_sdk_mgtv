package com.hunantv.mglive.mqtt.data;

import java.util.List;

/**
 * Created by admin on 2015/12/4.
 */
public class MqttLastMsgData {
    private List<String> lastMsg;
    private String online;
    private List<String> imgList;

    public List<String> getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(List<String> lastMsg) {
        this.lastMsg = lastMsg;
    }


    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public List<String> getImgList() {
        return imgList;
    }

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }
}
