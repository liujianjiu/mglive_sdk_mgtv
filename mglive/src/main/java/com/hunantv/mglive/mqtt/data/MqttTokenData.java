package com.hunantv.mglive.mqtt.data;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

/**
 * Created by admin on 2015/11/24.
 */
public class MqttTokenData implements Parcelable{
    //{"data":{"account":"Guest","clientId":"d84af4151f8d4a6db3bbac","password":"Guest","ping":10,"topic":"直播-123456","tryCount":3},"err_code":200,"err_msg":"操作成功"}
    private String account;

    private String clientId;

    private String password;

    private int ping;

    private String topic;

    private int tryCount;

    private String syringe_addr;

    private int syringe_port;

    public MqttTokenData(){
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPing() {
        return ping;
    }

    public void setPing(int ping) {
        this.ping = ping;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getTryCount() {
        return tryCount;
    }

    public void setTryCount(int tryCount) {
        this.tryCount = tryCount;
    }


    public String getSyringe_addr() {
        return syringe_addr;
    }

    public void setSyringe_addr(String syringe_addr) {
        this.syringe_addr = syringe_addr;
    }

    public int getSyringe_port() {
        return syringe_port;
    }

    public void setSyringe_port(int syringe_port) {
        this.syringe_port = syringe_port;
    }

    public String replaceNullToEmpty(String text){
        if(text.equalsIgnoreCase("null")){
            return "";
        }else{
            return text;
        }
    }

    public void parserData(JSONObject object) {
        try {
            account = object.getString("account");
            clientId = object.getString("clientId");
            password = object.getString("password");
            ping = object.getInt("ping");
            topic = object.getString("topic");
            tryCount = object.getInt("tryCount");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(account);
        dest.writeString(clientId);
        dest.writeString(password);
        dest.writeInt(ping);
        dest.writeString(topic);
        dest.writeInt(tryCount);
    }

    private MqttTokenData(Parcel in){
        account = in.readString();
        clientId = in.readString();
        password = in.readString();
        ping = in.readInt();
        topic = in.readString();
        tryCount = in.readInt();
    }

    public static final Parcelable.Creator<MqttTokenData> CREATOR = new Parcelable.Creator<MqttTokenData>() {

        @Override
        public MqttTokenData createFromParcel(Parcel source) {
            return new MqttTokenData(source);
        }

        @Override
        public MqttTokenData[] newArray(int size) {
            return new MqttTokenData[size];
        }
    };
}
