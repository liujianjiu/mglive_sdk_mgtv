package com.hunantv.mglive.mqtt.data;

import com.hunantv.mglive.data.live.ChatData;

import java.util.List;

/**
 * Created by admin on 2015/12/4.
 */
public class MqttResponseData {
    //{"datas":["{\"msg\":\"基督教dj\",\"type\":0,\"nickName\":\"Me\"}"],"total":1,"online":2}
    private String datas;

    private int total;

    private int online;

    public String getDatas() {
        return datas;
    }

    public void setDatas(String datas) {
        this.datas = datas;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getOnline() {
        return online;
    }

    public void setOnline(int online) {
        this.online = online;
    }
}
