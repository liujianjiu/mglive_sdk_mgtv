package com.hunantv.mglive.utils;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;

import java.util.ArrayList;
import java.util.List;

import master.flame.danmaku.controller.IDanmakuView;
import master.flame.danmaku.danmaku.model.BaseDanmaku;
import master.flame.danmaku.danmaku.model.android.DanmakuContext;
import master.flame.danmaku.danmaku.parser.BaseDanmakuParser;

/**
 * Created by qiudaaini@163.com on 16/1/3.
 */
public class DanMuUtil {
    private static final int MAX_NUM = 8;
    private DanmakuContext mContext;
    private Boolean mDanMuStarted = false;
    private IDanmakuView mDanmakuView;
    private BaseDanmakuParser mParser;
    private List<String> mDanmuList = new ArrayList<>();
    private Handler mHandler = new Handler() {

        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            List<String> tempDanmuStr = new ArrayList<>();
            synchronized (mDanmuList) {
                if (mDanmakuView != null) {
                    int i = 0;
                    for (; mDanmuList.size() > 0 && i < MAX_NUM; i++) {
                        tempDanmuStr.add(mDanmuList.get(0));
                        mDanmuList.remove(0);
                    }
                }
                for (int i = 0; i < tempDanmuStr.size(); i++) {
                    String danmuStr = tempDanmuStr.get(i);
                    addDanmaku(false, danmuStr);
                }
                synchronized (mDanMuStarted) {
                    if (tempDanmuStr.size() >= MAX_NUM) {
                        if (!mDanMuStarted) {
                            mDanMuStarted = true;
                        }
                        mHandler.sendEmptyMessageDelayed(0, 1000);
                    } else {
                        mDanMuStarted = false;
                    }
                }
            }


        }
    };

    public DanMuUtil(DanmakuContext mContext, IDanmakuView mDanmakuView, BaseDanmakuParser mParser) {
        this.mContext = mContext;
        this.mDanmakuView = mDanmakuView;
        this.mParser = mParser;
    }


    private void addDanmaku(boolean islive, String text) {
        BaseDanmaku danmaku = mContext.mDanmakuFactory.createDanmaku(BaseDanmaku.TYPE_SCROLL_RL);
        if (danmaku == null || mDanmakuView == null) {
            return;
        }
        // for(int i=0;i<100;i++){
        // }
        danmaku.text = text;
        danmaku.padding = 5;
        danmaku.priority = 0;  // 可能会被各种过滤器过滤并隐藏显示
        danmaku.isLive = islive;
        danmaku.time = mDanmakuView.getCurrentTime() + 1200;
        danmaku.textSize = 23f * (mParser.getDisplayer().getDensity() - 0.6f);
        danmaku.textColor = Color.WHITE;
        // danmaku.underlineColor = Color.GREEN;
        mDanmakuView.addDanmaku(danmaku);

    }

    public void addDanmu(String danmuStr) {
        if (StringUtil.isNullorEmpty(danmuStr)) {
            return;
        }
        synchronized (mDanmuList) {
            if (mDanmuList.size() < 500) {
                mDanmuList.add(danmuStr);
            } else {
                return;
            }
        }
        synchronized (mDanMuStarted) {
            if (!mDanMuStarted) {
                mDanMuStarted = true;
                mHandler.sendEmptyMessage(0);
            }
        }
    }

    public void clearDanmu() {
        synchronized (mDanmuList) {
            mDanmuList.clear();
        }
    }
}
