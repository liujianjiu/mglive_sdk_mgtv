package com.hunantv.mglive.utils;

import android.util.Log;

/**
 * Created by QiuDa on 15/11/28.
 */
public class L {


    public static void d(String tag, String str) {
        if (!StringUtil.isNullorEmpty(str)) {
            Log.d(tag, str);
        }
    }

    public static void d(String tag, String msg, Throwable e) {
        Log.d(tag, msg, e);
    }

    public static void e(String tag, String error) {
        Log.e(tag, error);
    }

    public static void e(String tag, Throwable e) {
        if (e != null && !StringUtil.isNullorEmpty(e.getMessage())) {
            Log.e(tag, e.getMessage(), e);
        } else {
            Log.e(tag, "", e);
        }
    }

    public static void e(String tag, String msg, Throwable e) {
        Log.e(tag, msg, e);
    }

}
