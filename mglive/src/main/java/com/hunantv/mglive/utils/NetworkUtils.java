package com.hunantv.mglive.utils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import com.hunantv.mglive.common.MaxApplication;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class NetworkUtils extends BroadcastReceiver {

    private static final String TAG = "NetworkUtils";


    public static enum NetType {
        None,
        Wifi,
        G2,
        G3,
        G4,
        Cable,
    }

    public static class NetworkSwitchListener
            implements NetworkStateListener {

        @Override
        public void onNone2Mobile() {
            onConnectNetwork();
        }

        @Override
        public void onNone2Wifi() {
            onConnectNetwork();
        }

        @Override
        public void onMobile2Wifi() {
            onNetworkSwitch();
        }

        @Override
        public void onMobile2None() {
            onDisconnectNetwork();
        }

        @Override
        public void onWifi2None() {
            onDisconnectNetwork();
        }

        @Override
        public void onWifi2Mobile() {
            onNetworkSwitch();
        }


        public void onDisconnectNetwork() {
        }

        public void onConnectNetwork() {
        }

        public void onNetworkSwitch() {
        }
    }


    public static interface NetworkStateListener {
        void onNone2Mobile();

        void onNone2Wifi();

        void onMobile2Wifi();

        void onMobile2None();

        void onWifi2None();

        void onWifi2Mobile();
    }

    public static class SimpleNetworkStateListener
            implements NetworkStateListener {
        @Override
        public void onNone2Mobile() {
        }

        @Override
        public void onNone2Wifi() {
        }

        @Override
        public void onMobile2Wifi() {
        }

        @Override
        public void onMobile2None() {
        }

        @Override
        public void onWifi2None() {
        }

        @Override
        public void onWifi2Mobile() {
        }
    }


    private static enum NetworkChangeType {
        Invalid,
        None2Mobile,
        None2Wifi,
        Mobile2Wifi,
        Mobile2None,
        Wifi2None,
        Wifi2Mobile,
    }


    private static NetworkUtils.NetType mCurrNetType = NetworkUtils.NetType.None;
    private static List<WeakReference<NetworkStateListener>> mNetworkStateListener =
            new LinkedList<WeakReference<NetworkStateListener>>();


    public static NetType getNetworkType() {
        mCurrNetType = getNetworkTypeInternal(MaxApplication.getAppContext());
        return mCurrNetType;
    }

    public static boolean hasNetwork() {
        return mCurrNetType != NetType.None;
    }

    /**
     * 检查当前网络是否可用
     *
     * @param activity
     * @return
     */

    public static boolean isNetworkAvailable(Context activity)
    {
        Context context = activity.getApplicationContext();
        // 获取手机所有连接管理对象（包括对wi-fi,net等连接的管理）
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager == null)
        {
            return false;
        }
        else
        {
            // 获取NetworkInfo对象
            NetworkInfo[] networkInfo = connectivityManager.getAllNetworkInfo();

            if (networkInfo != null && networkInfo.length > 0)
            {
                for (int i = 0; i < networkInfo.length; i++)
                {
                    // 判断当前网络状态是否为连接状态
                    if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isWifi() {
        return isWifi(getNetworkType());
    }

    public static boolean isMobile() {
        return isMobile(getNetworkType());
    }

    public static boolean isWifi(NetType netType) {
        return netType == NetType.Wifi;
    }

    public static boolean isMobile(NetType netType) {
        return netType == NetType.G2 || netType == NetType.G3 || netType == NetType.G4;
    }

    public static void addNetworkStateListener(@HoldWeakRef NetworkStateListener listener) {
        mNetworkStateListener.add(new WeakReference<NetworkStateListener>(listener));
    }

    public static void removeNetworkStateListener(NetworkStateListener l) {
        for (Iterator<WeakReference<NetworkStateListener>> iterator =
             mNetworkStateListener.iterator(); iterator.hasNext(); ) {

            WeakReference<NetworkStateListener> ref = iterator.next();
            NetworkStateListener listener = ref.get();
            if (listener == null || listener == l) {
                iterator.remove();
            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        NetworkUtils.NetType oldNetType = mCurrNetType;
        mCurrNetType = getNetworkType();

        if (oldNetType == mCurrNetType) {
            return;
        }

        NetworkChangeType type = NetworkChangeType.Invalid;
        if (oldNetType == NetType.None) {
            if (isMobile(mCurrNetType)) {
                type = NetworkChangeType.None2Mobile;
            } else if (isWifi(mCurrNetType)) {
                type = NetworkChangeType.None2Wifi;
            }
        } else {
            if (isMobile(oldNetType)) {
                if (mCurrNetType == NetType.None) {
                    type = NetworkChangeType.Mobile2None;
                } else if (isWifi(mCurrNetType)) {
                    type = NetworkChangeType.Mobile2Wifi;
                }
            } else if (isWifi(oldNetType)) {
                if (mCurrNetType == NetType.None) {
                    type = NetworkChangeType.Wifi2None;
                } else if (isMobile(mCurrNetType)) {
                    type = NetworkChangeType.Wifi2Mobile;
                }
            }
        }

        notifyStateChange(type);
    }

    private void notifyStateChange(NetworkChangeType type) {
        if (type == NetworkChangeType.Invalid) {
            return;
        }

        if (type == NetworkChangeType.None2Mobile) {
            L.d(TAG, "None -> 2/3/4/G");
        } else if (type == NetworkChangeType.None2Wifi) {
            L.d(TAG, "None -> Wifi");
        } else if (type == NetworkChangeType.Mobile2Wifi) {
            L.d(TAG, "2/3/4/G -> Wifi");
        } else if (type == NetworkChangeType.Mobile2None) {
            L.d(TAG, "2/3/4/G -> None");
        } else if (type == NetworkChangeType.Wifi2None) {
            L.d(TAG, "Wifi -> None");
        } else if (type == NetworkChangeType.Wifi2Mobile) {
            L.d(TAG, "Wifi -> 2/3/4/G");
        }

        for (WeakReference<NetworkStateListener> ref : mNetworkStateListener) {
            NetworkStateListener listener = ref.get();
            if (listener == null) {
                continue;
            }

            if (type == NetworkChangeType.None2Mobile) {
                listener.onNone2Mobile();
            } else if (type == NetworkChangeType.None2Wifi) {
                listener.onNone2Wifi();
            } else if (type == NetworkChangeType.Mobile2Wifi) {
                listener.onMobile2Wifi();
            } else if (type == NetworkChangeType.Mobile2None) {
                listener.onMobile2None();
            } else if (type == NetworkChangeType.Wifi2None) {
                listener.onWifi2None();
            } else if (type == NetworkChangeType.Wifi2Mobile) {
                listener.onWifi2Mobile();
            }
        }
    }

    public static NetType getNetworkTypeInternal(Context context) {
        ConnectivityManager mgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = mgr.getActiveNetworkInfo();

        if (networkInfo == null || !networkInfo.isConnected()) {
            return NetType.None;
        }

        switch (networkInfo.getType()) {
            case ConnectivityManager.TYPE_WIFI:
                return NetType.Wifi;

            case ConnectivityManager.TYPE_ETHERNET:
                return NetType.Cable;

            case ConnectivityManager.TYPE_MOBILE:
                switch (networkInfo.getSubtype()) {
                    case TelephonyManager.NETWORK_TYPE_GPRS:
                    case TelephonyManager.NETWORK_TYPE_EDGE:
                    case TelephonyManager.NETWORK_TYPE_CDMA:
                    case TelephonyManager.NETWORK_TYPE_IDEN:
                    case TelephonyManager.NETWORK_TYPE_1xRTT:
                        return NetType.G2;

                    case TelephonyManager.NETWORK_TYPE_UMTS:
                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    case TelephonyManager.NETWORK_TYPE_EVDO_B:
                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                    case TelephonyManager.NETWORK_TYPE_HSPA:
                    case TelephonyManager.NETWORK_TYPE_HSPAP:
                    case TelephonyManager.NETWORK_TYPE_EHRPD:
                        return NetType.G3;

                    case TelephonyManager.NETWORK_TYPE_LTE:
                        return NetType.G4;

                    default:
                        break;
                }
                break;

            default:
                break;
        }

        return NetType.None;
    }
}
