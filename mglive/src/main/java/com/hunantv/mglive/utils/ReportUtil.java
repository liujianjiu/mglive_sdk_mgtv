package com.hunantv.mglive.utils;

import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.hunantv.mglive.common.BuildConfig;
import com.hunantv.mglive.common.FormVVReportBuilder;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.ResultModel;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by qiuda on 16/2/20.
 */
public class ReportUtil implements HttpUtils.callBack {
    private static ReportUtil mReport;
    private HttpUtils mHttp;

    private ReportUtil() {
        mHttp = new HttpUtils(MaxApplication.getAppContext(), this);
    }

    public static ReportUtil instance() {
        if (mReport == null) {
            mReport = new ReportUtil();
        }
        return mReport;
    }

    private int what;
    private int extr;
    public void setErrMsg(int what,int extr){
        this.what = what;
        this.extr = extr;
    }

    public void report(String url, String requestUrl, String videoType, String requestType, String errorcode, boolean isSucceed) {
        try {
            String result;
            String version = "imgolive-aphone-" + DeviceInfoUtil.getVersionName(MaxApplication.getAppContext()) + "." + DeviceInfoUtil.getVersionCode(MaxApplication.getAppContext());
            String device = DeviceInfoUtil.getDeviceId(MaxApplication.getAppContext());

            String e = null;
            if (!StringUtil.isNullorEmpty(errorcode)) {
                e = errorcode;
            }

            if (isSucceed) {
                result = "0";
            } else {
                result = "-1";
            }

            Uri mUri = Uri.parse(url);
            String requestHost = mUri.getHost();
            int requestPort = mUri.getPort();
            if (requestPort != -1) {
                requestHost += ":" + requestPort;
            }
            String l;
            if (!StringUtil.isNullorEmpty(requestUrl)) {
                l = requestUrl.replace(requestHost, "");
                l = l.replace("http://", "");
                l = l.substring(l.indexOf("/"));
                try {
                    l = URLEncoder.encode(l, "utf-8");
                } catch (UnsupportedEncodingException ignored) {

                }
            } else {
                l = "/";
            }

            String macStr = DeviceInfoUtil.getMacAddress(MaxApplication.getAppContext());
            if(!StringUtil.isNullorEmpty(macStr)){
                macStr = macStr.replaceAll(":","");
            }
            Map<String, String> reportParam = new HashMap<>();
            reportParam.put("p", "3");
            reportParam.put("v", version);
            reportParam.put("u", device);
            reportParam.put("f", result);
            reportParam.put("z", "1");
            reportParam.put("s", requestType);
            reportParam.put("h", requestHost);
            reportParam.put("l", l);
            reportParam.put("a", "0");
            reportParam.put("b", "1");
            reportParam.put("t", videoType);
            reportParam.put("c", "2");
            reportParam.put("cv", "20160531");

            reportParam.put("sv", "aphone-"+android.os.Build.VERSION.RELEASE);//系统版本号
            reportParam.put("mf", android.os.Build.MANUFACTURER);//厂家
            reportParam.put("mod", android.os.Build.MODEL);//硬件型号
            reportParam.put("m", macStr);//MAC地址
            reportParam.put("pt", "5");//播放方式标示
            reportParam.put("i", "");//ip地址
            reportParam.put("si", "");//资源id
            reportParam.put("n", "");//资源长度
            String ex = "uuid="+MaxApplication.getInstance().getUid()+"&ft="+DateUtil.long2Date(System.currentTimeMillis());
            if(requestType != null && requestType.equals("3") && !isSucceed){
                ex = ex + "&what="+what+"&extra="+extr;
            }
            try {
                ex = URLEncoder.encode(ex,"UTF-8");
            }catch (Exception exce){
                exce.printStackTrace();
            }
            reportParam.put("ex", ex);//附加码

            if (!StringUtil.isNullorEmpty(e)) {
                reportParam.put("e", e);
            }else{
                reportParam.put("e", "");
            }

            Log.i("UrlMsg",reportParam.toString());

//            get(BuildConfig.http_report, reportParam);
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    public void reportVodVV(Map<String, String> params) {
        FormVVReportBuilder builder = new FormVVReportBuilder()
                .add("act", "play")
                .add("idx", "0")
                .add("pt", "0")
                .add("pay", "0")
                .add("def", "1")
                .add("ct", "")
                .add("td", "")
                .add("et", "")
                .add("def", "1")
                .add("istry", "0")
                .add("fpn", "")
                .add("fpid", "")
                .add("cpn", "")
                .add("cpid", "")
                .add(params);
//        get(BuildConfig.http_vv_vod_report, builder.build());
    }


    public void reportArtVV(Map<String, String> params) {
        FormVVReportBuilder builder = new FormVVReportBuilder()
                .add("act", "play")
                .add("idx", "0")
                .add("ap", "1")
                .add("pt", "1")
                .add("pay", "0")
                .add("def", "1")
                .add("istry", "0")
                .add("fpn", "")
                .add("fpid", "")
                .add("cpn", "")
                .add("cpid", "")
                .add("vts", "")
                .add("ct", "")
                .add("td", "")
                .add("et", "")
                .add(params);
//        get(BuildConfig.http_vv_art_report, builder.build());
    }

    public void reportActVV(Map<String, String> params) {
        FormVVReportBuilder builder = new FormVVReportBuilder()
                .add("act", "play")
                .add("idx", "0")
                .add("pt", "4")
                .add("pay", "0")
                .add("def", "1")
                .add("istry", "0")
                .add("fpn", "")
                .add("fpid", "")
                .add("cpn", "")
                .add("cpid", "")
                .add("vts", "")
                .add("ct", "")
                .add("td", "")
                .add("et", "")
                .add(params);
//        get(BuildConfig.http_vv_act_report, builder.build());
    }

    @Override
    public boolean get(String url, Map<String, String> param) {
        return mHttp.get(url, param);
    }

    @Override
    public boolean post(String url, Map<String, String> param) {
        return mHttp.post(url, param);
    }

    @Override
    public void onError(String url, Exception e) {

    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        return null;
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {

    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {

    }
}
