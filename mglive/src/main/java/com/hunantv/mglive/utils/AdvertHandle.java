package com.hunantv.mglive.utils;

import com.hunantv.mglive.data.advert.AdvertModel;

/**
 * Created by admin on 2016/2/19.
 */
public class AdvertHandle {
    private static AdvertHandle handle;
    private static AdvertModel advert;

    public static AdvertHandle getInstance(){
        if(handle == null)
        {
            handle = new AdvertHandle();
        }
        return handle;
    }

    private AdvertHandle(){

    }

    public static AdvertHandle getHandle() {
        return handle;
    }

    public static void setHandle(AdvertHandle handle) {
        AdvertHandle.handle = handle;
    }

    public static AdvertModel getAdvert() {
        return advert;
    }

    public static void setAdvert(AdvertModel advert) {
        AdvertHandle.advert = advert;
    }
}
