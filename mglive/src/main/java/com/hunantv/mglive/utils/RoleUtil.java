package com.hunantv.mglive.utils;

import com.hunantv.mglive.R;
import com.hunantv.mglive.data.login.UserInfoData;

/**
 * Created by admin on 2015/12/17.
 */
public class RoleUtil {
    public static final int ROLE_PT = 0;//普通用户
    public static final int ROLE_YR = 1;//艺人
    public static final int ROLE_MX = 3;//明星
    public static final int ROLE_SYSTEM = 4;//系统用户


    public static int getRoleIcon(int role)
    {
        int iconId = 0;
        switch (role){
            case ROLE_PT:
                iconId = android.R.color.transparent;
                break;
            case ROLE_YR:
                iconId  = R.drawable.icon_yr;
                break;
            case ROLE_MX:
                iconId  = R.drawable.icon_mx;
                break;
            case ROLE_SYSTEM:
                iconId = R.drawable.icon_sys;
                break;
            default:
                iconId = android.R.color.transparent;
                break;
        }
        return iconId;
    }
}
