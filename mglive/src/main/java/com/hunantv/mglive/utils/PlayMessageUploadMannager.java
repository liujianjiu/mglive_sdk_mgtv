package com.hunantv.mglive.utils;

import android.os.RemoteException;
import android.util.Log;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.ResultModel;
import org.json.JSONException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author liujianjiu
 *         <p/>
 *         播放视频信息上报管理类
 */
public class PlayMessageUploadMannager implements HttpUtils.callBack {

    public static int UP_PLAY_VEDIO_MSG_PER = 5 *60* 1000;
    public static String PLAY_ERR_CODE_SELF = "306";
    public static String URL_ERR_CODE_SELF = "106";
    public static String URL_ERR_CODE_HTTP = "101";
    public static String URL_ERR_CODE_PASE = "102";

    public static String UP_PLAY_VEDIO_MSG = "http://v1.play.log.hunantv.com/info.php";
//    public static String UP_REQUEST_VEDIO_MSG = "http://v2.log.hunantv.com/info.php";

    private static PlayMessageUploadMannager instance;

    private Timer mTimer;
    private TimerTask mTimerTask;
    private HttpUtils mHttp;

    private String p = "3";//平台参数
    private String v;
    private String guid;//用户标识(guid)
    private int f;//正常标记
    private String r;//上报类型
    private String l;//访问文件url(带参数)
    private String h;//访问接口（hostname）
    private String t;//播放类型标记
    private String sv;//系统版本号
    private String mf;//厂家
    private String mod;//硬件型号
    private String m;//MAC地址
    private String pt;//播放方式标示
    private String c;//客户端类型
    private String o;//视频卡/出错位置

    private String b;//清晰度标志
    private String e;//错误码
    private String cv = "20160531";
    private String userId;//用户Id
    private String ft;//最后一次卡顿时间撮

    private int what;//第三层错误what
    private int extra;//第三层错误extra

    private PlayMessageUploadMannager() {
    }

    public static PlayMessageUploadMannager getInstance() {
        if (instance == null) {
            instance = new PlayMessageUploadMannager();
        }
        return instance;
    }

    /**
     * 开启上报卡顿信息
     *
     */
    public void startUploadMessage(String host,String path,String tAtr) {

        UP_PLAY_VEDIO_MSG_PER = 5*60*1000;

        stopUploadMessage();
        initMsg(host,path,tAtr,"");
        f = 0;

        if (mTimer == null) {
            mTimer = new Timer();
        }
        if (mTimerTask == null) {
            mTimerTask = new TimerTask() {
                @Override
                public void run() {
                    r = "1";//循环累计上报
                    e = "";
                    upVedioMsg();
                    f = 0;
                }
            };
        }

        mTimer.schedule(mTimerTask, UP_PLAY_VEDIO_MSG_PER, UP_PLAY_VEDIO_MSG_PER);

        Log.i("playMsg","---->start");
    }

    /**
     * 取消上报流质量
     */
    public void stopUploadMessage() {
        if(mTimer != null){
            mTimer.cancel();
        }
        if(mTimerTask != null){
            mTimerTask.cancel();
        }
        mTimer = null;
        mTimerTask = null;
        Log.i("playMsg","---->stop");
    }

    /**
     *
     * 增加卡顿次数
     */
    public void addPuseCount(){
        f++;
        ft = DateUtil.long2Date(System.currentTimeMillis());
        if(f == 1){
            r = "0";//首次卡顿实时上报
            e = "";
            upVedioMsg();
        }
    }

    /**
     * 完成上报
     */
    public void doneUpMessage(){
        r = "3";//完成实时上报
        e = "";
        upVedioMsg();
        errCount = 0;
    }

    /**
     * 出错上报
     */
    long errCount = 0;
    public void errUpMessage(int what,int extra){
        this.what = what;
        this.extra = extra;
//        errCode = Math.abs(errCode);//暂时不用这个，不清楚ijk传入的code的意思
        int errCode = 100;
        errCount++;
        if(errCount>10){
            r = "2";//出错实时上报
            e = PLAY_ERR_CODE_SELF+errCode;
            upVedioMsg();
            errCount = -9223372036854775807l;
        }
    }

    /**
     * 初始化报错数
     */
    public void doErrCount(){
        if(errCount>0 && errCount<=10){//重连10次内播放成功，报一次卡顿
            addPuseCount();
        }
        errCount = 0;
    }

    /**
     * 初始化上报信息
     */
    public void initMsg(String host,String path,String tAtr,String eAtr){
        v =  "imgolive-aphone-"+PackageUtil.getAppVersionName(MaxApplication.getAppContext())+"."+PackageUtil.getAppVersionCode(MaxApplication.getAppContext());
        guid = DeviceInfoUtil.getDeviceId(MaxApplication.getAppContext());

        f = 0;
        try{
            l = URLEncoder.encode(path, "UTF-8");//访问文件url(带参数)
        }catch (Exception e){
            e.printStackTrace();
        }
        h = host;//访问接口（hostname）
        t = tAtr;//播放类型标记
        sv = "aphone-"+android.os.Build.VERSION.RELEASE;//系统版本号
        mf = android.os.Build.MANUFACTURER;//厂家
        mod = android.os.Build.MODEL;//硬件型号
        m = DeviceInfoUtil.getMacAddress(MaxApplication.getAppContext());//MAC地址
        if(!StringUtil.isNullorEmpty(m)){
            m = m.replaceAll(":","");
        }
        pt = "5";//播放方式标示
        c = "2";//客户端类型

        b = "1";//清晰度标志
        e = eAtr;//错误码
        cv = "20160531";
        userId = MaxApplication.getInstance().getUid();
    }

    /**
     * 设置错误码
     * @param errCode
     */
    public void setErrCode(String errCode){
        e = errCode;
    }
//
//    /**
//     * 推送获取流地址信息
//     */
//    public void upGetUrlMsg(Context context,int f,int s,String si,String errCode,String tAtr,String url,String path) {
//
//        initMsg(context,url,path,tAtr,errCode);
//
//        try {
//
//            Map<String, String> body = new HashMap<String, String>();
//            body.put("p", p);//平台参数
//            body.put("v", v);
//            body.put("guid", guid);//用户标识(guid)
//            if(!StringUtil.isNullorEmpty(l)){
//                body.put("l", l);//访问文件url(带参数)
//            }
//            if(!StringUtil.isNullorEmpty(h)){
//                body.put("h", h);//访问接口（hostname）
//            }
//            body.put("t", t);//播放类型标记
//            body.put("sv", sv);//系统版本号
//            body.put("mf", mf);//厂家
//            body.put("mod", mod);//硬件型号
//            body.put("m", m);//MAC地址
//            body.put("pt", pt);//播放方式标示
//            body.put("c", c);//客户端类型
////            body.put("o", o);//视频卡/出错位置
//
//            body.put("b", b);//清晰度标志
//            body.put("cv", "20160531");
//
//            body.put("f", f+"");//访问正常标记
//            body.put("z", "1");//转换率判断标志
//            body.put("s", s+"");//播放步骤
//            body.put("i", "");//访问接口的ip地址
//            body.put("a", "0");//是否切换码率
//
//            if(!StringUtil.isNullorEmpty(si)){
//                body.put("si", si);//访问cms时的sourceID
//
//            }
//
//            if(e != null){
//                body.put("e", e);//错误码
//            }else{
//                body.put("e", "");//错误码
//            }
//
//
//            Log.i("UrlMessage", body.toString());
//
//            get(UP_REQUEST_VEDIO_MSG, body);
//        } catch (RuntimeException e) {
//            e.printStackTrace();
//        }
//
//    }

    /**
     * 推送播放卡顿信息
     */
    private void upVedioMsg() {
        try {

            Map<String, String> body = new HashMap<String, String>();
            body.put("p", p);//平台参数
            body.put("v", v);
            body.put("u", guid);//用户标识(guid)
            body.put("f", f+"");//正常标记
            body.put("r", r);//上报类型
            body.put("l", l);//访问文件url(带参数)
            body.put("h", h);//访问接口（hostname）
            body.put("t", t);//播放类型标记
            body.put("sv", sv);//系统版本号
            body.put("mf", mf);//厂家
            body.put("mod", mod);//硬件型号
            body.put("m", m);//MAC地址
            body.put("pt", pt);//播放方式标示
            body.put("c", c);//客户端类型
            body.put("o", "");//视频卡/出错位置

            body.put("b", b);//清晰度标志
            body.put("cv", "20160531");

            if(r != null && r.equals("2") && e != null){
                body.put("e", e);//错误码
            }else{
                body.put("e", "");//错误码
            }

            if(userId == null){
                userId = "";
            }
            if(ft == null){
                ft = "";
            }
            String ex = "uuid="+userId+"&ft="+ft;
            if(r != null && r.equals("2")){
                ex = ex + "&what="+what+"&extra="+extra;
            }
            try {
                ex = URLEncoder.encode(ex,"UTF-8");
            }catch (Exception e){
                e.printStackTrace();
            }
            body.put("ex", ex);

            body.put("cv", "20160531");

            Log.i("PlayMessage", body.toString());

            get(UP_PLAY_VEDIO_MSG, body);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean get(String url, Map<String, String> param) {
        if (mHttp == null) {
            mHttp = new HttpUtils(MaxApplication.getAppContext(), this);
        }
        return mHttp.get(url, param);
    }

    @Override
    public boolean post(String url, Map<String, String> param) {
        return false;
    }

    @Override
    public void onError(String url, Exception e) {

    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        return null;
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException, RemoteException {

    }

}
