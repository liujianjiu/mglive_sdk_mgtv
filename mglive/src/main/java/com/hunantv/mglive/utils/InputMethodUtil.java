package com.hunantv.mglive.utils;

import android.content.Context;
import android.provider.Settings;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class InputMethodUtil
{
	//******************************以下的常量是和sogou输入法协商好的，不能随意改动**********************************
	public static final String FLAG_SOGOU_EXPRESSION = "SOGOU_EXPRESSION";
	public static final String ACTION_SOGOU_EXPRESSION = "com.sogou.inputmethod.expression";
	public static final String KEY_SOGOU_EXPRESSION = "SOGOU_EXP_PATH";
	
	public static final String ACTION_SOGOU_APP_ID = "com.sogou.inputmethod.appid";
	public static final String ACTION_SOGOU_OPEN_ID = "com.tencent.mobileqq.sogou.openid";
	public static final String KEY_SOGOU_APP_ID = "SOGOU_APP_ID";
	public static final String KEY_SOGOU_OPEN_ID = "SOGOU_OPENID";
	
	//****************************************************************

    public static void show(View view)
    {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, 0);
    }
    public static void hide(View view)
    {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    
    /**
     * @param context
     * @return
     * @author clarkhuang
     */
    public static boolean checkSogouInputDefault(Context context)
	{
		String mLastInputMethodId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD);
		if (mLastInputMethodId != null && mLastInputMethodId.contains("com.sohu.inputmethod.sogou"))
		{
			return true;
		}
		
		return false;
	}
}
