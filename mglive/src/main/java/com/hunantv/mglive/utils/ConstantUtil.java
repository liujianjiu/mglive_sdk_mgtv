package com.hunantv.mglive.utils;

import com.alibaba.fastjson.JSON;
import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.CityModel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by admin on 2016/1/26.
 */
public class ConstantUtil {
    private static String[] xzs = new String[]{"全部","水瓶座","双鱼座","白羊座","金牛座","双子座","巨蟹座","狮子座","处女座","天秤座","天蝎座","射手座","摩羯座"};
    private static List<CityModel> cityModels;
    public static String[] getXzs(){
        return xzs;
    }

    public static List<String> getXzArray(){
        return Arrays.asList(xzs);
    }

    public static List<CityModel> getCitys() {
        if(cityModels == null || cityModels .size() == 0)
        {
            InputStream in = null;
            try {
                in = MaxApplication.getAppContext().getResources().getAssets().open(Constant.CITY_FILE_NAME);
                byte[] buffer = new byte[in.available()];
                in.read(buffer);
                String json = new String(buffer, "UTF-8");
                cityModels = JSON.parseArray(json, CityModel.class);
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if(in != null)
                {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return cityModels;
    }

    public static List<String> getProVinces(){
        List<CityModel> cityModels = getCitys();
        List<String> provinces = new ArrayList<String>();
        for (CityModel model : cityModels){
            provinces.add(model.getProvince());
        }
        return provinces;
    }

    public static List<String> getCitys(String provincesName){
        List<CityModel> cityModels = getCitys();
        List<String> provinces = new ArrayList<String>();
        if(!StringUtil.isNullorEmpty(provincesName))
        {
            for (CityModel model : cityModels){
                if(provincesName.equals(model.getProvince()))
                {
                    provinces.addAll(model.getCities());
                    break;
                }
            }
        }
        return provinces;
    }
}
