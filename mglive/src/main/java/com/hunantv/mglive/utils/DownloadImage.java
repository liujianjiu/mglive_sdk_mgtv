package com.hunantv.mglive.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by qiudaaini@163.com on 15/12/25.
 */
public class DownloadImage extends AsyncTask<ShareUtil.ShareData, ShareUtil.ShareData, ShareUtil.ShareData> {
    private static String TAG = "DownloadImage";


    @Override
    protected ShareUtil.ShareData doInBackground(ShareUtil.ShareData[] params) {
        if (params == null || params.length == 0) {
            return null;
        }
        URL imgUrl = null;
        Bitmap bitmap = null;
        try {
            imgUrl = new URL(params[0].imageurl);
            // 使用HttpURLConnection打开连接
            HttpURLConnection urlConn = (HttpURLConnection) imgUrl
                    .openConnection();
            urlConn.setDoInput(true);
            urlConn.connect();
            // 将得到的数据转化成InputStream
            InputStream is = urlConn.getInputStream();
            // 将InputStream转换成Bitmap
            bitmap = BitmapFactory.decodeStream(is);
            /**
             * 微信分享 缩略图（thumb）：最大64KB
             * 假设Bitmap是ARGB_8888格式，那么存储1个单位像素，需要用到的内存就是：8+8+8+8=32bit=4Byte
             * 由此可以换算出来，如果图片不能超过64KB，那么一个正方形的图，长宽应该不能超过127.8592976674
             * 直接设置为120
             */
            Bitmap bitmapNew =  Bitmap.createScaledBitmap(bitmap, 120, 120, true);
            bitmap.recycle();
            is.close();
            params[0].bitmap = bitmapNew;
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            System.out.println("[getNetWorkBitmap->]MalformedURLException");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("[getNetWorkBitmap->]IOException");
            e.printStackTrace();
        }
        return params[0];
    }

    private Bitmap comp(Bitmap image) {
        if(image == null){
            return null;
        }
        //微信分享限制32K
        if (image.getByteCount() > 32 * 1000){
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());

            BitmapFactory.Options newOpts = new BitmapFactory.Options();
            //开始读入图片，此时把options.inJustDecodeBounds 设回true了
            newOpts.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
            newOpts.inJustDecodeBounds = false;
            int w = newOpts.outWidth;
            int h = newOpts.outHeight;

            int hh = 60;
            int ww = 60;
            //缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
            int be = 1;//be=1表示不缩放

            if (w >= h && w > ww) {//如果宽度大的话根据宽度固定大小缩放
                be = (int) (newOpts.outWidth / ww);
                if (be==1)          //有这样的情况：outWidth = 130, 那130 / 70 = 1,  但130 *130 *4 > 32K了。。。
                    be =2;
            } else if (w <= h && h > hh) {//如果高度高的话根据宽度固定大小缩放
                be = (int) (newOpts.outHeight / hh);
                if (be==1)
                    be =2;
            }
            if (be <= 0)
                be = 1;
            newOpts.inSampleSize = be;//设置缩放比例
            //重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
            isBm = new ByteArrayInputStream(baos.toByteArray());
            bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
            return bitmap;//压缩好比例大小后再进行质量压缩

        }else{
            return image;
        }


    }

/* maxxiang: 这段代码写的有问题，我重新写了个新的在上面。
    private Bitmap comp(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        if (baos.toByteArray().length / 1024 > 35) {//判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
            int sacl = (int) (35.0f / (baos.toByteArray().length / 1024.0f) * 100);
            baos.reset();//重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, sacl, baos);//这里压缩x%，把压缩后的数据存放到baos中
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        //开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        //现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 90f;//这里设置高度为800f
        float ww = 80f;//这里设置宽度为480f
        //缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;//be=1表示不缩放
        if (w > h && w > ww) {//如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {//如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;//设置缩放比例
        //重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        isBm = new ByteArrayInputStream(baos.toByteArray());
        bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        return bitmap;//压缩好比例大小后再进行质量压缩
    }
*/
    private Bitmap compressImage(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while (baos.toByteArray().length / 1024 > 35) {  //循环判断如果压缩后图片是否大于100kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
            options -= 10;//每次都减少10
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
        return bitmap;
    }

    @Override
    protected void onPostExecute(ShareUtil.ShareData shareData) {
        super.onPostExecute(shareData);
        if (shareData != null) {
            shareData.callBack.callBack(shareData);
        }

    }


    public interface IDownloadCallBack {

        void callBack(ShareUtil.ShareData shareData);
    }

}
