package com.hunantv.mglive.utils;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;

import com.hunantv.mglive.common.MaxApplication;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class FileUtils {

    private static final int WRITE_BLOCK_SIZE = 4 * 1024;


    public static interface CancelableTask {
        boolean needCancel();
    }


    public static boolean isSDCardMounted() {
        return Environment.MEDIA_MOUNTED.equals(
                Environment.getExternalStorageState());
    }

    /**
     * =>  /SDCard/
     */
    public static String getSDCardPath() {
        return Environment.getExternalStorageDirectory().getPath() + "/";
    }

    /**
     * =>  /SDCard/tmp/com.xx.xxx/
     * <p/>
     * OR  /data/data/com.xx.xx/files/tmp/
     */
    public static String getTempPath() {
        String tempPath = null;
        if (isSDCardMounted()) {
            String packageName = MaxApplication.getAppContext().getPackageName();
            tempPath = getSDCardPath() + "tmp/" + packageName + "/";

        } else {
            tempPath = getAppFilePath() + "tmp/";
        }

        FileUtils.createPaths(tempPath);
        return tempPath;
    }

    /**
     * =>   /data/data/com.xx.xx/files/
     */
    public static String getAppFilePath() {
        String appFilePath = MaxApplication.getAppContext().getFilesDir().getPath() + "/";
        FileUtils.createPath(appFilePath);
        return appFilePath;
    }

    /**
     * =>   /data/data/com.xx.xx/databases/
     */
    public static String getDatabasePath() {
        File tempFile = MaxApplication.getAppContext().getDatabasePath("temp");
        String databasePath = tempFile.getParent();
        FileUtils.createPaths(databasePath);
        return databasePath + "/";
    }

    /**
     * =>   /data/data/com.xx.xx/databases/subDbPath/
     */
    public static String getDatabaseSubPath(String subDbPath) {
        String databasePath = FileUtils.getDatabasePath() + "/" + subDbPath + "/";
        FileUtils.createPath(databasePath);
        return databasePath;
    }

    /**
     * =>   /data/data/com.xx.xx/databases/dbName
     */
    public static String getDatabaseFilePath(String dbName) {
        String databasePath = FileUtils.getDatabasePath() + dbName;
        FileUtils.createFile(databasePath);
        return databasePath;
    }

    /**
     * =>   /sdcard/data/data/com.xx.xx/cache/
     */
    public static String getExternalCachePath() {
        String cachePath = null;
        if (isSDCardMounted()) {
            File path = MaxApplication.getAppContext().getExternalCacheDir();
            if (path != null) {
                cachePath = path.getPath() + "/";
            }
        } else {
            cachePath = MaxApplication.getAppContext().getCacheDir().getPath() + "/";
        }

        FileUtils.createPath(cachePath);
        return cachePath;
    }

    public static long getFileSize(String filePath) {
        return getFileSize(new File(filePath));
    }

    public static long getFileSize(File file) {
        return file != null ? file.length() : 0;
    }

    public static boolean isFileExist(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return false;
        }

        File file = new File(filePath);
        return file.exists();
    }

    public static boolean createPath(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return false;
        }

        File file = new File(filePath);
        return file.mkdir();
    }

    public static boolean createPath(File file) {
        return file.mkdir();
    }

    public static boolean createPaths(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return false;
        }

        File file = new File(filePath);
        return file.mkdirs();
    }

    public static boolean createPaths(File file) {
        return file.mkdirs();
    }

    public static boolean createFile(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return false;
        }

        File file = new File(filePath);
        return createFile(file);
    }

    public static boolean createFile(File file) {
        if (!file.exists()) {
            try {
                return file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        return true;
    }

    public static boolean copyFile(String srcPath, String destPath) {
        InputStream inputStream = getFileInputStream(srcPath);
        if (inputStream == null) {
            return false;
        }

        OutputStream outputStream = getFileOutputStream(destPath);
        if (outputStream == null) {
            closeStream(inputStream);
            return false;
        }

        boolean result = writeToStream(inputStream, outputStream);
        closeStream(inputStream);
        closeStream(outputStream);

        return result;
    }

    public static boolean copyUriToFile(Uri srcUri, String destPath) {
        OutputStream outputStream = getFileOutputStream(destPath);
        if (outputStream == null) {
            return false;
        }

        boolean result = false;
        try {
            ContentResolver resolver = MaxApplication.getAppContext().getContentResolver();
            InputStream inputStream = resolver.openInputStream(srcUri);

            result = writeToStream(inputStream, outputStream);
            FileUtils.closeStream(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        FileUtils.closeStream(outputStream);
        return result;
    }

    public static void closeStream(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean writeToStream(
            InputStream inputStream,
            OutputStream outputStream) {
        return writeToStream(inputStream, outputStream, null);
    }

    public static boolean writeToStream(
            InputStream inputStream,
            OutputStream outputStream,
            CancelableTask task) {

        try {
            byte[] buffer = new byte[WRITE_BLOCK_SIZE];

            while (true) {
                int length = inputStream.read(buffer);
                if (length <= 0) {
                    break;
                }

                if (task != null && task.needCancel()) {
                    break;
                }

                outputStream.write(buffer, 0, length);
            }

            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean writeToStorage(
            InputStream inputStream,
            String filePath) {
        return writeToStorage(inputStream, filePath, null);
    }

    public static boolean writeToStorage(
            InputStream inputStream,
            String filePath,
            CancelableTask task) {

        if (inputStream == null) {
            return false;
        }

        boolean bRet = false;
        OutputStream outputStream = getFileOutputStream(filePath);
        if (outputStream != null) {
            bRet = writeToStream(inputStream, outputStream, task);
            closeStream(outputStream);
        }

        return bRet;
    }

    public static InputStream getFileInputStream(String filePath) {
        if (!isFileExist(filePath)) {
            return null;
        }

        try {
            return new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static OutputStream getFileOutputStream(String filePath) {
        if (!isFileExist(filePath)) {
            createFile(filePath);
        }

        try {
            return new FileOutputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Bitmap getLocalBitmap(String path) {
        try {
            FileInputStream inputStream = new FileInputStream(path);
            return BitmapFactory.decodeStream(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }


    private static long getDirectorySizeInternal(File dir) {
        long size = 0;
        if (dir.isDirectory()) {
            File[] fileLists = dir.listFiles();
            if (fileLists != null && fileLists.length > 0) {
                for (File subDir : fileLists) {
                    size += getDirectorySizeInternal(subDir);
                }
            }
        } else {
            size += dir.length();
        }

        return size;
    }

    /**
     * 获取一个文件夹大小.
     *
     * @return Bytes
     */
    public static long getDirectorySize(String dir) {
        return TextUtils.isEmpty(dir) ? 0 : getDirectorySizeInternal(new File(dir));
    }

    private static void cleanDirectoryInternal(File dir) {
        if (!dir.exists()) {
            return;
        }

        if (dir.isDirectory()) {
            File[] fileLists = dir.listFiles();
            if (fileLists != null && fileLists.length > 0) {
                for (File subDir : fileLists) {
                    cleanDirectoryInternal(subDir);
                }
            }
        } else {
            dir.delete();
        }
    }

    /**
     * 清空一个文件夹内容.但不删除这个文件夹.
     */
    public static void cleanDirectory(String dir) {
        if (TextUtils.isEmpty(dir)) {
            return;
        }

        cleanDirectoryInternal(new File(dir));
    }

    /**
     * 获取大小文本描述.
     */
    public static String getSizeTextDescription(long sizeBytes) {
        String desc = null;

        final long oneKB = 1024;
        final long oneMB = 1024 * oneKB;
        final long oneGB = 1024 * oneMB;

        if (sizeBytes >= oneGB) {
            desc = String.format("%.1f GB", (float) sizeBytes / (float) oneGB);
        } else if (sizeBytes >= oneMB) {
            desc = String.format("%.1f MB", (float) sizeBytes / (float) oneMB);
        } else if (sizeBytes >= oneKB) {
            desc = String.format("%.1f KB", (float) sizeBytes / (float) oneKB);
        } else {
            desc = String.format("%.1f B", (float) sizeBytes);
        }

        return desc;
    }

    public static String getFileUri(String filePath) {
        return "file://" + filePath;
    }

    public static boolean isFileUri(String filePath) {
        return !TextUtils.isEmpty(filePath) && filePath.startsWith("file://");
    }

    public static String getFileMD5(String filePath) {
        File file = new File(filePath);
        return getFileMD5(file);
    }

    /**
     * 获取文件 MD5.
     */
    public static String getFileMD5(File file) {
        if (file == null || !file.exists() || !file.isFile()) {
            return "";
        }
        try {
            FileInputStream stream = new FileInputStream(file);
            return getStreamMD5(stream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * 获取 Stream MD5.
     */
    public static String getStreamMD5(InputStream stream) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }

        byte buffer[] = new byte[1024];
        int len = 0;
        try {
            while ((len = stream.read(buffer, 0, 1024)) != -1) {
                digest.update(buffer, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeStream(stream);
        }

        BigInteger integer = new BigInteger(1, digest.digest());
        return integer.toString(16);
    }

    /**
     * 获取 Uri MD5.
     */
    public static String getUriMD5(Uri uri) {
        try {
            ContentResolver resolver = MaxApplication.getAppContext().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            return getStreamMD5(stream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * the traditional io way
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public static byte[] toByteArray(String filename) throws IOException {

        File file = new File(filename);
        if (!file.exists()) {
            throw new FileNotFoundException(filename);
        }

        return toByteArray(file);
    }

    /**
     * the traditional io way
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static byte[] toByteArray(File file) throws IOException {
        if(file != null && file.exists())
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream((int) file.length());
            BufferedInputStream in = null;
            try {
                in = new BufferedInputStream(new FileInputStream(file));
                int buf_size = 1024;
                byte[] buffer = new byte[buf_size];
                int len = 0;
                while (-1 != (len = in.read(buffer, 0, buf_size))) {
                    bos.write(buffer, 0, len);
                }
                return bos.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bos.close();
            }
        }
        return null;
    }
}
