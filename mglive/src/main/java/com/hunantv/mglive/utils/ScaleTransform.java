package com.hunantv.mglive.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import javax.xml.transform.Source;

/**
 * Created by June Kwok on 2015/12/4.
 */
public class ScaleTransform extends BitmapTransformation {

    private int width, height;

    public ScaleTransform(Context context, int width, int height) {
        super(context);
        this.width = width;
        this.height = height;
    }

    @Override
    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
        return roundCrop(toTransform);
    }

    private Bitmap roundCrop(Bitmap source) {
        if (source == null) return null;
        float scalW = (this.width * 1f) / source.getWidth();
        float scalH = (this.height * 1f) / source.getHeight();
        float scal = scalW > scalH ? scalW : scalH;
        source = ImageUtilsEx.scaleImage(source, scal, scal);
        Bitmap outBitmap = Bitmap.createBitmap(width, height, source.getConfig());
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        Canvas canvas = new Canvas(outBitmap);
        canvas.drawBitmap(source, null, new Rect(0, 0, width, height), paint);
        return outBitmap;
    }

    @Override
    public String getId() {
        return getClass().getName() + Math.round(width);
    }
}

