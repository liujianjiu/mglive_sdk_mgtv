package com.hunantv.mglive.utils;


public class AlbumConstants {
	public static final String TAG = AlbumConstants.class.getSimpleName();
	
	private static final String PREFIX = AlbumConstants.class.getName()+".";
	public static final String SELECTED_PHOTO_LIST = PREFIX+"SelectedPhotoList";
	
	public static final String FORWARD_WHERE_KEY = PREFIX+"ForwardWhere";
	
	public static final String FROM_WHERE_KEY = PREFIX+"FromWhere";
	
	public final static String ALBUM_NAME = PREFIX+"AlbumName";
	
	public final static String ALBUM_ID = PREFIX+"AlbumId";
	
	public static final String INIT_ACTIVITY_CLASS = PREFIX+"InitActivityClass";
	
	public static final String DEST_ACTIVITY_CLASS = PREFIX+"DestActivityClass";
	
	public static final String REQUEST_CODE = PREFIX+"RequestCode";
	public static final String RESULT_CODE = "result_code";
	
	public static final String TEMP_SELECT_PHOTO_MAP = PREFIX+"TempSelectPhotoMap";
	
	public static final String PASS_TO_DEST_ACTIVITY = PREFIX+"PassToDestActivity";
	
	public static final String PASS_FROM_DEST_ACTIVITY = PREFIX+"PassFromDestActivity";
	
	public static final String LEFT_BTN_CLICK = "LeftBtnClick";
	
	public static final String RIGHT_BTN_CLICK = "RightBtnClick";
	
	public static final String BACK_KEY_CLICK = "BackKeyClick";
	
	public static final String FROM_SECRETFILE = "fromSecretfile";
	
	public static final int  REQUEST_CODE_START_ALBUM_LIST = 1;
	
	public static final int REQUEST_CODE_START_PHOTO_LIST = 2;
	
	public static final int REQUEST_CODE_START_SINGLE_BIG_PHOTO_PREVIEW = 3;
	
	public static final int REQUEST_CODE_START_PHOTO_SELECT = 4;
	
	public static final int REQUEST_CODE_START_MULTIPLE_BIG_PHOTO_PREVIEW = 5;
	
	public static final String FORWARD_CHAT_ACTIVITY = "ForwardChatActivity";
	
	public static final String FORWARD_ALBUM_LIST_ACTIVITY = "ForwardAlbumListActivity";
		
	
	public static final String FORWARD_PHOTO_LIST_ACTIVITY = "ForwardPhotoListActivity";
	
	
	public static final String FORWARD_PHOTO_SELECT_ACTIVITY = "ForwardPhotoSelectActivity";
	
	
	
	public static final String FROM_INIT_ACTIVITY = "FromInitActivity";
	
	public static final String FROM_ALBUM_LIST_ACTIVITY = "FromAlbumListActivity";
	
	
	public static final String FROM_DEST_ACTIVITY = "FromDestActivity";
	
	public static final String FROM_PHOTO_LIST_ACTIVITY = "FromPhotoListActivity";
	
	public static final String FROM_PHOTO_SELECT_ACTIVITY = "FromPhotoSelectActivity";
	
	
	
	public static final String RECENT_ALBUM_ID = "$RecentAlbumId";
	public static final String RECENT_ALBUM_NAME = "最近照片";
	
	public static final int RECENT_PHOTO_MIN_WIDTH = 200;
	
	public static final int RECENT_PHOTO_MIN_HEIGHT = 200;
	
	public static final int MAX_SELECT_PHOTO_SIZE = 6;
	
	
}
