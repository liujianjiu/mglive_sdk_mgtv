package com.hunantv.mglive.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by max on 2015/12/18.
 */
public class RandomUtil {

    //生成yyyymmddhhmiss，再加上6位随机数的字符串  20位
    public static String makeDateRamdom(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String str = sdf.format(date);

        Random random = new Random();
        for(int i=0;i<6;i++){                   //生成6位随机数
            str = str + random.nextInt(10);
        }
        return str;
    }
}
