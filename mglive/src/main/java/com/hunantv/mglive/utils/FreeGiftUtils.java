package com.hunantv.mglive.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import com.hunantv.mglive.aidl.FreeGiftCallBack;
import com.hunantv.mglive.aidl.IGiftServiceAIDL;
import com.hunantv.mglive.common.BuildConfig;
import com.hunantv.mglive.common.FormEncodingBuilderEx;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.ResultModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Administrator on 2016-1-28.
 */
public class FreeGiftUtils implements HttpUtils.callBack {
    private static FreeGiftUtils INSTANCE;
    private Context mContext;
    private FreeGiftCallBack mCallback;
    private IGiftServiceAIDL mAidl;
    private ServiceConnection conn;
    private HttpUtils mHttp;

    private FreeGiftUtils(Context context) {
        mContext = context;
        mHttp = new HttpUtils(context, this);
    }

    public static FreeGiftUtils getInstance() {
        if (null == INSTANCE) {
            INSTANCE = new FreeGiftUtils(MaxApplication.getAppContext());
            INSTANCE.startService();
            INSTANCE.loadFreeGiftCount();
        }
        return INSTANCE;
    }

    public void setCallback(FreeGiftCallBack callback) {
        mCallback = callback;
        if (null != getService()) {
            try {
                getService().setCallback(getCallback());
            } catch (RemoteException e) {
                L.e(FreeGiftUtils.class.getName(), e);
            }
        }
    }

    public void addCallback(FreeGiftCallBack callback) {
        if (null != getService()) {
            try {
                getService().addCallback(callback);
            } catch (RemoteException e) {
                L.e(FreeGiftUtils.class.getName(), e);
            }
        }
    }

    public void removeAllCallback() {
        if (null != getService()) {
            try {
                getService().removeAllCallBack();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public FreeGiftCallBack getCallback() {
        return mCallback;
    }

    public IGiftServiceAIDL getService() {
        return mAidl;
    }

    public void setService(IGiftServiceAIDL mService) {
        this.mAidl = mService;
    }

    /**
     * 启动Service
     */
    public void startService() {
        if (mAidl == null) {
            conn = new ServiceConnection() {
                /**
                 * 获取服务对象时的操作
                 */
                public void onServiceConnected(ComponentName name, IBinder service) {
                    mAidl = IGiftServiceAIDL.Stub.asInterface(service);
                    setCallback(getCallback());
                }

                /** 无法获取到服务对象时的操作 */
                public void onServiceDisconnected(ComponentName name) {
                    mAidl = null;
                }
            };
//            Intent intent = new Intent(mContext, MaxBGService.class);
//            mContext.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        }
    }


    /**
     * 停止Service
     */
    public void stopService() {
        if (null != mContext && conn != null) {
            mContext.unbindService(conn);
        }
    }

    /**
     * 获取免费礼物数量
     */
    public void loadFreeGiftCount() {
        if (MaxApplication.getApp().isLogin()) {
            Map<String, String> body = new FormEncodingBuilderEx()
                    .add("uid", MaxApplication.getApp().getUid())
                    .add("token", MaxApplication.getApp().getToken()).build();
            post(BuildConfig.GET_FREE_GIFT_NUM, body);
        }
    }

    @Override
    public boolean get(String url, Map<String, String> param) {
        return mHttp.get(url, param);
    }

    @Override
    public boolean post(String url, Map<String, String> param) {
        return mHttp.post(url, param);
    }

    @Override
    public void onError(String url, Exception e) {
        e.printStackTrace();
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        return null;
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) {
        if (BuildConfig.GET_FREE_GIFT_NUM.equals(url)) {
            //免费礼物数量
            try {
                JSONObject json = new JSONObject(resultModel.getData());
                int freeGiftCount = json.getInt("count");
                MaxApplication.getInstance().setFreeGiftCount(freeGiftCount > 0 ? freeGiftCount : 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

