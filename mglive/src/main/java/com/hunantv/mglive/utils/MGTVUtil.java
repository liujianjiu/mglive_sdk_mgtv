package com.hunantv.mglive.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/**
 * Created by liujianjiu on 16-8-9.
 *
 * 操作芒果TV工具类
 */
public class MGTVUtil {

    private static MGTVUtil mMGLiveUtil;
    private MGTVUtil(){};

    public synchronized static MGTVUtil getInstance(){
        if(mMGLiveUtil == null){
            mMGLiveUtil = new MGTVUtil();
        }

        return mMGLiveUtil;
    }

    /**
     * 调起芒果TV登录
     * @param context
     */
    public void login(Context context){
        if(context == null){
            return;
        }
        try {
            Intent intent = new Intent();
            ComponentName cn = new ComponentName("com.hunantv.imgo.activity", "com.hunantv.imgo.activity.LoginDialogActivity");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setComponent(cn);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}