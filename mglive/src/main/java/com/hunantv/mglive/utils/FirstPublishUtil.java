package com.hunantv.mglive.utils;

import android.content.Intent;

import com.hunantv.mglive.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2016/5/12.
 */
public class FirstPublishUtil {
    private static final Map<String,Integer> channelMapping = new HashMap<>();

    static {
        channelMapping.put("qq", R.drawable.qq);
        channelMapping.put("_360dev",R.drawable._360);
        channelMapping.put("uc",R.drawable.uc);
        channelMapping.put("taobao",R.drawable.taobao);
        channelMapping.put("_25pp",R.drawable._25pp);
        channelMapping.put("huawei",R.drawable.huawei);
        channelMapping.put("sogou",R.drawable.sogou);
    }

    public static int getChannelImg(String channel){
        if(channelMapping.containsKey(channel))
        {
            return channelMapping.get(channel);
        }
        return  android.R.color.transparent;
    }
}
