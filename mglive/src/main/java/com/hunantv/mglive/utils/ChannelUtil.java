package com.hunantv.mglive.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;

import com.hunantv.mglive.common.Constant;

import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by admin on 2016/4/20.
 */
public class ChannelUtil {
    public static boolean isFirstChannel = false;
    private static String channel = null;

    public static String getChannel(Context context) {
        if (channel != null) {
            return channel;
        }
        final String start_flag = "META-INF/channel_";
        ApplicationInfo appinfo = context.getApplicationInfo();
        String sourceDir = appinfo.sourceDir;
        ZipFile zipfile = null;
        try {
            zipfile = new ZipFile(sourceDir);
            Enumeration<?> entries = zipfile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = ((ZipEntry) entries.nextElement());
                String entryName = entry.getName();
                if (entryName.contains(start_flag)) {
                    channel = entryName.replace(start_flag, "");
                    if (channel.endsWith(Constant.CHANNEL_FIRST_TAG)) {
                        channel = channel.replace(Constant.CHANNEL_FIRST_TAG, "");
                        isFirstChannel = true;
                    }
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (zipfile != null) {
                try {
                    zipfile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (channel == null || channel.length() <= 0) {
            channel = "mglive";//读不到渠道号就默认是官方渠道
        }
        return channel;
    }
}
