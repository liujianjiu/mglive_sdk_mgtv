package com.hunantv.mglive.utils;

import android.content.Context;

import com.hunantv.mglive.R;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.template.ElementModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2016/3/21.
 */
public class TemplateUtil {

    /**
     * 获取查看更多加载时节点
     *
     * @param context
     * @param type     模板类型
     * @param pageSize 分页数
     * @param colStr   显示列数
     * @return
     */
    public static List<ElementModel> getMoreElements(Context context, String type, int pageSize, String colStr) {
        List<ElementModel> list = new ArrayList<>();
        int col = 1;
        if (!StringUtil.isNullorEmpty(colStr)) {
            try {
                col = Integer.parseInt(colStr);
            } catch (Exception e) {
                col = 1;
            }
        }
        int row = pageSize;
        if (col > 1) {
            float n = ((float)pageSize)/2;
            row = (int) Math.ceil(n);
        }

        for (int i = 0; i < row; i++) {
            if (ElementModel.TYPE_VIDEO.equals(type)) {
                if (col == 1) {
                    //大图
                    list.add(createVideo1(context));
                } else {
                    //一行两图
                    list.add(createVideo2(context));
                }
            } else if (ElementModel.TYPE_DYNAMIC.equals(type)) {
                list.add(createEmpty(context));
                list.add(createDynamic(context));
            } else if (ElementModel.TYPE_USERLIVE.equals(type)) {
                list.add(createEmpty(context));
                list.add(createLive(context));
            }
        }
        return list;
    }

    /**
     * 解析视频节点
     *
     * @param element
     * @param isEndElement
     * @return
     */
    public static List<ElementModel> getElements(Context context, ElementModel element, boolean isEndElement) {
        List<ElementModel> elements = new ArrayList<>();
        if (ElementModel.TYPE_VIDEO.equals(element.getType())
                || ElementModel.TYPE_DYNAMIC.equals(element.getType())
                || ElementModel.TYPE_USERLIVE.equals(element.getType())) {
            int pagesize = getPageSize(element.getParameter());
            ArrayList<ElementModel> list = null;
            if (ElementModel.TYPE_VIDEO.equals(element.getType())) {
                list = TemplateUtil.parseVideo(context, element, isEndElement, pagesize);
            } else if (ElementModel.TYPE_DYNAMIC.equals(element.getType())) {
                list = TemplateUtil.parseDynamic(context, element, isEndElement, pagesize);
            } else if (ElementModel.TYPE_USERLIVE.equals(element.getType())) {
                list = TemplateUtil.parseLives(context, element, isEndElement, pagesize);
            }
            if (list != null && list.size() > 0) {
                elements.addAll(list);
            }

        } else {
            elements.add(element.clone());
        }
        return elements;
    }

    /**
     * 解析视频模板
     *
     * @param element
     * @return
     */
    public static ArrayList<ElementModel> parseVideo(Context context, ElementModel element, boolean isEndElement, int pagesize) {
        ArrayList<ElementModel> videos = new ArrayList<>();
        //添加标题栏
        if ("1".equals(element.getTitleIsShow())) {
            ElementModel barElement = createTitleBar(context, element);
            videos.add(barElement);
        }
        int col = 0;
        int row = 0;
        boolean isRow = true;
        try {
            if (!StringUtil.isNullorEmpty(element.getCol())) {
                col = Integer.parseInt(element.getCol());
            }
            if (isEndElement && "1".equals(element.getTurnPage())) {
                //最后一个节点才处理分页
                //允许分页,行数使用分页数
                row = pagesize;

            } else {
                if (!StringUtil.isNullorEmpty(element.getRow())) {
                    row = Integer.parseInt(element.getRow());
                    isRow = true;
                } else {
                    row = pagesize;
                    isRow = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //数据部分
        if ("1".equals(element.getFBshow())) {
            //首行大图
            videos.add(createVideo1(context));
            row = row - 1;
        }

        //如果是使用pagesize计算行数,需排除首行大图后重新计算行数
        if (col > 1 && !isRow) {
            float n = ((float)row)/2;
            row = (int) Math.ceil(n);
        }

        if (row > 0) {
            for (int i = 0; i < row; i++) {
                if (col == 1) {
                    //大图
                    videos.add(createVideo1(context));
                } else {
                    //一行两图
                    videos.add(createVideo2(context));
                }
            }
        }
        if (!StringUtil.isNullorEmpty(element.getFooter())) {
            ElementModel footer = createFooter(context, element);
            videos.add(footer);
        }
        return videos;
    }

    /**
     * 解析动态模板
     *
     * @param element
     * @return
     */
    public static ArrayList<ElementModel> parseDynamic(Context context, ElementModel element, boolean isEndElement, int pagesize) {
        ArrayList<ElementModel> dynamics = new ArrayList<>();
        //添加标题栏
        if ("1".equals(element.getTitleIsShow())) {
            ElementModel barElement = createTitleBar(context, element);
            barElement.setIsShowLine(true);
            dynamics.add(barElement);
        }

        int row = 0;
        try {
            if (isEndElement && "1".equals(element.getTurnPage())) {
                //最后一个节点才处理分页
                //允许分页,行数使用分页数
                row = pagesize;
            } else {
                if (!StringUtil.isNullorEmpty(element.getRow())) {
                    row = Integer.parseInt(element.getRow());
                } else {
                    row = pagesize;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < row; i++) {
            dynamics.add(createDynamic(context));
            if (i != row - 1) {
                dynamics.add(createEmpty(context));
            }
        }

        if (!StringUtil.isNullorEmpty(element.getFooter())) {
            ElementModel footer = createFooter(context, element);
            footer.setIsShowLine(true);
            dynamics.add(footer);
        }
        return dynamics;
    }

    /**
     * 解析动态模板
     *
     * @param element
     * @return
     */
    public static ArrayList<ElementModel> parseLives(Context context, ElementModel element, boolean isEndElement, int pagesize) {
        ArrayList<ElementModel> lives = new ArrayList<>();
        //添加标题栏
        if ("1".equals(element.getTitleIsShow())) {
            ElementModel barElement = createTitleBar(context, element);
            barElement.setIsShowLine(true);
            lives.add(barElement);
        }

        int row = 0;
        try {
            if (isEndElement && "1".equals(element.getTurnPage())) {
                //最后一个节点才处理分页
                //允许分页,行数使用分页数
                row = pagesize;
            } else {
                if (!StringUtil.isNullorEmpty(element.getRow())) {
                    row = Integer.parseInt(element.getRow());
                } else {
                    row = pagesize;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < row; i++) {
            lives.add(createLive(context));
            if (i != row - 1) {
                lives.add(createEmpty(context));
            }
        }

        if (!StringUtil.isNullorEmpty(element.getFooter())) {
            ElementModel footer = createFooter(context, element);
            footer.setIsShowLine(true);
            lives.add(footer);
        }
        return lives;
    }

    public static Map<String, String> paraseParam(String paramStr) {
        Map<String, String> paramMap = new HashMap<>();
        if (!StringUtil.isNullorEmpty(paramStr)) {
            String[] params = paramStr.split("&");
            for (String param : params) {
                String[] strs = param.split("=");
                if (strs.length == 2) {
                    paramMap.put(strs[0], strs[1]);
                }
            }
        }
        if (!StringUtil.isNullorEmpty(MaxApplication.getApp().getUid())) {
            paramMap.put("uid", MaxApplication.getApp().getUid());
        }
        return paramMap;
    }

    /**
     * 标题模板
     *
     * @param element
     * @return
     */
    private static ElementModel createTitleBar(Context context, ElementModel element) {
        ElementModel barElement = new ElementModel();
        barElement.setType(ElementModel.TYPE_CUSTOM_TITLEBAR);
        barElement.setData(element.getTitle());
        barElement.setLinkUrl(element.getLinkUrl());
        barElement.setTitle(element.getTitle());
        barElement.setTips(element.getTips());
        barElement.setCusHeight(context.getResources().getDimensionPixelOffset(R.dimen.height_40dp));
        return barElement;
    }

    /**
     * 更多模板
     *
     * @param element
     * @return
     */
    private static ElementModel createFooter(Context context, ElementModel element) {
        ElementModel footer = new ElementModel();
        footer.setType(ElementModel.TYPE_CUSTOM_MORE);
        footer.setData(element.getFooter());
        footer.setFooter(element.getFooter());
        footer.setCusHeight(context.getResources().getDimensionPixelOffset(R.dimen.height_45dp));
        footer.setLinkUrl(element.getLinkUrl());
        return footer;
    }

    /**
     * 一行一个视频
     *
     * @return
     */
    private static ElementModel createVideo1(Context context) {
        ElementModel video = new ElementModel();
        video.setType(ElementModel.TYPE_CUSTOM_VIDEO_1);
//        video.setCusHeight(context.getResources().getDimensionPixelOffset(R.dimen.height_375dp));
        return video;
    }

    /**
     * 一行两个视频
     *
     * @return
     */
    private static ElementModel createVideo2(Context context) {
        ElementModel video = new ElementModel();
        video.setType(ElementModel.TYPE_CUSTOM_VIDEO_2);
//        video.setCusHeight(context.getResources().getDimensionPixelOffset(R.dimen.height_187dp));
        return video;
    }

    /**
     * 动态
     *
     * @return
     */
    private static ElementModel createDynamic(Context context) {
        ElementModel dynamic = new ElementModel();
        dynamic.setType(ElementModel.TYPE_CUSTOM_DYNAMIC);
//        dynamic.setCusHeight(context.getResources().getDimensionPixelOffset(R.dimen.height_530dp));
        return dynamic;
    }

    /**
     * 直播
     *
     * @return
     */
    private static ElementModel createLive(Context context) {
        ElementModel live = new ElementModel();
        live.setType(ElementModel.TYPE_CUSTOM_LIVE);
//        live.setCusHeight(context.getResources().getDimensionPixelOffset(R.dimen.height_187dp));
        return live;
    }

    /**
     * 空行
     *
     * @return
     */
    public static ElementModel createEmpty(Context context) {
        ElementModel empty = new ElementModel();
        empty.setType(ElementModel.TYPE_CUSTOM_EMPTY);
        empty.setCusHeight(context.getResources().getDimensionPixelSize(R.dimen.height_7_5dp));
        return empty;
    }

    public static int getPageSize(String params) {
        int pagesize = 10;
        Map<String, String> map = paraseParam(params);
        if (map.containsKey("pageSize")) {
            String pageSizeStr = map.get("pageSize");
            try {
                pagesize = Integer.parseInt(pageSizeStr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return pagesize;
    }
}