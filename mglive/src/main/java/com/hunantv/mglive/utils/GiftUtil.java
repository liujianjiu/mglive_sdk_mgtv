package com.hunantv.mglive.utils;

import com.hunantv.mglive.data.GiftDataModel;
import com.hunantv.mglive.data.gift.ClassifyGiftModel;
import com.hunantv.mglive.ui.entertainer.data.ContrData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2016/4/11.
 */
public class GiftUtil {
    private static GiftUtil mGiftUtil;

    /**
     * 礼物列表
     */
    private Map<String,ClassifyGiftModel> mClassifyGifts = new HashMap<>();

    /**
     * 快捷礼物
     */
    private GiftDataModel mShortcutGift;

    /**
     * 喊话
     */
    private GiftDataModel mShout;

    /**
     * 守护列表
     */
    private List<ContrData> mContrs;

    private GiftUtil(){

    }

    public static GiftUtil getInstance(){
        if(mGiftUtil == null)
        {
            mGiftUtil = new GiftUtil();
        }
        return mGiftUtil;
    }

    public Map<String,ClassifyGiftModel> getGifts() {
        return mClassifyGifts;
    }

    public void setGifts(List<ClassifyGiftModel> gifts) {
        //缓存全部分类
        if(gifts != null && gifts.size() > 0){
            for (int i=0;i<gifts.size();i++){
                ClassifyGiftModel model = gifts.get(i);
                mClassifyGifts.put(model.getClassifyId(),model);
            }
        }
    }

    /**
     * 查找礼物
     * @param gid
     * @return
     */
    public GiftDataModel getGiftModel(String gid){
        GiftDataModel model = null;
        if(mClassifyGifts.size() > 0 && !StringUtil.isNullorEmpty(gid))
        {
            for(ClassifyGiftModel classifyGiftModel : mClassifyGifts.values()){
                if(classifyGiftModel != null && classifyGiftModel.getGifts() != null && classifyGiftModel.getGifts().size() > 0)
                {
                    for(int i=0;i<classifyGiftModel.getGifts().size();i++)
                    {
                        GiftDataModel giftModel = classifyGiftModel.getGifts().get(i);
                        if(gid.equals(String.valueOf(giftModel.getGid())))
                        {
                            model = giftModel;
                            break;
                        }
                    }
                    if(model != null)
                    {
                        break;
                    }
                }
            }
        }
        return model;
    }

    public boolean isLoadGifts(){
        return mClassifyGifts != null  && mClassifyGifts.size() != 0;
    }

    public GiftDataModel getShortcutGift() {
        return mShortcutGift;
    }

    public void setShortcutGift(GiftDataModel gift) {
        this.mShortcutGift = gift;
    }

    public GiftDataModel getShout() {
        return mShout;
    }

    public void setShout(GiftDataModel mShout) {
        this.mShout = mShout;
    }

    public List<ContrData> getContrs() {
        return mContrs;
    }

    public void setContrs(List<ContrData> mContrs) {
        this.mContrs = mContrs;
    }
}
