package com.hunantv.mglive.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.hunantv.mglive.common.Constant;

/**
 * Created by liujianjiu on 16-8-9.
 *
 * 跳到芒果直播金币充值工具类
 */
public class MGLiveMoneyUtil {

    private static MGLiveMoneyUtil mMGLiveUtil;
    private MGLiveMoneyUtil(){};

    public synchronized static MGLiveMoneyUtil getInstance(){
        if(mMGLiveUtil == null){
            mMGLiveUtil = new MGLiveMoneyUtil();
        }

        return mMGLiveUtil;
    }

    /**
     * 跳到芒果直播金币充值页面
     */
    public void goMGLiveMonneyPay(Context context){
        try{
            String mgtvPKG = "com.hunantv.mglive";
            boolean isInStallApp = PackageUtil.hasInstalledApp(context,mgtvPKG);
            if(isInStallApp){//本机有安装芒果直播

                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.MGLIVE_URL_PAY)));
            }else{//本机没有安装芒果直播

                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://promote.max.mgtv.com/redirect?c=mgtvpay")));
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

}