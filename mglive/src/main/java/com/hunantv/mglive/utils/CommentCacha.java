package com.hunantv.mglive.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.alibaba.fastjson.JSON;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.login.UserInfoData;
import com.hunantv.mglive.data.user.DynamicMessageData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2016/1/27.
 */
public class CommentCacha {
    private static CommentCacha mCache;
    private SharedPreferences sharedPreferences;
    private static final String KEY = "dynamic_comment";
    public static final int MAX_DYNAMIC_COMMENT_SIZE = 100;

    private CommentCacha() {
        sharedPreferences = MaxApplication.getAppContext().getSharedPreferences(MaxApplication.getAppContext().getPackageName() + "message", Context.MODE_PRIVATE);
    }

    public static CommentCacha getInstance() {
        if (mCache == null) {
            mCache = new CommentCacha();
        }
        return mCache;
    }

    /**
     * 只保留100条
     *
     * @param datas
     */
    public void saveDynamicComment(List<DynamicMessageData> datas,String uid) {
        if (datas != null && datas.size() > 0) {
            List<DynamicMessageData> saveDatas = getMaxDynamicComment(datas,uid);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(getKey(uid), JSON.toJSONString(saveDatas));
            editor.commit();
            L.d("Save Dynamic Comment", "Size=" + saveDatas.size() + "  KEY=" + getKey(uid));
        }
        L.d("Save Dynamic Comment", "New Dtaas Size=" + datas != null ? datas.size() + "" : "0");
    }

    public void removeDynamicComment(String uid) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getKey(uid), "");
        editor.commit();
    }

    public List<DynamicMessageData> getDynamicComment(String uid) {
        String infoJson = sharedPreferences.getString(getKey(uid), "");
        L.d("History Dynamic Comment", "KEY=" + getKey(uid) + "result__" + infoJson);
        return StringUtil.isNullorEmpty(infoJson) ? null : JSON.parseArray(infoJson, DynamicMessageData.class);
    }

    public List<DynamicMessageData> getMaxDynamicComment(List<DynamicMessageData> datas,String uid) {
        List<DynamicMessageData> saveDatas = new ArrayList<DynamicMessageData>();
        List<DynamicMessageData> historyDatas = getDynamicComment(uid);
        if (datas != null) {
            saveDatas.addAll(datas);
        }
        if (saveDatas.size() < MAX_DYNAMIC_COMMENT_SIZE) {
            if(historyDatas != null )
            {
                //点赞动态去重
                historyDatas = removeRepeatPraiseDynamic(saveDatas,historyDatas);
                int maxLength = MAX_DYNAMIC_COMMENT_SIZE - saveDatas.size();
                if (historyDatas.size() > maxLength) {
                    saveDatas.addAll(historyDatas.subList(0, maxLength));
                } else {
                    saveDatas.addAll(historyDatas);
                }
            }
        }else
        {
            saveDatas = saveDatas.subList(0,MAX_DYNAMIC_COMMENT_SIZE);
        }
        L.d("Max Dynamic Comment Size ", saveDatas.size()+"");
        return saveDatas;
    }

    /**
     * 去除重复的赞动态回复,取新数据的
     * * @param nowDatas
     * @param historyDatas
     * @return
     */
    private List<DynamicMessageData> removeRepeatPraiseDynamic(List<DynamicMessageData> nowDatas,List<DynamicMessageData> historyDatas){
        List<DynamicMessageData> newHistoryDatas = new ArrayList<DynamicMessageData>();
        if(historyDatas != null && historyDatas.size() > 0)
        {
            for (DynamicMessageData historyData : historyDatas){
                boolean isRepeat = false;
                if(nowDatas != null && nowDatas.size() > 0)
                {
                    for (DynamicMessageData nowData : nowDatas){
                        if(nowData.getDynamicId().equals(historyData.getDynamicId())
                                && nowData.getmType().equals(historyData.getmType())
                                && DynamicMessageData.TYPE_M_PRAISE.equals(nowData.getmType()))
                        {
                            isRepeat = true;
                            break;
                        }
                    }
                }

                if(!isRepeat)
                {
                    newHistoryDatas.add(historyData);
                }
            }
        }
        return newHistoryDatas;
    }

    private String getKey(String uid){
        return KEY + "-" + uid;
    }
}
