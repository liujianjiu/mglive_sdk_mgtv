/**
 *
 */
package com.hunantv.mglive.utils;

import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;

import com.hunantv.mglive.data.LiveUrlModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * String 工具类
 *
 * @author QiuDa
 */
public class StringUtil {
    private static final String TAG = "StringUtil";

    /**
     * 删除BOM字符
     *
     * @param data
     * @return
     */
    public static final String removeBOM(String data) {
        if (isNullorEmpty(data)) {
            return data;
        }

        if (data.startsWith("\ufeff")) {
            return data.substring(1);
        } else {
            return data;
        }
    }

    public static boolean isNullorEmpty(String str) {
        if (str == null || "".equals(str)) {
            return true;
        }
        return false;
    }

    /**
     * 验证手机格式
     */
    public static boolean isMobileNO(String mobiles) {

        // 移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188
        // 联通：130、131、132、152、155、156、185、186 \n 电信：133、153、180、189、（1349卫通）
        // 总结起来就是第一位必定为1，第二位必定为3或5或8，其他位置的可以为0-9
        String telRegex = "[1][34578]\\d{9}";// "[1]"代表第1位为数字1，"[358]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
        if (TextUtils.isEmpty(mobiles)) {
            return false;
        } else {
            return mobiles.matches(telRegex);
        }
    }

    /**
     * [\u4e00-\u9fa5] //匹配中文字符^[1-9]\d*$ //匹配正整数^[A-Za-z]+$
     * //匹配由26个英文字母组成的字符串^[A-Z]+$ //匹配由26个英文字母的大写组成的字符串^[a-z]+$
     * //匹配由26个英文字母的小写组成的字符串^[A-Za-z0-9]+$ //匹配由数字和26个英文字母组成的字符串
     *
     * @return false 不符合 true 符合
     * @描述 验证输入密码是否符合规范
     */
    public static boolean isPwdlegally(String mPassWord) {
        String pwdRegex = "[A-Za-z0-9]*";
        if (mPassWord.matches(pwdRegex)) {
            return true;
        }
        return false;

    }

    /**
     * 判断目标时间是否大于当前时间
     *
     * @param dateStr 目标时间
     * @return
     */
    public static boolean greaterThanCurDate(String dateStr) {
        long time = System.currentTimeMillis();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
        try {
            Date date = formater.parse(dateStr);
            if (time < date.getTime()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            L.e(TAG, e);
        }
        return false;
    }

    /**
     * 将手机号码中间4位改为*号
     *
     * @param mobile 手机号码
     * @return
     */
    public static String encryptMobile(String mobile) {
        if (!StringUtil.isNullorEmpty(mobile) && mobile.length() > 4) {
            int length = mobile.length();
            String encryMobileEnd = mobile.substring(length - 4, length);
            String encryMobileStart = "";
            if (length - 8 > 0) {
                encryMobileStart = mobile.substring(0, length - 8);
            }
            return encryMobileStart + "****" + encryMobileEnd;
        }
        return mobile;
    }

    /**
     * 复制文字到剪贴板
     *
     * @param context context
     * @param str     str
     */
    public static void copy(Context context, String str) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        cmb.setText(str.trim());
    }

    public static String getFormatNum(long fansCount) {
        String result;
        if (fansCount > 999999) {
            result = String.format("%.1f", fansCount / 10000.0f) + "万";
        } else {
            result = fansCount + "";
        }
        return result;
    }

    public static String getFormatNum(String fansCountstr) {
        String result;
        if (!isNullorEmpty(fansCountstr)) {
            try {
                long fanscount = Long.parseLong(fansCountstr);
                result = getFormatNum(fanscount);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                result = "0";
            }
        } else {
            result = "0";
        }

        return result;
    }


    /**
     * url拼接
     *
     * @param url   url
     * @param param 参数
     * @return 拼接后的url
     */
    public static String urlJoint(String url, Map<String, String> param) {
        String resultUlr = url;
        if (param != null) {
            String paramStr = "?";
            for (String key : param.keySet()) {
                if (!paramStr.equals("?")) {
                    paramStr += "&";
                }
                paramStr += key + "=" + param.get(key);
            }
            if (!paramStr.equals("?")) {
                resultUlr += paramStr;
            }
        }
        return resultUlr;
    }


    /**
     * 将十六进制 颜色代码 转换为 int
     *
     * @return
     */
    public static int HextoColor(String color) {
        int c = 0;
        try {
            c = Color.parseColor(color);
        } catch (Exception e) {
            return Color.parseColor("#ffee317b");
        }
        return c;
    }


    public static String getPlayUrl(List<LiveUrlModel> urls) {
        if (urls == null) {
            return "";
        }
        String flv = null;
        String hls = null;
        String rtmp = null;
        for (LiveUrlModel model : urls) {
            if ("rtmp".equals(model.getFormat())) {
                rtmp = model.getUrl();
            } else if ("flv".equals(model.getFormat())) {
                flv = model.getUrl();
            } else if ("hls".equals(model.getFormat())) {
                hls = model.getUrl();
            }
        }
        if (!StringUtil.isNullorEmpty(flv)) {
            return flv;
        }
        if (!StringUtil.isNullorEmpty(rtmp)) {
            return rtmp;
        }
        if (!StringUtil.isNullorEmpty(hls)) {
            return hls;
        }
        if (!urls.isEmpty()) {
            LiveUrlModel model = urls.get(0);
            if (model != null) {
                return model.getUrl();
            }
        }
        return "";
    }

    public static String getUrlHost(String url){
        if(url==null||url.trim().equals("")){
            return "";
        }
        String host = "";
        Pattern p =  Pattern.compile("(?<=//|)((\\w)+\\.)+\\w+");
        Matcher matcher = p.matcher(url);
        if(matcher.find()){
            host = matcher.group();
        }
        return host;
    }
}
