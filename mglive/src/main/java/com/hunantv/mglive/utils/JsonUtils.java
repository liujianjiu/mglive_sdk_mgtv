package com.hunantv.mglive.utils;

import org.json.JSONObject;

/**
 * Created by June Kwok on 2015-12-3.
 */
public class JsonUtils {


    /**
     * 获取数据的公用方法.如果没有此数据就范围空
     *
     * @param json
     * @param key
     * @return
     */
    public static final int getInt(JSONObject json, String key) {
        int str = -1;
        try {
            if (null != json && !isStringEmpty(key) && json.has(key)) {
                str = json.getInt(key);
            }
        } catch (Exception e) {
        }
        return str;
    }

    /**
     * 获取数据的公用方法.如果没有此数据就范围空
     *
     * @param json
     * @param key
     * @return
     */
    public static final String getString(JSONObject json, String key) {
        String str = null;
        try {
            if (null != json && !isStringEmpty(key) && json.has(key)) {
                str = json.get(key).toString();
            }
        } catch (Exception e) {
        }
        return str;
    }

    /**
     * 获取数据的公用方法.如果没有此数据就范围空
     *
     * @param json
     * @param key
     * @return
     */
    public static final long getLong(JSONObject json, String key) {
        long str = -1L;
        try {
            if (null != json && !isStringEmpty(key) && json.has(key)) {
                str = json.getLong(key);
            }
        } catch (Exception e) {
        }
        return str;
    }

    /**
     * 判断是否为null对象
     *
     * @param o
     * @return
     */
    public static final boolean isNull(Object o) {
        return null == o;
    }

    /**
     * 判断字符串是否为空
     *
     * @param str
     * @return
     */
    public static final boolean isStringEmpty(String str) {
        if (isNull(str)) {
            return true;
        } else {
            return str.trim().length() == 0;
        }
    }

}
