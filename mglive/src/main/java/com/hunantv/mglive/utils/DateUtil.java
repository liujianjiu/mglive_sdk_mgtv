package com.hunantv.mglive.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 2015/12/15.
 */
public class DateUtil {

    public static int MIN_YEAR = 1900;

    public static Calendar date2Calendar(String dateStr) throws ParseException
    {
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
        Date date =sdf.parse(dateStr);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static long date2long(String dateStr){
        try {
            SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date =sdf.parse(dateStr);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.getTimeInMillis();
        }catch (ParseException e){
            e.printStackTrace();
        }
        return 0;
    }

    public static String long2Date(long dateL){
        try {
            SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(dateL);
            Date date = calendar.getTime();

            return sdf.format(date);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "1970-01-01 00:00:00";
    }

    public static ArrayList<String> getYears()
    {
        ArrayList<String> years = new ArrayList<String>();
        for (int i=MIN_YEAR;i<=getNowYear();i++){
            years.add(i+"");
        }

        return years;
    }

    public static ArrayList<String> getMonths()
    {
        ArrayList<String> months = new ArrayList<String>();
        for (int i=1;i<=12;i++){
            months.add(i + "");
        }

        return months;
    }


    public static ArrayList<String> getDays()
    {
        return getDays(1980,1);
    }

    public static ArrayList<String> getDays(int year,int month)
    {
        ArrayList<String> days = new ArrayList<String>();
        int maxDay = getMaxDay(year,month);
        for (int i=1;i<=maxDay;i++){
            days.add(i + "");
        }

        return days;
    }

    public static int getMaxDay(int year,int month)
    {
        Calendar cl= Calendar.getInstance();
        cl.set(Calendar.YEAR,year);
        cl.set(Calendar.MONTH,month-1);
        return cl.getActualMaximum(Calendar.DATE);
    }

    public static int getNowYear()
    {
        Calendar cl= Calendar.getInstance();
        return cl.get(Calendar.YEAR);
    }

}
