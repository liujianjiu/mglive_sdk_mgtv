package com.hunantv.mglive.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.alibaba.fastjson.JSON;
import com.hunantv.mglive.data.login.UserInfoData;

/**
 * Created by Qiuda on 2016/2/25.
 */
public class SharePreferenceUtils {
    private SharedPreferences sharedPreferences;
    public static final String FILE_KEY_ZAN_DATA = "com.hunantv.mglive.FILE_KEY_ZAN_DATA";
    public static final String FILE_KEY_HINT_DATA = "com.hunantv.mglive.FILE_KEY_HINT_DATA";
    //动态赞
    private static final String DYNAMIC_ID_KEY = "DYNAMIC_ID_KEY";
    //多机位第1次显示标记
    private static final String CAMERA_DISPLAY_KEY = "CAMERA_DISPLAY_KEY";
    /**
     * 是否显示人气提醒弹窗KEY
     */
    public static final String REN_QI_SHOW_KEY = "REN_QI_SHOW_KEY";
    //更新token 的时间
    private static final String UPDATE_TOKEN_TIME = "UPDATE_TOKEN_TIME";
    //第一次启动
    private static final String IS_FIRST_START = "IS_FIRST_START";
    //第一次进入秀场直播
    private static final String IS_FIRST_START_STAR_LIVE = "IS_FIRST_START_STAR_LIVE";
    //登录成功的账号
    private static final String LOGIN_ACCOUNT = "LOGIN_ACCOUNT";


    public SharePreferenceUtils(Context context, String key) {
        sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
    }

    public boolean isZaned(String dynamicId) {
        if (StringUtil.isNullorEmpty(dynamicId)) {
            return false;
        }
        String dynamicIds = getZanList();
        return dynamicIds.contains(dynamicId);
    }

    public void setBoolean(String key, boolean value) {
        if (StringUtil.isNullorEmpty(key)) {
            return;
        }
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        if (StringUtil.isNullorEmpty(key)) {
            return defaultValue;
        }
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public synchronized String getZanList() {
        return sharedPreferences.getString(DYNAMIC_ID_KEY, "");
    }


    public synchronized void addZanDynamic(String dynamicId) {
        if (StringUtil.isNullorEmpty(dynamicId)) {
            return;
        }
        synchronized (this) {
            String dynamicIds = getZanList();
            if (!dynamicIds.contains(dynamicId)) {
                dynamicIds = dynamicIds + "," + dynamicId;
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(DYNAMIC_ID_KEY, dynamicIds);
                editor.apply();
            }
        }
    }

    public synchronized void removeZanDynamic(String dynamicId) {
        synchronized (this) {
            String dynamicIds = getZanList();
            dynamicIds = dynamicIds.replace(dynamicId, "");
            dynamicIds = dynamicIds.replace(",,", ",");
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(DYNAMIC_ID_KEY, dynamicIds);
            editor.apply();
        }
    }

    public void clearZanDynamic() {
        synchronized (this) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(DYNAMIC_ID_KEY);
            editor.apply();
        }
    }

    public boolean isCamreHintDisplay() {
        return sharedPreferences.getBoolean(CAMERA_DISPLAY_KEY, true);
    }

    public synchronized void setCamreHintDisplay(boolean display) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(CAMERA_DISPLAY_KEY, display);
        editor.apply();
    }

    public void updateSaveTokenTime() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(UPDATE_TOKEN_TIME, System.currentTimeMillis());
        editor.apply();
    }

    public long getUpdateTokenTime() {
        return sharedPreferences.getLong(UPDATE_TOKEN_TIME, 0);
    }

    public boolean isFirstStart() {
        boolean isFirstStart = sharedPreferences.getBoolean(IS_FIRST_START, true);
        if (isFirstStart) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(IS_FIRST_START, false);
            editor.apply();
        }
        return isFirstStart;
    }

    public void updateLoginAccount(String account) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LOGIN_ACCOUNT, account);
        editor.apply();
    }

    public String getLoginAccount() {
        return sharedPreferences.getString(LOGIN_ACCOUNT, "");
    }

    public boolean isStarLiveFirstInit() {
        boolean isFirstStart = sharedPreferences.getBoolean(IS_FIRST_START_STAR_LIVE, true);
        if (isFirstStart) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(IS_FIRST_START_STAR_LIVE, false);
            editor.apply();
        }
        return isFirstStart;
    }
}
