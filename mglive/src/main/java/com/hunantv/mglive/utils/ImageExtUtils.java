package com.hunantv.mglive.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import com.hunantv.mglive.common.BaseActivity;

import java.io.File;

public class ImageExtUtils {

    private static final int WIFI_IMAGE_MAX_SIZE = 1024;
    private static final int WIFI_IMAGE_QUALITY = 100;

    private static final int MOBILE_NETWORK_IMAGE_MAX_SIZE = 800;
    private static final int MOBILE_IMAGE_QUALITY = 60;

    private static final int MOBILE_G2_NETWORK_IMAGE_MAX_SIZE = 600;
    private static final int MOBILE_G2_IMAGE_QUALITY = 40;


    public static class CompressConfig {
        public int mMaxSize;
        public int mQuality;


        public CompressConfig() {
        }

        public CompressConfig(int maxSize, int quality) {
            update(maxSize, quality);
        }

        public void update(int maxSize, int quality) {
            mMaxSize = maxSize;
            mQuality = quality;
        }
    }


    public static interface OnChooseImageListener {
        void onChosen(Uri imageUri);

        void onCanceled();
    }


    public static interface OnCaptureImageByCameraListener {
        void onSuccess(String filePath);

        void onCanceled();
    }


    public static void chooseImage(
            final BaseActivity activity,
            final int chooseRequestCode,
            final OnChooseImageListener listener) {

        if (listener != null) {
            activity.addActivityResultListener(new BaseActivity.OnSingleActivityResultListener() {
                @Override
                protected void handleActivityResult(
                        BaseActivity activity, int requestCode, int resultCode, Intent data) {

                    if (chooseRequestCode == requestCode) {
                        if (resultCode == Activity.RESULT_OK) {
                            listener.onChosen(data.getData());
                        } else {
                            listener.onCanceled();
                        }
                    }
                }
            });
        }

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(intent, chooseRequestCode);
    }

    public static boolean saveBitmapToPathWithCompress(Bitmap bitmap, String filePath) {
        return saveBitmapToPathWithCompress(bitmap, filePath, null);
    }

    public static CompressConfig getCompressConfig() {
        CompressConfig config = new CompressConfig();
        NetworkUtils.NetType type = NetworkUtils.getNetworkType();

        if (type == NetworkUtils.NetType.Wifi) {
            config.update(WIFI_IMAGE_MAX_SIZE, WIFI_IMAGE_QUALITY);
        } else if (type == NetworkUtils.NetType.G2) {
            config.update(MOBILE_G2_NETWORK_IMAGE_MAX_SIZE, MOBILE_G2_IMAGE_QUALITY);
        } else {
            config.update(MOBILE_NETWORK_IMAGE_MAX_SIZE, MOBILE_IMAGE_QUALITY);
        }

        return config;
    }

    public static boolean saveBitmapToPathWithCompress(
            Bitmap bitmap, String filePath, ImageUtils.ImageSize outBitmapSize) {

        CompressConfig config = getCompressConfig();
        return saveBitmapToPathWithCompress(
                bitmap, filePath, config.mMaxSize, config.mQuality, outBitmapSize);
    }

    public static boolean saveBitmapToPathWithCompress(
            Bitmap bitmap, String filePath, int maxSize, int quality,
            ImageUtils.ImageSize outBitmapSize) {

        final int bmpWidth = bitmap.getWidth();
        final int bmpHeight = bitmap.getHeight();
        ImageUtils.ImageSize size = ImageUtils.getMaxScaleSize(
                bmpWidth, bmpHeight, maxSize);

        if (outBitmapSize != null) {
            outBitmapSize.setSize(size);
        }

        if (bmpWidth != size.getWidth() && bmpHeight != size.getHeight()) {
            bitmap = ImageUtils.resizeBitmap(bitmap, size.getWidth(), size.getHeight());
        }

        return ImageUtils.saveBitmapToFile(
                bitmap, filePath, Bitmap.CompressFormat.PNG, quality);
    }

    public static void captureImageByCamera(
            Activity activity, final String filePath, final int captureRequestCode,
            final OnCaptureImageByCameraListener listener) {

        File file = new File(filePath);
        Uri uri = Uri.fromFile(file);

        if (activity instanceof BaseActivity && listener != null) {
            BaseActivity baseActivity = (BaseActivity) activity;
            baseActivity.addActivityResultListener(
                    new BaseActivity.OnSingleActivityResultListener() {
                        @Override
                        protected void handleActivityResult(
                                BaseActivity activity, int requestCode,
                                int resultCode, Intent data) {

                            if (requestCode == captureRequestCode) {
                                if (resultCode == Activity.RESULT_OK) {
                                    listener.onSuccess(filePath);
                                } else {
                                    listener.onCanceled();
                                }
                            }
                        }
                    });
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        activity.startActivityForResult(intent, captureRequestCode);
    }
}
