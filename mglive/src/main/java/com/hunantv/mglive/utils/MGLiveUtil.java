package com.hunantv.mglive.utils;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.hunantv.mglive.common.Constant;
import com.hunantv.mglive.common.MaxApplication;
import com.hunantv.mglive.data.login.UserInfoData;
import com.hunantv.mglive.ui.discovery.DetailsActivity;
import com.hunantv.mglive.ui.live.LiveDetailActivity;
import com.hunantv.mglive.ui.live.StarDetailActivity;

import java.util.ArrayList;

/**
 * Created by liujianjiu on 16-7-22.
 *
 * 芒果直播对外提供服务工具类
 */
public class MGLiveUtil {

    private static MGLiveUtil mMGLiveUtil;
    private MGLiveUtil(){};

    public synchronized static MGLiveUtil getInstance(){
        if(mMGLiveUtil == null){
            mMGLiveUtil = new MGLiveUtil();
        }

        return mMGLiveUtil;
    }

    /**
     * 初始化芒果直播功能，程序启动的时候调用该方法。
     * @param mApp 外部Application
     * @return true：初始化成功；false:初始化失败
     */
    public boolean init(Application mApp){
        return MaxApplication.getInstance().initMGLive(mApp);
    }

    /**
     * 设置当前登录用户的用户信息
     * @param uid uid:当前用户id
     * @param token 访问令牌
     * @param nickName 昵称
     */
    public void setUserInfo(String uid,String token,String nickName){
        UserInfoData userInfoData = new UserInfoData();
        userInfoData.setUid(uid);
        userInfoData.setToken(token);
        userInfoData.setNickName(nickName);
        MaxApplication.getInstance().setUserInfo(userInfoData);
    }

    /**
     * 用户登出，清空用户信息
     */
    public void clearUserInfo(){

        MaxApplication.getInstance().logout();
    }

    /**
     * 进入个人直播页面
     * @param mContext 外部context（必须）
     * @param aid 艺人Id（必须）
     */
    public void startLivePlayActivity(Context mContext,String aid){
        if(MaxApplication.getAppContext() == null){
            MaxApplication.getInstance().initMGLive((Application)mContext.getApplicationContext());
        }

        StartActivityDelayUtil.instance(mContext).startAcitiy(aid);
    }

    /**
     * 进入场景直播页面
     * @param mContext 外部context（必须）
     * @param lid lid:直播节目Id（必须）
     */
    public void startSceneLivePlayActivity(Context mContext,String lid){
        if(MaxApplication.getAppContext() == null){
            MaxApplication.getInstance().initMGLive((Application)mContext.getApplicationContext());
        }


        Intent i = new Intent(mContext, LiveDetailActivity.class);
        i.putExtra(Constant.JUMP_INTENT_LID, lid);
        i.putExtra(Constant.JUMP_INTENT_TYPE, "0");//直播
        mContext.startActivity(i);
    }

    /**
     * 进入艺人空间页面
     * @param mContext 外部context（必须）
     * @param aid 艺人Id（必须）
     * @param aid vedioIds（可选），如果外部传入该视频ID列表那么芒果直播会优先播放该视频Id列表中的视频内容
     */
    public void startActorRoomActivity(Context mContext,String aid,ArrayList<String>vedioIds){
        if(MaxApplication.getAppContext() == null){
            MaxApplication.getInstance().initMGLive((Application)mContext.getApplicationContext());
        }

        Intent intent = new Intent(mContext, StarDetailActivity.class);
        intent.putExtra(StarDetailActivity.KEY_STAR_ID, aid);
        mContext.startActivity(intent);
    }

    /**
     * 进入点播页面
     * @param mContext 外部context（必须）
     * @param lid 点播节目Id（必须）
     */
    public void startVedioPlayActivity(Context mContext,String lid){
        if(MaxApplication.getAppContext() == null){
            MaxApplication.getInstance().initMGLive((Application)mContext.getApplicationContext());
        }


        Intent i = new Intent(mContext, LiveDetailActivity.class);
        i.putExtra(Constant.JUMP_INTENT_LID, lid);
        i.putExtra(Constant.JUMP_INTENT_TYPE, "1");//点播
        mContext.startActivity(i);
    }

    /**
     * 进入动态详情页面
     * @param mContext 外部context（必须）
     * @param did 动态Id（必须）
     */
    public void startDyDetailActivity(Context mContext,String did){
        if(MaxApplication.getAppContext() == null){
            MaxApplication.getInstance().initMGLive((Application)mContext.getApplicationContext());
        }

        Intent intent = new Intent(mContext, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_DYNAMIC_ID, did);
        mContext.startActivity(intent);
    }



}