// IGiftServiceAIDL.aidl
package com.hunantv.mglive.aidl;

import com.hunantv.mglive.aidl.FreeGiftCallBack;
// Declare any non-default types here with import statements

interface IGiftServiceAIDL {

    void startAction(int type);

    void addCallback(FreeGiftCallBack callback);

    void setCallback(FreeGiftCallBack callback);

    void removeAllCallBack();
}
