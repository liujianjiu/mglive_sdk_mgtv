// FreeGiftCallBack.aidl
package com.hunantv.mglive.aidl;

// Declare any non-default types here with import statements

interface FreeGiftCallBack {


  /**
     * 剩余时间回调
     *
     * @param left
     */
    void onTimeLeft(int left);

    /**
     * 倒数完毕添加一次免费机会回调
     *
     * @param num
     */
    void onNumAdd(int num);
}
