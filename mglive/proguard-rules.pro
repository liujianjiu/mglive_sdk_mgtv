# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\work\android-sdk-windows/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-optimizationpasses 5          # 指定代码的压缩级别
-dontusemixedcaseclassnames   # 是否使用大小写混合
-dontpreverify           # 混淆时是否做预校验
-verbose                # 混淆时是否记录日志
#-dontwarn
-ignorewarnings

#-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*  # 混淆时所采用的算法

-keep public class * extends android.app.Fragment
-keep public class * extends android.app.Activity      # 保持哪些类不被混淆
-keep public class * extends android.app.Application   # 保持哪些类不被混淆
-keep public class * extends android.app.Service       # 保持哪些类不被混淆
-keep public class * extends android.content.BroadcastReceiver  # 保持哪些类不被混淆
-keep public class * extends android.content.ContentProvider    # 保持哪些类不被混淆
-keep public class * extends android.app.backup.BackupAgentHelper # 保持哪些类不被混淆
-keep public class * extends android.preference.Preference        # 保持哪些类不被混淆
-keep public class com.android.vending.licensing.ILicensingService    # 保持哪些类不被混淆

-keepclasseswithmembernames class * {  # 保持 native 方法不被混淆
    native <methods>;
}
-keepclasseswithmembers class * {   # 保持自定义控件类不被混淆
    public <init>(android.content.Context, android.util.AttributeSet);
}
-keepclasseswithmembers class * {# 保持自定义控件类不被混淆
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keepclassmembers class * extends android.app.Activity { # 保持自定义控件类不被混淆
    public void *(android.view.View);
}
-keepclassmembers enum * {     # 保持枚举 enum 类不被混淆
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep class * implements android.os.Parcelable { # 保持 Parcelable 不被混淆
    public static final android.os.Parcelable$Creator *;
}

-keep class * implements java.io.Serializable { # 保持 Serializable 不被混淆
}

-keepclassmembers class * {
   public <init>(org.json.JSONObject);
}

#-keep public class com.hunantv.mglive.R$*{
#    public static final int *;
#}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep public class * extends com.sina.weibo.sdk.utils.MD5
-keep public class * extends com.google.thirdparty.publicsuffix.TrieParser

-dontwarn com.alibaba.**
-keep class com.alibaba.**
-keepclassmembers class com.alibaba.** {
    *;
}
-keep class com.taobao.**
-keepclassmembers class com.taobao.** {
    *;
}

-dontwarn com.google.common.**
-dontwarn com.fasterxml.jackson.**
-dontwarn com.amap.api.**
-dontwarn net.jcip.annotations.**

-keepattributes nnotation,EnclosingMethod,Signature,InnerClasses,Exception

-keep class com.fasterxml.jackson.**
-keepclassmembers class com.fasterxml.jackson.** {
    *;
}

-keep class com.duanqu.**
-keepclassmembers class com.duanqu.** {
    *;
}

-dontwarn javax.annotation.**
-keep class javax.annotation.** { *;}
-dontwarn org.w3c.**
-keep class org.w3c.** { *;}
-dontwarn sun.misc.**
-keep class sun.misc.** { *;}
-dontwarn javax.tools.**
-keep class javax.tools.** { *;}
-dontwarn com.nostra13.**
-keep class com.nostra13.** { *;}
-dontwarn java.nio.**
-keep class java.nio.** { *;}
-dontwarn com.google.**
-keep class com.google.** { *;}
-dontwarn android.util.**
-keep class android.util.** { *;}
-dontwarn java.lang.**
-keep class java.lang.** { *;}
-dontwarn dagger.shaded.**
-keep class dagger.shaded.** { *;}
-dontwarn org.codehaus.**
-keep class org.codehaus.** { *;}
-dontwarn com.duanqu.**
-keep class com.duanqu.** { *;}
-dontwarn android.app.**
-keep class android.app.** { *;}
-dontwarn dagger.internal.**
-keep class dagger.internal.** { *;}


-dontwarn com.taobao.**
-keep class com.taobao.** { *;}
-dontwarn com.alibaba.**
-keep class com.alibaba.** { *;}
-dontwarn javax.lang.**
-keep class javax.lang.** { *;}
-dontwarn com.baidu.**
-keep class com.baidu.** { *;}
-dontwarn com.bumptech.**
-keep class com.bumptech.** { *;}
-dontwarn com.imgo.max.**
-keep class com.imgo.max.** { *;}
-dontwarn com.hunantv.**
-keep class com.hunantv.** { *;}
-dontwarn com.tencent.**
-keep class com.tencent.** { *;}
-dontwarn org.eclipse.**
-keep class org.eclipse.** { *;}
-dontwarn com.squareup.**
-keep class com.squareup.** { *;}
-dontwarn com.umeng.**
-keep class com.umeng.** { *;}
-dontwarn com.sina.**
-keep class com.sina.** { *;}
-dontwarn org.json.**
-keep class org.json.** { *;}
-dontwarn com.alipay.**
-keep class com.alipay.** { *;}
-dontwarn mango.live.**
-keep class mango.live.** { *;}
-dontwarn tv.danmaku.**
-keep class tv.danmaku.** { *;}
-dontwarn dagger.producers.**
-keep class dagger.producers.** { *;}
-dontwarn com.fasterxml.**
-keep class com.fasterxml.** { *;}
-dontwarn javax.inject.**
-keep class javax.inject.** { *;}
-dontwarn net.jcip.**
-keep class net.jcip.** { *;}


-dontwarn ut.mini.**
-keep class ut.mini.** { *;}
# 保持自定义的View 不被混淆
#-keep class com.hunantv.mglive.ui.discovery.DetailsView{*;}
#-keep class com.hunantv.mglive.ui.discovery.Details{*;}
-keep class com.hunantv.mglive.ui.discovery.NormalView{*;}
-keep class com.hunantv.mglive.ui.discovery.DynamicLayout{*;}
-keep class com.hunantv.mglive.ui.discovery.EditBtnView{*;}
-keep class com.hunantv.mglive.ui.discovery.FreeGiftAnimation{*;}



-dontobfuscate
-keep class javax.annotation.** { *; }

-keep class * extends com.duanqu.qupai.jni.ANativeObject
-keep @com.duanqu.qupai.jni.AccessedByNative class *
-keep class com.duanqu.qupai.bean.DIYOverlaySubmit
-keepclassmembers @com.duanqu.qupai.jni.AccessedByNative class * {
    *;
}
-keepclassmembers class * {
    @com.duanqu.qupai.jni.AccessedByNative *;
}
-keepclassmembers class * {
    @com.duanqu.qupai.jni.CalledByNative *;
}

-keepclasseswithmembers class * {
    native <methods>;
}

-keepclassmembers class * {
    native <methods>;
}
-keepclassmembers class com.duanqu.qupai.** {
    *;
}

#如果接入nfc需要增加nfc的混淆配置 nfc start
-keep class com.baidu.nfc.** { *; }
#nfc end
# passsdk start
-keep class com.baidu.sapi2.** {*;}
-keepattributes JavascriptInterface
-keepattributes *Annotation*
#passsdk end
