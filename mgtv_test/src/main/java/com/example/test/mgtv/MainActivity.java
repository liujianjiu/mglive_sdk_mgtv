package com.example.test.mgtv;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.hunantv.mglive.utils.MGLiveUtil;

public class MainActivity extends AppCompatActivity {

    private Button buLive;
    private Button buLiveRoom;
    private Button buStarRoom;
    private Button buVedio;
    private Button buDy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        assignViews();
    }


    private void assignViews() {
        buLive = (Button) findViewById(R.id.bu_live);
        buLiveRoom = (Button) findViewById(R.id.bu_live_room);
        buStarRoom = (Button) findViewById(R.id.bu_star_room);
        buVedio = (Button) findViewById(R.id.bu_vedio);
        buDy = (Button) findViewById(R.id.bu_dy);

        String uid = "4d4ea591f734497e3f0d9a2f059dd5aa";//小黑
        String token = "1882KTHVDOCQTCMP0264";//小黑
        String userName = "小黑";//小黑

        //设置当前登录用户信息，芒果TV中登录成功后要设置信息进来，如果没有那芒果直播认为当前没有登录用户
        MGLiveUtil.getInstance().setUserInfo(uid,token,userName);

        buLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String aid = "6f8a46e650564c7d17dfd0e26ba218ae";//田小宝

                MGLiveUtil.getInstance().startLivePlayActivity(MainActivity.this, aid);
            }
        });
        buLiveRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lid = "590";//完美假期2
                MGLiveUtil.getInstance().startSceneLivePlayActivity(MainActivity.this, lid);
            }
        });
        buStarRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String aid = "5efa35d7ff53118c512201b72b051fde";//黄汐源
                MGLiveUtil.getInstance().startActorRoomActivity(MainActivity.this, aid,null);
            }
        });
        buVedio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lid = "3479616";//方圆视频
                MGLiveUtil.getInstance().startVedioPlayActivity(MainActivity.this, lid);
            }
        });
        buDy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String did = "241f40bd49cb4ec48a25a76379d426c7";//广告
                MGLiveUtil.getInstance().startDyDetailActivity(MainActivity.this, did);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
