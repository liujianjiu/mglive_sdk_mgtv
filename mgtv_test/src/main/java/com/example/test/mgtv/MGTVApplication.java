package com.example.test.mgtv;

import com.hunantv.mglive.utils.MGLiveUtil;

/**
 * Created by apple on 16-7-26.
 */
public class MGTVApplication extends android.support.multidex.MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        MGLiveUtil.getInstance().init(this);
    }
}
