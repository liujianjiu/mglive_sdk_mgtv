package com.example.test.mgtv.wxapi;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.hunantv.mglive.common.FormEncodingBuilderEx;
import com.hunantv.mglive.data.ResultModel;
import com.hunantv.mglive.ui.handle.WXHandle;
import com.hunantv.mglive.utils.HttpUtils;
import com.hunantv.mglive.utils.Toast;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;

import org.json.JSONException;

import java.util.Map;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler, HttpUtils.callBack {
    public static final String PLATFORM_TYPE_WX = "3";
    private WXHandle wx;
    private HttpUtils http;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new View(this));
        http = new HttpUtils(this, this);
        WXHandle.getInstance(this).handleIntent(getIntent(), this);
    }

    // 微信发送请求到第三方应用时，会回调到该方法
    @Override
    public void onReq(BaseReq req) {
        Log.e("微信登录取消", "");
        finish();
    }

    // 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法
    @Override
    public void onResp(BaseResp resp) {
        if (resp != null && resp.transaction != null && resp.transaction.contains("webpage")) {
            finish();
            return;
        }
        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                String openId = ((SendAuth.Resp) resp).code;
                Map<String, String> body = new FormEncodingBuilderEx()
                        .add("code", openId)
                        .add("platform", PLATFORM_TYPE_WX)
                        .build();

//                http.post(BuildConfig.URL_THIRD_LOGIN, body);
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onSucceed(String url, ResultModel resultModel) throws JSONException {
//        if (BuildConfig.URL_THIRD_LOGIN.equals(url)) {
//            UserInfoData userInfoData = JSON.parseObject(resultModel.getData(), UserInfoData.class);
//            MaxApplication.getInstance().setUserInfo(userInfoData);
//            finish();
//            WXHandle.getInstance().closeUi(true);
//        }
    }

    @Override
    public void onFailure(String url, ResultModel resultModel) {
        Toast.makeText(this, resultModel.getMsg(), Toast.LENGTH_SHORT).show();
        finish();
        WXHandle.getInstance(this).closeUi(false);
    }


    @Override
    public boolean get(String url, Map<String, String> param) {
        return http.get(url, param);
    }

    @Override
    public boolean post(String url, Map<String, String> param) {
        return http.post(url, param);
    }

    @Override
    public void onError(String url, Exception e) {
        finish();
        WXHandle.getInstance(this).closeUi(false);
    }

    @Override
    public Object asyncExecute(String url, ResultModel resultModel) {
        return null;
    }


}